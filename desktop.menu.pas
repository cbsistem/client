unit desktop.menu;

interface

{$I 'Smart.inc'}

uses 
  W3C.DOM,
  System.Types,
  System.Colors,
  System.Types.Graphics,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Device.Storage,

  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,
  SmartCL.Theme,

  desktop.types,

  System.Events,
  SmartCL.Events,

  SmartCL.EventManager,
  SmartCL.Borders,
  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System,
  SmartCL.Graphics,
  SmartCL.FileUtils,
  SmartCL.Forms,
  SmartCL.Fonts,
  SmartCL.Controls.Image,
  SmartCL.Controls.Label,
  SmartCL.Controls.ToolButton;

const
  CNT_MENU_MENUITEM_HEIGHT = 24;

type

  // Forward declarations
  TWbMenuStructure  = class;
  TWbMenuStructureItem = class;
  TWbMenuContainer  = class;
  TWbMenuItem = class;
  TWbMainMenu = class;

  // Where TWbMenuItem is the visual control, this class represents
  // an item in the menu blueprint. We naturally define our menus
  // first before they are turned into visual controls, and this class
  // represents a single node in that node-tree-structure.
  // It will also manage and release the visual control if one is created
  // for the item. The Enabled / Caption properties maps to the visual
  TWbMenuStructureItem = class(TW3OwnedObject)
  private
    FObjects:   array of TWbMenuStructureItem;
    FRoot:      TWbMenuStructureItem;
    FCaption:   string = 'Menuitem';
    FEnabled:   boolean = true;
    FSeperator: boolean = false;
    FControl:   TWbMenuItem = nil;
    FContainer: TWbMenuContainer;
    FMainMenu:  TWbMainMenu;
  protected
    procedure   RegisterChild(const NewChild: TWbMenuStructureItem);
    procedure   UnRegisterChild(const OldChild: TWbMenuStructureItem);
    function    GetOwner: TWbMenuStructureItem; reintroduce; virtual;

    function    GetCaption: string;
    procedure   SetCaption(NewCaption: string);

    function    GetEnabled: boolean;
    procedure   SetEnabled(NewEnabled: boolean);

    procedure   SetRoot(const MenuRoot: TWbMenuStructureItem);
    procedure   SetMainMenu(const ThisMenu: TWbMainMenu);
    procedure   SetControl(const NewControl: TWbMenuItem);

    function    AcceptOwner(const CandidateObject: TObject): boolean; override;
  public
    property    MainMenu: TWbMainMenu read FMainMenu;
    property    Root: TWbMenuStructureItem read FRoot;
    property    Owner: TWbMenuStructureItem read GetOwner;
    property    Separator: boolean read FSeperator write FSeperator;
    property    Container: TWbMenuContainer read FContainer write FContainer;
    property    Count: integer read (FObjects.Count);
    property    Items[index: integer]: TWbMenuStructureItem read (FObjects[index]);
    property    Caption: string read GetCaption write SetCaption;
    property    Enabled: boolean read GetEnabled write SetEnabled;
    property    Control: TWbMenuItem read FControl write SetControl;
    property    MenuOpen: boolean read (assigned(FContainer));

    property    TagData: variant;

    procedure   OpenMenu;
    procedure   CloseMenu;
    procedure   CloseChildMenus;

    procedure   ReleaseControl;
    procedure   ReleaseChildControls;

    function    Add(Caption: string): TWbMenuStructureItem; overload;
    function    Add(Caption: string; const TagValue: variant): TWbMenuStructureItem; overload;
    procedure   Clear;
    constructor Create(AOwner: TWbMenuStructureItem); reintroduce; virtual;
    destructor  Destroy; override;
  end;

  // This class is the root item in a menu structure.
  // Again this is not a visual control, but rather represents the
  // menu blueprint (see notes for TWbMenuStructureItem above). Since all structures
  // must originate somewhere, this class represents the "root" if you will.
  TWbMenuStructure = class(TWbMenuStructureItem)
  public
    constructor Create(MainMenu: TWbMainMenu); reintroduce; virtual;
    destructor Destroy; override;
  end;

  // This is used for the "Workbench" title on the main-menu
  // it is *NOT* used for menu-items inside a menu-container (!)
  TWbMainMenuTitle = class(TW3Label)
  protected
    procedure SetWidth(const NewWidth: integer); override;
    procedure SetSize(const NewWidth, NewHeight: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); override;
    procedure InitializeObject; override;
    procedure StyleTagObject; override;
  public
    function  CreationFlags: TW3CreationFlags; override;
  end;

  // Menu apps inherit from this
  TWbMainMenuAppDisplay = class(TW3CustomControl)
    procedure SetWidth(const NewWidth: integer); override;
    procedure SetSize(const NewWidth, NewHeight: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); override;
    procedure InitializeObject; override;
    procedure StyleTagObject; override;
  public
    function  CreationFlags: TW3CreationFlags; override;
  end;

  // This is the container of menu apps
  TWbMainMenuAppRegion = class(TW3CustomControl)
  protected
    procedure SetWidth(const NewWidth: integer); override;
    procedure SetSize(const NewWidth, NewHeight: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); override;
    procedure InitializeObject; override;
    procedure StyleTagObject; override;
  public
    function  CreationFlags: TW3CreationFlags; override;
  end;

  // This is used for the C= logo on the main menu
  // Note: This has *NOTHING* to do with glyphs on menu items (see below)
  TWbMenuGlyph = class(TW3Image)
  protected
    procedure StyleTagObject; override;
  end;

  // If a menu-item has child elements, an arrow glyph will be shown.
  // This glyph should be simple and never be larger than 16 pixels in width
  // and height. The image is set to scale, so probably use 14x14 in size or
  // something like that. The arrow is centered in the 16x16 rightmost space
  // allocated for it
  TWbMenuArrow = class(TW3Image)
  protected
    procedure StyleTagObject; override;
  end;

  TWbMenuShortcut = class(TW3Image)
  end;

  TWBMenuItemTitle = class(TW3Label)
  end;

  IWbMenuItem = interface
    ['{F4EAFB9E-2C89-4570-9712-3D533B37C9FF}']
    procedure SetHeight(const NewHeight: integer);
    procedure SetSize(const NewWidth, NewHeight: integer);
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
  end;

  IWbMainMenu = interface
    ['{CBDE8013-FF28-410D-82FB-5F4E7C8552DF}']
    function GetMenubarTitle: string;
    procedure SetMenuBarTitle(NewTitle: string);
  end;

  // This visual control represents a single menu row
  // Remember that this is for a 2desktop" and as such it
  // can have a glyph, text and decoration that denote child elements.
  // If it has child elements and is not expanded, an arrow > is typically
  // visible right-most in the line
  TWbMenuItem = class(TW3CustomControl)
  private
    FLabel:   TWBMenuItemTitle;
    FArrow:   TWbMenuArrow;
    FData:    TWbMenuStructureItem;
    procedure SetData(const NewMenuData: TWbMenuStructureItem);
    procedure HandleItemClicked(Sender: TObject);
  protected
    procedure SetHeight(const NewHeight: integer); override;
    procedure SetSize(const NewWidth, NewHeight: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); override;

    procedure InitializeObject; override;
    procedure StyleTagObject; override;
    procedure Resize; override;
  public
    property  Label: TW3Label read FLabel;
    property  Caption: string read (FLabel.Caption) write (FLabel.Caption := Value);
    property  MenuDataItem: TWbMenuStructureItem read FData write SetData;
    function  CreationFlags: TW3CreationFlags; override;
  end;

  TWbMenuContainer = class(TW3CustomControl)
  private
    FGeneralMouseEvent:  TW3MouseDownEvent;
    FItemHeight: integer;
    procedure SetItemHeight(NewHeight: integer);
    function  FindOwnerOfHandle(const Handle: TControlHandle): TW3TagObj;
    procedure GeneralMouseHandler(sender: TObject; EventObj: JEvent);
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure StyleTagObject; override;
  public
    property  MenuRoot: TWbMenuStructureItem;
    function  CreationFlags: TW3CreationFlags; override;
    procedure Populate(const Structure: TWbMenuStructureItem);
    class function Build(Parent: TW3TagContainer; Structure: TWbMenuStructureItem): TWbMenuContainer;
  published
    property  ItemHeight: integer read FItemHeight write SetItemHeight;
  end;

  TWBMainMenuItemClickEvent = procedure (Sender: TObject; MenuItem: TWbMenuStructureItem);

  TWbMainMenu = class(TW3CustomControl, IWbMainMenu)
  private
    FTitle:   TWbMainMenuTitle;
    FApps:    TWbMainMenuAppRegion;
    FLogo:    TWbMenuGlyph;

    FDesign:    TWbMenuStructure;
    FContainers:  array of TWbMenuContainer;

    procedure ImageIsSizedAndReady;
    procedure RegisterContainer(const Container: TWbMenuContainer);
    procedure UnRegisterContainer(const Container: TWbMenuContainer);

  protected
    { IMNPLEMENTS :: IWbMainMenu }
    function GetMenubarTitle: string;
    procedure SetMenuBarTitle(NewTitle: string);

  protected

    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure StyleTagObject; override;
    procedure Resize; override;
  public
    function  CreationFlags: TW3CreationFlags; override;

    function  MenuIsOpen: boolean;

  published
    property  Title: TWbMainMenuTitle read FTitle;
    property  Graphic: TWbMenuGlyph read FLogo;
    property  AppRegion: TWbMainMenuAppRegion read FApps;
    property  MenuData: TWbMenuStructure read FDesign;
    property  OnMenuItemClick: TWBMainMenuItemClickEvent;
  end;

implementation

resourcestring

CNT_ARROW_RIGHT ='data:image/png;base64,'+
'iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTnU1rJkAAAAs0lEQVQoU43Qrw5FcBTA8d8meioZlU0TjU37URRBVQiyJ/ACXkGSNEmT5C/Xnd/uhu2e7aTzOTt/RFEU6LqObduUZckR4imFpmlYlkVd13ieh+u6VFV11B7gMAyM48g0TbRtSxAE+L5P0zSq4QbneWZZFrquI0kSwjA88Stc15Vt2+j7njiO+QtGUfQOr9GfXR9HX8c4jkOe5ydS8Pc9pmmSZZkCCkopz4cbhkGapjfwTcQOSqIWBfQ3SjgAAAAASUVORK5CYII='
;

//CNT_MENULOGO ='data:image/png;base64,'+
//'iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAABIAAAASABGyWs+AAAPvUlEQVR42u2de3Bc1X3HP7979yHJL2wLY+/K2BmQHHASI8mm4EAIaZ0m1A1taZuSpk2bDE2Y0IAtmRIC1AOBkCBhHGjIlA7tpB1c2k6TNC5pIW1xaI3Blo1JILZFiEHWymD5JVmPfdzz6x9HUEyttR5779Wu72dmxw9pz95zznfP8/eAiIiIiIiIiIiIiIiIiIiIiIgzAgn7ASbLkuVfZsArYERtZVRttcZZs5N+Xey/nLzD6y9+Pewq+krZCmDR8nV4RlEg7xRwTSzhQFJUqxWpRagFaoGzgJlANZAA+xZgGDgOHAN6RehF6QWGxHGyuXwuF3NjCIIRQ6ajPewq+0LZCCC1bB3iKjLy7XYgaVQXKTQAS0Ze5wMpYDqQHHklgDjgvKtIA+SwYsiOvE4APcArwB5gryCdRsx+RzWrOKgImY77wm6OkhEL+wGKkW5cC2L7LTatQGHAnadiGlBZ6cFl2E5PA9MmULwDVI28Zrzj/5cAHx75+4Ci3aKyV5FnQLYAL2CFUxFMOQGce/mNmMEYRgV14jgmP0ehsTDgrgJWovJ+7LAeBNOwI0wDsBr0RZRrgJ+H3U6lYkoJYPHSteRPuDgxz8GT88TkP67wCaCZ4Dp9NASYjpIIu51KyZQQQLpxDSIuVb0FKdQ6FxrPuVbgGuycPiWe8R1o2A9QSkJt3HOb1mBwkdxRMYnZDf21sU8DnwTqw26YM4VQBFDXtAZFGaKaJPmUScy+Fvhj4ELKaGdSCQQugPlNrRgE0XxVUrKrFbkBuBQqa24tFwIVQLqpFQEUbUBiaxR+j/AXd2c0gQhg1qrPMePIbFDiClcBtwHLw658REACmPnmXDRmZip8AVgLnBN2xSMsvgpg4fJWjAGDSaPcDnwGe/IWMUXwTQB1TS0M55RknGWq8jXgV/n/5/ERIeOLAM5tWoPgSSIW+5Aq7diTvIgpSMm/kQuXt/Jv+zZQILYKeJio86c0JR0B6ppbcV1kVUPrKuBB7CVKxBSmZCPA2StuoiYu5PN6BbCRqPPLgpIJoCaXZCCvHwBpB94bdsUixkZJpoB0Ywt58dKi3As0hV2piLEz6RFgQXMrwCyB24GPhl2hiPExKQGkGltRiCPyBeAPATfsCkWMj0kJwI07uMrHsce71WFXJmL8THgNkGpswRRMPXbonxd2Rd5FH9a6982RPw8BgyAjxpyaAGpGnnv+yJ8LsObjZxQTEkCqsRXQauw3fyoc9GSB14DtwDZgjwhdimQclRP5bLW6iRMgBQBc1yXpCUNxT9SLTceQAl0IXABcPPJahDUrr2gmJACT93AT7lXY+/wwLXiOAM8AP1DkaREOiCNZ9czbv9Avizj20hdP+eZzP3K9ev3T+4G9CnsN5keOSlJE6oArgdXA5cCcEOvoK+PuvHRTK4qkBN0EfCik584AT4BsUtUdbizW53neiGuYIZEd5BcvPzyuApeu+ArHvRw6YvPp5B00aWaqajMqn8KKYVCVq0TYG1K9S85EBOBgh/57sB43QTIAbAYeUtHnRSWnCoV8ljd/+mBJP+icD95EfDhmXQ0dTWDkUuBqVTaL0BVwvf0iPy4BnL3yBhLDyQaQ72INOIPkJWCjQR53oE8D9NdLN7eAERDqgW8B5wZcdz8YBB4Z1xpAMQ7Ipwn2qDcPfF/h7pqavt0Dg7MUPDId9wf2AN0d7aSb1yGoUWUh5X/PUQA2gvPYmEeAVNONQHypwHcJzm6/H3hQkPsVPSxiOBBgx7+buubW81TZTHnfdeSBb4tyu4ocH/sIoLGYCNcC5wX0oEeAuz2chx3MUGZnWwhtVXEUgEdB1+NwvLujbWzbwNnvvxFHWKzwWwRj1nUEuAPlEYRc1PklwQB/i3Kb43Cka2T9NKbOFDEosppgvv39wD048giQO7jzG2E1WCWhwD+I8BWgd7Dwf+ckYxoBqt3kXODX8d97Jwc8hHp/gcZy3buib34JMMD3BG5WpefdbXraESDV3ILAMvx35FDg++LQBjLcXUFROEJEgX9VpUWRru5TTKWnFUChahgVPsrJUTT84Gcqeo+T0CPdu8Jb6VcYTyG0OI7ul1G82k8rgPhQ9dnAB/H3zH8Q2DA8Lb/bG4ycg0vEFoS1GDo9TzgwykK6qAAWLW8Fu+f1+9RvMyqPJ4Zj2v1CNO+XgGdBb0J5iTj0FGnTootAEQT4Jfy9DTsIPFRwvP43no+G/hKwXeFLMSfxwus7vnbaXy46AhSMJvD3xk+BzWC2z85OD7aZKpMXUblpwb5FO/JedkxvKDoCGJWFYo0k/OIIyibEGd7z0zuDbKhK5GfAlxw9vLX7wgIHn98wpjeNOgIsbGpFVC7EX3Ov/0bZ3h2d9E2WTuDGIS1s8dz0mDsfighAAESX4N/2Lwv8S1y0P8CGqkT2A2uypvpHyxK1ZDruGtebRxWAWnu48/Fv+7df0C2eRtu+SXAAaCEXeyLhDOt/PXfbuAsYVQDGWs36efbfIS6vdUXbvonSA6zLGfM9Yp5mdk7s5LTYLqAKG4fXL7adNb2/4GP5lcwbwC2OyD+6Iqb7hYkfm48uAGUm/u3/BxReyhYiR6IJcAS4w3Fkk6LewV2TM4sbXQDC2fhn9Nkt0L3vmfFZ7kZwHFgv8Deqmj/QMfnps9gU4KcAMmK9dsoNITw/iD7gq6r8JZArRedD8YOg2fgXROpNjcWP+1S2b6hqHiSDDSMfZNDoHPCoCN9SJXughOcmxTr4LPwTQC+qZvLFBIuqZkSczxB8WFtP0IMKw5kSG8kU6+Bq/LP/G+zefq9PRftKHsJzCuku0bD/TooJ4FR5dkpFWaZcyVSgoUoUuPEMp9gIkMcaFPohkrIMDd+ZSr2dUzBwRKg/cKDkxRYTwBD+CaAmveIWynAdEMcGlAh8EQgc7Eynh+u7u0tacDEBHMN6kvixE6hFxMEKrGxQkZTAX2Nd44LdBqo+isgDnanUYH0mU7KCi3XuUawA/GCeFPKzRj6jfBCJo5oC6kL49D9DNQc8+EpdXfb8Ek0HxYb3Q9h1gB+kdOrFFRoLSnhZw2YCtyHyJ0Cis640Gix2GeSnANIK6YbLr/ep+IplFrBeVf9IROL7SiCCYpdBfdibJz+YJrA0GfN8Kr6imQPcqcZcqyJu5/z5kyqs2BQwDJR2yXkylxw7MWOqJYUsF84B7nWM+R08z+lMT9xswynyg0H8zZHbrB6LFl7U6uNHVDQLgPtIJH5DQDpTqQkVUswoNItNo+7XomexIle4UlGZWIOmDmhXuIpcTjJXXjnuAooZhYLKXqy/vh8kgU/kVfx2Oq10FgMbSCZ/5cTu3XSNcyQYVQBdO9tQ0Zfx13DjMoQV6aZoGpgk9cBGqa6+osd1+cU4FoZFj3kd0S6sx4lfzEG4FkzVe993RzBNVblcAHxztjErX0mneWXBgjG9qagAYo7kgB/7+NACrAZnxdHkicBaqoL5APDAe3p6lpv42Kz5igpAbdzU5/DvPADs5coNMePOmH/x2qAaqpJZoSLfFM+7qDOd5nRbxKICeG1HG8Ae4GWfH3o1op/MVRUkHW0LS8GlwAPAUgU6i0wHp73qzVcPHQL+B3/PwGuANVUD8WVuTbQtLBFXAPcD9eq6vDLKsfFpBRAbrkKUJ/FvO/gWF4jKrSYnc9KN0VRQIlYJtIsxi1UnGCMo09GOwm5gh88PK8DVamgFrUo3rwu+uSoPAX4NkXYRWXiq9cCYrH2GvOxh4Af4b8yZAG5A3C8iJNKN0XqgBDjAb6rqNxBZ0JlO0zl37kk/PC2qDoJuxt+7gbeYAdyK0euAxPymm8NotEpDgN9F9W6glpqat38wJgEc/clGjLIf+GeCMeOaA9yJcANQnYpOCkuBA/wB8FWMmfOWLcHYDT6lUFDYRDCjAFgRrHcxtznI3HRTK3XN0eJwksSAzwLrgVmd6fTYEz329zxH8j0XHY4V4rXYREpB2EcngUuAeoXO6pr+N5K1H2HWgkvo73k20JZLN6/jswM/mQP8PlAb6IeXFhdoFtu2z4/LIENwDOjfgVxDcClj4sBvC1wwNDhzI/A4uH2p5paAU8Yone5sZ5F3vCuuphLi2vwy8GqUNGoUTpU0KqmFq68Z2Lf58wO7uyrkuGp8SaPgzE4bpzB4gvhVO9/4zt4KEcDE5vH571uDm3CvAf4K60YeFqMmjhQBRRhgMcc6Rk0cidc/HTxQFIPhNIkjf67K6syutj0h1rmkTMgo04m7qOoTIvL3wOcJL2rGHOBq4GOCvoayXT3dhr3A6gIyM/S1E1VL/3wkday1Qn47deyAJyjTgZTAQhfnAuTMSh074Y5LNbYgIvXAY/ifTGK8+JU8OhoB3iKzq52FF9/caQrmLuARppanz8yR15KT/7tSZu7SMSnPXy9v8IQfYq8dh8KuTMT4mZQAMrvaEMij+m3gO1g35ogyYtK+/z02bs1xhbuAJ8OuUMT4KEnwh+5d7cQ9txvhFmBn2JWKGDsli/4xmMgyLS4vgrZgt2ERZUDJBHBo+wMM5pV4XLYANwL7wq5cxOkpafyfAx1teB761L62J4E/xV+nkogSUPIAUF072vhYwxpiFJ4Crgc6wq5kxOj4Eifw9Z0b8HA1V9AtIvo54IeUWUCoMwXfAkUe2NlOVUJQld3AddjTwuGwKxxxMr5mbOjLbKW/ZytnzbusH1d/DJzA+q+Va5LAo8Bj/Qe39ob9IKUikFCxffMOA/QJbMDeHvrtYxAxRgLJ2ZJ9dRd9PVuZsWClEdgLbMFaE52PzU1ULkQjwGTo3tlGAVBkH1pYI+h1WDGUZfTwSiDwaOEHd7bhUABhOEvynwT5FHAr8BLRfW3ghJK2q69nG/0926hdsBxF+p3c0W3qVv8HNinSAmDuJD/CLypuCgg1b9vxnm12bZD+MCLO4Rm9hafzNc5/Yht6HtbecCrlNIgE4Af9B60QptetxHOdQ07MPG1U/l3gVazD6BymxmLxKLApEoBPHDv0LLPrV6A5VxU5ghN/zlGzGWv524NdI8wiHDEo0IWwqb9na3lFOS/ClPZwSTeuBbEzQGyaR2HAnYdoAyorgcuwNn9pbBo3PxjAhsvdCzwDskWFFzId91XMrmVKC+CdpJatQ1xFEBBwIGlUFyk0YIWwBHuukMKeNCZHXglOnQDLYLefeWxU1Cz2pLIHGyF1D7BXkE4jZr+jmlUcVIRMx8Rz9U41ykYA72bR8nV4xvrx5J0CroklHEiKarUitQi1WCfOs7AWwtVYMSi204exu45jQK8IvSi9wJA4TjaXz+VibgxBMGIC80MMmrIVwFssWf5lBrwCRnTENUxttcZZs5N+fSQxlJN3eP3Fr4ddxYiIiIiIiIiIiIiIiIiIiIiIiMnzv6ClhaZZz+g9AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE1LTA5LTIxVDAxOjI5OjI3LTA0OjAwMrEyswAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNS0wOS0yMVQwMToyOToyNy0wNDowMEPsig8AAAAASUVORK5CYII='
//;

CNT_MENULOGO ='data:image/png;base64,'+
'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuMWMqnEsAAA9vSURBVGhD7VoHdJZVmg6QQHpvpJMGmBCKIQQCCMQCDsisu47joIygojPLKDMMKjKLAzICKlJDEkhCSKPEFFoSShoEAqGTUBJaaKEpgt3Z8sz7vPm+8IOcGXZWdzx79p7zHO5371ue99b3/sHq/8v/sRIkGCl4VTBLMN8A62xjH2V+NMVGMEqQIbgowH2CstRhQLTxv148BTMEVwRtxNpb26CjgzPs3Lzh6O0PJ98ghaN3AOylraODi8pY6ghog7Zo8wcvdoLpgs8FSsC6ky0cPP3gHtIdXl37wKtbH3h3e7AV3WNvQ769CJFx79Jddahr2jFs0jZ9/CBliOC0QB3a2DvBxT8MnhE94RnZC16RvYUcwSCMADSI23UGp/0iRx3qugSEqS3TruGDvr630l4wU/CfAnTo2AnOfl3gEdYDHuExrQFECBkG0RbAnUHoyFuQZ7BmAJ5iwyO8h9qkbfowfNEnff+Pir2gWKCGbV08dKm4h0ZJANHqmAQsZ8E3uh8C+gxCUN+hCO6XqAiKG6pt7LMcfYKDoIMh9ty7PKA+TH+Gb3L4u4qToFqAdu3b60Z0I3lx0haAOCYJ/14JCB04HJGJoxEx9AmEPTQSoYMel7YRrZA629hHGcpSpzUAg7zYVNvig77ok74NDo6C/1axFVQIxFAHPUncgrtKAN10A9IRHQfGPtRKePBPENz/UQT1exiBfYfBt4eMdKSQC2OgUVpnG/soQ1nqUJc2PMWWGQAHyS24m/gMVt/kYHAhp/sq7QS5Ah0FJ58guAZGwDVIAhDDdNBZyIQPGYkuMrok5Nd7IPy798aDAxMxfPRTeG7CbzBxynT8fvpsBevPvTRR+/oMHCayvVSHurRBW7TJ0ecgcbBcgyJ14Cxmgpzuq7wmgFW7dnrUuQSEGwFEqgOu6VAZPTrvHBOPHvEP4Zlxr+Dt9xYjPa8QW7fX4vCxJjRfaMGVax8rWGfb1upapOUW4N/mLMLPn38Z0aJLG7Slsyi2OcP0RZ/07eDlp1yUUyu3v1qiBd8IdDM5+4fqUUlDnF6uXU5/QOwQdIsdiBdffR3zUrKwuaoWZ4Xk1Y9vCD7FtU/uAWlnH2UoW1a5Cx8kZ+KFib9H19gEtUnb9OEe+oARgPgWDhYbm9zI8Z6FR1atQC4YOzh3DtGjjQY48jQcFP8I/HsOwOP/MgazF6dj3ZZqNJ45jzPnW4TUZZy9eAXNgnOXrraixYDUmwnpo8wZkaUOdYs3V+FPC5djxD//Qm3ThwYhM8HBc/YLVS7kRG4GRy7z75QxgtalI9PGTURFjkSXAa0bNLjXAEyY9CaSMteiZt8R1DeexdGTzTh26hxOCJnGsxfR1HwRJ89dwilFi4FL2sa+xrMXVJY61K1vPIOavYexeMUandEg8aF7Y8BjspQidBCdhIeDl7/lUiLXO0oHwUmB5CtOcPQJ1A3EAHiG02BInwT8dtosLMsrxs599dh7pBH7j57EoeOnbwdymoFcUJIMppGElfRFnJC24wbxBpE9IsQPHT+F/Q1NYuuEDkhqbhFemzoTIb0T1Cd9kwMH00k4Mc8iR0GTwbmtPClAO2Pj8hymgm9UXwTLlAbKmU3DqbnFqKg9gKLSCuw+dAx7DotjGb3DJ86gvkmCEHIkSbLfCUACO376vMpQ9tCJ0zggA8CBqD14TAKoV9vJOYX4zRt/FJ8D1LdvdF89CR29A5UbOZKrwbmtlAlgY+eg5AnXwHCEyKby7z0IY1+ZhCWZ+Sip2o2kjFzkry/FLnH6/qJkmYUmDYCjekwIKnkh3WQuGwNsYxCUqRfZQ6LDGayT0d914Ciq646gfNcBbKrcjUUZa/DshFfVNzmQi8mLHMlVUEriLExj/11gpMGtgrwpOY0DHxuN+WmrULy1BpW7D+LJp5/FERnBpIw8vDPnPRyUZcDvo0reIC4b9JRs1NOyYQnW2ca+4xIgZ+GILLsDx06hrr5RZ1KXktRr9jegeMsOzEvNQcKjo5QDuZi8yJFcDc4eAqtn2MBbzxRixLp0Yvph7uI0bNi2E4VlVViUvgojRj6BlOx8xCUMRkPTaSHfjAYuHZIXkiR8Rk6csy3X0Hy5FayzjYE0yaZmEA0nJQgJnBt6s9wPy+V+WLA8BylZ+SgsrcT6bTWYvXAZAnvEKRfLWbC4ocndKokf1rb2bQK+0XEIjBuGn419EfmbKpC2ZhNSctfhvaUrEd2zj+yHGRg0JBGZBWW6EZUYyV+SY1QIn7/6MS5c+wQXDbB+/opcaNLHABkEZ4QHQEZ+CT5clotJb83UtU8f9JW2ZiPWbirHU8KBXMjJ5EeuRgDkblXDj46OLq0CsnmD4uS2lVOHN2vOum1YmlWA/oOHof3tax3W8rIanDhc+j7SzagEr1xXspfkwrr8yU1cuXETlwmps419zZev45zIHZTlszS7AA89PALWNrdfafQRP2goksQuffPmDntQlrNwIjdyJFdDntytWvjB246dbsGRuu5G/exZFMlazMzfiM7+garAE4CgE6JDhw7w9vHFwuXZuq5JtEVw9dNbuHbrc1w3cO3mZ7gibS1yI7dIINzIi9Jy4O3bWW2Y9kz79OXr548VazegUDg88fRzyoncyNHiZiZ3qz/zw87VSzuZOQbJtT5txrvYVnsQ/RIGtZE3SVtbW8NGRo3o2LEj3D08kZyZp7etEv/sC3zyxZe48eVXCtbZdu3m52iWmzll5Wp4eHqprmmHNs1gzCD6xicohz/MnC3viyHKjRzJlf0G99apMwPwkbM/KCYOWfnFWLmm8A7iJmFbW1vY2dndAS9vH+QWbMA1GekbX3yFm199jVtffaO4+eXX0val9q0q2iiz5vMdfdo0A7IMJCMvH9n56xDcM1653RUAYfUtK7YuntrJ7LDvsOF48+13MHjYI2qIo2MSt7eXze7oCGdnZ4WLi4v+6+TkhOCQEJTv3KOEb339DT775lsF62yr3FWHkC5d2vRNXYJttG0GQp/0HRs/AHMXJCG8Z1/lRo7kapAnd6sL/Ojk5GoE0B+BEd3lYjmIKVOn6WjQIEeJTujUzc0NHh4eeMq7Mwok5QiT5eDu7q59sX3jcOr8JSX++bd/VrB+WjLQvv3iVYay1KEubXh6eqpN9tEHfZlBvDZ5Cipr9yM4Mkq5kSO5GgGQu1UlP8xbuHOPeLj7+qOorAKZeat1Sk3yrq6u6szX1xd+fn4ICAhQ+Pv7a5uXl5cSeemVX+syuqWj/63WX/7XidpHGcpSx9SnLbbRNn1wNs0gMrJzlYunX6ByI0eL25jcrebxo30Ha+30eSAWts5umLswBSeaLyCmp2wcgzyd01lwcDBCQ0MRHh6uCAsLQ4gsH5Ly9vbWES7dVoFPZdkQmyuqtI19lKE+dUx92mIbbdMHfdFndI8YTU0+WJIGO2d35UaO5GoEQO5WPzE+YO/uIw/tGLmuvfDkz5/DxvKdqNmzD+ERETo6dECikZGRiIqKQkxMjCI6Ohpdu3ZVIqw/Onw4Zv7pXXwsJw8xa/YcbWMfZSjLuqlPW7TZRfYHfdBXmAS2fXedcKjBmPEvKydyI0eTr8FdH8s32WBjL2tcXl9OnYPh4uaO+cuyNUPcX38M4154QR3QEZ3GxsYiPj4eCQkJGC99KcvTcOBIA67Lmc974PINS8ilJm3sO1jfgFSRHf/Ci6pLG7RFmwyMPp4fPx776o+q78WSvvAeUk7CjRzJ1eDc9tBfKtBjiykrH9b8jpK0IUVy9NLqOk2B9wnBFdk5WJKcgsTHRmCLLI1zLVdwSW7Ys3K+n2LyxpcXb1tJJ85flRRCwPpZaWMf0w7mRUwxqFtevQPZeauQlJKKFVk52Hu4QX3RJ9P3/oMTlQs53ZVOk3NbiRT8h0A2iKM+59q1a00bHojprdlo3oZylO3Yi52S+jJrXJohziSDZDa5X9KCw2ZG2izps+Q5J4Xo5qodCtYbpe24pNQNInOIgyE6e+QtQBv7aEfqzETLtu9F7vpyLJSU+kG5yMiBXMiJ3PhtcCXnO8pygQpwndm6uJvCcJcjb9Jb72jitaakCsXbdqFEHPGW3L6vAbWHjwuRkzggjxQGckTy/V+M/SWCgoIUY8Y+r23sOyAJXJ0Q3nXoOKrlEUMbJTLaRVt3YvWmSqSvLdGXn6e3b5t/crlr7ZPrd4qX4LpA01V7j9sGTDw1ZizeemeupNMfSRA71fFWeYRUyWOkRmamVvJ6kps2Y5aeLpMnT1ZEyCHAtrr6Jg12h8hW1h3Glp3ygKneI+RrkCxp9FSxTR93+yUXixSaHMn1nuWfBP8lQAcbudZt285bxQep2TpCWcVbsWZTFQrF8QZ5QZXt2KcjWSmBcFSnvPkWGs+ck8tLHjVcPvJWnvLmVFTvrVfiW0W2VJYjdQu21OjIryzagjSx/X5y1h0+bSR1bi9cjG9yI8e/WmYLVIFBEKw7u7jqKyk5p1jS4EKsLNwi+6IC+WXbdTY2CplSWVYktk1mpXz3IQsc1JliP5ceiXPJ5JdWI1f2VmbBZkmfC8RukQbg5NyaLlv6N0Buf7Pw96EsgSrx0iBCQiPwh9kL1MGLEyfrIyfjo1KdjZUFpUqmUEaT+2N9Ra1iY9UehfnNvgJJj9eKbKboZMmop+eXIjVvPcb9ahLmJmVi6qx5CA4Nb/Nr8jA43fP3oHsV/mSRLmhVlqMrOCxCfxN6fcZ76B7dE5kflSBZjrnlEgh/XVu9sVIfIJyVD1OzkL66WIkSi9LzsCynQPsok5K3DpOmvo1lqzdiaU4RZgvxPnHxeGPm+3jp1dclgAjL34AIcrnjZ5T7KYx2mkD/sEG4uXsi8fHRmmTlFG7CnCUrsCB9NaJ6xSK7sFSD4Yg+OWa8Pli4NIg3Zn6A302dLn0lKjPjw1Q8Puqnqstf+H739ly1+ey4CerD9Gf4Jof7Hvl7lUSBZqyWmDBxkr5hJ4tzOwcHefrlK6H5gvDuPTAvabm+bYlxMkMjR/8Uy1atV5lfTZku7wdvecCvxcTXp2PoiCfusG2APun7eynOggUCfb2Z6NTJFiHhXbU+b0mKrN8P8dvpc2Bn74C58xfj3UXpilFPj4WnJGkFJeVyVM7D6GfGqU5/ee3RhqVNAX3QF31+7yVcwL/v6i/Yd8PB0Qm+AUFaH/rwI/pDQPygYW1B9uzdR2UsdSxAm7RNHz948RFMEewX6L3xd4K6tEFbtPkPKQGCsQImV/yZ46qgbeNbgG3sowxlqUPdH2Xhfx/giAYbYP0f8l8KfuTFyuovPtkH2vBfKycAAAAASUVORK5CYII='
;

//#############################################################################
// TWbMenuStructure
//#############################################################################

constructor TWbMenuStructure.Create(MainMenu: TWbMainMenu);
begin
  inherited Create(nil);
  SetRoot(self);
  SetMainMenu(MainMenu);
end;

destructor TWbMenuStructure.Destroy;
begin
  inherited;
end;

//#############################################################################
// TWbMenuStructureItem
//#############################################################################

constructor TWbMenuStructureItem.Create(AOwner: TWbMenuStructureItem);
begin
  inherited Create(AOwner);
  if AOwner <> nil then
    AOwner.RegisterChild(self);
end;

destructor TWbMenuStructureItem.Destroy;
begin
  // Clear all child nodes.
  // Note: this calls unregister in this instance for all child
  // nodes. So you dont need to manually clean up the structure
  Clear();

  // Is a visual control connected to this
  // menu item? If so, time to release it!
  if Control <> nil then
  begin
    Control.free;
    Control := nil;
  end;

  // Tell our owner that we are releasing.
  // Check that owner has been set first
  if Owner <> nil then
    Owner.UnRegisterChild(self);

  inherited;
end;

procedure TWbMenuStructureItem.Clear;
begin
  try
    for var item in FObjects do
    begin
      try
        item.free;
      except
      end;
    end;
  finally
    FObjects.Clear();
  end;
end;

function TWbMenuStructureItem.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := (CandidateObject <> nil) and (CandidateObject is TWbMenuStructureItem);
end;

procedure TWbMenuStructureItem.SetControl(const NewControl: TWbMenuItem);
begin
  if NewControl <> nil then
  begin
    FControl := NewControl;
    FControl.enabled := FEnabled;
    FControl.Caption := FCaption;
  end else
  FControl := NewControl;
end;

procedure TWbMenuStructureItem.OpenMenu;
begin
  if MenuOpen then
    CloseMenu();

  if Control <> nil then
  begin
    Control.Label.Font.Color := CNT_TEXT_HIGHLIGHTED;
    Control.Handle.style['background-color'] := 'rgba(255, 255, 255, 0.3)';
  end;

  FContainer := TWbMenuContainer.Build(FMainMenu.Parent, self);
end;

procedure TWbMenuStructureItem.CloseMenu;
begin
  if MenuOpen then
  begin
    CloseChildMenus();

    if Control <> nil then
    begin
      Control.Label.Font.Color := clBlack;
      Control.Handle.style['background-color'] := '';
    end;

    // Release current menu
    ReleaseChildControls();
    if FContainer <> nil then
    begin
      FContainer.free;
      FContainer := nil;
    end;

  end;
end;

procedure TWbMenuStructureItem.CloseChildMenus;
begin
  // Close any child menus open
  for var xItem in FObjects do
  begin
    if xItem.MenuOpen then
      xItem.CloseMenu();
  end;
end;

procedure TWbMenuStructureItem.ReleaseChildControls;
begin
  for var Child in FObjects do
  begin
    if Child.Count > 0 then
      Child.ReleaseChildControls();
    Child.ReleaseControl();
  end;
end;

procedure TWbMenuStructureItem.ReleaseControl;
begin
  if FControl <> nil then
  begin
    try
      // This may seem excessive, but the only exception
      // here is that the control is already in the process of
      // destroying itself.
      if not (csDestroying in FControl.ComponentState) then
      begin
        try
          FControl.free;
        except
          // Just sink the exception
        end;
      end;
    finally
      // Ensure our reference to the control is void
      FControl := nil;
    end;
  end;
end;

procedure TWbMenuStructureItem.SetRoot(const MenuRoot: TWbMenuStructureItem);
begin
  FRoot := MenuRoot;
end;

procedure TWbMenuStructureItem.SetMainMenu(const ThisMenu: TWbMainMenu);
begin
  FMainMenu := ThisMenu;
  if FObjects.Count > 0 then
  begin
    for var item in FObjects do
    begin
      item.SetMainMenu(FMainMenu);
    end;
  end;
end;

function TWbMenuStructureItem.GetOwner: TWbMenuStructureItem;
begin
  result := TWbMenuStructureItem( inherited GetOwner() );
end;

procedure TWbMenuStructureItem.RegisterChild(const NewChild: TWbMenuStructureItem);
begin
  if NewChild <> nil then
  begin
    if FObjects.IndexOf(NewChild) < 0 then
    begin
      FObjects.Add(NewChild);
      NewChild.SetRoot(FRoot);
      NewChild.SetMainMenu(FMainMenu);
    end;
  end;
end;

procedure TWbMenuStructureItem.UnRegisterChild(const OldChild: TWbMenuStructureItem);
begin
  // Note: This is called from the destructor of the child
  if OldChild <> nil then
  begin
    var Index := FObjects.IndexOf(OldChild);
    if Index >=0 then
      FObjects.delete(Index, 1);
  end;
end;

function TWbMenuStructureItem.Add(Caption: string; const TagValue: variant): TWbMenuStructureItem;
begin
  result := TWbMenuStructureItem.Create(self);
  result.Caption := Caption;
  result.TagData := TagValue;
end;

function TWbMenuStructureItem.Add(Caption: string): TWbMenuStructureItem;
begin
  // Note: Register() is called from the constructor, so we dont
  // have to deal with that here
  result := TWbMenuStructureItem.Create(self);
  result.Caption := Caption;
end;

function TWbMenuStructureItem.GetCaption: string;
begin
  result := FCaption;
end;

procedure TWbMenuStructureItem.SetCaption(NewCaption: string);
begin
  FCaption := NewCaption;
  if (Control <> nil) then
  begin
    // Make sure control is not being destroyed
    if not (csDestroying in Control.ComponentState) then
    begin
      if NewCaption <> Control.Caption then
        Control.Caption := NewCaption;
    end;
  end;
end;

function TWbMenuStructureItem.GetEnabled: boolean;
begin
  result := FEnabled;
end;

procedure TWbMenuStructureItem.SetEnabled(NewEnabled: boolean);
begin
  FEnabled := NewEnabled;
  if (Control <> nil) then
  begin
    // Make sure control is not being destroyed
    if not (csDestroying in Control.ComponentState) then
    begin
      if NewEnabled <> Control.Enabled then
        Control.Enabled := NewEnabled;
    end;
  end;
end;

//#############################################################################
// TWbMenuItem
//#############################################################################

procedure TWbMenuItem.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode :=  dmBlock;

  TransparentEvents := false;
  SimulateMouseEvents := true;

  FLabel := TWBMenuItemTitle.Create(self);
  FLabel.Font.Size := 14;
  FLabel.Caption := ClassName;

  FArrow := TWbMenuArrow.Create(self);
  FArrow.Visible := false;
  FArrow.LoadFromUrl(CNT_ARROW_RIGHT, procedure (Success: boolean)
  begin
    FArrow.SetSize(FArrow.PixelWidth, FArrow.PixelHeight);
    invalidate();
  end);

  OnClick := @HandleItemClicked;
end;

function TWbMenuItem.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
  Exclude(result, cfKeyCapture);
end;

procedure TWbMenuItem.StyleTagObject;
begin
  inherited;
  Handle.style['flex-grow'] := '0';
  Handle.style['flex-shrink'] := '0';
  Handle.style['flex-basis'] := 'auto';
  Handle.style['margin-bottom'] := '2px';
end;

procedure TWbMenuItem.SetData(const NewMenuData: TWbMenuStructureItem);
begin
  if NewMenuData <> FData then
  begin
    FData := NewMenuData;
  end;
end;

procedure TWbMenuItem.HandleItemClicked(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FData <> nil then
    begin
      if FData.Count > 0 then
      begin
        if not FData.MenuOpen then
        begin
          FData.OpenMenu();

          var LPos := TPoint.Create(0, Top);
          LPos := ClientToScreen(LPos);
          FData.Container.moveTo( (LPos.x + clientwidth)-1, Lpos.y);

        end else
        FData.CloseMenu();
      end else
      begin
        if FData.MainMenu <> nil then
        begin
          FData.Control.Label.Font.color := CNT_TEXT_HIGHLIGHTED;

          TW3Dispatch.Execute( procedure ()
          begin
            var LMain := FData.MainMenu;
            var LNode := FData;

            // Close the menu
            FData.MainMenu.MenuData.CloseMenu();

            // Fire the Click handler
            TW3Dispatch.Execute( procedure ()
            begin
              if assigned(LMain.OnMenuItemClick) then
                LMain.OnMenuItemClick(LMain, LNode);
            end, 50);

          end, 100);

        end;
      end;
    end;
  end;
end;

procedure TWbMenuItem.Resize;

  procedure HideArrowIfVisible;
  begin
    if FArrow.Visible then
      FArrow.Visible := false;
  end;

  function CenterRectWithin(const ParentRect,  ChildRect: TRect): TRect;
  begin
    // Make sure child-rect size can be divided by 2
    if ( (ChildRect.Width < 2) or (ChildRect.Height < 2) ) then
      Exit(ParentRect);

    // Make sure parent-rect size can be divided by 2
    if ( (ParentRect.width < 2) or (ParentRect.height <2) ) then
      Exit(ParentRect);

    // Calculate center offset
    var dx := (ParentRect.Width div 2) - (ChildRect.Width div 2);
    var dy := (ParentRect.Height div 2) - (ChildRect.Height div 2);

    // move into parent-rect space
    inc(dx,ParentRect.left);
    inc(dy,ParentRect.top);

    result.left := dx;
    result.top := dy;
    result.right := dx + ChildRect.Width;
    result.bottom := dy + ChildRect.Height;
  end;

begin
  inherited;

  if not (csReady in ComponentState) then
    exit;

  var LXOff := Border.GetVSpace();
  var LYOff := Border.GetHSpace();
  var LBounds := Border.AdjustClientRect(ClientRect);
  dec(LBounds.right, LXOff);
  dec(LBounds.bottom, LYOff);

  if MenuDataItem <> nil then
  begin
    if MenuDataItem.Count > 0 then
    begin
      FArrow.Visible := true;
      dec(LBounds.right, 16);
      var LArrowSpace := TRect.Create(LBounds.right, LBounds.top, LBounds.right + 15, LBounds.top + LBounds.Height);
      var LArrowPos := CenterRectWithin(LArrowSpace, FArrow.BoundsRect);
      FArrow.MoveTo(LArrowPos.left, LArrowPos.top);
    end else
      HideArrowIfVisible();
  end else
    HideArrowIfVisible();

  FLabel.SetBounds(LBounds);
end;

procedure TWbMenuItem.SetHeight(const NewHeight: integer);
begin
  inherited SetWidth(NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewHeight);
end;

procedure TWbMenuItem.SetSize(const NewWidth, NewHeight: integer);
begin
  inherited SetSize(NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewHeight);
end;

procedure TWbMenuItem.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
  inherited SetBounds(NewLeft, NewTop, NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewHeight);
end;

//#############################################################################
// TWbMenuContainer
//#############################################################################

procedure TWbMenuContainer.InitializeObject;
begin
  inherited;
  DisplayMode := dmFlex;
  FItemHeight := CNT_MENU_MENUITEM_HEIGHT;
  FGeneralMouseEvent := TW3MouseDownEvent.Create(self, dmBubble);
  FGeneralMouseEvent.OnEvent := @GeneralMouseHandler;
end;

procedure TWbMenuContainer.FinalizeObject;
begin
  // Unregister with main menu if assigned
  if MenuRoot <> nil then
  begin
    if MenuRoot.MainMenu <> nil then
      MenuRoot.MainMenu.UnRegisterContainer(self);
  end;

  FGeneralMouseEvent.free;
  inherited;
end;

procedure TWbMenuContainer.GeneralMouseHandler(sender: TObject; EventObj: JEvent);
var
  LHandle:  TControlHandle;
begin
  // First, try to check if we hit a menuitem.
  // If that is true, then check if that menuitem would
  // open a child menu. If it does then just exit, we dont want
  // to accidentally close a menu as it's opening
  if MenuRoot <> nil then
  begin
    LHandle := variant(EventObj.Target);
    if (LHandle) then
    begin
      // Get the element we hit
      var LControl := TW3ControlTracker.GetControlByHandle(LHandle);

      // Not a control? Check if its a managed html element created
      // by one of our controls
      if LControl = nil then
      begin
        LControl := FindOwnerOfHandle(LHandle);
        {$IFDEF DEBUG}
        if LControl <> nil then
          writelnF("You hit a managed element, locating owner control (type is %s)", [LControl.ClassName]);
        {$ENDIF}
      end;

      if LControl <> nil then
      begin
        if (LControl is TWBMenuItemTitle) then
        begin
          LControl := TWBMenuItemTitle(LControl).Parent;
        end;

        {$IFDEF DEBUG}
        writelnF("You hit a control of type: %s", [LControl.ClassName]);
        {$ENDIF}
        if (LControl is TWbMenuItem) then
        begin
          var LMenuData := TWbMenuItem(LControl).MenuDataItem;
          if LMenuData <> nil then
          begin
            if LMenuData.Count > 0 then
            begin
              if LMenuData.MenuOpen then
              begin
                LMenuData.CloseChildMenus();
                LMenuData.CloseMenu();
                EventObj.stopImmediatePropagation();
                self.SetFocus();
              end;
              exit;
            end else
            begin
              MenuRoot.CloseChildMenus();
              self.SetFocus();
              exit;
            end;
          end;
        end;
      end;
    end;

    if MenuRoot.MainMenu.MenuIsOpen() then
    begin
      {$IFDEF DEBUG}
      writeln("Event caught by menucontainer, closing all child menus");
      {$ENDIF}
      MenuRoot.CloseChildMenus();
      Self.SetFocus();
      EventObj.stopImmediatePropagation();
    end;
  end;
end;

function TWbMenuContainer.FindOwnerOfHandle(const Handle: TControlHandle): TW3TagObj;
begin
  // Check that handle is valid
  if (Handle) then
  begin
    // is there a control for this one?
    var LAncestor := Handle;
    while (LAncestor.parentNode) do
    begin
      result := TW3ControlTracker.GetControlByHandle(LAncestor);
      if (result <> nil) then
        break
      else
        LAncestor := LAncestor.parentNode;
    end;
  end;
end;

procedure TWbMenuContainer.SetItemHeight(NewHeight: integer);
begin
  FItemHeight := if NewHeight < CNT_MENU_MENUITEM_HEIGHT
    then
      CNT_MENU_MENUITEM_HEIGHT
    else
      NewHeight;
end;

procedure TWbMenuContainer.StyleTagObject;
begin
  inherited;
  ThemeReset();
  StyleClass := "TWbMenuContainer";
  Handle.style["flex-direction"] := "column";
  Handle.style["flex-wrap"] := "nowrap";
  Handle.style["flex-flow"] := "center";
  Handle.style["align-content"] := "space-between";
end;

function TWbMenuContainer.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
  Include(result, cfKeyCapture);
end;

class function TWbMenuContainer.Build(Parent: TW3TagContainer;
  Structure: TWbMenuStructureItem): TWbMenuContainer;
begin
  if Parent <> nil then
  begin
    if Structure <> nil then
    begin
      result := TWbMenuContainer.Create(Parent);
      result.BeginUpdate();
      try
        result.SetSize(160, CNT_MENU_MENUITEM_HEIGHT);
        result.Populate(Structure);
      finally
        result.EndUpdate();
      end;

      if Structure.MainMenu <> nil then
        Structure.MainMenu.RegisterContainer(result);

    end else
    raise Exception.Create('Failed to build menu, structure was nil error');
  end else
  raise Exception.Create('Failed to build menu, parent control was nil error');
end;

procedure TWbMenuContainer.Populate(const Structure: TWbMenuStructureItem);
var
  LChild: TWbMenuItem;
begin
  if Structure <> nil then
  begin
    if Structure.Count > 0 then
    begin

      MenuRoot := Structure;

      // Init some values
      var LBounds := ClientRect;
      var LYPos := LBounds.Top;

      BeginUpdate();
      try

        // Calculate height of menu
        var Estimate := Structure.Count * ItemHeight;
        inc(Estimate, (Structure.Count-2) * 4);
        inc(Estimate, 8);

        Height := Estimate;

        for var x := 0 to Structure.Count -1 do
        begin
          var ChildData := Structure.Items[x];
          if ChildData.Control = nil then
          begin
            // Create control if not in the structure
            LChild := TWbMenuItem.Create(self);

            // Attach menu-data node to control
            LChild.MenuDataItem := ChildData;

            // Attach control to menu-data node
            ChildData.Control := LChild;

            // Set default size
            LChild.SetSize(LBounds.Width, ItemHeight);

            // Separator? OK, style it with a bevel
            if ChildData.Separator then
              LChild.TagStyle.Add("TWbMenuItemSeparator");
          end else
          begin
            // Recycle previously created element
            LChild := ChildData.Control;

            // Has other owner? Unregister from that
            if LChild.Parent <> nil then
              LChild.Parent.UnRegisterChild(LChild);

            // Take ownership and attach to this control
            Self.RegisterChild(LChild);

            // Set size, the flexbox will do the rest
            LChild.SetSize(LBounds.Width, ItemHeight);
          end;
        end;
      finally
        EndUpdate();
      end;

    end else
    raise Exception.Create('Failed to populate menu, structure has no members error');
  end else
  raise Exception.Create('Failed to populate menu, structure was nil error');
end;

//#############################################################################
// TWbMenuArrow
//#############################################################################

procedure TWbMenuArrow.StyleTagObject;
begin
  inherited;
  ThemeReset();
  StyleClass := 'TWbMenuArrow';
  Draggable := false;
  SetFit(fsNone);
end;

//#############################################################################
// TWbMenuGlyph
//#############################################################################

procedure TWbMenuGlyph.StyleTagObject;
begin
  inherited;
  ThemeReset();

  PositionMode := pmRelative;
  DisplayMode :=  dmBlock;
  Handle.style['margin-right'] := '2px';

  Draggable := false;

  Handle.style['flex-grow'] := '0';
  Handle.style['padding'] := '0px';
end;

//#############################################################################
// TWbMainMenuTitle
//#############################################################################

procedure TWbMainMenuTitle.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode :=  dmBlock;
  Font.Size := 14;
  Caption := 'Quartex Web OS ~ Copyright © Quartex Components LTD, all rights reserved';
end;

function TWbMainMenuTitle.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  //Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
end;

procedure TWbMainMenuTitle.StyleTagObject;
begin
  inherited;
  Handle.style['flex-grow'] := '1';
  Handle.style['flex-shrink'] := '0';
  Handle.style['margin'] := '0px';
  Handle.style['margin-right'] := '2px';
  Handle.style['padding'] := '0px';
end;

procedure TWbMainMenuTitle.SetWidth(const NewWidth: integer);
begin
  inherited SetWidth(NewWidth);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

procedure TWbMainMenuTitle.SetSize(const NewWidth, NewHeight: integer);
begin
  inherited SetSize(NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

procedure TWbMainMenuTitle.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
  inherited SetBounds(NewLeft, NewTop, NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

//#############################################################################
// TWbMainMenuAppDisplay
//#############################################################################

procedure TWbMainMenuAppDisplay.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode :=  dmBlock;
end;

function TWbMainMenuAppDisplay.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
end;

procedure TWbMainMenuAppDisplay.StyleTagObject;
begin
  inherited;
  Handle.style['margin'] := '2px';
  Handle.style['flex-grow'] := '1';
end;

procedure TWbMainMenuAppDisplay.SetWidth(const NewWidth: integer);
begin
  inherited SetWidth(NewWidth);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

procedure TWbMainMenuAppDisplay.SetSize(const NewWidth, NewHeight: integer);
begin
  inherited SetSize(NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

procedure TWbMainMenuAppDisplay.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
  inherited SetBounds(NewLeft, NewTop, NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

//#############################################################################
// TWbMainMenuAppRegion
//#############################################################################

procedure TWbMainMenuAppRegion.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode :=  dmFlex;
  Font.Size := 16;
end;

function TWbMainMenuAppRegion.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
end;

procedure TWbMainMenuAppRegion.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

procedure TWbMainMenuAppRegion.SetWidth(const NewWidth: integer);
begin
  inherited SetWidth(NewWidth);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

procedure TWbMainMenuAppRegion.SetSize(const NewWidth, NewHeight: integer);
begin
  inherited SetSize(NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

procedure TWbMainMenuAppRegion.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
  inherited SetBounds(NewLeft, NewTop, NewWidth, NewHeight);
  Handle.style['flex-basis'] := TInteger.ToPxStr(NewWidth);
end;

//#############################################################################
// TWbMainMenu
//#############################################################################

procedure TWbMainMenu.InitializeObject;
begin
  inherited;
  TransparentEvents := false;
  Displaymode := dmFlex;

  FLogo := TWbMenuGlyph.Create(self);
  FLogo.OnClick := procedure (Sender: TObject)
  begin
  end;

  FTitle:= TWbMainMenuTitle.Create(Self);
  FTitle.AlignText := TTextAlign.taLeft;
  FTitle.VAlign := TTextVAlign.tvCenter;
  FTitle.AutoSize := false;

  FApps := TWbMainMenuAppRegion.Create(self);
  FApps.OnClick := procedure (Sender: TObject)
  begin
    //
  end;

  // Design the menu structure
  FDesign := TWbMenuStructure.Create(self);
  FDesign.Add('Execute Command');
  FDesign.Add('Redraw all');
  FDesign.Add('Update all');
  var msg := FDesign.Add('Last message');
  msg.Add('Subitem1');
  msg.Add('Subitem2');


  var dat := FDesign.Add('About');
  dat.Separator := true;
  dat.TagData := 1;
  FDesign.Add('Quit');

  self.OnMouseDown := procedure (Sender: TObject; Button: TMouseButton;
                        Shift: TShiftState; X, Y: integer)
  begin
    if Button = TMouseButton.mbLeft then
    begin
      if FDesign.MenuOpen then
      begin
        FDesign.CloseMenu();
        exit;
      end;

      // Calculate screen position relative to the hosting form
      var myform := Application.CurrentForm;
      var dx := self.left + FTitle.left + FLogo.width + 6;
      var dy := 0;
      var rp := myform.ClientToScreen(TPoint.Create(dx, dy));

      FDesign.OpenMenu();
      FDesign.Container.MoveTo(rp.x, rp.y);
    end;
  end;
end;

function TWbMainMenu.MenuIsOpen: boolean;
begin
  result := FContainers.Count > 0;
end;

function TWbMainMenu.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  //Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
end;

procedure TWbMainMenu.FinalizeObject;
begin
  FTitle.Free;
  FApps.free;
  FLogo.free;
  inherited;
end;

procedure TWbMainMenu.StyleTagObject;
begin
  inherited;
  ThemeReset();
  handle.style['flex-flow'] := 'row nowrap';
  handle.style['align-items'] := 'left';
  handle.style['justify-content'] := 'center'; // space-around
end;

function TWbMainMenu.GetMenubarTitle: string;
begin
  result := FTitle.Caption;
end;

procedure TWbMainMenu.SetMenuBarTitle(NewTitle: string);
begin
  FTitle.Caption := NewTitle;
end;

procedure TWbMainMenu.RegisterContainer(const Container: TWbMenuContainer);
begin
  if Container <> nil then
  begin
    FContainers.add(Container);
  end else
  raise Exception.Create('Failed to register menu container, instance was nil error');
end;

procedure TWbMainMenu.UnRegisterContainer(const Container: TWbMenuContainer);
begin
  if Container <> nil then
  begin
    var LIndex := FContainers.indexOf(Container);
    if LIndex >= 0 then
    begin
      FContainers.Delete(LIndex, 1);
    end;
  end else
  raise Exception.Create('Failed to un-register menu container, instance was nil error');
end;

procedure TWbMainMenu.ImageIsSizedAndReady;
begin
  if not (csDestroying in ComponentState) then
  begin
    if FLogo.Url = '' then
      FLogo.LoadFromURL(CNT_MENULOGO);
    invalidate();
  end;
end;

procedure TWbMainMenu.Resize;
begin
  // inherited;

  var CH := ClientHeight;
  var CW := CH + 2;

  if FLogo <> nil then
  begin
    // Make sure C= glyph container is in proportion
    if (FLogo.Width <> CW)
    or (FLogo.Height <> CH) then
    begin
      FLogo.SetSize(CW, CH);

      // With the size adjusted, make sure the image
      // has been loaded. This should only happen after
      // the container has been positioned
      if FLogo.Url = '' then
        ImageIsSizedAndReady;
    end;
  end;

  if FTitle <> nil then
    FTitle.height := CH;

  if FApps <> nil then
    FApps.Height := CH;
end;


initialization
begin
  var sheet := TW3StyleSheet.GlobalStyleSheet;
  var prefix := BrowserAPI.PrefixDef('');
  sheet.Append( StrReplace(#"

    .TWbMainMenuAppRegion {
      margin-left: 2px;
      background: rgba(0, 0, 0, .07);
    }

    .TWbMenuArrow {
      margin: 0px !important;
      padding: 0px !important;
      border-width: 0px !important;
    }

    TWbMenuItem:not(:first-child):not(:last-child) {
      margin: 1px !important;
    }

    .TWbMenuItem {
      padding: 0px !important;
      border-width: 0px;
    }

    .TWbMenuContainer {
      margin: 0px !important;
      padding: 0px !important;
      background: §linear-gradient(#EEEEEE, #D2D2D2);
      border-style: solid;
      border-top-width: 1px;
      border-top-color: #FFFFFF;
      border-bottom-width: 1px;
      border-bottom-color: #626262;
      box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.3);
    }

    .TWbMenuItemSeparator {
      border-style: groove;
      border-bottom-width: 2px;
    }

    .TWbMainMenu {
      padding: 2px !important;
      background: §linear-gradient(#EEEEEE, #D2D2D2);
      border-style: solid;
      border-top-width: 1px;
      border-top-color: #FFFFFF;
      border-bottom-width: 1px;
      border-bottom-color: #626262;
      box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.3);
    }
  ","§",prefix));
end;


end.