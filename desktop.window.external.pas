unit desktop.window.external;

interface

uses 
  W3C.Dom, W3C.Html5,
  W3C.WebMessaging,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Time,
  System.Colors,

  Desktop.Control,
  Desktop.Window,

  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,
  SmartCL.Components,
  SmartCL.Controls.Elements,
  SmartCL.ControlWrapper,
  SmartCL.System;

type

  TWbExternalWindowOptions = set of (
    eoNoScrolling,
    eoAllowFocus,
    eoCheckFrameFocus,
    eoHandshake
    );

  TWbExternalWindow = class(TWbWindow)
  private
    FURL:     string;
    FFrame:   TW3IFrameHTMLElement;
    FOptions: TWbExternalWindowOptions;
    function  FrameHasFocus: boolean;
  protected
    procedure HandleMessageFromClient(Sender: TObject; &Message: variant);
    procedure HandleInputEnabled(Sender: TObject);
    function  QueryChildInThisBranch(Child: TW3TagContainer): boolean; override;
    procedure InitializeObject; override;
  public
    property  FrameObject: TW3IFrameHTMLElement read FFrame;
    property  RemoteURL: string read FURL;
    property  RemoteOptions: TWbExternalWindowOptions read FOptions;
    procedure OpenURL(const FullURL: string; const Options: TWbExternalWindowOptions);

    // Use this
    procedure OpenURLEx(const FullURL: string;
              const Options: TWbExternalWindowOptions; const CB: TStdCallback );

    procedure OpenURLEx2(const FullURL: string;
              const Options: TWbExternalWindowOptions; const Setup: TStdCallback; const CB: TStdCallback );

    procedure CloseUrl;
  end;


implementation


//#############################################################################
// TWbExternalWindow
//#############################################################################

procedure TWbExternalWindow.InitializeObject;
begin
  inherited;
  TransparentEvents := false;
  Content.OnInputEnabled := @HandleInputEnabled;
end;

procedure TWbExternalWindow.HandleMessageFromClient(Sender: TObject; &Message: variant);
begin
  writeln("Intercepted message from hosted application in window-handler");
  //writeln("A message from the client: " + TVariant.AsString(&Message));
end;

procedure TWbExternalWindow.HandleInputEnabled(Sender: TObject);
begin
  // Make sure either a child or we have focus
  var LFocus := TW3ControlTracker.GetFocusedControl();
  if LFocus <> nil then
  begin
    if not Content.QueryChildInThisBranch(TW3TagContainer(LFocus)) then
    begin
      // Examine the container using a element wrapper
      var LWrapper := TW3HtmlElement.create(Content.handle);

      // Do we have child elements?
      if LWrapper.Children.length > 0 then
      begin
        // OK, make the child element the focal-point
        var LChild := LWrapper.Wrap(LWrapper.Children.Items[0]);
        try
          case (LChild.handle.contentWindow) of
          true:   LChild.handle.contentWindow.focus();
          false:  LChild.Handle.setFocus();
          end;
        except
          on e: exception do;
        end;
        LChild := nil;
        LWrapper := nil;
      end else
        Content.SetFocus(); // Nope, ok content will have to do
    end;
  end;
end;

procedure TWbExternalWindow.OpenURL(const FullURL: string; const Options: TWbExternalWindowOptions);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FFrame <> nil then
      CloseURL();

    try
      FFrame := TW3IFrameHTMLElement.Create(Content);
      FFrame.Handshake := (eoHandshake in Options);
      FFrame.handle.style.width  := "100%";
      FFrame.handle.style.height := "100%";
      FFrame.Background.FromColor(clWhite);
      FFrame.Scrolling := not (eoNoScrolling in Options);
      FFrame.Src := FullURL;

    finally
      FURL := FullURL;
      FOptions := Options;
    end;
  end;
end;

procedure TWbExternalWindow.OpenURLEx2(const FullURL: string;
          const Options: TWbExternalWindowOptions;
          const Setup: TStdCallback;
          const CB: TStdCallback );
begin
  if not (csDestroying in ComponentState) then
  begin
    if FFrame <> nil then
      CloseURL();

    try
      FFrame := TW3IFrameHTMLElement.Create(Content);
    except
      on e: exception do
      begin
        if assigned(CB) then
          CB(false);
        exit;
      end;
    end;

    try
      FFrame.Handshake := (eoHandshake in Options);
      FFrame.handle.style.width  := "100%";
      FFrame.handle.style.height := "100%";
      FFrame.Background.FromColor(clWhite);
      FFrame.Scrolling := not (eoNoScrolling in Options);
    except
      on e: exception do
      begin
        showmessage("It crapped out!");
        // If this kicks in then the prototype is probably screwed
        // anyways. But lets try to release it
        try
          FFrame.free;
        except
          on e: exception do;
        end;

        // Screw it, flush the sucker
        FFrame := nil;

        // No sigar buddy!
        if assigned(CB) then
          CB(false);
      end;
    end;

    if assigned(Setup) then
      Setup(true);

    if assigned(CB) then
    begin
      FFrame.OnLoad := procedure (Sender: TObject)
      begin
        TW3Dispatch.Execute( procedure ()
        begin
          CB(true);
        end, 200);
      end;
    end;

    writeln("OK, about to load this shit!");

    FURL := FullURL;
    FOptions := Options;
    FFrame.Src := FullURL;
  end;
end;

procedure TWbExternalWindow.OpenURLEx(const FullURL: string;
          const Options: TWbExternalWindowOptions;
          const CB: TStdCallback );
begin
  if not (csDestroying in ComponentState) then
  begin
    if FFrame <> nil then
      CloseURL();

    FFrame := TW3IFrameHTMLElement.Create(Content);
    FFrame.Handshake := (eoHandshake in Options);
    FFrame.handle.style.width  := "100%";
    FFrame.handle.style.height := "100%";
    FFrame.Background.fromColor(clWhite);
    FFrame.Scrolling := not (eoNoScrolling in Options);

    // Wait for frame object to be created
    TW3Dispatch.WaitFor([FFrame],
    procedure ()
    begin
      FFrame.OnMessage := @HandleMessageFromClient;
      FFrame.OnLoad := procedure (Sender: TObject)
      begin
        if assigned(CB) then
          CB(true);
      end;

      FURL := FullURL;
      FOptions := Options;
      FFrame.Src := FullURL;
    end);

  end;
end;

function TWbExternalWindow.QueryChildInThisBranch(Child: TW3TagContainer): boolean;
begin
  result := inherited QueryChildInThisBranch(Child);
  if not result then
    result := FrameHasFocus();
end;

procedure TWbExternalWindow.CloseUrl;
begin
  if FFrame <> nil then
  begin
    try
      // release the frame no matter what
      try
        FFrame.free;
      except
        // mute any exceptions
        //on e: exception do;
      end;

    finally
      // Flush frame pointer & kill all content
      FFrame := nil;
      FURL := '';
      Content.InnerHTML :='';
      if not (csDestroying in ComponentState) then
        invalidate();
    end;

  end;
end;

function TWbExternalWindow.FrameHasFocus: boolean;
begin
  result := TW3TagObj.GetDOMFocusedElement() = FFrame.Handle;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #'

    .TWbExternalWindow {
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
    }

  ';
  StyleCode := StrReplace(StyleCode,"§",prefix);
  Sheet.Append(StyleCode);
end;

end.
