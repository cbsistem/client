unit desktop.filesystem.node;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Objects,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Device.Storage,
  System.Memory.Buffer,
  System.Widget,
  System.IOUtils,
  System.JSON,

  Desktop.Types,
  Desktop.Network.Connection,
  SmartCL.Net.WebSocket,

  ragnarok.messages.factory,
  ragnarok.messages.base,
  ragnarok.messages.callstack,
  ragnarok.messages.network,

  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

type

  // This is a storagedevice class that communicates with the
  // Ragnarock server's FileIO API. It allows our desktop to
  // unify how it deals with files and folder, regardless of how
  // those might actually exist server-side

  TW3RagnarockFileSystem = class(TW3StorageDevice)
  private
    FClient:  TWbNetworkClient;
    FPath:    string;
    procedure HandleNetworkError(Sender: TW3WebSocket);

  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;

  public
    procedure GetPath(CB: TW3DeviceGetPathCallback); override;
    procedure GetFileSize(Filename: string; CB: TW3DeviceGetFileSizeCallback); override;
    procedure FileExists(Filename: string; CB: TW3DeviceGetFileExistsCallback); override;
    procedure DirExists(FolderName: string; CB: TW3DeviceGetFileExistsCallback); override;

    procedure MakeDir(FolderName: string; Mode: TW3FilePermissionMask; CB: TW3DeviceMakeDirCallback); override;
    procedure RemoveDir(FolderName: string; CB: TW3DeviceRemoveDirCallback); override;
    procedure Examine(FolderPath: string; CB: TW3FileOperationExamineCallBack); override;

    procedure GetStorageObjectType(ObjName: string; CB: TW3DeviceObjTypeCallback); override;

    procedure ChDir(FolderPath: string; CB: TW3DeviceChDirCallback); override;
    procedure CdUp(CB: TW3DeviceCdUpCallback); override;

    procedure Load(Filename: string; TagValue: variant; CB: TW3DeviceLoadCallback); override;
    procedure Save(Filename: string; TagValue: variant; Data: TStream; CB: TW3DeviceSaveCallback); override;

    procedure Mount(Authentication: TW3DeviceAuthenticationData;
              CB: TW3StorageDeviceMountEvent); override;
    procedure UnMount(CB: TW3StorageDeviceUnMountEvent); override;
  end;


implementation

//############################################################################
// TW3RagnarockFileSystem
//############################################################################

procedure TW3RagnarockFileSystem.InitializeObject;
begin
  inherited;
  // Set the device options
  // 1. we require authentication for mounting
  // 2. this device is read only.. for now anyways
  SetDeviceOptions([doRequireLogin, doReadOnly], nil);

  FClient := TWbNetworkClient.Create;
  FClient.Connection.OnError := HandleNetworkError;
end;

procedure TW3RagnarockFileSystem.FinalizeObject;
begin
  FClient.free;
  inherited;
end;

procedure TW3RagnarockFileSystem.HandleNetworkError(Sender: TW3WebSocket);
begin
  writeln("Connection error:" + Sender.LastError);
end;

procedure TW3RagnarockFileSystem.Mount(Authentication: TW3DeviceAuthenticationData; CB: TW3StorageDeviceMountEvent);
begin
  if Active then
    UnMount(nil);

  // Initial path is always POSIX "home"
  FPath := '~/';

  if Authentication <> nil then
  begin
    // perform connect
    //var LHost := Format('ws://%s:%d', [Authentication.adHostName, Authentication.adHostPort]);
    var LHost := Authentication.adEndpoint;
    FClient.Connection.Connect(LHost, [],
      procedure (Socket: TW3WebSocket; Success: boolean)
      begin
        // Did we connect OK?
        if Success then
        begin
          // perform login
          FClient.Login(Authentication.adUserName, Authentication.adPassword,
          procedure (const Response: TQTXBaseMessage)
          begin

            var Success := TQTXServerMessage(Response).Code = 200;
            if Success then
              SetActive( Success );

            // Fire callback, if the responsecode is 200 then login went ok
            if assigned(CB) then
              CB(self, Success);
          end);
      end else
      if assigned(CB) then
        CB(self, Success);
    end );
  end;
end;

procedure TW3RagnarockFileSystem.UnMount(CB: TW3StorageDeviceUnMountEvent);
begin
  if Active then
  begin
    if FClient.Connection.Connected then
    begin
      FClient.Connection.Disconnect( procedure (Socket: TW3WebSocket; Success: boolean)
      begin
        if assigned(CB) then
          CB(Self, true);
      end);
    end else
    begin
      if assigned(CB) then
        CB(Self, false);
    end;
  end else
  begin
    if assigned(CB) then
      CB(Self, false);
  end;
end;

procedure TW3RagnarockFileSystem.GetPath(CB: TW3DeviceGetPathCallback);
begin
  if assigned(CB) then
    CB(self, FPath, Active);
end;

procedure TW3RagnarockFileSystem.GetFileSize(Filename: String; CB: TW3DeviceGetFileSizeCallback);
begin
  if Active then
  begin
    //
  end else
  if assigned(CB) then
    CB(Self, Filename, 0, false);
end;

procedure TW3RagnarockFileSystem.FileExists(Filename: String; CB: TW3DeviceGetFileExistsCallback);
begin
  if Active then
  begin
    //
  end else
  if assigned(CB) then
    CB(self, filename, false);
end;

procedure TW3RagnarockFileSystem.DirExists(FolderName: String; CB: TW3DeviceGetFileExistsCallback);
begin
  if Active then
  begin
    //
  end else
  if assigned(CB) then
    CB(self, FolderName, false);
end;

procedure TW3RagnarockFileSystem.MakeDir(FolderName: String; Mode: TW3FilePermissionMask; CB: TW3DeviceMakeDirCallback);
begin
end;

procedure TW3RagnarockFileSystem.RemoveDir(FolderName: String; CB: TW3DeviceRemoveDirCallback);
begin
end;

procedure TW3RagnarockFileSystem.Examine(FolderPath: String; CB: TW3FileOperationExamineCallBack);
{var
  LData:  TStrArray; }
begin
  ClearLastError();
  if Active then
  begin
    var LToExamine := 'dir ' + FolderPath;

    FClient.FileIODir(LToExamine, null,
      procedure (Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean)
      begin
        if not Success then
        begin
          SetLastError(FClient.LastError);
          if assigned(CB) then
            CB(self, FolderPath, Files, false);
          exit;
        end;

        {for var FileItem in Files.dlItems do
        begin
          LData.add(FileItem.diFileName);
        end;    }

        if assigned(CB) then
          CB(Self, FolderPath, Files, true);
      end);

  end else
  begin
    SetLastError('Filesystem not mounted error');
    if assigned(CB) then
    begin
      var Files: TNJFileItemList;
      Files.dlPath := '';
      CB(self, FolderPath, Files, false);
    end;
  end;
end;

procedure TW3RagnarockFileSystem.GetStorageObjectType(ObjName: String; CB: TW3DeviceObjTypeCallback);
begin
end;

procedure TW3RagnarockFileSystem.ChDir(FolderPath: String; CB: TW3DeviceChDirCallback);
begin
end;

procedure TW3RagnarockFileSystem.CdUp(CB: TW3DeviceCdUpCallback);
begin
end;

procedure TW3RagnarockFileSystem.Load(Filename: String; TagValue: variant; CB: TW3DeviceLoadCallback);
begin
  if ErrorOptions.AutoResetError then
    ClearLastError();

  if not Active then
  begin
    SetLastError('Filesystem not mounted error');
    if assigned(CB) then
      CB(self, TagValue, filename, nil, false);
    exit;
  end;

  FClient.FileIORead(Filename, TagValue,
    procedure (Sender: TObject; Filename: string; TagValue: variant; FileData: TBinaryData; Success: boolean)
    begin
      case Success of
      true:
        begin
          if assigned(CB) then
          CB(self, TagValue, Filename, FileData.ToStream(), true);
        end;
      false:
        begin
          SetLastError(FClient.LastError);
          if assigned(CB) then
          CB(self, TagValue, Filename, nil, false);
        end;
      end;
    end);

  (* exit;

  FClient.FileIO('read ' + FileName , procedure (const Response: TQTXBaseMessage)
  begin
    if (response is TQTXServerError) then
    begin
      SetLastError(TQTXServerError(response).Response);
      if assigned(CB) then
        CB(self, TagValue, Filename, nil, false);
      exit;
    end;

    if (response is TQTXFileIOResponse) then
    begin
      var packet := TQTXFileIOResponse(response);

      // Delegate data to caller
      if assigned(CB) then
        CB(self, TagValue, filename, Packet.Attachment.ToStream(), true);
    end else
    begin
      SetLastErrorF('Unexpected IO reply (class %s): %s', [response.classname, TQTXServerMessage(response).response]);
      if assigned(CB) then
        CB(self, TagValue, Filename, nil, false);
      exit;
    end;
  end);  *)


end;

procedure TW3RagnarockFileSystem.Save(Filename: String; TagValue: variant; Data: TStream; CB: TW3DeviceSaveCallback);
begin
end;


end.
