unit desktop.icondock;

interface

uses
  W3C.DOM,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Colors,
  System.Time,
  System.Inifile,
  System.Memory.Buffer,
  System.Device.Storage,
  System.Streams,

  desktop.types,
  desktop.control,
  desktop.icons,
  desktop.network.connection,
  desktop.filesystem.node,

  SmartCL.System,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,
  SmartCL.Fonts,
  SmartCL.Borders,
  SmartCL.Theme,
  //SmartCL.Net.WebSocket,


  SmartCL.Controls.Image,
  SmartCL.Controls.Label,
  SmartCL.Controls.Button
  //SmartCL.Controls.Toolbar,
  //SmartCL.Effects
  ;

const
  CNT_ICONDOCK_FILENAME = '~/Prefs/settings.icondoc.ini';

type

  TWbDesktopIconDockItem = class(TWbCustomControl)
  private
    FTitle:   TW3Label;
  protected
    function  AcceptOwner(const CandidateObject: TObject): boolean; override;
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
  public
    property  Title: TW3Label read FTitle;
    property  Path: string;
    property  IconId: string;
  end;

  TWbDesktopIconDockGraphic = class(TWbDesktopIconDockItem)
  private
    FGlyph:   TW3Image;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
  public
    property  Glyph: TW3Image read FGlyph;
    property  GlyphFileName: string;
  end;

  TWbIconDockOrientation = (
    doHorizontal,
    doVertical
    );

  // Event declarations
  TWbDesktopIconDockOrientationEvent = procedure (Sender: TObject; Orientation: TWbIconDockOrientation);
  TWbDesktopIconDockItemClickedEvent = procedure (Sender: TObject; Item: TWbDesktopIconDockItem);

  TWBDesktopIconDock = class(TWbCustomControl)
  private
    FOrientation: TWbIconDockOrientation = doVertical;
    FSpacing: integer = 4;
    FSize:    integer = 100;
    procedure SetOrientation(const Value: TWbIconDockOrientation);
    procedure SetSpacing(const NewSpacing: integer);
    procedure SetDockSize(const NewSize: integer);

    procedure HandleItemClicked(Sender: TObject);
  protected
    procedure Resize; override;
  public
    property  Spacing: integer read FSpacing write SetSpacing;
    property  Orientation: TWbIconDockOrientation read FOrientation write SetOrientation;
    property  Size: integer read FSize write SetDockSize;

    procedure LoadPreferences(const IniFile: TIniFile; const CB: TStdCallback); overload;
    procedure LoadPreferences(const CB: TStdCallback); overload;

    function  AddGraphic(ImageFileName: string; Caption: string): TWbDesktopIconDockGraphic;
    procedure Clear;

    property  OnOrientationChanged: TWbDesktopIconDockOrientationEvent;
    property  OnItemClicked: TWbDesktopIconDockItemClickedEvent;
  end;

implementation

uses
  SmartCL.FileUtils;

//############################################################################
// TWBDesktopIconDock
//############################################################################

procedure TWBDesktopIconDock.SetDockSize(const NewSize: integer);
begin
  if NewSize <> FSize then
  begin
    var LTemp := TInteger.EnsureRange(NewSize, 32, MAX_INT);
    if not (csCreating in ComponentState) then
    begin
      if not (csDestroying in ComponentState) then
      begin
        FSize := LTemp;
        Invalidate();
      end;
    end else
    FSize := LTemp;
  end;
end;

procedure TWBDesktopIconDock.SetSpacing(const NewSpacing: integer);
begin
  if NewSpacing <> FSpacing then
  begin
    var LTemp := TInteger.EnsureRange(NewSpacing, 2, 22);
    if not (csCreating in ComponentState) then
    begin
      if not (csDestroying in ComponentState) then
      begin
        FSpacing := LTemp;
        Invalidate();
      end;
    end else
    FSpacing := LTemp;
  end;
end;

procedure TWBDesktopIconDock.SetOrientation(const Value: TWbIconDockOrientation);
begin
  if Value <> FOrientation then
  begin
    if not (csCreating in ComponentState) then
    begin
      if not (csDestroying in ComponentState) then
      begin
        FOrientation := Value;

        if assigned(OnOrientationChanged) then
          OnOrientationChanged(self, Value);

        Invalidate();
      end;
    end else
    FOrientation := Value;
  end;
end;

procedure TWBDesktopIconDock.Resize;
begin
  inherited;

  if (csReady in ComponentState) then
  begin
    var LCount := GetChildCount();
    if LCount > 0 then
    begin
      case FOrientation of
      doVertical:
        begin
          var dx := 4;
          var dy := FSpacing;
          var wd := clientwidth - (dx * 2);
          var hd := clientwidth - (dx * 2) - (clientwidth div 4);
          for var x :=0 to LCount-1 do
          begin
            var LChild := TWbDesktopIconDockItem( GetChildObject(x) );
            LChild.SetBounds(dx, dy, wd, hd);
            inc(dy, LChild.Height);
            inc(dy, FSpacing);
          end;
        end;
      doHorizontal:
        begin
          var dx := FSpacing;
          var dy := 4;
          var wd := clientHeight - (dx * 2);
          var hd := clientheight - (dx * 2) - (clientHeight div 4);
          for var x :=0 to LCount-1 do
          begin
            var LChild := TWbDesktopIconDockItem( GetChildObject(x) );
            LChild.SetBounds(dx, dy, wd, hd);
            inc(dx, LChild.Width);
            inc(dx, FSpacing);
          end;
        end;
      end;
    end;
  end;
end;

procedure TWBDesktopIconDock.HandleItemClicked(Sender: TObject);
begin
  var LObj := TWbDesktopIconDockGraphic(sender);
  var LDock := TWBDesktopIconDock( LObj.Parent );
  if assigned(LDock.OnItemClicked) then
    LDock.OnItemClicked(LDock, LObj);
end;

procedure TWBDesktopIconDock.LoadPreferences(const IniFile: TIniFile; const CB: TStdCallback);

  procedure __Failed(Text: string);
  begin
    writeln(Text);
    if assigned(CB) then
      CB(false);
  end;

begin

  // Make sure instance is valid
  if IniFile = nil then
  begin
    writeln('Failed to load preferences, inifile was nil error');
    if assigned(CB) then
      CB(false);
    exit;
  end;

  // Make sure inifile has content
  if IniFile.Empty then
  begin
    writeln('Failed to load preferences, inifile was empty error');
    if assigned(CB) then
      CB(false);
    exit;
  end;

  // Get a reference to the icon manager
  var LIconManager := IconManager();
  if LIconManager = nil then
  begin
    writeln('Failed to load preferences, icon manager not initialized error');
    if assigned(CB) then
      CB(false);
    exit;
  end;

  try
    for var LCategory in IniFile.GetCategories() do
    begin
      if LCategory.ToLower() = 'general' then
      begin
        // Read and set the orientation, default to vertical
        var LOrient := IniFile.ReadString(LCategory,'orientation', 'vertical').trim();
        case LOrient.ToLower() of
        'horizontal': SetOrientation(TWbIconDockOrientation.doHorizontal);
        else          SetOrientation(TWbIconDockOrientation.doVertical);
        end;

        FSize := IniFile.ReadInteger(LCategory, 'size', 100);

      end else
      begin
        var LTitle := IniFile.ReadString(LCategory,'title', '').trim();
        if LTitle.Length > 0 then
        begin
          var LPath := Inifile.ReadString(LCategory, 'path', '').trim();
          if LPath.length > 0 then
          begin
            var LIconFileName := IniFile.ReadString(LCategory, 'IconFilename', '').Trim();
            if LIconFileName.length > 0 then
            begin
              var LButton := AddGraphic(LIconFileName, LTitle);
              LButton.Path := LPath;
              LButton.GlyphFileName := LIconFileName;
              LButton.OnClick := @HandleItemClicked;
            end;
          end;
        end;

      end;
    end;
  finally
    if assigned(CB) then
      CB(true);
  end;
end;

procedure TWBDesktopIconDock.LoadPreferences(const CB: TStdCallback);

  procedure __Failed(Text: string);
  begin
    writeln(Text);
    if assigned(CB) then
      CB(false);
  end;

begin
  Clear();

  // Setup always visible preferences dock-item
  var LPrefs := AddGraphic('~/Prefs/Themes/default/sysicons/Preferences.png', 'Preferences');
  LPrefs.Path := 'client::preferences';
  LPrefs.OnClick := @HandleItemClicked;

  var LAccess := GetDesktop();
  if LAccess <> nil then
  begin
    var LNetwork := LAccess.GetWorkbenchChannel();
    if LNetwork <> nil then
    begin

      LNetwork.FileIORead(CNT_ICONDOCK_FILENAME, null,
        procedure (Sender: TObject; Filename: string; TagValue: variant; FileData: TBinaryData; Success: boolean)
        begin
          if not Success then
          begin
            __failed("Could not load file [" + Filename + "] error");
            exit;
          end;

          try
            var LIniFile := TIniFile.Create();
            try
              LIniFile.LoadFromStream(FileData.ToStream());
              LoadPreferences(LIniFile,
                procedure (Success: boolean)
                begin
                  if assigned(CB) then
                    CB(Success);
                end);
            finally
              LIniFile.free;
            end;
          except
            on e: exception do
              __failed(e.message);
          end;

        end);

    end else
    __Failed("Failed to access workbench network channel");
  end else
  __Failed("Failed to access workbench interface");
end;

procedure TWBDesktopIconDock.Clear;
begin
  if not (csDestroying in ComponentState) then
    FreeChildren();
end;

function TWBDesktopIconDock.AddGraphic(ImageFileName: string;
         Caption: string): TWbDesktopIconDockGraphic;
begin
  if not (csDestroying in ComponentState) then
  begin
    // DO-NOT-CHANGE
    //TW3Storage.BatchLoadImages([ImageUrl, ImageSelectedUrl], procedure ()
    //begin
    //end);

    result := TWbDesktopIconDockGraphic.Create(self);
    result.Title.Caption := Caption;
    result.GlyphFilename := ImageFileName;
    //result.NormalImage := ImageURL;
    //result.SelectedImage := ImageSelectedURL;
    //result.Glyph.LoadFromURL(ImageURL);
    result.width := ClientWidth;
    result.height:= ClientWidth;
    Invalidate();

    IconManager.LoadGraphic(ImageFilename, result,
      procedure (Success: boolean; const TagValue: variant; ImageData: string)
      begin
        if Success then
        begin
          var LTarget := TWbDesktopIconDockGraphic( TVariant.AsObject(TagValue) );
          LTarget.glyph.LoadFromUrl(ImageData);
        end;
      end);

  end;
end;

//############################################################################
// TWbDesktopIconDockGraphic
//############################################################################

procedure TWbDesktopIconDockGraphic.InitializeObject;
begin
  inherited;
  FGlyph := TW3Image.Create(self);
  FGlyph.ImageFit := fsContain;
  StyleClass :='TWbDesktopIconDockItem';
end;

procedure TWbDesktopIconDockGraphic.FinalizeObject;
begin
  FGlyph.free;
  inherited;
end;

procedure TWbDesktopIconDockGraphic.Resize;
begin
  inherited;
  FGlyph.SetBounds(0, 0, Clientwidth, clientHeight - Title.height);
end;

//############################################################################
// TWbDesktopIconDockItem
//############################################################################

procedure TWbDesktopIconDockItem.InitializeObject;
begin
  inherited;
  FTitle := TW3Label.Create(self);
  FTitle.Font.Color := clWhite;
  FTitle.Font.Name :='Ubuntu';
  FTitle.Font.size := 14;
  FTitle.AlignText := taCenter;
  FTitle.VAlign := tvCenter;
  FTitle.TextShadow.Shadow(1, 1, 2, clBlack);
end;

procedure TWbDesktopIconDockItem.FinalizeObject;
begin
  FTitle.free;
  inherited;
end;

function TWbDesktopIconDockItem.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := assigned(CandidateObject) and (CandidateObject is TWBDesktopIconDock);
end;

procedure TWbDesktopIconDockItem.Resize;
begin
  inherited;
  FTitle.SetBounds(0, ClientHeight-18, ClientWidth, 18);
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := StrReplace(
  #"

    .TWBDesktopIconDock {
      margin: 0px !important;
      padding: 2px;
      cursor: default;
      background-color: #EFECE9;
      border-bottom: 1px solid #979797;
    }

    .TWbDesktopIconDockItem  {
      cursor: default;
      padding: 0px;

      border-top: 1px solid #E0E0E0;
      border-left: 1px solid #E0E0E0;
      border-bottom: 1px solid #979797;
      border-right: 1px solid #979797;

      outline: 1px solid #42415A;
      outline-offset: 0px;
      outline-bottom: 1px solid #000;

      background-image: -ms-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
      background-image: -moz-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
      background-image: -o-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
      background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
      background-image: -webkit-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
      background-image: linear-gradient(to bottom, #B0B0B0 0%, #D7D7D7 100%);

      font-size: normal;
      text-align: center;
      -webkit-text-size-adjust: auto;
         -moz-text-size-adjust: auto;
          -ms-text-size-adjust: auto;
           -o-text-size-adjust: auto;
    }

    .TWbDesktopIconDockItem:active {
      border-top: 1px solid #979797;
      border-left: 1px solid #979797;
      border-bottom: 1px solid #E0E0E0;
      border-right: 1px solid #E0E0E0;

      background-image: -ms-linear-gradient(bottom, #B0B0B0 0%, #D7D7D7 100%);
      background-image: -moz-linear-gradient(bottom, #B0B0B0 0%, #D7D7D7 100%);
      background-image: -o-linear-gradient(bottom, #B0B0B0 0%, #D7D7D7 100%);
      background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
      background-image: -webkit-linear-gradient(bottom, #B0B0B0 0%, #D7D7D7 100%);
      background-image: linear-gradient(to top, #B0B0B0 0%, #D7D7D7 100%);
    }

    .TWbDesktopIconDockItem:focus {
      color: #618ECE;
     }


    .TWbDesktopIconDockItemSelected {
      background-image: -ms-linear-gradient(top, #A8A8A8 60%, #EDEDED 100%);
      background-image: -moz-linear-gradient(top, #A8A8A8 60%, #EDEDED 100%);
      background-image: -o-linear-gradient(top, #A8A8A8 60%, #EDEDED 100%);
      background-image: -webkit-gradient(linear, left top, left bottom, color-stop(60, #A8A8A8), color-stop(100, #EDEDED));
      background-image: -webkit-linear-gradient(top, #A8A8A8 60%, #EDEDED 100%);
      background-image: linear-gradient(to bottom, #A8A8A8 60%, #EDEDED 100%);
    }

  ","§",prefix);
  Sheet.Append(StyleCode);
end;


end.
