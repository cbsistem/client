unit desktop.window.folder;

interface

uses
  W3C.DOM,
  W3C.TypedArray,
  System.JSON,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Colors,
  System.Time,
  System.Widget,
  System.IOUtils,
  System.Streams,
  System.Memory.Buffer,

  desktop.types,
  desktop.window,
  desktop.iconview,
  desktop.iconinfo,
  desktop.panel,
  desktop.edit,
  desktop.button,
  desktop.toolbar,
  desktop.scrollbar,
  desktop.icons,
  desktop.network.connection,

  desktop.control.pathpanel,
  desktop.control.toolbarpanel,
  desktop.window.picture,
  desktop.window.external,

  SmartCL.Scroll,

  SmartCL.Time,
  SmartCL.System,
  SmartCL.Layout,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Application,
  SmartCL.Effects,
  SmartCL.Fonts,
  SmartCL.CSS.StyleSheet,
  SmartCL.Controls.Image,

  System.Device.Storage,
  Desktop.FileSystem.Node
  ;

type

  TWbWindowDirectory = class(TWbWindow)
  private
    FTool:        TWbToolbarPanel;
    FView:        TWbIconView;
    FCurrentPath: string;
    FUpdateTimer: TW3Timer;
    FLayout:      TLayout;
    FBtnBack:     TWbToolbarButton;
    FBtnCut:      TWbToolbarButton;
    FBtnCopy:     TWbToolbarButton;
    FBtnPaste:    TWbToolbarButton;
    procedure UpdateButtonStates(Sender: TObject);
  protected
    procedure WindowStateChanged(const NewState: TWbWindowState); override;

    procedure HandleDirectoryAvailable(Sender: TW3StorageDevice;
              Path: string; DirList: TNJFileItemList; Success: boolean);

    procedure HandleFolderSelect(Sender: TObject);
    procedure HandleFolderUnSelected(Sender: TObject);
    procedure HandleFolderDblClick(Sender: TObject);

    procedure HandleFileSelect(Sender: TObject);
    procedure HandleFileUnSelected(Sender: TObject);
    procedure HandleFileDblClick(Sender: TObject);

    procedure HandleItemDblClick(Sender: TObject; Item: TWbListItem);
    procedure HandleBtnBackClick(Sender: TObject);

    procedure OpenExternal(const Info: TWbLinkInfo);


    procedure SetCurrentPath(DevicePath: string);
    procedure MuteScrollBar(Scrollbar: TWBCustomScrollbar);
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  Path: TWbPathPanel read (FTool.PathPanel);
    property  Device: TW3StorageDevice;
    procedure BrowseTo(DevicePath: string);

    procedure UpdateScrollbar(const FullUpdate: boolean);
  end;


implementation


//#############################################################################
// TWbWindowContent
//#############################################################################

procedure TWbWindowDirectory.InitializeObject;
begin
  inherited;
  // We want the window to be sizable, and we want scrollbars
  SetOptions([woSizeable, woVScroll, woAutoInitSize]);
  MinimumWidth := 380;
  MinimumHeight := 300;
  Height := 400;

  FTool := TWbToolbarPanel.Create(Content);
  FView := TWbIconView.Create(Content);

  FView.ScrollBars := sbNone;

  FView.OnItemDblClick := @HandleItemDblClick;
  FView.ScrollController.OnScrolling := procedure (Sender: TObject)
  begin
    if VerticalScroll.Enabled then
    begin
      if not VerticalScroll.Updating then
      begin
        VerticalScroll.Position := abs(FView.ScrollController.ContentTop);
      end;
    end;
  end;

  VerticalScroll.OnPositionChanged := procedure (Sender: TObject)
  begin
    var YPos := VerticalScroll.Position;

    VerticalScroll.BeginUpdate;
    FView.ScrollController.ScrollTo(0, -YPos);
    VerticalScroll.EndUpdate;
  end;

  FUpdateTimer := TW3Timer.Create(nil);
  FUpdateTimer.Delay := 500;
  FUpdateTimer.OnTime := UpdateButtonStates;
end;

procedure TWbWindowDirectory.MuteScrollBar(Scrollbar: TWBCustomScrollbar);
begin
  if Scrollbar <> nil then
  begin
    Scrollbar.BeginUpdate;
    Scrollbar.Position := 0;
    Scrollbar.Total := 10;
    Scrollbar.PageSize := 10;
    Scrollbar.Enabled := false;
    Scrollbar.EndUpdate;
  end;
end;

procedure TWbWindowDirectory.WindowStateChanged(const NewState: TWbWindowState);
begin
  if not (csDestroying in ComponentState) then
  begin
    FView.LayoutChildren();
    Resize();
  end;
end;

procedure TWbWindowDirectory.UpdateScrollbar(const FullUpdate: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      if FView <> nil then
      begin
        if not (csDestroying in FView.ComponentState) then
        begin
          if not FView.Updating then
          begin

            case FullUpdate of
            true:
              begin
                var LTotalHeight := FView.Content.Height;
                var LPageHeight := FView.Container.Height;
                var LPos := abs(FView.ScrollController.ContentTop);

                if LTotalHeight <= LPageHeight then
                begin
                  if VerticalScroll.Enabled then
                    MuteScrollBar(VerticalScroll);
                  exit;
                end;

                VerticalScroll.BeginUpdate;
                VerticalScroll.Enabled := true;
                VerticalScroll.Total := LTotalHeight;
                VerticalScroll.PageSize := LPageHeight;
                VerticalScroll.Position := LPos;
                VerticalScroll.EndUpdate;
              end;
            false:
              begin

                var LTotalHeight := FView.Content.Height;
                var LPageHeight := FView.Container.Height;
                var LPos := abs(FView.ScrollController.ContentTop);

                if LTotalHeight <= LPageHeight then
                begin
                  if VerticalScroll.Enabled then
                    MuteScrollBar(VerticalScroll);
                  exit;
                end;

                VerticalScroll.BeginUpdate;
                VerticalScroll.Position := LPos;
                VerticalScroll.EndUpdate;

              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TWbWindowDirectory.FinalizeObject;
begin
  FUpdateTimer.Enabled := false;
  FUpdateTimer.OnTime := nil;
  FUpdateTimer.free;

  FView.free;
  FTool.free;
  inherited;
end;

procedure TWbWindowDirectory.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([FTool, FTool.Toolbar],
  procedure ()
  begin
    FTool.Toolbar.ButtonWidth := 80;
    FTool.Toolbar.ButtonHeight := 32;

    FLayout := Layout.Client(
    [
      Layout.Top(Layout.Height(76), FTool),
      Layout.Client(layout.stretch, FView)
    ]);

    FBtnBack := FTool.Toolbar.Add();
    FBtnBack.Caption := "Parent";
    FBtnBack.Layout := glGlyphLeft;
    FBtnBack.Glyph.LoadFromURL('res/parent.png');
    FBtnBack.OnClick := @HandleBtnBackClick;

    FBtnCut := FTool.Toolbar.Add();
    FBtnCut.Layout := glGlyphLeft;
    FBtnCut.Glyph.LoadFromURL('res/cut.png');
    FBtnCut.Caption := "Cut";

    FBtnCopy := FTool.Toolbar.Add();
    FBtnCopy.Layout := glGlyphLeft;
    FBtnCopy.Glyph.LoadFromURL('res/copy.png');
    FBtnCopy.Caption := "Copy";

    FBtnPaste := FTool.Toolbar.Add();
    FBtnPaste.Layout := glGlyphLeft;
    FBtnPaste.Glyph.LoadFromURL('res/paste.png');
    FBtnPaste.Caption := "Paste";

    VerticalScroll.OnChanged := procedure (Sender: TObject)
    begin
      if not (csUpdating in VerticalScroll.ComponentState) then
        FView.ScrollInfo.ScrollTo(0,VerticalScroll.Position);
    end;

    TW3Dispatch.Execute( procedure ()
    begin
      FUpdateTimer.Enabled := true;
    end , 200);

    TW3Dispatch.Execute(Invalidate, 100);
  end);

  {HorizontalScroll.OnChanged := procedure (Sender: TObject)
  begin
    if not (csUpdating in VerticalScroll.ComponentState) then
      FView.ScrollInfo.ScrollTo(HorizontalScroll.Position, VerticalScroll.Position);
  end;  }


end;

procedure TWbWindowDirectory.HandleBtnBackClick(Sender: TObject);
begin
  var LTemp := FCurrentPath;
  if LTemp.endswith('/') then
    LTemp := Copy(LTemp, 1, LTemp.Length-1);

  LTemp := TPath.GetDirectoryName(LTemp);
  writeln("Trying to browse to:" + LTemp);
  BrowseTo(LTemp);
end;

procedure TWbWindowDirectory.UpdateButtonStates(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      // Update state for back [parent] button
      FBtnBack.Enabled := not (FCurrentPath.trim() in ['', '~/']);

      // Update state for Cut button
      FBtnCut.Enabled := FView.SelectedItem <> nil;

      // Update state for Copy button
      FBtnCopy.Enabled := FView.SelectedItem <> nil;

      //FBtnCopy.Enabled := false;
      FBtnPaste.Enabled := false;
    end;
  end else
  FUpdateTimer.Enabled := false;
end;

procedure TWbWindowDirectory.HandleFolderSelect(Sender: TObject);
begin
  //TWbListItemIcon(Sender).Background.FromURL(IconManager.GetSelected(CNT_ICONID_FOLDER));
end;

procedure TWbWindowDirectory.HandleFolderUnSelected(Sender: TObject);
begin
  //TWbListItemIcon(Sender).Background.FromURL(IconManager.GetUnSelected(CNT_ICONID_FOLDER));
end;

procedure TWbWindowDirectory.HandleFileSelect(Sender: TObject);
begin
end;

procedure TWbWindowDirectory.HandleFileUnSelected(Sender: TObject);
begin
end;

procedure TWbWindowDirectory.HandleItemDblClick(Sender: TObject; Item: TWbListItem);
begin
  if Item <> nil then
  begin
    if (item is TWbListItemFolder) then
      HandleFolderDblClick(Item)
    else
      HandleFileDblClick(Item);
  end;
end;

procedure TWbWindowDirectory.HandleFolderDblClick(Sender: TObject);
begin
  var LParser := GetDirectoryParser();
  var LName := TWbListItemIcon(Sender).Text.Caption.trim();
  var LPath := LParser.IncludeTrailingPathDelimiter(FCurrentPath) + LName;

  BrowseTo(LPath);
end;

procedure TWbWindowDirectory.OpenExternal(const Info: TWbLinkInfo);
begin
  var LAccess := GetDesktop();

  var LGame := TWbExternalWindow.Create( LAccess.GetWindowHost() );

  if Info.Container.Resize then
  begin
    LGame.Options := [woSizeable];
  end else
  begin
    LGame.Header.MinimizeGlyph.Visible := false;
  end;

  LGame.header.FullScreen.Visible := Info.Container.FullScreen;

  // Wait for the window to finish construction
  TW3Dispatch.WaitFor([LGame, LGame.Header, LGame.LeftEdge, LGame.RightEdge, LGame.Content],
  procedure ()
  var
    LLoadOptions: TWbExternalWindowOptions;
  begin
    var Access := GetDesktop();
    if Access <> nil then
    begin
      var LTitle := UnEscapeText( TString.DecodeBase64(Info.Container.Title) );

      LGame.Header.Title.Caption := LTitle;
      LGame.SetBounds(200, 80, Info.Container.Width, Info.Container.Height);
      LGame.Constraints.MinWidth := Info.Container.Width;
      LGame.Constraints.MinHeight := Info.Container.Height;
      LGame.Constraints.Enabled := true;
      LGame.CenterOnParent();

      Access.SetFocusedWindow(LGame);

      LLoadOptions :=  [eoNoScrolling, eoAllowFocus, eoCheckFrameFocus];
      if Info.Container.HandShake then
        include(LLoadOptions, eoHandshake);

      var LUrlOfContent := TString.DecodeBase64(Info.Action.Source);

      LGame.OpenURLEx(LUrlOfContent, LLoadOptions,
      procedure (Success: boolean)
      begin
        if Success then
        begin
          LGame.FrameObject.SetFocus();
          LGame.FrameObject.Handle.focus();
        end else
          raise EW3Exception.CreateFmt('Failed to open url [%s] error', [LUrlOfContent]);
      end);

      Info.free;

    end else
      raise EW3Exception.Create('Failed to access desktop interface error');
  end);
end;

procedure TWbWindowDirectory.HandleFileDblClick(Sender: TObject);
begin
  var LIcon := TWbListItemIcon(Sender);
  var LNewPath := string( LIcon.Data );
  var LFileName := LIcon.Text.Caption.trim();

  if not LNewPath.EndsWith('/') then
    LNewPath += '/' + LFileName
  else
    LNewPath += LFileName;

  if TPath.GetExtension(LFilename).ToLower() = '.lnk' then
  begin
    var LAccess := GetDesktop();
    var LChannel := LAccess.GetWorkbenchChannel();

    LChannel.FileIORead(LNewPath, LIcon,
      procedure (Sender: TObject; Filename: string; TagValue: variant; FileData: TBinaryData; Success: boolean)
      begin
        if not Success then
          exit;


        var codec := JTextDecoder.Create("utf8");

        { var LTemp := FileData.ToTypedArray();
        var LBuff := LTemp.slice(0, LTemp.length-1); }
        //var LText := codec.decode( JUint8Array(LBuff) );
        var LText := StripZeroBytes( codec.decode( JUint8Array(FileData.ToTypedArray) ) );
        codec := nil;




        var LInfo := TWbLinkInfo.Create();
        try
          LInfo.FromJSON(LText);
        except
          on e: exception do
          begin
            LInfo.free;
            writeln("Failed to open link:" + e.message);
            exit;
          end;
        end;

        OpenExternal(LInfo);

      end);
    exit;
  end;

  GetDesktop().SetGlobalCursor(PID_DESKTOP, TCursor.crWait);
  Device.Load(LNewPath, nil,
  procedure (Sender: TW3StorageDevice; TagValue: variant; Filename: string; Data: TStream; Success: boolean)
  begin
    GetDesktop().SetGlobalCursor(PID_DESKTOP, TCursor.crInherited);

    if Success then
    begin

      var LAcess := GetDesktop();

      var LWindow := TWbImageViewer.Create(LAcess.GetWindowHost());
      LWindow.SetBounds(100, 100, 500, 420);


      LAcess.SetFocusedWindow(LWindow);
      LWindow.LoadImageFromStream(Filename, Data);

    end else
      showmessage(Device.LastError);
  end);
end;

procedure TWbWindowDirectory.HandleDirectoryAvailable(Sender: TW3StorageDevice; Path: string;
    DirList: TNJFileItemList; Success: boolean);
var
  LIcon:  TWbListItemIcon;
  LCache: array of TWbListItemIcon;

  function  GetFileHasInfo(QFilename: string): boolean;
  begin
    var temp := TPath.ChangeFileExt(QFilename, '.info');
    temp := temp.ToLower();
    for var cc in Dirlist.dlItems do
    begin
      if cc.diFileName.ToLower() = temp then
      begin
        result := true;
        break;
      end;
    end;
  end;

begin
  if Success then
  begin
    // Examine was good, assign path to display
    SetCurrentPath(Path);

    FView.BeginUpdate();
    try
      self.MuteScrollbar(VerticalScroll);

      if DirList.dlItems.Count > 0 then
        FView.AddGroup("Listing " + Dirlist.dlItems.Count.ToString() + " files")
      else
        FView.AddGroup("No files");

      for var FileItem in Dirlist.dlItems do
      begin
        var LName := FileItem.diFileName;
        if LName = '' then
          continue;

        case FileItem.diFileType of
        wtFolder:
          begin
            LIcon := FView.AddFolder(LName);
            LIcon.Data := Path;

            IconManager.GetFolderGraphic(LIcon,
            procedure (Success: boolean; const TagValue: variant; ImgData: string)
            begin
              if Success then
              begin
                var LIcon := TWbListItemFolder( TVariant.AsObject(TagValue) );
                LIcon.Background.FromURL(ImgData);
              end;
            end);

            //LIcon.Background.FromURL(IconManager.GetUnSelected(CNT_ICONID_FOLDER));
            LIcon.OnSelected := @HandleFolderSelect;
            LIcon.OnUnSelected := @HandleFolderUnSelected;
          end;
        wtFile:
          begin
            // Info-files should be hidden by default
            if TPath.GetExtension(LName).ToLower() = '.info' then
              continue;

            LIcon := FView.AddIcon(LName);
            LIcon.Data := Path;

            if GetFileHasInfo(LName) then
            begin
              var LAccess := GetDesktop();
              var LChannel := LAccess.GetWorkbenchChannel();
              var LFileName := TPath.IncludeTrailingPathDelimiter(Path) + LName;
              LFileName := TPath.ChangeFileExt(LFilename, '.info');

              LIcon.SetSize(64, 80);
              LIcon.AdjustSizeToCaption();
              LCache.add(LIcon);

              LChannel.FileIORead(LFileName, LIcon,
                procedure (Sender: TObject; Filename: string; TagValue: variant; FileData: TBinaryData; Success: boolean)
                begin
                  var LIcon := TWbListItemIcon( TVariant.AsObject(TagValue) );

                  if not Success then
                  begin
                    IconManager.LoadDefaultIconForFile(LIcon.Text.Caption, LIcon,
                    procedure (Success: boolean; const TagValue: variant; ImgData: string)
                    begin
                      if Success then
                      begin
                        var LIcon := TWbListItemIcon( TVariant.AsObject(TagValue) );
                        LIcon.Background.FromURL(ImgData);
                      end;
                    end);
                  end;

                  var LFileData := FileData.ToBase64();
                  LIcon.Background.FromURL('data:image/png;base64,' + LFileData);
                end);
              continue;
            end;

            { if TPath.GetExtension(LName).ToLower() = '.lnk' then
            begin
              var LAccess := GetDesktop();
              var LChannel := LAccess.GetWorkbenchChannel();
              var LFileName := TPath.IncludeTrailingPathDelimiter(Path) + LName;

              LIcon.SetSize(64, 80);
              LIcon.AdjustSizeToCaption();
              LCache.add(LIcon);

              var LFileToLoad := TPath.ChangeFileExt(LFilename, '.info');
              LChannel.FileIORead(LFileToLoad, LIcon,
                procedure (Sender: TObject; Filename: string; TagValue: variant; FileData: TBinaryData; Success: boolean)
                begin
                  if not Success then
                    exit;

                  var LFileData := FileData.ToBase64();
                  LIcon.Background.FromURL('data:image/png;base64,' + LFileData);
                end);

              continue;
            end;  }

            IconManager.LoadDefaultIconForFile(LName, LIcon,
            procedure (Success: boolean; const TagValue: variant; ImgData: string)
            begin
              if Success then
              begin
                var LIcon := TWbListItemIcon( TVariant.AsObject(TagValue) );
                LIcon.Background.FromURL(ImgData);
              end;
            end);
            {var LExt := TPath.GetExtension(LName);
            var LIconId := '';

            if LExt.startsWith('.') then
            begin
              if LExt.length > 4 then
                SetLength(LExt, 4);

              case LExt.ToLower() of
              '.png': LIconId := CNT_ICONID_PNG;
              '.jpg': LIconId := CNT_ICONID_JPG;
              '.pas': LIconId := CNT_ICONID_PAS;
              end;
            end else
              LIconId := CNT_ICONID_FOLDER;
            LIcon.Background.FromURL(IconManager.GetUnSelected(LIconId));  }
          end;
        end;

        LIcon.SetSize(64, 80);
        LIcon.AdjustSizeToCaption();
        LCache.add(LIcon);
      end;
    finally
      TW3Dispatch.WaitFor(LCache, procedure ()
      begin
        FView.LayoutChildren();
        FView.EndUpdate();
        FView.Enabled := true;

        UpdateScrollbar(true);

        //Cursor := TCursor.crInherited;
        GetDesktop().SetGlobalCursor(PID_DESKTOP, TCursor.crInherited);
      end);
    end;

  end else
  begin
    //Cursor := TCursor.crInherited;
    GetDesktop().SetGlobalCursor(PID_DESKTOP, TCursor.crInherited);
    FView.enabled := true;
  end;

end;

procedure TWbWindowDirectory.SetCurrentPath(DevicePath: string);
begin
  DevicePath := DevicePath.trim();
  if DevicePath <> FCurrentPath then
  begin
    FCurrentPath := DevicePath;

    // Set window title
    if (DevicePath in ['/', '']) then
      Header.Title.Caption := 'Examining ' + Device.Name
    else
      Header.Title.Caption := 'Examining ' + Device.Name + ' [' + FCurrentPath + ']';

    // Set path in panel
    FTool.PathPanel.Edit.Text := FCurrentPath;
  end;
end;

procedure TWbWindowDirectory.BrowseTo(DevicePath: string);
begin
  // Device assigned?
  if Device = nil then
  begin
    showmessage("No device assigned, examination of <" + DevicePath +"> failed error");
    exit;
  end;

  // device mounted?
  if not Device.Active then
  begin
    Showmessage("Device is not mounted, examination of <" + DevicePath +"> failed error");
    exit;
  end;

  DevicePath := DevicePath.trim();

  // Shift window in "wait" mode
  //Cursor := TCursor.crWait;
  GetDesktop().SetGlobalCursor(PID_DESKTOP, TCursor.crWait);
  FView.enabled := false;
  FView.Clear();

  TW3Dispatch.Execute( procedure ()
  begin
    Device.Examine(DevicePath, @HandleDirectoryAvailable);
  end, 400);
end;

procedure TWbWindowDirectory.Resize;
begin
  inherited;

  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      if assigned(FLayout) then
      begin
        if Content <> nil then
        begin
          if VerticalScroll <> nil then
          begin

            try
              FLayout.Resize(content);
            except
              // Sink any exceptions
            end;

            UpdateScrollbar(true);
          end;
        end;
      end;
    end;
  end;
end;


end.
