unit desktop.control;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Device.Storage,

  SmartCL.Theme,
  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

type

  TWbCustomControl = class(TW3CustomControl)
  protected
    procedure StyleTagObject; override;
  end;

implementation


//#############################################################################
// TWbCustomControl
//#############################################################################

procedure TWbCustomControl.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;


end.
