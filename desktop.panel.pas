unit desktop.panel;

{$I 'Smart.inc'}

interface

uses
  W3C.DOM,
  System.Types,
  {$IFDEF THEME_AUTOSTYLE}
  SmartCL.Theme,
  {$ENDIF}

  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,

  SmartCL.System,
  SmartCL.Components,
  SmartCL.Controls.Panel;

type

  TWbPanel = class(TW3Panel)
  public
    procedure StyleTagObject; override;
  end;

implementation

procedure TWbPanel.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := StrReplace( //EFECE9
  #"
  .TWbPanel {
    padding: 0px !important;
    margin: 0px !important;

    background: #D6D6D6;
    border-style: solid;

    border-left-width: 1px;
    border-left-color: #FFFFFF;

    border-top-width: 1px;
    border-top-color: #FFFFFF;

    border-bottom-width: 1px;
    border-bottom-color: #93939A;

    border-right-width: 1px;
    border-right-color: #93939A;
  }

  ","§",prefix);
  Sheet.Append(StyleCode);
end;

end.
