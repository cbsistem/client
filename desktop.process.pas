unit desktop.process;

interface

uses 
  W3C.WebMessaging,

  System.Types,
  System.Colors,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.dictionaries,
  System.Device.Storage,

  System.Inifile,

  desktop.types,
  desktop.window,
  desktop.window.external,
  desktop.requesters,
  desktop.process.manifest,
  desktop.process.factory,
  desktop.iconinfo,

  ragnarok.JSON,
  ragnarok.messages.application,
  ragnarok.messages.factory,
  ragnarok.messages.base,
  ragnarok.messages.callstack,

  SmartCL.Net.WebSocket,
  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

type

  // Forward declarations
  TWbHostedProcess      = class;
  TWbServiceProcess     = class;
  TWbSoftKernel         = class;
  TWbProcessManager     = class;

  // Exception classes
  EProcess = class(EW3Exception);

  // custom types
  TWindowCollection     = array of TWbCustomWindow;
  TProcessCollection    = array of TWbHostedProcess;

  // Events and callback definitions
  TAPIEntryPoint = procedure (const Message: TQTXApplicationMessage);

  TWbSoftKernel = class(TObject)
  private
    // Reference to the process that created
    // this class instance. Each application must have their own
    // instance of the soft-kernel. We want to keep it simple.
    FProcess:   TWbHostedProcess;

    // If we have attached to the process, this value is TRUE
    FAttached:  boolean;

    // Lookup-table for entrypoints. Since messages contains the
    // method id, we just use a name-to-reference table
    FLookup:    Variant;

    // The first message to the kernel from an app, should
    // be SetApplicationManifest(). None of the other methods will
    // work until a valid manifest has been delivered. This
    // field will be true when a manifest has been delivered
    FManifestOK:  boolean = true;

    // If the manifest has been delivered, it has been loaded into
    // the Ini-file instance
    FManifest:  TIniFile;

    // Reference to Message factory for process.
    // Set once during the construction
    FFactory: TAmibianDesktopMessageFactory;

    procedure   _SetAppManifest(const Message: TQTXApplicationMessage);
    procedure   _SetWindowTitle(const Message: TQTXApplicationMessage);
    procedure   _GetWindowTitle(const Message: TQTXApplicationMessage);
    procedure   _SetGadgetAttrs(const Message: TQTXApplicationMessage);
    procedure   _GetDirList(const Message: TQTXApplicationMessage);
    procedure   _GetDeviceList(const Message: TQTXApplicationMessage);

    procedure   _ShowFileOpenRequester(const Message: TQTXApplicationMessage);

  protected
    procedure   ValidateManifest;
    procedure   SendError(const TicketId: string; const ErrorText: string);
    procedure   SendOK(const TicketId: string);

    procedure   Dispatch(Sender: TObject; &Message: variant);
    procedure   RegisterAPIMethod(Name: string; const Entrypoint: TAPIEntryPoint);
  public
    property    ManifestAvailable: boolean read FManifestOK;
    property    Manifest: TIniFile read FManifest;
    property    Attached: boolean read FAttached;
    property    Process: TWbHostedProcess read FProcess;
    procedure   AttachToProcess();
    procedure   DetachFromProcess();
    constructor Create(const Process: TWbHostedProcess); virtual;
    destructor  Destroy; override;
  end;

  TWbHostedProcess = class(TObject)
  private
    FId:        TPID;
    FWindows:   TWindowCollection;
    FManifest:  TWbHostedAppManifest;
    FKernel:    TWbSoftKernel;
  protected
    procedure   RegisterWindow(const WinInstance: TWbCustomWindow);
    procedure   UnRegisterWindow(const WinInstance: TWbCustomWindow);
    procedure   ForceCloseOpenWindows;
  public

    property    ManifestAvailable: boolean read (FKernel.ManifestAvailable);
    property    Manifest: TIniFile read (FKernel.Manifest);

    property    MessagePort: JMessageport;
    property    Kernel: TWbSoftKernel read FKernel;
    property    Id: TPID read FId;
    property    Window[index: integer]:TWbCustomWindow read (FWindows[index]);
    property    WindowCount: integer read (FWindows.Count);
    property    MainWindow: TWbCustomWindow;
    property    AppEndpoint: string;

    procedure   Terminate;

    function    GetAppManifest(var Manifest: TWbHostedAppManifest): boolean;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TWbServiceProcess = class(TWbHostedProcess)
  private
    FServiceSocket: TW3WebSocket;
  public
    property    ServiceEndpoint: string;

    constructor Create; override;
    destructor  Destroy; override;
  end;

  TWbCreateProcessCB = procedure (Success: boolean; Reason: string; Process: TWbHostedProcess);

  TWbProcessCreatedEvent = procedure (Sender: TObject; Process: TWbHostedProcess);
  TWbProcessDestroyedEvent = procedure (Sender: TObject; Process: TWbHostedProcess);

  TWbProcessManager = class(TObject)
  private
    FObjects:   TProcessCollection;
    FPLUT:      TW3ObjDictionary;
  protected
    procedure   HandleWindowClosing(Sender: TObject);
    procedure   ProcessEnds(const Process: TWbHostedProcess);
  public
    property    Process[index: integer]: TWbHostedProcess read (FObjects[index]);
    property    ProcessCount: integer read (FObjects.Count);

    function    GetProcessById(const ProcId: string): TWbHostedProcess;

    procedure   RegisterHostedProcess(AppEndPoint: string; WinOptions: TWbWindowOptions; const CB: TWbCreateProcessCB);
    procedure   RegisterHostedProcessEx(AppEndPoint: string; const CB: TWbCreateProcessCB);
    procedure   RegisterHostedProcessEx2(const Info: TWbLinkInfo; const CB: TWbCreateProcessCB);

    constructor Create; virtual;
    destructor  Destroy; override;
  public
    property  OnProcessCreated: TWbProcessCreatedEvent;
    property  OnProcessDestroyed: TWbProcessDestroyedEvent;
  end;

  function ProcessManager: TWbProcessManager;

implementation

uses
  Desktop.Types,
  Desktop.Window,
  Desktop.Window.External;

var
  __ProcessMan: TWbProcessManager;

function ProcessManager: TWbProcessManager;
begin
  if __ProcessMan = nil then
    __ProcessMan := TWbProcessManager.Create;
  result := __ProcessMan;
end;

//#############################################################################
// TWbProcessStartInfo
//#############################################################################

{ class function TWbProcessStartInfo.Create(Endpoint, title: string; initWidth, initHeight: integer; flags: TWbWindowOptions): TWbProcessStartInfo;
begin
  result.psEndpoint := Endpoint;
  result.psInitWidth := initWidth;
  result.psInitHeight := initHeight;
  result.psWinTitle := title;
  result.psWinFlags := flags;
  result.psHandshake := false;
end;       }

//#############################################################################
// TWbSoftKernel
//#############################################################################

constructor TWbSoftKernel.Create(const Process: TWbHostedProcess);
begin
  inherited Create;

  FManifest := TIniFile.Create();

  if Process <> nil then
    FProcess := Process
  else
    raise  EW3Exception.Create('Failed to create soft-kernel, process was nil error');

  FLookup := TVariant.CreateObject();
  FFactory := GetDesktopMessageFactory();

  RegisterAPIMethod('SetWindowTitle', @_SetWindowTitle);
  RegisterAPIMethod('GetWindowTitle', @_GetWindowTitle);
  RegisterAPIMethod('SetGadgetAttr', @_SetGadgetAttrs);
  RegisterAPIMethod('GetDirList', @_GetDirList);
  RegisterAPIMethod('GetDeviceList', @_GetDeviceList);
  RegisterAPIMethod('SetApplicationManifest', @_SetAppManifest);

  RegisterAPIMethod('ShowFileOpenRequester', @_ShowFileOpenRequester);
end;

destructor TWbSoftKernel.Destroy;
begin
  FLookup := nil;

  try
    try
      if FProcess <> nil then
        DetachFromProcess();
    except
      on e: exception do;
    end;
  finally
    FProcess := nil;
  end;

  FManifest.free;

  inherited;
end;

procedure TWbSoftKernel.AttachToProcess;
begin

  // Already attached? Detach
  if FAttached then
    DetachFromProcess();

  if FProcess <> nil then
  begin
    if FProcess.MainWindow <> nil then
    begin
      if FProcess.MainWindow <> nil then
      begin

        // Decouple message handling
        try
          TWbExternalWindow(FProcess.MainWindow).FrameObject.OnMessage := @Dispatch;
        except
          on e: exception do
          raise EW3Exception.CreateFmt('Failed to attach message-handler to process: %s', [e.message]);
        end;

        FAttached := true;

      end;
    end;
  end;

end;

procedure TWbSoftKernel.DetachFromProcess;
begin
  if FAttached then
  begin
    try
      if FProcess <> nil then
      begin
        if FProcess.MainWindow <> nil then
        begin
          if FProcess.MainWindow <> nil then
          begin

            // Decouple message handling
            try
              TWbExternalWindow(FProcess.MainWindow).FrameObject.OnMessage := nil;
            except
              on e: exception do;
            end;

          end;
        end;
      end;
    finally
      FAttached := false;
    end;
  end;
end;

procedure TWbSoftKernel.SendError(const TicketId: string; const ErrorText: string);
begin
  try
    var LResponse := TQTXDesktopError.Create(TicketID);
    try
      LResponse.Response := ErrorText;
      Process.MessagePort.postMessage( LResponse.Serialize() );
    finally
      LResponse.free;
    end;
    //writeln(ErrorText);
  except
    // sink exception, nothing we can do here
    on e: exception do;
  end;
end;

procedure TWbSoftKernel.Dispatch(Sender: TObject; Message: Variant);
var
  LMessage: TQTXBaseMessage;
begin
  writeln("Message from hosted app received on process channel");
  writeln(message);

  if (Message) then
  begin

    var LIdentifier := TQTXBaseMessage.QueryIdentifier(Message);
    writeln("Message-Identifier:" + LIdentifier);

    if FFactory.Build(LIdentifier, LMessage) then
    begin
      var LAppCast := TQTXApplicationMessage(LMessage);

      try
        LAppCast.Parse(Message);
      except
        on e: exception do
        begin
          // We need to issue a proper error message here
          writeln("Parsing failed:" + e.message);
          exit;
        end;
      end;

      try
        // lookup the methodname
        var LEntryName := LAppCast.&MethodName.Trim().ToLower();
        writeln("Message-MethodName:" + LEntryName);
        if FLookup.hasOwnProperty(LEntryName) then
        begin
          // Dispatch to entrypoint
          var LEntry: TAPIEntryPoint;

          try
            asm
            @LEntry = (@self.FLookup)[@LEntryName];
            end;
            LEntry(LAppCast);
          except
            on e: exception do
            begin
              //To-Do: Log error by sending it to the global logging service
              var LError := Format('Error: API method (%s) failed with: %s', [LAppCast.MethodName, e.message]);
              SendError(LMessage.Ticket, LError);
            end;
          end;
        end else
        begin
          //To-Do: Log error by sending it to the global logging service
          var LError := Format('Error: Invalid or unknown API method called (%s), operation failed', [LAppCast.MethodName]);
          SendError(LMessage.Ticket, LError);
        end;

      finally
        LMessage.free;
      end;
    end else
    SendError('', 'Error: Unknown message error');
  end else
  begin
    //To-Do: Log error by sending it to the global logging service
    SendError('', 'Error: An empty message was attempted invoked');
  end;
end;

procedure TWbSoftKernel.RegisterAPIMethod(Name: string; const Entrypoint: TAPIEntryPoint);
begin
  Name := Name.Trim().ToLower();
  if Name.length > 0 then
  begin
    if not FLookup.hasOwnProperty(name) then
    begin
      FLookup[Name] := @EntryPoint;
    end else
    raise EW3Exception.CreateFmt('Failed to register API method %s, entrypoint already exists', [name]);
  end else
  raise EW3Exception.Create('Failed to register API method, name was empty error');
end;

procedure TWbSoftKernel._GetWindowTitle(const Message: TQTXApplicationMessage);
begin
  // Check that a valid application-manifest has been set.
  // This must be in place before any API calls are handled
  if not FManifestOK then
  begin
    var LError := 'Error: No application manifest has been set';
    SendError(Message.Ticket, LError);
    exit;
  end;

  var LResponse := TQTXDesktopMessage.Create(Message.ticket);
  try
    LResponse.Response := Process.MainWindow.Header.Title.Caption;
    Process.MessagePort.postMessage(LResponse.Serialize());
  finally
    LResponse.free;
  end;
end;

procedure TWbSoftKernel._GetDeviceList(const Message: TQTXApplicationMessage);
begin
  // Check that a valid application-manifest has been set.
  // This must be in place before any API calls are handled
  if not FManifestOK then
  begin
    var LError := 'Error: No application manifest has been set';
    SendError(Message.Ticket, LError);
    exit;
  end;
end;

procedure TWbSoftKernel._GetDirList(const Message: TQTXApplicationMessage);
begin
  writeln("Entering GetDirList");

  // Check that a valid application-manifest has been set.
  // This must be in place before any API calls are handled
  if not FManifestOK then
  begin
    var LError := 'Error: No application manifest has been set';
    SendError(Message.Ticket, LError);
    exit;
  end;

  var LPath := TQTXApplicationGetDirMessage(Message).Path;
  var LAccess := GetDesktop();
  var LChannel := LAccess.GetWorkbenchChannel();

  LChannel.FileIODir('dir ' + LPath, Message.Ticket,
    procedure (Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean)
    begin
      case Success of
      true:
        begin
          var LResponse := TQTXDesktopDirResponse.Create(TagValue);
          try
            LResponse.Response := 'OK';
            LResponse.LoadDirListFromString(JSON.stringify(Files));
            Process.MessagePort.postMessage(LResponse.Serialize());
          finally
            LResponse.free;
          end;
        end;
      false:
        begin
          SendError(TagValue, 'Failed to obtain directory listing for:' + LPath);
        end;
      end;
    end);
end;

procedure TWbSoftKernel.SendOK(const TicketId: string);
begin
  var LResponse := TQTXResponseMessage.Create(TicketId);
  try
    LResponse.Response := 'OK';
    LResponse.Code := 200;
    Process.MessagePort.postMessage( LResponse.Serialize() );
  finally
    LResponse.free;
  end;
end;

procedure TWbSoftKernel._SetGadgetAttrs(const Message: TQTXApplicationMessage);
begin
  // Check that a valid application-manifest has been set.
  // This must be in place before any API calls are handled
  if not FManifestOK then
  begin
    var LError := 'Error: No application manifest has been set';
    SendError(Message.Ticket, LError);
    exit;
  end;

  var LCast := TQTXApplicationSetGadAttrReqMessage(Message);

  case LCast.Id.ToLower() of
  STD_GADID_VSCROLL:
    begin
     case LCast.Attr of
      STD_ATTR_SCROLL_TOTAL:
        begin
          var LBar := Process.MainWindow.RightEdge.Scrollbar;
          LBar.Total := LCast.Value;
        end;
      STD_ATTR_SCROLL_PAGESIZE:
        begin
          var LBar := Process.MainWindow.RightEdge.Scrollbar;
          LBar.PageSize := LCast.Value;
        end;
      STD_ATTR_SCROLL_VALUE:
        begin
          var LBar := Process.MainWindow.RightEdge.Scrollbar;
          LBar.Position := LCast.Value;
        end;
      STD_ATTR_GADGET_ENABLED:
        begin
          var LBar := Process.MainWindow.RightEdge.Scrollbar;
          LBar.Enabled := LCast.Value;
        end;
      STD_ATTR_GADGET_VISIBLE:
        begin
          var LOptions := process.MainWindow.Options;
          case boolean(LCast.Value) of
          true:   include(LOptions, woVScroll);
          false:  exclude(LOptions, woVScroll);
          end;
          Process.MainWindow.Options := LOptions;
          Process.MainWindow.Invalidate();
        end;
      else
        begin
          SendError(Message.Ticket,
          format('Unknown vscroll attribute [$%s] error', [LCast.Attr.ToHexString(8)]));
          exit;
        end;
      end;

      SendOK(Message.Ticket);
    end;

  STD_GADID_HSCROLL:
    begin
     case LCast.Attr of
      STD_ATTR_SCROLL_TOTAL:
        begin
          var LBar := Process.MainWindow.Footer.Scrollbar;
          LBar.Total := LCast.Value;
        end;
      STD_ATTR_SCROLL_PAGESIZE:
        begin
          var LBar := Process.MainWindow.Footer.Scrollbar;
          LBar.PageSize := LCast.Value;
        end;
      STD_ATTR_SCROLL_VALUE:
        begin
          var LBar := Process.MainWindow.Footer.Scrollbar;
          LBar.Position := LCast.Value;
        end;
      STD_ATTR_GADGET_ENABLED:
        begin
          var LBar := Process.MainWindow.Footer.Scrollbar;
          LBar.Enabled := LCast.Value;
        end;
      STD_ATTR_GADGET_VISIBLE:
        begin
          var LOptions := process.MainWindow.Options;
          case boolean(LCast.Value) of
          true:   include(LOptions, woHScroll);
          false:  exclude(LOptions, woHScroll);
          end;
          Process.MainWindow.Options := LOptions;
          Process.MainWindow.Invalidate();
        end;
      else
        begin
          SendError(Message.Ticket,
          format('Unknown hscroll attribute [$%s] error', [LCast.Attr.ToHexString(8)]));
        end;
      end;

      SendOK(Message.Ticket);
    end;
  else
    begin
      SendError(Message.Ticket,
        format('Unknown element id [%s] error', [LCast.id]));
    end;
  end;
end;

procedure TWbSoftKernel.ValidateManifest;
begin
end;

procedure TWbSoftKernel._SetAppManifest(const Message: TQTXApplicationMessage);
begin
  if Message.Attachment.Size > 0 then
  begin

    try
      FManifest.LoadFromStream( Message.Attachment.ToStream() );
      ValidateManifest();
    except
      on e: exception do
      begin
        FManifest.Clear();
        raise;
      end;
    end;

    FManifestOK := true;
  end;
end;

procedure TWbSoftKernel._ShowFileOpenRequester(const Message: TQTXApplicationMessage);
begin
  // Check that a valid application-manifest has been set.
  // This must be in place before any API calls are handled
  if not FManifestOK then
  begin
    var LError := 'Error: No application manifest has been set';
    SendError(Message.Ticket, LError);
    exit;
  end;

  var LInitialPath := TQTXApplicationShowFileReqMessage(Message).InitialPath;
  var LFilePattern := TQTXApplicationShowFileReqMessage(Message).FilePattern;

  // Create requester dialog window
  var LAccess := GetDesktop();
  var FBlocker := LAccess.ShowClickBlocker(Process.Id);

  var LDefWidth := 380;
  var LDefHeight := 400;
  var dx  := (Application.Display.View.width div 2) - (LDefWidth div 2);
  var dy  := (Application.Display.View.Height div 2) - (LDefHeight div 2);

  var LRequest := TWbFileLoadRequester.Create( Application.Display );
  LRequest.SetBounds(dx, dy, LDefWidth, LDefHeight);
  LRequest.Header.Title.Caption := 'Select file to load';
  LRequest.Options := [woSizeable, woVScroll];
  LRequest.TicketId := Message.Ticket;

  LRequest.Blocker := FBlocker;
  Process.MainWindow.StyleAsUnFocused();
  LAccess.SetFocusedWindow(LRequest);

  var LConnection := LAccess.GetConnection(true);
  LRequest.InitRequester(LConnection);

  // Process result on exit
  LRequest.OnWindowClose := procedure (Sender: TObject)
  begin
    var LRequest := TWbFileLoadRequester(Sender);

    var LAccess := GetDesktop();
    LAccess.CloseClickBlocker(Process.Id);

    // Create response with return ticket
    var LResponse := TQTXApplicationShowFileReqResponse.Create(LRequest.TicketId);
    try
      LResponse.Response := 'OK';
      LResponse.Filename := LRequest.Filename;
      Process.MessagePort.postMessage( LResponse.Serialize() );
    finally
      LResponse.free;
    end;

    TW3Dispatch.Execute( procedure ()
    begin
      var LAccess := GetDesktop();
      LAccess.SetFocusedWindow( self.Process.MainWindow );
    end, 200);

  end;

end;

procedure TWbSoftKernel._SetWindowTitle(const Message: TQTXApplicationMessage);
begin
  // Check that a valid application-manifest has been set.
  // This must be in place before any API calls are handled
  if not FManifestOK then
  begin
    var LError := 'Error: No application manifest has been set';
    SendError(Message.Ticket, LError);
    exit;
  end;

  Process.MainWindow.Header.Title.Caption := TQTXApplicationSetWindowTitleMessage(Message).Title;

  var LResponse := TQTXDesktopMessage.Create(Message.ticket);
  try
    LResponse.Response := 'OK';
    Process.MessagePort.postMessage(LResponse.Serialize());
  finally
    LResponse.free;
  end;
end;

//#############################################################################
// TWbProcessManager
//#############################################################################

constructor TWbProcessManager.Create;
begin
  inherited Create;
  FPLUT := TW3ObjDictionary.Create;
end;

destructor TWbProcessManager.Destroy;
begin
  FPLut.Clear();
  FPLut.free;
  inherited;
end;

procedure TWbProcessManager.HandleWindowClosing(Sender: TObject);
begin
  writeln("Window is closing");
  var LProcess := TWbHostedProcess( TWbExternalWindow(Sender).TagObject );
  ProcessEnds(LProcess);
end;

procedure TWbProcessManager.ProcessEnds(const Process: TWbHostedProcess);
begin
  try
    if assigned(OnProcessDestroyed) then
      OnProcessDestroyed(self, Process);
  finally
    // Remove from dictionary
    FPLut.Delete(Process.Id);
  end;
  writeln("Process is terminating");
end;

procedure TWbProcessManager.RegisterHostedProcess(AppEndPoint: string; WinOptions: TWbWindowOptions; const CB: TWbCreateProcessCB);
begin
  AppEndPoint := AppEndPoint.trim();
  if AppEndPoint.length > 0 then
  begin
    // Create process
    var LProcess := TWbHostedProcess.Create;

    // Register process
    FPLut.Values[LProcess.Id] := LProcess;

    // Set application endpoint
    LProcess.AppEndpoint := AppEndPoint;

    // Create main window
    var LAccess := GetDesktop();
    var LWindowHost := LAccess.GetWindowHost();
    var LMainWindow := TWbExternalWindow.Create(LWindowHost);
    LMainWindow.Options := WinOptions; //[woSizeable, woVScroll];

    // Keep process reference in the tag
    LMainWindow.TagObject := LProcess;

    // Map up window's close event
    LMainWindow.OnWindowClose := @HandleWindowClosing;

    // Wait for window instance to construct
    TW3Dispatch.WaitFor([LMainWindow, LMainWindow.Header, LMainWindow.Content,
      LmainWindow.Footer, LMainWindow.LeftEdge, LMainWindow.RightEdge],
      procedure ()
      begin
        // Set as main-window
        LProcess.MainWindow := LMainWindow;

        // Register window with process
        LProcess.RegisterWindow(LMainWindow);

        //LMainWindow.Header.Title.Caption := 'External application';
        LMainWindow.SetBounds(200, 64, 640, 400);
        LMainWindow.Constraints.MinWidth := 200;
        LMainWindow.Constraints.MinHeight := 200;
        LMainWindow.Constraints.Enabled := true;
        LMainWindow.Header.Title.Caption := 'Loading ' + AppEndPoint;

        LMainWindow.StyleAsFocused();

        // Initiate loading of process
        LMainWindow.OpenURLEx2(AppEndPoint,
          [ //eoNoScrolling
            eoAllowFocus
            ,eoCheckFrameFocus
            ,eoHandshake
          ], nil,
          procedure (Success: boolean)
          begin

            if not LMainWindow.FrameObject.HandshakeOK then
            begin
              writeln("Premature handshake, back off");
              exit;
            end else
            begin
              LProcess.MessagePort := LMainWindow.FrameObject.MessagePort;
              writeln("Handshake ok, continuing");
            end;

            if Success then
            begin
              writeln("Success! Now attaching soft-kernel to process");

              // Bind message processing to the process object
              LProcess.Kernel.AttachToProcess();


              if assigned(OnProcessCreated) then
                OnProcessCreated(self, LProcess);

              if assigned(CB) then
                CB(true, '', LProcess);
            end else
            begin
              var LLastError := LProcess.MainWindow.LastError;
              try
                if LProcess.Kernel.Attached then
                  LProcess.Kernel.DetachFromProcess();

                LProcess.MainWindow.free;
                LProcess.MainWindow := nil;
                LProcess.free;
                LProcess := nil;
              finally
                if assigned(CB) then
                  CB(Success, LLastError, nil);
              end;
            end;
          end);
      end);

  end else
  begin
    if assigned(CB) then
    CB(false, 'Invalid endpoint error', nil);
  end;
end;

procedure TWbProcessManager.RegisterHostedProcessEx2(const Info: TWbLinkInfo; const CB: TWbCreateProcessCB);
begin
  if Info = nil then
  begin
    if assigned(CB) then
      CB(false, 'Link reference was nil error', nil);
    exit;
  end;
end;

procedure TWbProcessManager.RegisterHostedProcessEx(AppEndPoint: string; const CB: TWbCreateProcessCB);
begin
  RegisterHostedProcess(AppEndPoint, [woSizeable, woVScroll], CB);
end;

function TWbProcessManager.GetProcessById(const ProcId: string): TWbHostedProcess;
begin
  if ProcId.length > 0 then
  begin
    if FObjects.Count > 0 then
    begin
      for var process in FObjects do
      begin
        if Process.Id = ProcId then
        begin
          result := Process;
          break;
        end;
      end;
    end;
  end;
end;


//#############################################################################
// TWbServiceProcess
//#############################################################################

constructor TWbServiceProcess.Create;
begin
  inherited Create;
  FServiceSocket := TW3WebSocket.Create;
end;

destructor TWbServiceProcess.Destroy;
begin
  FServiceSocket.free;
  inherited;
end;

//#############################################################################
// TWbHostedProcess
//#############################################################################

constructor TWbHostedProcess.Create;
begin
  inherited Create;
  FId := TString.CreateGUID();
  FKernel  := TWbSoftKernel.Create(self);
end;

destructor TWbHostedProcess.Destroy;
begin
  if FKernel.Attached then
    FKernel.DetachFromProcess();

  if FWindows.Count > 0 then
    ForceCloseOpenWindows();
  FKernel.free;
  FKernel := nil;
  inherited;
end;

procedure TWbHostedProcess.Terminate;
begin
  if assigned(MainWindow) then
    MainWindow.CloseWindow();
end;

procedure TWbHostedProcess.ForceCloseOpenWindows;
begin
  if TVariant.ClassInstance(self) then
  begin
    try
      for var LItem in FWindows do
      begin
        if LItem <> Mainwindow then
        begin
          try
            LItem.free;
          except
            // sink exceptions
          end;
        end;
      end;
    finally
      FWindows.Clear();
    end;
  end;
end;

function TWbHostedProcess.GetAppManifest(var Manifest: TWbHostedAppManifest): boolean;
begin
  Manifest := FManifest;
  result := FManifest <> nil;
end;

procedure TWbHostedProcess.RegisterWindow(const WinInstance: TWbCustomWindow);
begin
end;

procedure TWbHostedProcess.UnRegisterWindow(const WinInstance: TWbCustomWindow);
begin
end;


end.
