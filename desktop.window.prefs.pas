unit desktop.window.prefs;

interface

uses
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,

  System.Device.Storage,
  SmartCL.Device.Storage,

  Desktop.Types,
  Desktop.Control,
  Desktop.Window,

  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System,
  SmartCL.Controls.Image;

type

  TWbPreferencesWindow = class(TWbWindow)
  private
    FImage:   TW3Image;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  Image: TW3Image read FImage;
  end;

implementation

//############################################################################
// TWbImageViewer
//############################################################################

procedure TWbPreferencesWindow.InitializeObject;
begin
  inherited;
  Header.Title.Caption := 'Preferences';

  TransparentEvents := true;
  Options := [woSizeable];
  MinimumWidth := 400;
  MinimumHeight := 400;
end;

procedure TWbPreferencesWindow.FinalizeObject;
begin
  inherited;
end;

procedure TWbPreferencesWindow.ObjectReady;
begin
  inherited;
end;

procedure TWbPreferencesWindow.Resize;
begin
  inherited;
end;


end.
