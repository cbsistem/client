unit desktop.requesters;

interface

uses 
  System.Colors,
  System.Types,
  System.Types.Graphics,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,

  Desktop.Types,
  Desktop.control,
  Desktop.loginprofile,
  Desktop.Toolbar,
  Desktop.IconView,
  Desktop.Panel,
  Desktop.Window,
  Desktop.Edit,
  Desktop.Button,
  Desktop.Scrollbar,

  // We access files via our mapped device driver
  System.Device.Storage,
  Desktop.Filesystem.Node,
  Desktop.Network.Connection,

  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,

  SmartCL.Scroll,
  SmartCL.Net.WebSocket,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

type

  TWbModalResult = (
    mrSeparator = 0,  // resolves to toolbar::separator
    mrOK        = 1,
    mrCancel    = 2,
    mrYes       = 3,
    mrNo        = 4
  );

  TWbRequesterType = (
    rtSaveFileReq,
    rtLoadFileReq,
    rtCreateFolderReq,
    rtPickFolderReq
  );

  TWbShowRequestCB = procedure (Sender: TObject; ModalResult: TWbModalResult);

  TWbCustomRequester = class(TWbWindow)
  private
    FFooter:  TWbPanel;
    FToolbar: TWbToolbar;
    FView:    TWbIconView;
    procedure AttachScrollBehavior;
    procedure MuteScrollBar(Scrollbar: TWBCustomScrollbar);
    procedure UpdateScrollbar(const FullUpdate: boolean);

  protected
    procedure SetupToolbar(const Toolbar: TWbToolbar); virtual;
    procedure SetupView(const View: TWbIconView); virtual;

    procedure HandleDirReady(Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean); virtual;

    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  ListView: TWbIconView read FView;
    property  Footer: TWbPanel read FFooter;
    property  TicketId: string;
    property  Connection: TWbNetworkClient;
    property  Blocker: TW3BlockBox;

    procedure InitRequester(const Client: TWbNetworkClient);
  end;

  TWbFileLoadRequester = class(TWbCustomRequester)
  private
    procedure HandleItemSelect(Sender: TObject);
    procedure HandleItemUnSelect(Sender: TObject);
    procedure HandleItemDblClick(Sender: TObject; Item: TWbListItem);

    procedure HandleSelectClick(Sender: TObject);
    procedure HandleCancelClick(Sender: TObject);
  protected
    procedure SetupToolbar(const Toolbar: TWbToolbar); override;
    procedure SetupView(const View: TWbIconView); override;
    procedure HandleDirReady(Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean); override;
  public
    property  Filename: string;
  end;

  TWbRequesterAPI = class(TObject)
  private
    FActive:  boolean;
  public
    property  Active: boolean read FActive;
  end;

implementation

//#############################################################################
// TWbFileLoadRequester
//#############################################################################

procedure TWbFileLoadRequester.SetupView(const View: TWbIconView);
begin
  View.Spacing := 2;
  View.OnItemDblClick := @HandleItemDblClick;
end;

procedure TWbFileLoadRequester.HandleItemDblClick(Sender: TObject; Item: TWbListItem);
begin
  Filename := TWbListItemText(Item).Text.Caption;
  CloseWindow();
end;

procedure TWbFileLoadRequester.HandleSelectClick(Sender: TObject);
begin
  if ListView.SelectedItem <> nil then
  begin
    Filename := TWbListItemText(ListView.SelectedItem).Text.Caption;
    CloseWindow();
  end;
end;

procedure TWbFileLoadRequester.HandleCancelClick(Sender: TObject);
begin
  TW3Dispatch.Execute( CloseWindow, 100);
end;

procedure TWbFileLoadRequester.HandleItemSelect(Sender: TObject);
begin
  TWbListItemText(Sender).TagStyle.Add("TWbListViewSelect");
end;

procedure TWbFileLoadRequester.HandleItemUnSelect(Sender: TObject);
begin
  TWbListItemText(Sender).TagStyle.RemoveByName("TWbListViewSelect");
end;

procedure TWbFileLoadRequester.HandleDirReady(Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean);
begin
  ListView.BeginUpdate();
  try
    ListView.Spacing := 0;
    ListView.Clear();

    if not Success then
    begin
      writeln("Directory not successful");
      writeln(Connection.LastError);
      exit;
    end;

    {ListView.OnItemClicked := procedure (Sender: TObject; Item: TWbListItem)
    begin
      writeln( TWbListItemText(item).Text.Caption );
    end;      }

    //Listview.TransparentEvents := true;
    //ListView.SimulateMouseEvents := true;

    for var FileObject in Files.dlItems do
    begin
      var LItem := ListView.AddText(FileObject.diFileName);
      LItem.Height := 22;
      LItem.Selected := false;
      (LItem as IWbListItem).SetOptions([ioAllowSelection, ioFullSize]);
      LItem.OnSelected := @HandleItemSelect;
      LItem.OnUnSelected := @HandleItemUnSelect;
    end;


  finally
    ListView.EndUpdate();
  end;
end;

procedure TWbFileLoadRequester.SetupToolbar(const Toolbar: TWbToolbar);
begin
  inherited SetupToolbar(Toolbar);

  var LButton := Toolbar.Add();
  LButton.Caption := 'Load';
  LButton.OnClick := @HandleSelectClick;

  LButton := Toolbar.Add();
  LButton.Caption := 'Parent';

  LButton := Toolbar.Add();
  LButton.Caption := 'Volumes';

  LButton := Toolbar.Add();
  LButton.Caption := 'Cancel';
  LButton.OnClick := @HandleCancelClick;
end;

//#############################################################################
// TWbCustomRequester
//#############################################################################

procedure TWbCustomRequester.InitializeObject;
begin
  inherited;
  FFooter := TWbPanel.Create(self.Content);
  FToolbar := TWbToolbar.Create(FFooter);
  FView := TWbIconView.Create(self.Content);
  FView.ScrollBars := TW3ScrollBarType.sbNone;
end;

procedure TWbCustomRequester.FinalizeObject;
begin
  FToolbar.free;
  FFooter.free;
  FView.free;
  inherited;
end;

procedure TWbCustomRequester.MuteScrollBar(Scrollbar: TWBCustomScrollbar);
begin
  if Scrollbar <> nil then
  begin
    Scrollbar.BeginUpdate;
    Scrollbar.Position := 0;
    Scrollbar.Total := 10;
    Scrollbar.PageSize := 10;
    Scrollbar.Enabled := false;
    Scrollbar.EndUpdate;
  end;
end;

procedure TWbCustomRequester.UpdateScrollbar(const FullUpdate: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      if FView <> nil then
      begin
        if not (csDestroying in FView.ComponentState) then
        begin
          if not FView.Updating then
          begin

            case FullUpdate of
            true:
              begin
                var LTotalHeight := FView.Content.Height;
                var LPageHeight := FView.Container.Height;
                var LPos := abs(FView.ScrollController.ContentTop);

                if LTotalHeight <= LPageHeight then
                begin
                  if VerticalScroll.Enabled then
                    MuteScrollBar(VerticalScroll);
                  exit;
                end;

                VerticalScroll.BeginUpdate;
                VerticalScroll.Enabled := true;
                VerticalScroll.Total := LTotalHeight;
                VerticalScroll.PageSize := LPageHeight;
                VerticalScroll.Position := LPos;
                VerticalScroll.EndUpdate;
              end;
            false:
              begin

                var LTotalHeight := FView.Content.Height;
                var LPageHeight := FView.Container.Height;
                var LPos := abs(FView.ScrollController.ContentTop);

                if LTotalHeight <= LPageHeight then
                begin
                  if VerticalScroll.Enabled then
                    MuteScrollBar(VerticalScroll);
                  exit;
                end;

                VerticalScroll.BeginUpdate;
                VerticalScroll.Position := LPos;
                VerticalScroll.EndUpdate;

              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TWbCustomRequester.HandleDirReady(Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean);
begin
end;

procedure TWbCustomRequester.AttachScrollBehavior;
begin
  //FView.OnItemDblClick := @HandleItemDblClick;
  FView.ScrollController.OnScrolling := procedure (Sender: TObject)
  begin
    if VerticalScroll.Enabled then
    begin
      if not VerticalScroll.Updating then
      begin
        VerticalScroll.Position := abs(FView.ScrollController.ContentTop);
      end;
    end;
  end;

  VerticalScroll.OnPositionChanged := procedure (Sender: TObject)
  begin
    var YPos := VerticalScroll.Position;
    VerticalScroll.BeginUpdate;
    FView.ScrollController.ScrollTo(0, -YPos);
    VerticalScroll.EndUpdate;
  end;
end;

procedure TWbCustomRequester.InitRequester(const Client: TWbNetworkClient);
begin
  Connection := Client;

  var LAccess := GetDesktop();
  var LHostInfo := LAccess.GetHostInfo();
  var LUrl := LHostInfo.htWebSocketEndpoint;

  Client.Connection.Connect(LUrl, [],
    procedure (Socket: TW3WebSocket; Success: boolean)
    begin
      case Success of
      true:
        begin
          var LAccess := GetDesktop();
          var LProfile := LAccess.GetLoginProfile();
          Connection.Username := LProfile.Username;
          Connection.Password := LProfile.Password;
          Connection.Login( procedure (Success: boolean)
          begin
            writeln("Login came back with: " + Success.ToString());
            if Success then
            begin
              Connection.FileIODir('dir ~/Photos', null, @HandleDirReady);
              AttachScrollBehavior();
            end;
          end);
        end;
      false:
        begin
          writeln("This sucks dude");
        end;
      end;
    end);
end;

procedure TWbCustomRequester.SetupView(const View: TWbIconView);
begin
end;

procedure TWbCustomRequester.SetupToolbar(const Toolbar: TWbToolbar);
begin
end;

procedure TWbCustomRequester.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([FFooter, FToolbar, FView], procedure ()
  begin
    FToolbar.ElementJustify := TWbToolbarJustification.tjSpaceEvenly;

    FToolbar.handle.style['width'] := "100%";
    FToolbar.handle.style['height'] := "100%";

    FFooter.Border.Size := 0;
    FFooter.Border.Margin := 0;
    FFooter.ThemeReset();

    SetupView(FView);
    SetupToolbar(FToolbar);
  end);
end;

procedure TWbCustomRequester.Resize;

  function ScaleRect(const Value: TRect; const X, Y: integer): TRect;
  begin
    result := Value;
    inc(result.left, x);
    dec(result.right, x);
    inc(result.top, y);
    dec(result.bottom, y);
  end;

begin
  inherited;
  var LBounds := Content.ClientRect;
  LBounds := Content.AdjustClientRect(LBounds);

  if FFooter <> nil then
  begin
    var LFooterRect := LBounds;
    LFooterRect.top := LFooterRect.Bottom - 46;
    FFooter.SetBounds(LFooterRect);

    if FView <> nil then
    begin
      var LViewRect := LBounds;
      dec(LViewRect.Bottom, Footer.height );
      LViewRect:= ScaleRect(LViewRect, 6, 6);
      FView.SetBounds(LViewRect);

      UpdateScrollbar(true);
    end;
  end;
end;


initialization
begin
  // light background: C9C9D0
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #'
    .TWbListViewSelect {
      background: #0055AA !important;
      color: #FFFFFF !important;
      font-weight: bold;
    }
  ';
  //StyleCode := StrReplace(StyleCode,"§",prefix);
  Sheet.Append(StyleCode);
end;


end.
