unit desktop.datatypes;

interface

uses
  System.Types,
  System.Types.Convert,
  System.IOUtils;

  /* Identifiers for common objects */
  const
  CNT_ID_DATATYPE_FOLDER = 'BC4D4E7E-6593-47B3-8EB5-2DE9DC8E71C4';
  CNT_ID_DATATYPE_TEXT   = 'BF04CD30-7C28-48DC-92D4-F1A3A224D0A6';
  CNT_ID_DATATYPE_PASCAL = '1BFAEE2B-54A1-41DA-8803-725F7BF9035C';

type

  TWbDatatypeInfo = class(JObject)
  public
    property  TypeName: string;         // Datatype name: folder, file, picture, movie, music, program, symbol (shortcut)
    property  Description: string;      // Short description of file
    property  Identifier: string;       // Unique identifier for datatype [GUID]
    property  SelectedIcon: string;     // URL for selected icon
    property  UnSelectedIcon: string;   // URL for unselected icon
    property  FileExt: string;          // Ex: .txt, .png, .gif
    property  MimeType: string;         // text/plain, text/html .. see mime-types

    function Serialize: string;
    class function DeSerialize(const JSONText: string): TWbDatatypeInfo;
    constructor Create;
  end;

  TWbDatatypeRegistryEnumProc = function (const Item: TWbDatatypeInfo): TEnumResult;

  IWbDatatypeRegistry = interface
    ['{CB2C0A30-B6D1-4B8D-A350-E1B523B51466}']
    function  GetCount: integer;
    function  GetItem(const Index: integer): TWbDataTypeInfo;
    function  GetInfoByExt(FileName: string): TWbDataTypeInfo;
    function  GetInfoByType(TypeName: string): TWbDataTypeInfo;
    procedure ForEach(const Callback: TWbDatatypeRegistryEnumProc);
    procedure ForEachEx(const Before: TProcedureRef;
              const Process: TWbDatatypeRegistryEnumProc;
              const After: TProcedureRef);
  end;

  TWbDatatypeRegistry = class(TObject, IWbDatatypeRegistry)
  private
    FObjects: array of TWbDatatypeInfo;
  protected
    function  GetCount: integer;
    function  GetItem(const Index: integer): TWbDataTypeInfo;
  public
    property  Count: integer read GetCount;
    property  Item[const index: integer]: TWbDataTypeInfo read GetItem;
    function  Register(const Info: TWbDataTypeInfo): TWbDataTypeInfo;
    procedure Clear;

    function  GetInfoByExt(FileName: string): TWbDataTypeInfo;
    function  GetInfoByType(TypeName: string): TWbDataTypeInfo;

    procedure ForEach(const Callback: TWbDatatypeRegistryEnumProc);
    procedure ForEachEx(const Before: TProcedureRef;
              const Process: TWbDatatypeRegistryEnumProc;
              const After: TProcedureRef);

    destructor Destroy; override;
  end;

implementation

uses
  System.JSon;
  //System.FileSystem.Memory;

//#############################################################################
// TWbDatatypeInfo
//#############################################################################

constructor TWbDatatypeInfo.Create;
begin
  Identifier := TDatatype.CreateGUID();
end;

function TWbDatatypeInfo.Serialize: string;
begin
  asm
    @result = JSON.stringify(@self)
  end;
end;

class function TWbDatatypeInfo.DeSerialize(const JSONText: string): TWbDatatypeInfo;
begin
  result := TWbDatatypeInfo( JSON.Parse(JSONText) );
end;

//#############################################################################
// TWbDatatypeRegistry
//#############################################################################

destructor TWbDatatypeRegistry.Destroy;
begin
  Clear();
  inherited;
end;

function TWbDatatypeRegistry.GetCount: integer;
begin
  result := FObjects.length;
end;

function TWbDatatypeRegistry.GetItem(const Index: integer): TWbDataTypeInfo;
begin
  result := FObjects[Index];
end;

procedure TWbDatatypeRegistry.ForEach(const Callback: TWbDatatypeRegistryEnumProc);
begin
  if assigned(Callback) then
  begin
    for var LItem in FObjects do
    begin
      if Callback(LItem) = erBreak then
        break;
    end;
  end;
end;

procedure TWbDatatypeRegistry.ForEachEx(const Before: TProcedureRef;
          const Process: TWbDatatypeRegistryEnumProc;
          const After: TProcedureRef);
begin
  try
    if assigned(Before) then
    Before();
  finally
    try
      if assigned(Process) then
      begin
        for var LItem in FObjects do
        begin
          if Process(LItem) = erBreak then
          break;
        end;
      end;
    finally
      if assigned(After) then
        After();
    end;
  end;
end;

procedure TWbDatatypeRegistry.Clear;
begin
  FObjects.Clear();
end;

function TWbDatatypeRegistry.Register(const Info: TWbDataTypeInfo): TWbDataTypeInfo;
begin
  result := Info;
  if Info <> nil then
  begin
    if FObjects.IndexOf(Info) <= -1 then
      FObjects.add(Info);
  end;
end;

function TWbDatatypeRegistry.GetInfoByType(TypeName: string): TWbDataTypeInfo;
begin
  TypeName := TypeName.trim();
  if TypeName.length > 0 then
  begin
    for var LItem in FObjects do
    begin
      if LItem.TypeName.ToLower() = TypeName then
      begin
        result := LItem;
        break;
      end;
    end;
  end;
end;

function TWbDatatypeRegistry.GetInfoByExt(FileName: string): TWbDataTypeInfo;
begin
  var LExt := TPath.GetExtension(Filename).ToLower();
  for var LItem in FObjects do
  begin
    if LExt = LItem.FileExt then
    begin
      result := LItem;
      break;
    end;
  end;
end;

end.

