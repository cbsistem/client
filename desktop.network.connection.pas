unit desktop.network.connection;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Objects,
  System.Time,
  System.Streams,
  System.Memory.Buffer,
  System.Reader,
  System.Writer,
  System.Dictionaries,
  System.JSON,
  System.Device.Storage,
  //System.Hash,

  ragnarok.messages.application,
  ragnarok.messages.factory,
  ragnarok.messages.base,
  ragnarok.messages.callstack,
  ragnarok.messages.network,

  SmartCL.Net.WebSocket,
  SmartCL.Components,
  SmartCL.System;

type

  TWbNetworkDirCallback = procedure (Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean);
  TWbNetworkReadCallback = procedure (Sender: TObject; Filename: string; TagValue: variant; FileData: TBinaryData; Success: boolean);

  IWbNetwork = interface
    ["{436A0F79-B772-4734-99A6-C96A876E9D79}"]
    procedure   Send(const Message: TQTXClientMessage;
                const Handler: TWbCStackTicketHandler;
                const CB: TStdCallback);

    procedure   FileIO(CmdLine: string;
                const Handler: TWbCStackTicketHandler);

    procedure   FileIODir(Path: string; TagValue: variant; CB: TWbNetworkDirCallback);
    procedure   FileIORead(FilePath: string; TagValue: variant; CB: TWbNetworkReadCallback);

    function    GetSessionId: string;
    function    GetLoginState: boolean;
  end;

  TWbNetworkMessageFactory = class(TMessageFactory)
  protected
    procedure RegisterIntrinsic; override;
  end;

  TWbNetworkClient = class(TW3ErrorObject, IWbNetwork)
  private
    FConnection:  TW3WebSocket;
    FCallStack:   TWbCallStack;
    FSessionId:   string;
    FLoggedIn:    boolean;
    FMsgFactory:  TWbNetworkMessageFactory;
  protected
    function    GetSessionId: string;
    function    GetLoginState: boolean;
    procedure   HandleOpen(sender: TW3WebSocket); virtual;
    procedure   HandleClosed(sender: TW3WebSocket); virtual;
    procedure   HandleMessage(Sender: TW3WebSocket; Message: TWebSocketMessageData);
  public
    property    Connection: TW3WebSocket read FConnection;
    property    MessageFactory: TWbNetworkMessageFactory read FMsgFactory;
    property    CallStack: TWbCallStack read FCallstack;

    property    SessionId: string read FSessionId write FSessionId;
    property    Username: string;
    property    Password: string;

    procedure   Send(const Message: TQTXClientMessage;
                const Handler: TWbCStackTicketHandler;
                const CB: TStdCallback);

    // This is for quickly re-connecting if the connection was lost
    procedure   Login(const Handler: TWbCStackTicketHandler); overload;

    // This is a full login with extended callback [success:bool]
    procedure   Login(Username, password: string;
                const Handler: TWbCStackTicketHandler;
                const CB: TStdCallback ); overload;

    // Clean login using username/password set in properties
    procedure   Login(const CB: TStdCallback); overload;

    // Full login, but only with a network callback
    procedure   Login(Username, password: string;
                const Handler: TWbCStackTicketHandler); overload;

    // standard fileIO shell call. Note, you can send more than a single
    // shell command. Use ";" as delimiter (see message-envelope)
    procedure   FileIO(CmdLine: string;
                const Handler: TWbCStackTicketHandler); overload;

    procedure   FileIODir(Path: string; TagValue: variant; CB: TWbNetworkDirCallback);
    procedure   FileIORead(FilePath: string; TagValue: variant; CB: TWbNetworkReadCallback);

    constructor Create; override;
    destructor  Destroy; override;
  published
    property    OnConnected: TNotifyEvent;
    property    OnDisconnected: TNotifyevent;
  end;

implementation

//#############################################################################
// TWbNetworkMessageFactory
//#############################################################################

procedure TWbNetworkMessageFactory.RegisterIntrinsic;
begin
  &Register(TQTXServerError);
  &Register(TQTXLoginRequest);
  &Register(TQTXLoginResponse);
  &Register(TQTXFileIORequest);
  &Register(TQTXFileIOResponse);
  &Register(TQTXFileIODirResponse);
end;


//#############################################################################
// TWbNetworkClient
//#############################################################################

constructor TWbNetworkClient.Create;
begin
  inherited Create;
  FCallStack := TWbCallStack.Create;
  FMsgFactory := TWbNetworkMessageFactory.Create;

  FConnection := TW3WebSocket.Create;
  FConnection.OnOpen := @HandleOpen;
  FConnection.OnClosed := @HandleClosed;
  FConnection.OnMessage := @HandleMessage;
end;

destructor TWbNetworkClient.Destroy;
begin
  FCallStack.free;
  FMsgFactory.free;
  FConnection.free;
  inherited;
end;

function TWbNetworkClient.GetSessionId: string;
begin
  result := FSessionId;
end;

function TWbNetworkClient.GetLoginState: boolean;
begin
  result := FConnection.SocketState = stOpen;
end;

procedure TWbNetworkClient.HandleMessage(Sender: TW3WebSocket; Message: TWebSocketMessageData);
var
  LEnvelope: TQTXBaseMessage;
begin
  var LClassName := TQTXBaseMessage.QueryIdentifier(message.mdText);
  LClassName := LClassname.trim();
  if LClassname.length < 1 then
  begin
    SetLastError('Unknown or damaged message packet error');
    exit;
  end;

  if not FMsgFactory.Build(LClassName, LEnvelope) then
  begin
    SetLastErrorF('Unknown or unsupported message [%s] error', [LClassName]);
    exit;
  end;


  try
    LEnvelope.Parse(Message.mdText);
  except
    on e: exception do
    begin
      SetLastError("Failed to parse response:" + e.message);
      exit;
    end;
  end;

  try
    if (LEnvelope is TQTXLoginResponse) then
    begin
      if TQTXServerMessage(LEnvelope).code = 200 then
        FSessionId := TQTXLoginResponse(LEnvelope).SessionId
      else
        FSessionId := '';
    end;

    if FCallStack.Contains(LEnvelope.Ticket) then
    begin
      if LEnvelope is TQTXLoginResponse then
        FLoggedIn := TQTXLoginResponse(LEnvelope).Code = 200;
      FCallstack.Invoke(LEnvelope.Ticket, TQTXServerMessage(LEnvelope));
    end;
  finally
    LEnvelope.free;
  end;
end;

procedure TWbNetworkClient.FileIORead(FilePath: string; TagValue: variant; CB: TWbNetworkReadCallback);
begin
  if self.ErrorOptions.AutoResetError then
    ClearLastError();

  if FConnection.SocketState = stOpen then
  begin
    FileIO( 'read ' + FilePath.trim(),
      procedure (const Response: TQTXBaseMessage)
      begin
        if TQTXServerMessage(Response).code = 200 then
        begin
          if assigned(CB) then
            CB(self, FilePath, TagValue, TQTXServerMessage(Response).Attachment, true);

        end else
        begin
          SetLastError(  TQTXServerMessage(Response).Response );
          if assigned(CB) then
            CB(self, '', TagValue, nil, false);
        end;
      end);
  end else
  begin
    SetLastError('Not connected error');
    if assigned(CB) then
      CB(self, FilePath, TagValue, nil, false);
  end;
end;

procedure TWbNetworkClient.FileIODir(Path: string; TagValue: variant; CB: TWbNetworkDirCallback);
begin
  if self.ErrorOptions.AutoResetError then
    ClearLastError();

  if FConnection.SocketState = stOpen then
  begin
    FileIO(Path.trim(),
    procedure (const Response: TQTXBaseMessage)
    begin
      if TQTXServerMessage(Response).code = 200 then
      begin
        var LDirObj := TQTXFileIODirResponse(Response).DirList;

        LDirObj.dlItems.Sort(
          function (left, right: TNJFileItem): integer
          begin
            if right.diFileType = wtFolder then
              result := 1
            else
            if left.diFileType = wtFolder then
              result := -1
            else
              result := 0;
          end);

        if assigned(CB) then
          CB(Self, TagValue, LDirObj, assigned(LDirObj));
      end else
      begin
        SetLastError(  TQTXServerMessage(Response).Response );
        if assigned(CB) then
          CB(self, TagValue, nil, false);
      end;
    end);
  end else
  begin
    SetLastError('Not connected error');
    if assigned(CB) then
      CB(self, TagValue, nil, false);
  end;
end;

procedure TWbNetworkClient.FileIO(CmdLine: string; const Handler: TWbCStackTicketHandler);
var
  FileIOMsg:  TQTXFileIORequest;
begin
  ClearLastError();
  if FConnection.SocketState <> stOpen then
  begin
    SetLastError('Failed to send message, not connected error');
    exit;
  end;

  FileIOMsg := TQTXFileIORequest.Create( TQTXBaseMessage.CreateTicket() );
  try
    FileIOMsg.SessionId := FSessionId;
    FileIOMsg.Command := CmdLine;
    Send(FileIOMsg, @Handler, nil);
  finally
    FileIOMsg.free;
  end;

end;

procedure TWbNetworkClient.Login(Username, Password: string;
          const Handler: TWbCStackTicketHandler);
begin
  Login(Username, Password, @Handler, nil);
end;

procedure TWbNetworkClient.Login(const CB: TStdCallback);
begin
  Login(self.UserName, self.password,
    procedure (const Response: TQTXBaseMessage)
    begin
      if (response is TQTXServerError) then
      begin
        SetLastError( TQTXServerError(response).Response );
        if assigned(CB) then
          CB(false);
        exit;
      end;

      var Msg := TQTXLoginResponse(Response);

      if assigned(CB) then
        CB(Msg.Code = 200);

    end, nil);
end;

procedure TWbNetworkClient.Login(const Handler: TWbCStackTicketHandler);
begin
  if assigned(Handler) then
    Login(self.Username, self.Password, @Handler, nil)
  else
    Login(self.Username, self.password, nil, nil);
end;

procedure TWbNetworkClient.Login(Username, Password: string;
  const Handler: TWbCStackTicketHandler; const CB: TStdCallback );
begin
  if ErrorOptions.AutoResetError then
    ClearLastError();

  // Check socket state
  if not FConnection.SocketState = stOpen then
  begin
    SetLastError('Failed to send login message, not connected error');
    if assigned(CB) then
      CB(false);
    exit;
  end;

  // Initialize message data
  var LoginMsg := TQTXLoginRequest.Create(TQTXBaseMessage.CreateTicket());
  LoginMsg.Username := UserName;
  LoginMsg.Password := Password;

  // Send message
  Send(LoginMsg, @Handler, procedure (Success: boolean)
  begin
    if not Success then
      SetLastErrorF('Login failed with error: %s', [FConnection.LastError]);

    if assigned(CB) then
      CB(Success);
  end);
end;

procedure TWbNetworkClient.Send(const Message: TQTXClientMessage;
  const Handler: TWbCStackTicketHandler; const CB: TStdCallback);
begin
  ClearLastError();
  if FConnection.SocketState = stOpen then
  begin

    if Message = nil then
    begin
      SetLastError('Failed to send message, object was nil error');
      if assigned(CB) then
        CB(false);
      exit;
    end;

    if not assigned(Handler) then
    begin
      SetLastError('Failed to send message, no callback handler defined');
      if assigned(CB) then
        CB(false);
      exit;
    end;

    Message.Ticket := Message.CreateTicket();

    // Register with callstack
    FCallstack.Commit(Message.Ticket, @Handler);

    // Issue the message
    FConnection.Write(Message.Serialize(), procedure (Success: boolean)
    begin
      if not Success then
        SetLastError(FConnection.LastError);
      if assigned(CB) then
        CB(Success);
    end);

  end else
  begin
    SetLastError('Failed to send message, not connected error');
    if assigned(CB) then
      CB(false);
  end;
end;

procedure TWbNetworkClient.HandleOpen(sender: TW3WebSocket);
begin
  if assigned(OnConnected) then
    OnConnected(self);
end;

procedure TWbNetworkClient.HandleClosed(sender: TW3WebSocket);
begin
  FSessionId := '';
  FLoggedIn := false;
  if assigned(OnDisconnected) then
    OnDisconnected(Self);
end;

end.
