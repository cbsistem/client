unit desktop.process.factory;

interface

uses 
  System.Types,
  System.Types.Convert,
  ragnarok.messages.factory,
  ragnarok.messages.application;

type

  // Message-factory used by desktop
  TAmibianDesktopMessageFactory = class(TMessageFactory)
  protected
    procedure RegisterIntrinsic; override;
  end;

function  GetDesktopMessageFactory: TAmibianDesktopMessageFactory;

implementation

var
  __Factory: TAmibianDesktopMessageFactory;

function  GetDesktopMessageFactory: TAmibianDesktopMessageFactory;
begin
  if __Factory = nil then
    __Factory := TAmibianDesktopMessageFactory.Create;
  result := __Factory;
end;

//#############################################################################
// TAmibianDesktopMessageFactory
//#############################################################################

procedure TAmibianDesktopMessageFactory.RegisterIntrinsic;
begin
  &Register(TQTXApplicationMessage);
  &Register(TQTXApplicationSetWindowTitleMessage);
  &Register(TQTXApplicationGetDirMessage);
  &Register(TQTXApplicationShowFileReqMessage);
  &Register(TQTXApplicationSetGadAttrReqMessage);
  &Register(TQTXApplicationGetGadAttrReqMessage);
end;


end.
