unit desktop.process.linkfile;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  SmartCL.System;


type

  TLinkAccessMode = (
    acOpen,     // Opens the file via datatypes
    acExecute,  // Regards the file as executable
    acPrint     // Sends file to printer services
  );

  TLinkType = (
    rtLocal,    // Local application [shortcut]
    rtRemote    // external application
  );

  TLinkFile = class(TObject)
  public
    property  AccessMode: TLinkAccessMode;
    property  LinkType: TLinkType;
    property  EndPoint: string;
  end;


implementation



end.
