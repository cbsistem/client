unit desktop.iconview;

{$I 'Smart.inc'}

{$DEFINE DEBUG}
{$DEFINE CREATE_ITEMS_OFFSCREEN}

interface

uses 
  W3C.DOM,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Time,
  System.Colors,

  Desktop.Control,

  SmartCL.Theme,
  SmartCL.System,
  SmartCL.Fonts,
  SmartCL.Borders,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,

  SmartCL.Scroll,
  SmartCL.Controls.ScrollBox,
  SmartCL.Controls.Image,
  SmartCL.Controls.Label;


type

  TWbListItem         = class;
  TWbListItemGroup    = class;
  TWbListItemIcon     = class;
  TWbLayoutSelectMask = class;
  TWbIconViewContent  = class;
  TWbIconView         = class;

  TWbListItemOptions = set of (
      ioAllowSelection,
      ioBreak,
      ioFullSize
    );

  TWbListLayoutMode = (
    lmHorizontal,
    lmVertical
    );

  IWbListItem = interface
    function    GetOptions: TWbListItemOptions;
    procedure   SetOptions(NewOptions: TWbListItemOptions);
    procedure   SetSelected(const NewValue: boolean);
    procedure   ItemSelected;
    procedure   ItemUnSelected;
    procedure   SetIconView(const View: TWbIconView);
  end;

  TWbListItem = class(TWbCustomControl, IWbListItem)
  private
    FOptions:   TWbListItemOptions;
    FSelected:  boolean;
    FData:      Variant;
    FView:      TWbIconView;
  protected
    function    GetOptions: TWbListItemOptions; virtual;
    procedure   SetOptions(NewOptions: TWbListItemOptions); virtual;
    procedure   SetSelected(const NewValue: boolean); virtual;
    procedure   ItemSelected; virtual;
    procedure   ItemUnSelected; virtual;

    procedure   SetIconView(const View: TWbIconView);

    procedure   InitializeObject; override;
    procedure   ObjectReady; override;
  public
    property    Options: TWbListItemOptions read GetOptions;
    property    IconView: TWbIconView read FView;

    procedure   Select; virtual;
    procedure   UnSelect; virtual;
    function    CreationFlags: TW3CreationFlags; override;

    property    Selected: boolean read FSelected write SetSelected;
    property    Data: variant read FData write FData;

    property    OnSelected: TNotifyEvent;
    property    OnUnSelected: TNotifyEvent;
  end;
  TWbListItemClass = class of TWbListItem;

  TWbListItems = array of TWbListItem;

  TWbListItemText = class(TWbListItem)
  private
    FText:    TW3Label;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  Text: TW3Label read FText;

    procedure ItemSelected; override;
    procedure ItemUnSelected; override;
  end;

  TWbListItemGroup = class(TWbListItemText)
  private
    FDefHeight: integer := 24;
    procedure SetDefHeight(NewHeight: integer);
  protected
    procedure InitializeObject; override;
    procedure ObjectReady; override;
    procedure StyleTagObject; override;
    procedure Resize; override;
  public
    property  DefaultHeight: integer read FDefHeight write SetDefHeight;
  end;

  TWbListItemIcon = class(TWbListItemText)
  private
    procedure HandleTextChanged(Sender: TObject);
  protected
    procedure InitializeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;

  public
    procedure AdjustSizeToCaption;
  end;
  TWbListItemIconArray = array of TWbListItemIcon;

  TWbListItemFolder = class(TWbListItemIcon)
  end;

  TWbLayoutSelectMask = class(TW3CustomControl)
  protected
    procedure InitializeObject; override;
  end;

  //###########################################################################
  // Layout is pre-calculated. Below are the structures we use to
  // calculate the final layout
  //###########################################################################

  // Represent a single item in the layout
  TCLayoutElement = record
    leNode: TWbListItem;
    leX:  integer;
    leY:  integer;
    class function Create(const Node: TWbListItem;
      const XPos, YPos: integer): TCLayoutElement;
  end;

  // Represents a row of items
  TCLayoutRow = record
    lrWidth:  integer;
    lrHeight: integer;
    lrCols:   array of TCLayoutElement;
    lrYpos:   integer;
    procedure Reset;
    function GetRowRect: TRect;
    class function Create(const Width, Height: integer;
      const ColItems: array of TCLayoutElement): TCLayoutRow;
  end;

  // Represents the full items of rows in the layout
  TCLayout = record
    clRows: array of TCLayoutRow;
    procedure Reset;
  end;

  TWbIconViewItemSelectEvent    = procedure (Sender: TObject; const NewItem, OldItem: TWbListItem);
  TWbIconViewItemClickEvent     = procedure (Sender: TObject; Item: TWbListItem);
  TWbIconViewItemDblClickEvent  = procedure (Sender: TObject; Item: TWbListItem);

  TWbIconViewContent = class(TW3ScrollContent)
  end;

  TWbIconView = class(TW3Scrollbox)
  private
    FSpacing:     integer = 10;
    FStartPos:    TPoint;
    FRangeSelect: boolean;
    FLayout:      TCLayout;
    FSelectMask:  TWbLayoutSelectMask;
    FSelectItem:  TWbListItem;
    FSelected:    TWbListItems;

    procedure SetSpacing(const NewSpacing: integer);
    function  GetSelectMask: TWbLayoutSelectMask;
    function  GetOverlap(const SelRect: TRect): TWbListItems;

    // Event handlers for items
    procedure HandleItemClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure HandleItemDblClick(Sender: TObject);

    // Event handlers for content
    procedure HandleContentChildAdded(Sender: TObject; Child: TW3TagContainer);
    procedure HandleContentMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure HandleContentMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure HandleContentMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);

    function  CalculateLayout: TCLayout;
  protected
    property  SelectMask: TWbLayoutSelectMask read GetSelectMask;

    function  GetScrollContentClass: TW3ScrollContentClass; override;

    procedure MapEventsForItem(const Item: TWbListItem);

    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
    procedure StyleTagObject; override;

  public
    property  SelectedItem: TWbListItem read FSelectItem;
    function  GetItemList: TWbListItems;

    procedure UnSelectAll;
    procedure UnSelectItem(const Item: TWbListItem);

    function  CreationFlags: TW3CreationFlags; override;

    function  AddText(Caption: string): TWbListItemText;
    function  AddIcon(Caption: string): TWbListItemIcon;
    function  AddGroup(Caption: string): TWbListItemGroup;
    function  AddFolder(Caption: string): TWbListItemFolder;

    procedure ClearSelected;
    procedure Clear;

  published
    property  Spacing: integer read FSpacing write SetSpacing;
    property  OnItemSelected: TWbIconViewItemSelectEvent;
    property  OnItemClicked:  TWbIconViewItemClickEvent;
    property  OnItemDblClick: TWbIconViewItemDblClickEvent;
  end;


implementation

//#############################################################################
// TCLayout
//#############################################################################

procedure TCLayout.Reset;
begin
  for var row in clRows do
  begin
    Row.Reset;
  end;
  clRows.Clear();
end;

//#############################################################################
// TCLayoutRow
//#############################################################################

class function TCLayoutRow. Create(const Width, Height: integer;
  const ColItems: array of TCLayoutElement): TCLayoutRow;
begin
  result.lrWidth := Width;
  result.lrHeight := Height;

  // Records are structures, they must be copied since
  // they have no self-construct, thus no references
  for var el in ColItems do
  begin
    result.lrCols.add(el);
  end;
end;

procedure TCLayoutRow.Reset;
begin
  lrWidth := 0;
  lrHeight := 0;
  lrYpos := 0;
  lrCols.Clear();
end;

function TCLayoutRow.GetRowRect: TRect;
const
  _offscreen = 100000;
begin
  result.SetBounds(_offscreen, _offscreen, 0, 0);
  for var x := 0 to lrCols.Count-1 do
  begin
    var col := lrCols[x];
    var LPos := col.leNode.BoundsRect;
    result.left := if LPos.left < result.left then LPos.left else result.left;
    result.top := if LPos.top < result.top then LPos.top else result.top;
    result.right := if LPos.right > result.right then LPos.right else result.right;
    result.bottom := if LPos.bottom > result.bottom then Lpos.bottom else result.bottom;
  end;
end;

//#############################################################################
// TCLayoutElement
//#############################################################################

class function TCLayoutElement.Create(const Node: TWbListItem;
  const XPos, YPos: integer): TCLayoutElement;
begin
  result.leNode := Node;
  result.leX := XPos;
  result.leY := YPos;
end;

//#############################################################################
// TWbListItem
//#############################################################################

procedure TWbListItem.InitializeObject;
begin
  inherited;
  FOptions := [ioAllowSelection];
end;

procedure TWbListItem.ObjectReady;
begin
  inherited;
  if FSelected then
  begin
    FSelected := false;
    if Visible then
      Invalidate;
  end;
end;

function TWbListItem.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfAllowSelection);
  Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfReportMovement);
  Exclude(result, cfKeyCapture);
end;

procedure TWbListItem.ItemSelected;
begin
  if assigned(OnSelected) then
    OnSelected(Self);
end;

procedure TWbListItem.ItemUnSelected;
begin
  if assigned(OnUnSelected) then
    OnUnSelected(self);
end;

procedure TWbListItem.SetIconView(const View: TWbIconView);
begin
  FView := View;
end;

procedure TWbListItem.SetSelected(const NewValue: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (ioAllowSelection in FOptions) then
    begin
      if (csReady in ComponentState) then
      begin
        if NewValue <> FSelected then
        begin
          FSelected := NewValue;

          if FSelected then
            ItemSelected()
          else
            ItemUnSelected();

          if Visible then
            Invalidate();
        end;
      end else
      FSelected := NewValue;
    end;
  end;
end;

procedure TWbListItem.Select;
begin
  if not (csDestroying in ComponentState) then
  begin
    if (ioAllowSelection in FOptions) then
    begin
      if (csReady in ComponentState) then
      begin
        if not FSelected then
          SetSelected(True);
      end else
        FSelected := True;
    end;
  end;
end;

procedure TWbListItem.UnSelect;
begin
  if not (csDestroying in ComponentState) then
  begin
    if (ioAllowSelection in FOptions) then
    begin
      if (csReady in ComponentState) then
      begin
        if FSelected then
          SetSelected(false);
      end else
        FSelected := false;
    end;
  end;
end;

function TWbListItem.GetOptions: TWbListItemOptions;
begin
  result := FOptions;
end;

procedure TWbListItem.SetOptions(NewOptions: TWbListItemOptions);
begin
  // fullsize includes break, so remove that
  if (ioFullSize in NewOptions) then
    Exclude(NewOptions, ioBreak);

  FOptions := NewOptions;
end;


//#############################################################################
// TWbListItemText
//#############################################################################

procedure TWbListItemText.InitializeObject;
begin
  inherited;
  SetOptions([ioAllowSelection]);
  FText := TW3Label.Create(self);
  FText.VAlign := tvTop;
  FText.Background.fromColor(clNone);
  Background.FromColor(clNone);

  TransparentEvents := false;
  SimulateMouseEvents := true;
end;

procedure TWbListItemText.FinalizeObject;
begin
  FText.free;
  inherited;
end;

procedure TWbListItemText.ObjectReady;
begin
  inherited;
  TW3Dispatch.WaitFor([FText], 8, procedure (Success: boolean)
  begin
    if Success then
      Resize();
  end);
end;

procedure TWbListItemText.Resize;
begin
  inherited;
  if FText <> nil then
  begin
    var LBounds := AdjustClientRect(ClientRect);
    FText.SetBounds(LBounds);
  end;
end;

procedure TWbListItemText.ItemSelected;
begin
  Handle.style['background-color'] := ColorToWebStr($0, $0, $0, 100);
  //Background.FromColor(RGBToColor($FF,0,0,33));
  inherited;
end;

procedure TWbListItemText.ItemUnSelected;
begin
  Background.FromColor(clNone);
  inherited;
end;

//#############################################################################
// TWbListItemGroup
//#############################################################################

procedure TWbListItemGroup.InitializeObject;
begin
  inherited;
  SetOptions([ioFullSize]);
end;

procedure TWbListItemGroup.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

procedure TWbListItemGroup.SetDefHeight(NewHeight: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewHeight := TInteger.EnsureRange(NewHeight, 2, 200);
    if NewHeight <> FDefHeight then
    begin
      FDefHeight := NewHeight;
      Height := NewHeight;
    end;
  end;
end;

procedure TWbListItemGroup.ObjectReady;
begin
  inherited;
  Border.Bottom.Width := 2;
  Border.Style := besGroove;
  Background.FromColor(clNone);
  Height := FDefHeight;

  Text.Font.Color := $204D8D;
  //Text.AlignText:= TTextAlign.taLeft;
end;

procedure TWbListItemGroup.Resize;
begin
  inherited;
  {if (csReady in ComponentState) then
  begin
    var LRect := ClientRect;
    var dy := (LRect.Height div 2) - (Text.Height div 2);
    Text.MoveTo(LRect.left, dy);

    var hd := Text.Height;
    inc(hd, Text.Border.GetVSpace() );
    inc(hd, Border.GetVSpace());
    Height := if hd < FDefHeight then FDefHeight else hd;
  end;  }
end;

//#############################################################################
// TWBListItemIcon
//#############################################################################

procedure TWbListItemIcon.InitializeObject;
begin
  inherited;
end;

procedure TWbListItemIcon.ObjectReady;
begin
  inherited;

  Text.Font.Size := 12;
  Text.Font.Color := clWhite;
  Text.OnChanged := @HandleTextChanged;
  Text.AlignText:= TTextAlign.taCenter;
  Text.TextShadow.Shadow(1, 1, 2,clBlack);

  Background.FromColor(clNone);
  Background.Repeat := brNoRepeat;
  Handle.style['background-position'] := 'center top';
  Handle.style['background-size'] := '64px 64px';
end;

procedure TWbListItemIcon.HandleTextChanged(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
    AdjustSizeToCaption();
end;

procedure TWbListItemIcon.AdjustSizeToCaption;
begin
  var LSize := Text.MeasureText(Text.Caption);
  if LSize.tmWidth > ClientWidth then
    Width := LSize.tmWidth + Text.Border.GetHSpace + Border.GetHSpace;
end;

procedure TWbListItemIcon.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
  begin
    //if TW3Dispatch.AssignedAndReady([Text]) then
    //begin

      var LSize := Text.MeasureText(Text.Caption);
      var LBounds := AdjustClientRect(ClientRect);
      LBounds.top := LBounds.bottom - (LSize.tmHeight + Text.Border.GetHSpace + Border.GetHSpace + 4 );

      Text.SetBounds(LBounds);


      { var LBounds := ClientRect;
      var dx := (LBounds.width div 2) - (Text.Width div 2);
      Text.MoveTo(dx, (LBounds.Bottom - Text.Height)-1); }
    //end;
  end;
end;

//#############################################################################
// TWbLayoutSelectMask
//#############################################################################

procedure TWbLayoutSelectMask.InitializeObject;
begin
  inherited;
  ThemeReset();
  Border.Size := 1;
  StyleClass := 'TWbLayoutSelectMask headline marching-ants marching';
end;

//#############################################################################
// TLayoutView
//#############################################################################

procedure TWbIconView.InitializeObject;
begin
  inherited InitializeObject;
  //ScrollController.Enabled := false;
  SimulateMouseEvents := true;
  TransparentEvents := true;

  // Default to indicator
  ScrollBars := sbIndicator;

  content.OnMouseDown := @HandleContentMouseDown;
  content.OnMouseMove := @HandleContentMouseMove;
  content.OnMouseUp := @HandleContentMouseUp;
  Content.OnChildAdded := @HandleContentChildAdded;
end;

procedure TWbIconView.FinalizeObject;
begin
  FSelectMask.free;
  inherited;
end;

function TWbIconView.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfAllowSelection);
end;

procedure TWbIconView.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

function  TWbIconView.GetScrollContentClass: TW3ScrollContentClass;
begin
  result := TWbIconViewContent;
end;

procedure TWbIconView.HandleItemClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if FSelectItem <> nil then
    UnSelectItem(FSelectItem);
  UnSelectAll();

  TWbListItem(Sender).Selected := true;
  FSelectItem := TWbListItem(Sender);

  if assigned(OnItemClicked) then
    OnItemClicked(Self, FSelectItem);
end;

procedure TWbIconView.HandleItemDblClick(Sender: TObject);
begin
  if assigned(OnItemDblClick) then
    OnItemDblClick(self, TWbListItem(sender) );
end;

procedure TWbIconView.MapEventsForItem(const Item: TWbListItem);
begin
  if Item <> nil then
  begin
    Item.OnMouseDown := @HandleItemClick;
    Item.OnDblClick := @HandleItemDblClick;
  end;
end;

function TWbIconView.AddText(Caption: string): TWbListItemText;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    result := TWbListItemText.Create(Content);
    {$IFDEF CREATE_ITEMS_OFFSCREEN}
    result.MoveTo(-2000, -2000);
    {$ENDIF}
    result.Text.Caption := Caption.trim();
    result.SetIconView(self);
    AddToComponentState([csSized]);
    EndUpdate();
  end;
end;

function  TWbIconView.AddGroup(Caption: string): TWbListItemGroup;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    result := TWbListItemGroup.Create(Content);
    {$IFDEF CREATE_ITEMS_OFFSCREEN}
    result.MoveTo(-2000, -2000);
    {$ENDIF}
    result.Text.Caption := Caption.trim();
    result.SetIconView(self);
    AddToComponentState([csSized]);
    EndUpdate();
  end;
end;

function TWbIconView.AddFolder(Caption: string): TWbListItemFolder;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    result := TWbListItemFolder.Create(Content);
    {$IFDEF CREATE_ITEMS_OFFSCREEN}
    result.MoveTo(-2000, -2000);
    {$ENDIF}
    result.Text.Caption := Caption.trim();
    result.SetIconView(self);
    AddToComponentState([csSized]);
    EndUpdate();
  end;
end;

function TWbIconView.AddIcon(Caption: string): TWbListItemIcon;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    result := TWbListItemIcon.Create(Content);
    {$IFDEF CREATE_ITEMS_OFFSCREEN}
    result.MoveTo(-2000, -2000);
    {$ENDIF}
    result.Text.Caption := Caption.trim();
    result.SetIconView(self);
    AddToComponentState([csSized]);
    EndUpdate();
  end;
end;

procedure TWbIconView.SetSpacing(const NewSpacing: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      BeginUpdate();
      FSpacing := TInteger.EnsureRange(NewSpacing, 0, MAX_INT);
      AddToComponentState([csSized]);
      EndUpdate();
    end else
    FSpacing := TInteger.EnsureRange(NewSpacing, 0, MAX_INT);
  end;
end;

function TWbIconView.GetSelectMask: TWbLayoutSelectMask;
begin
  result := FSelectMask;
end;

function TWbIconView.GetOverlap(const SelRect: TRect): TWbListItems;
begin
  var LCount := GetChildCount();

  if LCount > 0 then
  begin
    var LRowIndex := -1;
    var LRowBounds: TRect;

    // Locate first row that the mouse is in, no point checking for
    // cordinates in rows that is not visible
    if FLayout.clRows.Count > 0 then
    begin
      for var y := 0 to FLayout.clRows.count-1 do
      begin
        LRowBounds := FLayout.clRows[y].GetRowRect();
        if (LRowBounds.top >= SelRect.top)
        or (LRowBounds.bottom >= SelRect.top) then
        begin
          LRowIndex := y;
          break;
        end;
      end;


      if LRowIndex > -1 then
      begin
        // Get last visible line in view
        var BottomY := abs(ScrollController.ContentTop) + ClientHeight;

        for var y := LRowIndex to FLayout.clRows.Count-1 do
        begin
          var LRow := FLayout.clRows[y];
          for var x := 0 to LRow.lrCols.Count-1 do
          begin
            if (LRow.lrCols[x].leNode is TWbListItem) then
            begin
              var LChildRect := LRow.lrCols[x].leNode.BoundsRect;
              if (SelRect.Expose(LChildRect) in [esVisible, esPartly]) then
                result.add( TWbListItem(LRow.lrCols[x].leNode) );
            end;
          end;

          var LRowRect := LRow.GetRowRect();
          if LRowRect.top >= BottomY then
            break;
        end;
      end;
    end;
  end;
end;

procedure TWbIconView.Clear;
begin
  if (csReady in ComponentState) then
  begin
    if (csDestroying in ComponentState) then
    begin
      FSelectItem := nil;
      FSelected := [];
      exit;
    end;

    FSelectItem := nil;

    BeginUpdate();
    try
      var LList := GetItemList();
      for var LItem in LList do
      begin
        LItem.free;
      end;
      AddToComponentState([csSized]);
    finally
      FSelected.Clear();
      EndUpdate();
    end;

  end;
end;

procedure TWbIconView.ClearSelected;
begin
  if (csReady in ComponentState) then
  begin
    if not (csDestroying in ComponentState) then
    begin

      if FSelectItem <> nil then
      begin
        if assigned(FSelectItem.OnUnSelected) then
          FSelectItem.OnUnSelected(FSelectItem);
      end;

      if assigned(OnItemSelected) then
        OnItemSelected(self, nil, FSelectItem );

      FSelectItem := nil;
    end;
  end;
end;

function TWbIconView.GetItemList: TWbListItems;
begin
  var LCache := Content.GetChildren();
  if LCache.Count > 0 then
  begin
    for var Candidate in LCache do
    begin
      if Candidate is TWbListItem then
        result.add( TWbListItem(Candidate) );
    end;
  end;
end;

procedure TWbIconView.UnSelectItem(const Item: TWbListItem);
begin
  if Item <> nil then
  begin
    if Item.Selected then
    begin
      /* Perform unselection if this is *the* selected item */
      if Item = FSelectItem then
      begin
        if assigned(OnItemSelected) then
          OnItemSelected(self, nil, FSelectItem);
        FSelectItem := nil;
      end;

      Item.Selected := false;
    end;
  end;
end;

procedure TWbIconView.UnSelectAll;
begin
  if not (csDestroying in ComponentState) then
  begin
    var LLitems := GetItemList();
    for var xi in LLitems do
    begin
      if xi.Selected then
        UnSelectItem(xi);
    end;
    LLitems.Clear();
  end;
end;

procedure TWbIconView.HandleContentChildAdded(Sender: TObject; Child: TW3TagContainer);
begin
  if (child is TWbListItemIcon)
  or (child is TWbListItemText) then
    MapEventsForItem(TWbListItem(Child));
end;

procedure TWbIconView.HandleContentMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  if not w3_getIsMobile() then
  begin
    FRangeSelect := true;
    FStartPos := TPoint.Create(X, Y);
    inc(FStartPos.x, abs(ScrollController.ContentLeft));
    inc(FStartPos.y, abs(ScrollController.ContentTop));


    FSelectMask := TWbLayoutSelectMask.Create(Content);

    FSelectMask.SetBounds(FStartPos.x, FStartPos.y, 1, 1);
    Content.SetCapture();
  end;

  if FSelected.Count > 0 then
    UnSelectAll();
end;

procedure TWbIconView.HandleContentMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
begin
  if FRangeSelect then
  begin
    var LRect: TRect;
    var fx := TPoint.Create(X, Y);
    inc(fx.x, abs(ScrollController.ContentLeft));
    inc(fx.y, abs(ScrollController.ContentTop));

    LRect := TRect.Create(FStartPos.X, FStartPos.Y, fx.X, fx.Y).Normalize();

    FSelectMask.SetBounds(LRect);

    if not LRect.Empty then
    begin

      // Select those within the select rectangle
      var ControlSet := GetOverlap(LRect);
      if ControlSet.length > 0 then
      begin
        for var xx := 0 to ControlSet.Count-1 do
        begin
          Controlset[xx].Selected := true;
        end;
      end;

      // Unselect those that are not in both new selection
      // and older selection
      if FSelected.length > 0 then
      begin
        for var yy := 0 to FSelected.Count-1 do
        begin
          if Controlset.IndexOf( FSelected[yy] ) < 0 then
            FSelected[yy].UnSelect();
        end;
      end;
      FSelected.Clear();

      // Make new selection the current one
      for var LItem in Controlset do
      begin
        FSelected.add(LItem);
      end;
    end;

  end;
end;

procedure TWbIconView.HandleContentMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  // Range select active? Disable
  if FRangeSelect then
  begin
    try
      Content.ReleaseCapture();

      var LRect: TRect;
      var fx := TPoint.Create(X, Y);
      inc(fx.x, abs(ScrollController.ContentLeft));
      inc(fx.y, abs(ScrollController.ContentTop));

      // Nothing selected? Clean "click"
      LRect := TRect.Create(FStartPos.X, FStartPos.Y, fx.X, fx.Y).Normalize();
      if LRect.Empty then
      begin
        if FSelectItem <> nil then
        begin
          FSelectItem.selected := false;
          FSelectItem := nil;
        end;
        UnSelectAll();
      end;

      ScrollController.Enabled := true;
    finally
      //writeln("Releasing select mask");
      FSelectMask.free;
      FSelectMask := nil;

      FRangeSelect := false;
    end;
  end;
end;

function TWbIconView.CalculateLayout: TCLayout;
var
  RowStack: array of TCLayoutElement;

  procedure StackToRow(const RowWidth, RowHeight, Ypos: integer);
  begin
    var NewRow := TCLayoutRow.Create(RowWidth, RowHeight, RowStack);
    NewRow.lrYpos := Ypos;
    result.clRows.add( NewRow );
    RowStack.Clear();
  end;

begin

  var IconList := GetItemList();
  if IconList.Count > 0 then
  begin
    var LRowHeight := 0;
    var LRowWidth := 0;
    var x := 0;
    var dx := FSpacing;
    var dy := FSpacing;

    while x < IconList.Count do
    begin
      var IconItem := IconList[x];
      var ItemBounds := IconItem.BoundsRect;

      LRowWidth := IconItem.Width;
      if x < IconList.High then
        inc(LRowWidth, FSpacing);

      // Does icon force to be last in row?
      if (ioBreak in IconItem.Options) then
      begin
        if ItemBounds.Height > LRowHeight then
          LRowHeight := ItemBounds.Height;

        RowStack.add( TCLayoutElement.Create(IconItem, dx, dy) );
        StackToRow(LRowWidth, LRowHeight, dy);

        // reset X and update Y
        dx := FSpacing;
        inc(dy, LRowHeight);
        inc(dy, FSpacing);

        LRowHeight := 0;
        LRowWidth := 0;

        inc(x);
        continue;
      end;

      // Child should scale to full width?
      if (ioFullSize in IconItem.Options) then
      begin
        // Anything collected on the stack?
        // if so we push that to a row and start a new row
        // with this item since it demands a whole row to itself
        if RowStack.Count > 0 then
          StackToRow(LRowWidth, LRowHeight, dy);

        // reset X and update Y
        dx := FSpacing;
        inc(dy, LRowHeight);
        inc(dy, FSpacing);

        LRowHeight := ItemBounds.Height;
        LRowWidth := Content.ClientWidth;

        // OK, lets put our princess here into its own row
        // because .. well, la di da
        RowStack.add( TCLayoutElement.Create(IconItem, dx, dy) );
        inc(dy, ItemBounds.Height);
        inc(dy, FSpacing);

        StackToRow(LRowWidth, LRowHeight, dy);

        // Reset rowheight (we already know it since there is
        // only one posh prick living there) and push stack to a row
        LRowHeight := 0;
        LRowWidth := 0;

        inc(x);
        continue;
      end;

      // Will adding this item to the row go beyond the right edge?
      if dx + ItemBounds.Width + FSpacing > Content.ClientWidth then
      begin
        // Sadly yes, so push what we have on the stack
        // to a row before we do anything with it
        StackToRow(LRowWidth, LRowHeight, dy);

        // reset X and update Y
        dx := FSpacing;
        inc(dy, LRowHeight);
        inc(dy, FSpacing);

        LRowHeight := 0;
        LRowWidth := 0;
      end;

      // Is the height of this item larger than other items
      // in this row? If so, we use the largest size
      if ItemBounds.Height > LRowHeight then
        LRowHeight := ItemBounds.Height;

      // Collect the item onto our stack and keep going
      // until a row has been filled (its a loop after all)
      RowStack.add( TCLayoutElement.Create(IconItem, dx, dy) );
      inc(dx, ItemBounds.Width);

      // Avoid adding any right-spacing if this is the last item
      if x < high(IconList) then
        inc(dx, FSpacing);

      // Next please
      inc(x);
    end;

    // Anything left on the stack that havent
    // been pushed to a row? OK deal with it
    if RowStack.Count > 0 then
      StackToRow(LRowWidth, LRowHeight, dy);

  end;
end;

procedure TWbIconView.Resize;
begin
  inherited;
  if not (csDestroying in ComponentState) then
  begin

    var TotalHeight := 0;

    // Reset existing layout
    if FLayout.clRows.Count > 0 then
      FLayout.Reset();

    // Calculate new layout
    FLayout := CalculateLayout();

    if FLayout.clRows.Count > 0 then
    begin

      for var LRow in FLayout.clRows do
      begin
        inc(TotalHeight, LRow.lrHeight);

        for var y :=0 to LRow.lrCols.Length-1 do
        begin
          var LItem := LRow.lrCols[y].leNode;

          if (ioFullSize in LItem.Options) then
          begin
            LItem.SetBounds(
              LRow.lrCols[y].leX,
              LRow.lrCols[y].leY,
              (clientwidth - (LRow.lrCols[y].leX * 2) - (FSpacing * 2)),
              LItem.Height);
          end else
          LItem.MoveTo(LRow.lrCols[y].leX, LRow.lrCols[y].leY);

          var ItemBounds := LItem.BoundsRect;
          if ItemBounds.Bottom > TotalHeight then
            TotalHeight := ItemBounds.Bottom;
        end;
      end;
    end;

    if TotalHeight < Container.ClientHeight then
      TotalHeight := Container.ClientHeight;

    Content.SetSize(Container.ClientWidth, TotalHeight);
    if ScrollController <> nil then
      ScrollController.Refresh;

  end;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #'
    .marching-ants {
      background-size: 20px 1px, 20px 1px, 1px 20px, 1px 20px;
      background-position: 0 0,  0 100%,  0 0,  100% 0;
      background-repeat: repeat-x,  repeat-x,  repeat-y,  repeat-y;
      -webkit-animation: marching-ants-1 1s;
              animation: marching-ants-1 1s;
      -webkit-animation-timing-function: linear;
              animation-timing-function: linear;
      -webkit-animation-iteration-count: infinite;
              animation-iteration-count: infinite;
      -webkit-animation-play-state: paused;
              animation-play-state: paused;
    }
    .marching-ants:hover, .marching-ants.marching {
      -webkit-animation-play-state: running;
              animation-play-state: running;
    }
    .marching-ants.reverse {
      -webkit-animation-direction: reverse;
              animation-direction: reverse;
    }
    .marching-ants.bnw {
      background-image: -webkit-linear-gradient(left, #fff 50%, #000 50%), -webkit-linear-gradient(left, #fff 50%, #000 50%), -webkit-linear-gradient(top, #fff 50%, #000 50%), -webkit-linear-gradient(top, #fff 50%, #000 50%);
      background-image: linear-gradient(to right, #fff 50%, #000 50%), linear-gradient(to right, #fff 50%, #000 50%), linear-gradient(to bottom, #fff 50%, #000 50%), linear-gradient(to bottom, #fff 50%, #000 50%);
    }
    .marching-ants.headline {
      background-image: -webkit-linear-gradient(left, #fff 50%, #444 50%), -webkit-linear-gradient(left, #fff 50%, #444 50%), -webkit-linear-gradient(top, #fff 50%, #444 50%), -webkit-linear-gradient(top, #fff 50%, #444 50%);
      background-image: linear-gradient(to right, #fff 50%, #444 50%), linear-gradient(to right, #fff 50%, #444 50%), linear-gradient(to bottom, #fff 50%, #444 50%), linear-gradient(to bottom, #fff 50%, #444 50%);
      color: #fff;
    }
    .marching-ants.info {
      background-image: -webkit-linear-gradient(left, #dd2 50%, transparent 50%), -webkit-linear-gradient(left, #dd2 50%, transparent 50%), -webkit-linear-gradient(top, #dd2 50%, transparent 50%), -webkit-linear-gradient(top, #dd2 50%, transparent 50%);
      background-image: linear-gradient(to right, #dd2 50%, transparent 50%), linear-gradient(to right, #dd2 50%, transparent 50%), linear-gradient(to bottom, #dd2 50%, transparent 50%), linear-gradient(to bottom, #dd2 50%, transparent 50%);
      background-color: #ffa;
      color: #dd2;
    }
    .marching-ants.warning {
      background-size: 40px 4px, 40px 4px, 4px 40px, 4px 40px;
      background-position: 0 0,  0 100%,  0 0,  100% 0;
      background-repeat: repeat-x,  repeat-x,  repeat-y,  repeat-y;
      -webkit-animation: marching-ants-2 2s;
              animation: marching-ants-2 2s;
      -webkit-animation-timing-function: linear;
              animation-timing-function: linear;
      -webkit-animation-iteration-count: infinite;
              animation-iteration-count: infinite;
      -webkit-animation-play-state: paused;
              animation-play-state: paused;
      background-image: -webkit-linear-gradient(left, #f00 50%, #fff 50%), -webkit-linear-gradient(left, #f00 50%, #fff 50%), -webkit-linear-gradient(top, #f00 50%, #fff 50%), -webkit-linear-gradient(top, #f00 50%, #fff 50%);
      background-image: linear-gradient(to right, #f00 50%, #fff 50%), linear-gradient(to right, #f00 50%, #fff 50%), linear-gradient(to bottom, #f00 50%, #fff 50%), linear-gradient(to bottom, #f00 50%, #fff 50%);
      color: #a00;
      background-color: #faa;
    }
    .marching-ants.warning:hover, .marching-ants.warning.marching {
      -webkit-animation-play-state: running;
              animation-play-state: running;
    }
    .marching-ants.warning.reverse {
      -webkit-animation-direction: reverse;
              animation-direction: reverse;
    }

    @-webkit-keyframes marching-ants-1 {
      0% {
        background-position: 0 0,  0 100%,  0 0,  100% 0;
      }
      100% {
        background-position: 40px 0, -40px 100%, 0 -40px, 100% 40px;
      }
    }

    @keyframes marching-ants-1 {
      0% {
        background-position: 0 0,  0 100%,  0 0,  100% 0;
      }
      100% {
        background-position: 40px 0, -40px 100%, 0 -40px, 100% 40px;
      }
    }
    @-webkit-keyframes marching-ants-2 {
      0% {
        background-position: 0 0,  0 100%,  0 0,  100% 0;
      }
      100% {
        background-position: 40px 0, -40px 100%, 0 -40px, 100% 40px;
      }
    }
    @keyframes marching-ants-2 {
      0% {
        background-position: 0 0,  0 100%,  0 0,  100% 0;
      }
      100% {
        background-position: 40px 0, -40px 100%, 0 -40px, 100% 40px;
      }
    }
    @-webkit-keyframes marching-ants-3 {
      0% {
        background-position: 0 0,  0 100%,  0 0,  100% 0;
      }
      100% {
        background-position: 40px 0, -40px 100%, 0 -40px, 100% 40px;
      }
    }
    @keyframes marching-ants-3 {
      0% {
        background-position: 0 0,  0 100%,  0 0,  100% 0;
      }
      100% {
        background-position: 40px 0, -40px 100%, 0 -40px, 100% 40px;
      }
    }

    .TWbListItemIcon {
      background-position: center top;
    }

    .TWbLayoutSelectMask {
      margin: 0px !important;
      padding: 0px !important;
    }

    .TWbIconView {
      border: 1px solid #fff;
      border-radius: 0;

      background-color: #D6D6D6;

      overflow-x: hidden;
      overflow-y: hidden;

      border-top: 1px solid #979797;
      border-left: 1px solid #979797;
      border-bottom: 1px solid #E0E0E0;
      border-right: 1px solid #E0E0E0;

      outline: 1px solid #42415A;
      outline-offset: 0px;
      outline-bottom: 1px solid #000;
    	-webkit-user-select: auto;
    }';
  StyleCode := StrReplace(StyleCode,"§",prefix);
  Sheet.Append(StyleCode);
end;


end.
