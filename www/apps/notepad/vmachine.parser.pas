unit vmachine.parser;

interface

{$I 'vmachine.inc'}

{.$DEFINE DEBUG_OUT}
{.$DEFINE DEBUG_LIST_PARAMS}

uses 
  SmartCL.System,
    System.Objects
  , System.Types
  , System.Types.Convert
  , System.Text.Parser
  , vmachine.data
  , vmachine.Types
  , vmachine.model
  , vmachine.assembler;

type

  TAsmFileParserGetIncludeFileEvent = procedure (Sender: TObject;
    Filename: string; var Text: string; var Error: EAssemblerError);

  TAsmDataBlockParser = class(TCustomParser)
  private
    FContext:   TAsmParserContext;
    FModel:     TAsmModelSourceFile;

    function    GetText: string;
    procedure   SetText(const NewText: string);

    procedure   ParseDcS(const Buffer: TTextBuffer);
    procedure   ParseDcB(const Buffer: TTextBuffer);
    procedure   ParseDcW(const Buffer: TTextBuffer);
    procedure   ParseDcL(const Buffer: TTextBuffer);

  public
    property    Text: string read GetText write SetText;
    property    Context: TAsmParserContext read FContext;
    property    Model: TAsmModelSourceFile read FModel;

    function    Parse: boolean; override;

    constructor Create(const Context: TAsmParserContext); reintroduce;
  end;

  TAsmFileParser = class(TCustomParser)
  private
    FContext:   TAsmParserContext;
    FModel:     TAsmModelSourceFile;

    function    GetText: string;
    procedure   SetText(const NewText: string);

    procedure   ParseDataDeclaration(const Buffer: TTextBuffer);
    procedure   ParseExportDeclaration(const Buffer: TTextBuffer);
    procedure   ParseExportOrData(const Buffer: TTextBuffer);

    procedure   ParseInclude(const Buffer: TTextBuffer);
    procedure   ParseLabel(const Buffer: TTextBuffer);
    procedure   ParseInstruction(const Buffer: TTextBuffer);
    procedure   ParseDeclaredConstant(const Buffer: TTextBuffer);
  public
    property    Context: TAsmParserContext read FContext;
    property    Model: TAsmModelSourceFile read FModel;
    property    Text: string read GetText write SetText;
    property    Filename: string;

    function    ExportModel: TAsmModelSourceFile;

    function    Parse: boolean; override;

    constructor Create(Filename: string); reintroduce; virtual;
    destructor  Destroy; override;
  published
    property    OnGetIncludeFile: TAsmFileParserGetIncludeFileEvent;
  end;

implementation

//##########################################################################
// TAsmFileParser
//##########################################################################

constructor TAsmFileParser.Create(Filename: string);
begin
  inherited Create(nil);
  FContext := TAsmParserContext.Create;
  FModel := TAsmModelSourceFile.Create(nil);
  FModel.Context := FContext;
  FContext.Model := FModel;
  Self.Filename := Filename;
  FModel.Filename := Filename;
  SetContext(FContext);
end;

destructor TAsmFileParser.Destroy;
begin
  FModel.free;
  FContext.free;
  inherited;
end;

function TAsmFileParser.ExportModel: TAsmModelSourceFile;
begin
  result := FModel;
  FContext := TAsmParserContext.Create;
  FModel := TAsmModelSourceFile.Create(nil);
  FModel.Context := FContext;
  FContext.Model := FModel;
  SetContext(FContext);
end;

function TAsmFileParser.GetText: string;
begin
  result := Context.Buffer.CacheData;
end;

procedure TAsmFileParser.SetText(const NewText: string);
begin
  if NewText <> GetText() then
    Context.buffer.LoadBufferText(NewText);
end;

procedure TAsmFileParser.ParseLabel(const Buffer: TTextBuffer);
var
  LabName: string;
begin
  if buffer.Next() then
  begin
    if buffer.ReadWord(LabName) then
    begin
      // Make sure label is delimited with a ":" after
      Buffer.ConsumeJunk();
      if Buffer.current <> ':' then
      begin
        {$IFDEF DEBUG_OUT}
        writeln(buffer.trail);
        {$ENDIF}
        SetLastError('Syntax error: labels must be terminated with ":" ');
        exit;
      end;

      { Its imperative that we keep track of our labels.
        But during the parsing process it's impossible to know
        the *true offset* of a label. So we simply register the label
        and the line-nr it's found on for now. Later during code
        emission we update the labels with correct offset and
        patch each jump / branch instruction }
      Model.AddLabel(LabName, Buffer.Row);

      { Instead of testing for labels everywhere, we inject a label
        marker on instruction level. This is just to tell the codegen
        to update the offset(s) in the jump table. This instruction
        generates no data and is purely for internal use }
      var LabelInstruction: TAsmInstruction;
      LabelInstruction.aiOpcode := TCPUInstruction.ocLabel;
      LabelInstruction.aiLineNr := Model.LabelCount-1;
      FModel.AddInstruction(LabelInstruction);

      // Move beyond ":" but dont eat EOL or sink EOF
      // Labels can be on the same line as an instruction
      Buffer.NextNoCrLf();

    end else
    SetLastError('Failed to read label-name: ' + buffer.LastError);
  end;
end;

procedure TAsmFileParser.ParseDataDeclaration(const Buffer: TTextBuffer);
begin
  // Eat any spaces between @export and "name"
  Buffer.ConsumeJunk();

  // first char should now be "
  if buffer.Current <> '"' then
  begin
    SetLastError('Syntax error: expected quoted name after @data statement');
    exit;
  end;

  // Read the name / identifier
  var LIdent := buffer.ReadQuotedString();
  if LIdent.length < 1 then
  begin
    SetLastError('Syntax error: @data name not properly quoted');
    exit;
  end;

  // Eat any spaces between "name" and =
  Buffer.ConsumeJunk();
  if Buffer.Current <> '=' then
  begin
    SetLastError('Syntax error: expected assign (=) in @data declaration');
    exit;
  end else
    Buffer.Next();

  buffer.ConsumeJunk();
  if Buffer.Current <> '{' then
  begin
    SetLastError('Syntax error: expected { in @data declaration');
    exit;
  end else
    buffer.Next();

  buffer.ConsumeJunk();

  var LDataDef: string := '';
  if buffer.ReadTo('}', LDataDef) then
  begin

    FModel.AddNamedData(LIdent, LDataDef);

    writeln(LIdent + ' data registered');

    buffer.next;
  end;
end;

procedure TAsmFileParser.ParseExportOrData(const Buffer: TTextBuffer);
begin
  if buffer.Next() then
  begin
    var Temp := '';
    if buffer.ReadWord(Temp) then
    begin
      // Make sure @ is followed by "include"
      temp := temp.LowerCase();

      case temp of
      'export': ParseExportDeclaration(Buffer);
      'data':   ParseDataDeclaration(Buffer);
      else
        begin
          SetLastError('Syntax error: expected @export or @data keyword');
          exit;
        end;
      end;

    end else
    SetLastError('Failed to read export declaration: ' + buffer.LastError);
  end;
end;

procedure TAsmFileParser.ParseExportDeclaration(const Buffer: TTextBuffer);
begin
  var Temp := '';

  // Eat any spaces between #export and "label"
  Buffer.ConsumeJunk();

  // first char should now be "
  if buffer.Current <> '"' then
  begin
    SetLastError('Syntax error: expected quoted label after @export statement');
    exit;
  end;

  // Read the filename
  var labelname := buffer.ReadQuotedString();
  if labelname.length < 1 then
  begin
    SetLastError('Syntax error: label not quoted correctly in @export statement ');
    exit;
  end;

  // read "as"
  temp:= '';
  buffer.ConsumeJunk();
  if not Buffer.ReadWord(Temp) then
  begin
    SetLastError('Syntax error: expected keyword <as> following label-name error');
    exit;
  end;

  // validate "as"
  Temp := Temp.ToLower().Trim();
  if temp <> 'as' then
  begin
    SetLastError('Syntax error: expected keyword <as> following label-name error');
    exit;
  end;

  buffer.ConsumeJunk();
  if buffer.current <> '"' then
  begin
    SetLastError('Syntax error: expected quoted export-name error');
    exit;
  end;

  // Get the export name
  var ExportName := buffer.ReadQuotedString();
  ExportName := ExportName.trim();

  // now find the first "(" in (void | e[x] as name, e[x] as name)
  buffer.ConsumeJunk();
  if buffer.current <> '(' then
  begin
    SetLastError('Syntax error: expected parameter list or (void) in export statement');
    exit;
  end;

  // Skip over "("
  Buffer.Next();

  // Parse the paramlist, this terminates when it hits ")", then skip
  var tardis := TAsmToolbox.ReadParamList(Buffer);

end;

procedure TAsmFileParser.ParseInclude(const Buffer: TTextBuffer);
begin
  if buffer.Next() then
  begin
    var Temp := '';
    if buffer.ReadWord(Temp) then
    begin
      // Make sure # is followed by "include"
      temp := temp.LowerCase();

      if temp <>'include' then
      begin
        SetLastError('Syntax error: expected "include" keyword after # character');
        exit;
      end;

      var LOffset := Buffer.Offset;

      // Eat any spaces between #include and "filename"
      Buffer.ConsumeJunk();

      // first char should now be "
      if buffer.current <> '"' then
      begin
        SetLastError('Syntax error: expected quoted filename after #include statement');
        exit;
      end;

      // Read the filename
      var IncFilename := buffer.ReadQuotedString();
      if IncFilename.length < 1 then
      begin
        SetLastError('Syntax error: failed to parse #include statement, filename not quoted properly');
        exit;
      end;

      if not assigned(OnGetIncludeFile) then
      begin
        SetLastError('No event handler set of include-file loader error');
        exit;
      end;

      Buffer.ReadToEOL();

      var Success := true;
      var NewText := '';
      var Error: EAssemblerError;
      OnGetIncludeFile(self, IncFilename, NewText, Error);

      if Error <> nil then
      begin
        SetLastError(error.message);
        exit;
      end;

      // First parameter is the filename
      var Lparam : TAsmSymbolInfo;
      LParam.SymbolType := TAsmSymbolType.cstString;
      LParam.SymbolValue := IncFilename;
      LParam.SymbolPTR := false;

      var LData: TAsmInstruction;
      LData.aiOpcode := TCPUInstruction.ocInclude;
      LData.aiOffset := LOffset;
      LData.aiLineNr := Buffer.Row;
      LData.aiParams.add(Lparam);

      FModel.AddInstruction(LData);

      var LSubParser := TAsmFileParser.create(IncFilename);
      try
        LSubParser.OnGetIncludeFile := @OnGetIncludeFile;
        LSubParser.SetText(NewText);
        if LSubParser.Parse() then
        begin
          // Export the model generated by the parser
          var LModel := LSubParser.ExportModel();

          // Register with our model
          TAsmModelSourceFile(FModel).registerIncludeFile(LModel);
        end else
        begin
          SetLastError(LSubParser.LastError);
          writeln("Trail ->" + LSubParser.Context.buffer.Trail);
          exit;
        end;
      finally
        LSubParser.free;
      end;

    end else
    SetLastError('Failed to read include declaration: ' + buffer.LastError);
  end;
end;

procedure TAsmFileParser.ParseDeclaredConstant(const Buffer: TTextBuffer);
var
  ConstName:  string;
  ConstValue: string;
  ConstEqu:   string;
begin
  Buffer.ConsumeJunk();

  // Read name of constant
  if Buffer.ReadWord(ConstName) then
  begin
    Buffer.ConsumeJunk();

    // Read EQU def
    if Buffer.ReadWord(constEqu) then
    begin
      ConstEqu := constEqu.Trim().ToLower();
      if ConstEqu <> 'equ' then
      begin
        SetLastError('Syntax error, expected EQU for constant declaration error');
        exit;
      end;

      // Read textual Value
      Buffer.ConsumeJunk();
      if buffer.ReadToEOL(constValue) then
      begin
        constName := ConstName.ToLower().trim();
        constValue := ConstValue.trim();

        // Add textual representation to model
        FModel.AddNamedConst(constName, ConstValue);
      end else
      SetLastError('Invalid constant declaration, expected value error');
    end else
    SetLastError('Invalid constant declaration, expected EQU assignment error');
  end else
  SetLastError('Invalid constant declaration, expected name error');
end;

procedure TAsmFileParser.ParseInstruction(const Buffer: TTextBuffer);
var
  ParamList: TStrArray;
  InstrOpcode: integer;
  InstructionData: TAsmInstruction;
begin
  // Skip junk
  Buffer.ConsumeJunk;

  // Is this the EOF?
  if Buffer.EOF then
  begin
    // Since we consume junk before we hit this spot, an EOF
    // is not a bug. We used to throw an error here, but
    // its actually fully legal, so we just exit
    exit;
  end;

  // Bookmark position
  var mark := Buffer.Bookmark();

  // Read the instruction
  var command := '';
  if not Buffer.ReadWord(Command) then
  begin
    SetLastError('Failed to read instruction: ' + Buffer.LastError);
    exit;
  end else
  command := Command.ToLower().Trim();

  if Buffer.NextNonControlText('equ') then
  begin
    // Its a const declaration, jump back
    // and let the const-parser deal with it
    Buffer.Restore(mark);
    ParseDeclaredConstant(Buffer);
    exit;
  end else
  begin
    Mark.free;
    mark := nil;
  end;

  // Make sure this instruction is valid
  InstrOpcode := TAsmToolbox.ValidateInstruction(Command);
  if InstrOpcode < 0 then
  begin
    SetLastErrorF('Syntax error: Unknown instruction %s error',[Command]);
    exit;
  end;

  var ParamCount := TAsmToolbox.GetInstructionParamsFor(Command);
  if ParamCount > 0 then
  begin
    ParamList := TAsmToolbox.ReadCommaList(buffer);

    {$IFDEF DEBUG_LIST_PARAMS}
    writeln("Listing found params:");
    for var item in ParamList do
    begin
      writeln("Param:" + item);
    end;
    writeln(" ");
    {$ENDIF}

    // Failed? Set error and exit
    if Failed then
    begin
      SetLastErrorF('Failed to read parameters for %s error', [Command]);
      exit;
    end;

    // Make sure param count matches
    if ParamList.Count <> ParamCount then
    begin
      {$IFDEF DEBUG_LIST_PARAMS}
      writeln("Listing found params:");
      for var item in ParamList do
      begin
        writeln("   Param: " + item);
      end;
      writeln(" ");
      {$ENDIF}
      SetLastErrorF('Syntax error: expected %d params for instruction %s, not %d', [ParamCount, Command, ParamList.Count]);
      exit;
    end;

    (* A little recap of where we are:
       We have now mapped the instruction to its
       bytecode, which is held in the InstrOpcode
       variable.
       Next step is to recognize and validate the
       parameters for the instruction.

       Please note: we give some allowances when it comes
       to constants. At this point there is no way to
       explicitly validate if a constant has been defined
       and registered. But that will change in the future.
       So for now we check for all manner of supported
       notations, like hex and binary, clean
       integers, # prefixed non signed integers, string
       types etc. (see below).
       But when we face a "#abc_def" style notation that
       validates as a constant, we pick it up and allow it
       for now. It will be checked later. *)

    //var InstructionData: TAsmInstruction;
    InstructionData.aiOpcode := TCPUInstruction(InstrOpcode);
    InstructionData.aiLineNr := Buffer.Row;

    if InstructionData.aiOpcode in [ocJSR, ocJMP, ocBNE, ocBEQ, ocBLE, ocBGT] then
    begin
      if ParamList.count = 1 then
      begin
        var parItem := ParamList[0];

        if TAsmToolbox.ValidLabelChars(parItem) then
        begin
          var info: TAsmSymbolInfo;
          info.SymbolType := TAsmSymbolType.cstLabel;
          info.SymbolValue := parItem;
          info.SymbolPTR := false;
          InstructionData.aiParams.add(Info);
        end else
          SetLastErrorF('Invalid parameter, expected label on line #%d', [buffer.row]);
      end else
        SetLastErrorF('Invalid parameter, expected single label on line #%d', [Buffer.Row]);

    end else
    begin
      for var parItem in ParamList do
      begin
        var info := FContext.GetSymbolInfo(parItem);
        if info.SymbolType = cstInvalid then
        begin
          SetLastErrorF('Invalid parameter error: %s', [parItem]);
          break;
        end else
        InstructionData.aiParams.add(Info);
      end;
    end;

    // Each param valid? OK, add the instruction
    // to our code buffer
    if not Failed then
      FModel.AddInstruction(InstructionData);

  end else
  begin
    // no params
    InstructionData.aiOpcode := TCPUInstruction(InstrOpcode);
    FModel.AddInstruction(InstructionData);
    Buffer.ConsumeJunk;
  end;
end;

function TAsmFileParser.Parse: boolean;
var
  OldPos: TTextBufferBookmark;
  NewPos: TTextBufferBookmark;
  Buffer: TTextBuffer;
begin
  Buffer := Context.Buffer;

  if Buffer.Empty then
  begin
    SetLastError('Failed to parse file, buffer is empty error');
    exit;
  end;

  Buffer.First();

  try
    //repeat
    while not Buffer.EOF do
    begin
      // Snapshot our position before a parse round
      OldPos := Buffer.GetCurrentLocation();
      try

        // skip any non-control characters, tabs, remarks
        Buffer.ConsumeJunk();

        // Parse based on first char on each line.
        // Subsequent sub-parsers take over at various junctions
        case Buffer.Current of
        CNT_PREFIX_INCLUDE: ParseInclude(Buffer);
        CNT_PREFIX_EXPORT:  ParseExportOrData(Buffer);
        CNT_PREFIX_LABEL:   ParseLabel(Buffer);

        // Handle pascal and C style remarks
        '/':  if (buffer.Peek in ['/', '*']) then
                Buffer.ConsumeJunk();

        // Handle second pascal remark type
        '(':  if Buffer.Peek = '*' then
                Buffer.ConsumeJunk();

        ';':
          begin
            Buffer.ReadToEOL();
            Buffer.ConsumeJunk();
          end;

        else
          ParseInstruction(Buffer);

        end;

        // Did any parser operation above fail?
        // If so just break and a syntax error will be issued
        if Failed then
          break;

        // Compare position after a parse round. If its
        // is unchanged then we need to move it
        NewPos := Buffer.GetCurrentLocation();
        try
          if OldPos.Equals(NewPos) then
            Buffer.Next();
        finally
          NewPos.free;
        end;
      finally
        OldPos.free;
      end;
    end;
  except
    on e: exception do
      SetLastError('Internal error: ' + e.message);
  end;

  {$IFDEF DEBUG_OUT}
  if failed then
  begin
    Writeln('*** Failed');
    writeln('*** Trail:' + Buffer.Trail);
  end;
  {$ENDIF}
  result := not failed;
end;



//##########################################################################
// TAsmDataBlockParser
//##########################################################################

constructor TAsmDataBlockParser.Create(const Context: TAsmParserContext);
begin
  inherited Create(nil);
  FContext := Context;
  FModel := TAsmModelSourceFile(Context.Model);
  SetContext(FContext);
end;

function TAsmDataBlockParser.GetText: string;
begin
  result := Context.Buffer.CacheData;
end;

procedure TAsmDataBlockParser.SetText(const NewText: string);
begin
  if NewText <> GetText() then
    Context.Buffer.LoadBufferText(NewText);
end;

procedure TAsmDataBlockParser.ParseDcS(const Buffer: TTextBuffer);
begin
end;

procedure TAsmDataBlockParser.ParseDcB(const Buffer: TTextBuffer);
begin
end;

procedure TAsmDataBlockParser.ParseDcW(const Buffer: TTextBuffer);
begin
end;

procedure TAsmDataBlockParser.ParseDcL(const Buffer: TTextBuffer);
begin
end;

function TAsmDataBlockParser.Parse: boolean;
var
  OldPos: TTextBufferBookmark;
  NewPos: TTextBufferBookmark;
  Buffer: TTextBuffer;
  LTemp: string;
begin
  Buffer := Context.Buffer;

  if Buffer.Empty then
  begin
    SetLastError('Failed to parse file, buffer is empty error');
    exit;
  end;

  Buffer.First();

  try
    while not Buffer.EOF do
    begin
      // Snapshot our position before a parse round
      OldPos := Buffer.GetCurrentLocation();
      try

        // skip any non-control characters, tabs, remarks
        Buffer.ConsumeJunk();

        // Parse based on first char on each line.
        // Subsequent sub-parsers take over at various junctions
        case Buffer.Current of
        // Handle pascal and C style remarks
        '/':  if (buffer.Peek in ['/', '*']) then
                Buffer.ConsumeJunk();

        // Handle second pascal remark type
        '(':  if Buffer.Peek = '*' then
                Buffer.ConsumeJunk();
        '.':
          begin
            LTemp := LTemp.ToLower().Trim();
            if LTemp = 'dc' then
            begin
              var LSize := Buffer.Peek();
              if LSize in ['b', 'w', 'l'] then
                buffer.Next();

              case LSize of
              'b': self.ParseDcB(Buffer);
              'w': self.ParseDcW(Buffer);
              'c': self.ParseDcL(Buffer);
              's': self.ParseDcS(Buffer);
              else
                begin
                  SetLastError('Syntax error, expected dc.[b, w, l] error');
                  break;
                end;
              end;

            end else
            begin
              SetLastError('Syntax error, expected dc.[b, w, l] error');
              break;
            end;
          end;

        else
          LTemp += Buffer.Current;
        end;

        // Did any parser operation above fail?
        // If so just break and a syntax error will be issued
        if Failed then
          break;

        // Compare position after a parse round. If its
        // is unchanged then we need to move it
        NewPos := Buffer.GetCurrentLocation();
        try
          if OldPos.Equals(NewPos) then
            Buffer.Next();
        finally
          NewPos.free;
        end;
      finally
        OldPos.free;
      end;
    end;
  except
    on e: exception do
      SetLastError('Internal error: ' + e.message);
  end;

  {$IFDEF DEBUG_OUT}
  if failed then
  begin
    Writeln('*** Failed');
    writeln('*** Trail:' + Buffer.Trail);
  end;
  {$ENDIF}
  result := not failed;
end;


end.
