unit vmachine.model;

interface

{$I 'vmachine.inc'}

{.$DEFINE DEBUG_OUT}

uses 
  {$IFDEF DEBUG_OUT}
    SmartCL.System,
  {$ENDIF}
    W3C.TypedArray
  , System.Types
  , System.Types.Convert
  , System.Streams
  , System.Stream.Reader
  , System.Stream.Writer
  , System.Memory.Buffer
  , System.Text.Parser
  , vmachine.types
  , vmachine.data
  ;

type

  // Forward declarations
  TAsmParserContext   = class;
  TAsmModelLabel      = class;
  TAsmModelNamedConst = class;
  TAsmModelNamedData  = class;
  TAsmModelFiles      = class;
  TAsmModelSourceFile = class;

  // Exception declarations
  EAsmParserContext = class(EAssemblerError);

  TAsmParserContext = class(TParserContext)
  public
    function    GetSymbolInfo(Text: string): TAsmSymbolInfo;
    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TAsmModelFiles = class(TObject)
  private
    FObjects:   array of TAsmModelSourceFile;
  public
    property    Items[index: integer]: TAsmModelSourceFile read (FObjects[index]);
    property    Count: integer read (FObjects.Count);

    procedure   &Register(File: TAsmModelSourceFile);
    function    Find(Filename: string): TAsmModelSourceFile; overload;
    function    Find(const File: TAsmModelSourceFile): TAsmModelSourceFile; overload;

    procedure   Clear;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TAsmModelLabel = class(TParserModelObject)
  public
    property  Name: string;
    property  LineNr: integer;
    property  ByteOffset: integer;
  end;

  TAsmModelNamedConst = class(TParserModelObject)
  public
    property  Name: string;
    property  TextValue: string;
    property  Value: variant;
    property  LineNr: integer;
    property  &Index: integer;
  end;

  TAsmModelNamedDataOrigin = (noInline, noFile);

  TAsmModelNamedData = class(TParserModelObject)
  private
    FRawData:   TBinaryData;
  public
    property    DataOrigin: TAsmModelNamedDataOrigin;   // inline "dc.b xyz" or filename?
    property    Name: string;                           // name of the resource chunk
    property    DataText: string;                       // Data text (only applies is Origin = noInline)
    property    DataBuffer: TBinaryData read FRawData;  // Data in binary form

    constructor Create(const AParent: TParserModelObject); override;
    destructor  Destroy; override;
  end;

  TAsmModelSourceFile = class(TParserModelObject)
  private
    FLabels:    array of TAsmModelLabel;
    FConsts:    array of TAsmModelNamedConst;
    FData:      array of TAsmModelNamedData;
    FCode:      TAsmInstrArray;
    FFiles:     array of TAsmModelSourceFile;
  public
    property    Code: TAsmInstrArray read FCode;

    property    Labels[index: integer]: TAsmModelLabel
                read (FLabels[index])
                write (FLabels[index] := Value);

    property    Constants[index: integer]: TAsmModelNamedConst
                read  (FConsts[index])
                write (FConsts[index] := value);

    property    LabelCount: integer read (FLabels.Count);
    property    ConstCount: integer read (FConsts.Count);

    function    AddLabel(Name: string): TAsmModelLabel; overload;
    function    AddLabel(const Name: string; const LineNr: integer): TAsmModelLabel; overload;
    function    AddLabel(const Name: string; const LineNr, ByteOffset: integer): TAsmModelLabel; overload;

    function    AddNamedConst(const Name: string; const TextValue: string): TAsmModelNamedConst; overload;
    function    AddNamedConst(const Name: string; const TextValue: string; LineNr: integer): TAsmModelNamedConst; overload;
    function    AddNamedData(const Name: string; const DataText: string): TAsmModelNamedData;

    procedure   RegisterIncludeFile(const Instance: TAsmModelSourceFile);
    function    GetIncludeFile(Filename: string): TAsmModelSourceFile;

    procedure   AddInstruction(const Data: TAsmInstruction);

    function    GetDataByName(Name: string): TAsmModelNamedData;
    function    GetNamedConstant(const Name: string): TAsmModelNamedConst;
    function    GetLabelByName(Name: string): TAsmModelLabel;
    function    GetLabelForLineNr(LineNr: integer): TAsmModelLabel;

    function    GetIndexOfLabel(Name: string): integer; overload;
    function    GetIndexOfLabel(const Obj: TAsmModelLabel): integer; overload;

    property    Filename: string;

    destructor  Destroy; override;
  end;


implementation

uses vmachine.assembler;

//##########################################################################
// TAsmModelNamedData
//##########################################################################

constructor TAsmModelFiles.Create;
begin
  inherited Create;
end;

destructor TAsmModelFiles.Destroy;
begin
  Clear();
  inherited;
end;

procedure TAsmModelFiles.Clear;
begin
  try
    for var item in FObjects do
    begin
      item.free;
    end;
  finally
    FObjects.clear();
  end;
end;

procedure TAsmModelFiles.&Register(File: TAsmModelSourceFile);
begin
  if Find(File) = nil then
    FObjects.add(File)
  else
    raise EAssemblerError.CreateFmt('File [%s] is already loaded and registered', [file.filename]);
end;

function TAsmModelFiles.Find(Filename: string): TAsmModelSourceFile;
begin
  Filename := Filename.trim().ToLower();
  if Filename.length > 0 then
  begin
    for var item in FObjects do
    begin
      if item.filename.ToLower() = Filename then
      begin
        result := Item;
        break;
      end;
    end;
  end;
end;

function TAsmModelFiles.Find(const File: TAsmModelSourceFile): TAsmModelSourceFile;
begin
  if File <> nil then
    result := Find(File.Filename.Trim().ToLower());
end;


//##########################################################################
// TAsmModelNamedData
//##########################################################################

constructor TAsmModelNamedData.Create(const AParent: TParserModelObject);
begin
  inherited Create(AParent);
  FRawData := TBinaryData.Create(nil);
end;

destructor TAsmModelNamedData.Destroy;
begin
  FRawData.free;
  inherited;
end;

//##########################################################################
// TAsmParserContext
//##########################################################################

constructor TAsmParserContext.Create;
begin
  inherited Create('');
end;

destructor TAsmParserContext.Destroy;
begin
  inherited;
end;

function TAsmParserContext.GetSymbolInfo(Text: string): TAsmSymbolInfo;
begin
  // Default to invalid
  result.EntryIndex := -1;
  result.SymbolType := TAsmSymbolType.cstInvalid;

  Text := Text.Trim();
  if Text.length < 1 then
    exit;

  // Check for PTR
  if  text.StartsWith("(") then
  begin
    if text.EndsWith(")") then
    begin
      //Its a pointer
      result.SymbolPTR := true;
      text := text.DeleteLeft(1);
      text := text.DeleteRight(1);
    end;
  end;

  // Check for label, this must be done before we check
  // for alpha-numeric identifiers
  var lbInstance := TAsmModelSourceFile(Model).GetLabelByName(Text);
  if lbInstance <> nil then
  begin
    result.SymbolType := TAsmSymbolType.cstLabel;
    result.SymbolValue := Text;
    result.EntryIndex := TAsmModelSourceFile(Model).GetIndexOfLabel(lbInstance);
    exit;
  end;

  case Text[1].ToLower() of
  'r':
    begin
      if text[2] = '[' then
        result.SymbolType := TAsmSymbolType.cstRegister
      else
      begin
        // Grab potential numeric register-index
        var LTemp := copy(Text, 2, Text.length);
        if TAsmToolbox.ValidDecChars(LTemp) then
        begin
          // VM has a max threshold of 64 registers
          var LRegId := StrToInt(LTemp);
          if (LRegId >=0) and (LRegId <= 64) then
          begin
            // Remap to standard
            Text := 'r[' + LRegId.ToString() + ']';
            result.SymbolType := TAsmSymbolType.cstRegister;
          end;
        end;
      end;
    end;

  // variable reference
  'v':
    begin
      if text[2] = '[' then
        result.SymbolType := TAsmSymbolType.cstVariable;
    end;

  'c':
    begin
      if text[2] = '[' then
        result.SymbolType := TAsmSymbolType.cstConstant;
    end;

  // DC reference
  'd':
    begin
      if Text = 'dc' then
        result.SymbolType := TAsmSymbolType.cstDC;
    end;

  // Inline string reference
  '"':
    begin
        {$IFDEF DEBUG_OUT}
        writelnf("Inline string reference found: %s", [text]);
        {$ENDIF}

      result.SymbolType := TAsmSymbolType.cstString;
      delete(Text, 1, 1);
      var index := Text.IndexOf('"',1);
      if index = High(Text) then
      begin
        delete(Text, high(text),1);
        result.SymbolValue := Text;
        exit;
      end else
      SetLastError('Unterminated string error');
      exit;
    end;

  // integer constant
  '#':
    begin

      // Note: A constant can be a literal value, or a
      // previously defined constant. So we need to figure out
      // what is what first.

      var token := copy(Text, 2, length(Text));

      {$IFDEF DEBUG_OUT}
      writelnF("Determining the datatype for %s", [token]);
      {$ENDIF}

      var LDetermined: TRTLDatatype;
      case TString.ResolveDataType(Token, LDetermined) of
      true:
        begin
          case LDetermined of
          itString, itChar:
            begin
              {$IFDEF DEBUG_OUT}
              writeln("Datatype resolved to string");
              {$ENDIF}
              result.SymbolType := TAsmSymbolType.cstString;
              result.SymbolValue := token;
            end;
          itBoolean:
            begin
              {$IFDEF DEBUG_OUT}
              writeln("Datatype resolved to boolean");
              {$ENDIF}
              SetLastError('Boolean is not a supported literal error');
              exit;
            end;
          itFloat32, itFloat64:
            begin
              {$IFDEF DEBUG_OUT}
              writeln("Datatype resolved to float");
              {$ENDIF}
              SetLastError('Floatingpoint is not supported error');
              exit;
            end;
          itWord, itLong, itInt16, itInt32:
            begin
              {$IFDEF DEBUG_OUT}
              writeln("Datatype resolved to number");
              {$ENDIF}
              result.SymbolType := TAsmSymbolType.cstNumber;
              result.SymbolValue := token;
              exit;
            end
          else
            begin
              {$IFDEF DEBUG_OUT}
              writelnF("Examine of prefix for: %s", [TString.QuoteString(Token)]);
              {$ENDIF}

              var LPrefix := TString.ExaminePrefixType(token);

              case LPrefix of
              vpNone:
                begin
                  {$IFDEF DEBUG_OUT}
                  writeln("Prefix was: plain number");
                  {$ENDIF}
                  result.SymbolType := TAsmSymbolType.cstNumber;
                  result.SymbolValue := token;
                end;
              vpHexPascal,
              vpHexC:
                begin
                  {$IFDEF DEBUG_OUT}
                  writeln("Prefix was: hexadecimal");
                  {$ENDIF}
                  result.SymbolType := TAsmSymbolType.cstHexNumber;
                  result.SymbolValue := token;
                end;
              vpBinPascal, vpBinC:
                begin
                  {$IFDEF DEBUG_OUT}
                  writeln("Prefix was: binary");
                  {$ENDIF}
                  result.SymbolType := TAsmSymbolType.cstBinNumber;
                  result.SymbolValue := token;
                end;
              vpString:
                begin
                  {$IFDEF DEBUG_OUT}
                  writeln("Prefix was: string");
                  {$ENDIF}
                  result.SymbolType := TAsmSymbolType.cstString;
                  result.SymbolValue := token;
                end;
              end;

            end;
          end;
        end;
      false:
        begin
          // Reference to a defined constant?
          if TAsmToolbox.ValidConstText(token) then
          begin
            {$IFDEF DEBUG_OUT}
            writeln("Datatype was: a named constant reference");
            {$ENDIF}
            result.SymbolType := TAsmSymbolType.cstNamedConstant;
            result.SymbolValue := token;
            exit;
          end;
        end;
      end;
    end;

  // Standard hex number
  CNT_PREFIX_HEXVALUE:
    begin
      {$IFDEF DEBUG_OUT}
      writeln("Text for prefix_hexvalue:" + Text);
      {$ENDIF}

      // 2: skip "#" char
      if TAsmToolbox.ValidHexChars(copy(Text, 2, length(Text) )) then
      begin
        result.SymbolType := TAsmSymbolType.cstHexNumber;
        result.SymbolValue := Text;
        exit;
      end else
      begin
        SetLastError('Invalid hex value error');
        exit;
      end;
    end;

  // binary value
  CNT_PREFIX_BINVALUE:
    begin
      if TAsmToolbox.ValidBinChars(copy(Text, 2, length(Text) )) then
      begin
        result.SymbolType := TAsmSymbolType.cstBinNumber;
        result.SymbolValue := Text;
        exit;
      end else
      begin
        SetLastError('Invalid binary value error');
        exit;
      end;
    end;

  // Decimal numbers
  '0'..'9':
    begin
      // Not hex? Check for normal decimal numbers
      if TAsmToolbox.ValidDecChars(Text) then
      begin
        result.SymbolType := TAsmSymbolType.cstNumber;
        result.SymbolValue := Text;
        exit;
      end else
      begin
        SetLastError('Invalid integer error');
        exit;
      end;
    end;
  else
    begin
      exit;
    end;
  end;

  // We we get here, we have eliminated the following
  // parameter types:
  //  - #integer
  //  - "string"
  //  - "$" hexvalue
  //  - decimal number
  //  - %Binary number
  //  - #number
  //  - #named_constant

  // Remove R, V or C
  if result.SymbolType in
    [ cstRegister,
      cstVariable,
      cstConstant
    ] then
    delete(Text, 1, 1);

  // Extract param index
  if Text[1]='[' then
  begin
    var index := Text.IndexOf(']', 1);
    if index = High(Text) then
    begin
      delete(Text, 1, 1);
      delete(Text, high(text),1);

      {$IFDEF DEBUG_OUT}
      writeln("Obtaining symbol for r:v:c[" + Text + "] content");
      {$ENDIF}
      var LSubSymbol := GetSymbolInfo(Text);
      case LSubSymbol.SymbolType of
      cstNamedConstant:
        begin
          var LModel := TAsmModelSourceFile(self.model);
          var LCo := LModel.GetNamedConstant(LSubSymbol.SymbolValue);
          {$IFDEF DEBUG_OUT}
          writelnF("Symbol was: named constant: %d", [LCo.Value]);
          {$ENDIF}

          result.EntryIndex := LCo.Value;
        end;
      else
        begin
          {$IFDEF DEBUG_OUT}
          var LSymName: string;
          case LSubSymbol.SymbolType of
          cstInvalid:   LSymName := 'unknown';
          cstRegister:  LSymName := 'register';
          cstVariable:  LSymName := 'variable';
          cstConstant:  LSymName := 'inline-constant';
          cstString:    LSymName := 'string';
          cstNumber:    LSymName := 'number';
          cstHexNumber: LSymName := 'Hex-number';
          cstBinNumber: LSymName := 'Binary-number';
          cstLabel:     LSymName := 'Label';
          cstInclude:   LSymName := 'Include-file';
          end;
          writelnF("Symbol resolved to: %s (%d)", [LSymName, LSubSymbol.SymbolValue]);
          {$ENDIF}
          result.EntryIndex := StrToInt(Text);
        end;
      end;
    end;
  end;

end;

//##########################################################################
// TAsmModelSourceFile
//##########################################################################

function TAsmModelSourceFile.GetIncludeFile(Filename: string): TAsmModelSourceFile;
begin
  if FFiles.Count > 0 then
  begin
    Filename := FileName.trim().ToLower();
    for var x := low(FFiles) to high(FFiles) do
    begin
      if FFiles[x].Filename.Trim().ToLower() = Filename then
      begin
        result := FFiles[x];
        break;
      end;
    end;
  end;
end;


procedure TAsmModelSourceFile.RegisterIncludeFile(const Instance: TAsmModelSourceFile);
begin
  // Does a file with that name already exist?
  if GetIncludeFile(Instance.Filename) <> nil then
    raise EW3Exception.CreateFmt('A file with that name [%s] is already registered', [Instance.Filename]);

  // Ok, add it to our collection
  FFiles.add(Instance);
end;

destructor TAsmModelSourceFile.Destroy;
begin
  try
    for var fileitem in FFiles do
    begin
      fileitem.free;
    end;
  finally
    FFiles.Clear();
  end;

  try
    for var labelitem in FLabels do
    begin
      labelitem.free;
    end;
  finally
    FLabels.Clear();
  end;

  try
    for var constitem in FConsts do
    begin
      constitem.free;
    end;
  finally
    FConsts.Clear();
  end;

  inherited;
end;

procedure TAsmModelSourceFile.AddInstruction(const Data: TAsmInstruction);
begin
  FCode.Add(Data);
end;

function TAsmModelSourceFile.GetNamedConstant(const Name: string): TAsmModelNamedConst;
begin
  var TempName := Name.Trim().ToLower();
  if TempName.Length > 0 then
  begin

    for var NamedConst in FConsts do
    begin
      if NamedConst.Name = TempName then
      begin
        result := NamedConst;
        break;
      end;
    end;

    // Check includes, recursive
    if result = nil then
    begin
      for var LItem in FFiles do
      begin
        result := LItem.GetNamedConstant(Name);
        if result <> nil then
          break;
      end;
    end;

  end;
end;

function TAsmModelSourceFile.GetDataByName(Name: string): TAsmModelNamedData;
begin
  Name := Name.Trim().ToLower();
  if Name.Length > 0 then
  begin
    for var xItem in FData do
    begin
      if xItem.Name = Name then
      begin
        result := xItem;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.AddNamedData(const Name: string; const DataText: string): TAsmModelNamedData;
begin
  if GetDataByName(Name) = nil then
  begin
    var LContext := TAsmParserContext(Context);
    result := TAsmModelNamedData.Create(self);
    result.Name := Name.Trim().ToLower();
    result.DataText := DataText.Trim();
    FData.add(result);
  end else
  raise EW3Exception.CreateFmt("Failed to add named data, item [%s] already in collection", [Name]);
end;

function TAsmModelSourceFile.AddNamedConst(const Name: string;
         const TextValue: string): TAsmModelNamedConst;
begin
  var LContext := TAsmParserContext(Context);

  // Register the const first
  result := TAsmModelNamedConst.Create(self);
  result.Name := Name.Trim().ToLower();
  result.TextValue := TextValue;
  result.&Index := FConsts.Count;
  FConsts.add(result);

  try

    //Now lets figure out what the hell it is
    var info := LContext.GetSymbolInfo(TextValue);
    case info.SymbolType of
    cstNamedConstant:
      begin
        raise EW3Exception.CreateFmt
        ('A named constant with that name (%s) already exists error', [TextValue]);
      end;
    cstString:
      begin
        {$IFDEF DEBUG_OUT}
        writeln("Const resolved to string");
        {$ENDIF}
        var temp := copy(TextValue,2, length(TextValue)-2);
        result.Value := string(temp);
      end;
    cstHexNumber:
      begin
        {$IFDEF DEBUG_OUT}
        writeln("Const resolved to Hex-Number");
        {$ENDIF}
        var temp := copy(TextValue,2, length(TextValue)-1);
        result.value := HexToInt(temp);
      end;
    cstBinNumber:
      begin
        {$IFDEF DEBUG_OUT}
        writeln("Const resolved to Binary-Number");
        {$ENDIF}
        result.value := TAsmToolbox.BinToInt(TextValue);
      end;
    cstNumber:
      begin
        {$IFDEF DEBUG_OUT}
        writeln("Const resolved to number");
        {$ENDIF}
        result.Value := StrToInt(TextValue);
      end;
    else
      begin
        raise EW3Exception.CreateFmt("Invalid constant value for %s (%s)", [Name, TextValue]);
      end;
    end;

  except
    on e: exception do
    begin
      {$IFDEF DEBUG_OUT}
      writeln(e.message);
      {$ELSE}
      raise EW3Exception.Create(e.message);
      {$ENDIF}
    end;
  end;

  {$IFDEF DEBUG_OUT}
  writelnF("Const found: %s = %s", [Name, TVariant.AsString(result.Value)]);
  {$ENDIF}
end;

function TAsmModelSourceFile.AddNamedConst(const Name: string;
         const TextValue: string; LineNr: integer): TAsmModelNamedConst;
begin
  result := AddNamedConst(Name, TextValue);
  result.LineNr := LineNr;
end;

function TAsmModelSourceFile.GetIndexOfLabel(Name: string): integer;
begin
  var LLabel := GetLabelByName(Name);
  if LLabel <> nil then
    result := FLabels.IndexOf(LLabel)
  else
    result := -1;
end;

function TAsmModelSourceFile.GetIndexOfLabel(const Obj: TAsmModelLabel): integer;
begin
  if Obj <> nil then
    result := FLabels.IndexOf(obj)
  else
    result := -1;
end;

function TAsmModelSourceFile.GetLabelForLineNr(LineNr: integer): TAsmModelLabel;
begin
  result := nil;
  if LineNr >= 0 then
  begin
    for var lb in FLabels do
    begin
      if lb.LineNr = LineNr then
      begin
        result := lb;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.GetLabelByName(Name: string): TAsmModelLabel;
begin
  Name := Name.trim().ToLower();
  if Name.length > 0 then
  begin
    for var lb in FLabels do
    begin
      if lb.Name.ToLower() = Name then
      begin
        result := lb;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.AddLabel(Name: string): TAsmModelLabel;
begin
  Name := Name.trim();
  if Name.length > 0 then
  begin
    result := TAsmModelLabel.Create(self);
    result.Name := Name;
    FLabels.add(result);
  end else
  raise EModelObject.Create('Failed to register label in model, name trims to empty error');
end;

function TAsmModelSourceFile.AddLabel(const Name: string; const LineNr: integer): TAsmModelLabel;
begin
  result := AddLabel(Name);
  result.LineNr := LineNr;
end;

function TAsmModelSourceFile.AddLabel(const Name: string; const LineNr, ByteOffset: integer): TAsmModelLabel;
begin
  result := AddLabel(Name);
  result.LineNr := LineNr;
  result.ByteOffset := ByteOffset;
end;


end.
