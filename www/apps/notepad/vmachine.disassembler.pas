unit vmachine.disassembler;

interface

{$I 'vmachine.inc'}

uses
  System.Types,
  System.Types.Convert,
  System.Reader,
  System.Objects,
  System.Streams,
  System.Stream.Reader,
  System.Stream.Writer,
  System.Memory.Allocation,
  System.Memory.Buffer,

  {$IFDEF DEBUG_OUT}
  SmartCL.System,
  {$ENDIF}

  vmachine.assembler,
  vmachine.types,
  vmachine.data;

type

  TDisassemblyEmitProc = function (const Reader:TReader; const layout, ParamA, ParamB: byte): string;

  TASMDisassembler = class(TW3ErrorObject)
  private
    FBuffer:  TMemoryStream;
    FOffset:  longword;
    FLut:     variant;
  protected
    function  ReadConst(const Reader: TReader): longword;
    function  ReadIndex(const Reader: TReader): longword;
    function  ReadLength(const Reader: TReader): byte;
    function  ReadInlineData(const Reader: TReader): variant;

    // call this to register a text-emitter for an instruction
    procedure RegisterEmitter(const Instruction: word;
        const Emitter: TDisassemblyEmitProc);

    // override and populate instructions
    procedure SetupInstructions; virtual;

    function  NOOP(const Reader: TReader; const layout, ParamA, ParamB: byte): string;
    function  VAlloc (const Reader: TReader; const layout, ParamA, ParamB: Byte): string;
    function  VFree(const Reader: TReader; const layout, ParamA, ParamB: Byte): string;
    function  PMOVE(const Reader: TReader; const layout, ParamA, ParamB: Byte): string;
    function  Push(const Reader: TReader; const layout, ParamA, ParamB: Byte): string;
    function  Pop(const Reader: TReader; const Layout, ParamA, ParamB: byte): string;
    function  PAdd(const Reader: TReader; const layout, ParamA, ParamB: Byte): string;
    function  PSub(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PCMP(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PMUL(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PDIV(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PMOD(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PLSR(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PLSL(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PBSET(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PBTST(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PBCLR(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
    function  PRts(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PJsr(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PJMP(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PBeq(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PBne(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PBLE(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PBGT(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PAnd(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  POr(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PNot(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PXOR(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
    function  PSys(const Reader: TReader; const  Kind, ParamA, ParamB: byte): string;

  public
    Property    Offset: longword read FOffset;

    procedure   LoadFrombuffer(const Buffer: TBinaryData);
    procedure   LoadFromStream(const Stream: TStream);

    function    Next(var Text: string): boolean;

    constructor Create; override;
    destructor  Destroy; override;
  end;

function REGT(const Index: integer; const InDirect: boolean): string;
function CONSTT(const Index: integer;const InDirect: boolean): string;
function VART(const Index: integer;const InDirect: boolean): string;
function DATAT(const value: variant;const InDirect: boolean): string;

implementation


function REGT(const Index: integer; const InDirect: boolean): string;
begin
  case Indirect of
  true:   result := 'R[' + Index.toString +']^';
  false:  result := 'R[' + Index.toString +']';
  end;
end;

function CONSTT(const Index: integer; const InDirect: boolean): string;
begin
  case Indirect of
  true:   result := 'C[' + Index.toString +']^';
  false:  result := 'C[' + Index.toString +']';
  end;
end;

function VART(const Index: integer; const InDirect: boolean): string;
begin
  case Indirect of
  true:   result := 'V[' + Index.toString +']^';
  false:  result := 'V[' + Index.toString +']';
  end;
end;

function DATAT(const value: variant; const InDirect: boolean): string;
begin
  if Indirect then
    result := Format('"%s"^',[value])
  else
    result := Format('"%s"',[value]);
end;

//#############################################################################
// TASMDisassembler
//#############################################################################

Constructor TASMDisassembler.Create;
begin
  inherited create;
  FBuffer := TMemoryStream.Create;

  (* JS objects act as lookup directories, so we use a normal
     Javascript object as our instruction lookup table (LUT) *)
  FLut:= TVariant.CreateObject;

  (* Setup the instructions for this disassembler *)
  SetupInstructions;
end;

Destructor TASMDisassembler.Destroy;
begin
  FBuffer.free;
  FLut := null;
  inherited;
end;

procedure TASMDisassembler.SetupInstructions;
begin
  FLut:= TVariant.CreateObject;

  RegisterEmitter(TCPUInstruction.ocAlloc, @VAlloc);
  RegisterEmitter(TCPUInstruction.ocFree,  @VFree);

  RegisterEmitter(TCPUInstruction.ocMOVE,  @PMove);
  RegisterEmitter(TCPUInstruction.ocPUSH,  @Push);
  RegisterEmitter(TCPUInstruction.ocPOP,   @Pop);

  RegisterEmitter(TCPUInstruction.ocADD,   @PAdd);
  RegisterEmitter(TCPUInstruction.ocSUB,   @PSub);
  RegisterEmitter(TCPUInstruction.ocMUL,   @PMUL);
  RegisterEmitter(TCPUInstruction.ocDIV,   @PDIV);

  RegisterEmitter(TCPUInstruction.ocCMP,   @PCmp);
  RegisterEmitter(TCPUInstruction.ocJSR,   @PJSR);
  RegisterEmitter(TCPUInstruction.ocJMP,   @PJMP);

  RegisterEmitter(TCPUInstruction.ocBNE,   @PBNE);
  RegisterEmitter(TCPUInstruction.ocBEQ,   @PBEQ);
  RegisterEmitter(TCPUInstruction.ocBLE,   @PBLE);
  RegisterEmitter(TCPUInstruction.ocBGT,   @PBGT);

  RegisterEmitter(TCPUInstruction.ocRTS,   @PRTS);
  RegisterEmitter(TCPUInstruction.ocSys,   @PSys);
  RegisterEmitter(TCPUInstruction.ocNOOP,  @NOOP);

  RegisterEmitter(TCPUInstruction.ocMOD,   @PMOD);
  RegisterEmitter(TCPUInstruction.ocLSR,   @PLSR);
  RegisterEmitter(TCPUInstruction.ocLSL,   @PLSL);
  RegisterEmitter(TCPUInstruction.ocBSET,  @PBSET);
  RegisterEmitter(TCPUInstruction.ocBTST,  @PBTST);
  RegisterEmitter(TCPUInstruction.ocBCLR,  @PBCLR);

  RegisterEmitter(TCPUInstruction.ocAnd,   @PAnd);
  RegisterEmitter(TCPUInstruction.ocOR,    @POr);
  RegisterEmitter(TCPUInstruction.ocNOT,   @PNot);
  RegisterEmitter(TCPUInstruction.ocXOR,   @PXOR);
end;

function TASMDisassembler.ReadConst(const Reader: TReader): longword;
begin
  result := Reader.ReadLong();
  //result := Reader.ReadWord();
end;

function TASMDisassembler.ReadIndex(const Reader: TReader): longword;
begin
  result := Reader.ReadLong();
end;

function TASMDisassembler.ReadLength(const Reader: TReader): byte;
begin
  result := Reader.ReadByte();
end;

function TASMDisassembler.ReadInlineData(const Reader: TReader): variant;
begin
  var cLen := ReadLength(Reader);
  if cLen > 0 then
  begin
    var cBytes := Reader.Read(cLen);
    result := Reader.BytesToVariant( cBytes );
  end else
    result := unassigned;
end;

function TASMDisassembler.VAlloc(const Reader: TReader;
         const Layout, ParamA, ParamB: Byte): String;
var
  id: variant;
  &type: variant;
  LayoutMask: string;
begin
  if (Layout in [clValue_Value, clReg_Value, clReg_Reg, clReg_Var, clReg_Const,
     clVar_Const, clVar_Reg, clVar_Var, clVar_Value]) then
  begin
    LayoutMask := 'ALLOC ' + TAsmToolbox.InstructionLayoutToStr(Layout);

    case Layout of
    clReg_Value,
    clVar_Value:
      begin
        id := ReadIndex(Reader);
        &Type := ReadInlineData(Reader);
      end;
    clValue_Value:
      begin
        id := ReadInlineData(Reader);
        &Type := ReadInlineData(Reader);
      end;
    clReg_Const, clVar_Const:
      begin
        id := ReadIndex(Reader);
        &Type := ReadConst(Reader);
      end;
    else
      begin
        id := ReadIndex(Reader);
        &Type := ReadIndex(Reader);
      end;
    end;

    { if Layout in [clReg_Value, clVar_Value] then
    begin
      id := ReadIndex(Reader);
      &Type := ReadInlineData(Reader);
    end else
    if (Layout = clValue_Value) then
    begin
      id := ReadInlineData(Reader);
      &Type := ReadInlineData(Reader);
    end else
    begin
      id := ReadIndex(Reader);
      &Type := ReadIndex(Reader);
    end;    }

    result := Format(LayoutMask, [id, &Type]);
  end else
  raise Exception.Create('Invalid instruction layout [var id, type] error');
end;

function TASMDisassembler.VFree(const Reader: TReader;
  const  layout, ParamA, ParamB: Byte): String;
var
  LayoutMask: string;
  id: variant;
begin
  if Failed then
    ClearLastError();

  if (Layout in [clRegOnly, clVarOnly, clCntOnly, clDatOnly]) then
  begin
    LayoutMask := 'FREE ' + TAsmToolbox.InstructionLayoutToStr(Layout);

    case Layout of
    clDatOnly:  id := ReadInlineData(Reader);
    clCntOnly:  id := ReadConst(Reader);
    else        id := ReadIndex(Reader);
    end;

    result := Format(LayoutMask, [id]);
  end else
  SetLastError('Invalid instruction layout, expected register, variable, constant or inline');
end;

function TASMDisassembler.Pop(const Reader: TReader; const
  Layout, ParamA, ParamB: byte): string;
begin
  if (Layout in [clRegOnly, clVarOnly]) then
  begin
    var LElement := if Layout = clRegOnly then "R[$%s]" else "V[$%s]";
    result := 'POP ' + Format(LElement, [IntToHex( ReadIndex(Reader), 4)]);
  end;
end;

function TASMDisassembler.Push(const Reader: TReader;
         const  Layout,ParamA, ParamB: Byte): string;
var
  LElement: string;
begin
  if (Layout in [clRegOnly, clDatOnly, clCntOnly, clVarOnly]) then
  begin
    case Layout of
    clRegOnly: LElement := "R[%d]";
    clCntOnly: LElement := "C[%d]";
    clVarOnly: LElement := "V[%d]";
    clDatOnly: LElement := "#%s";
    end;

    case Layout of
    clDatOnly:  LElement := Format(LElement,[ReadInlineData(Reader)]);
    clCntOnly:  LElement := Format(LElement,[ReadConst(Reader)]);
    else        LElement := Format(LElement,[ReadIndex(Reader)]);
    end;

    result := 'PUSH ' + LElement;
  end;
end;

function TASMDisassembler.PSub(const Reader: TReader; const  layout, ParamA, ParamB: Byte):String;
var
  LMask:  string;
  LFirst, LSecond:  variant;
begin
  case Layout of
  clReg_Value:      LMask := 'SUB R[%d], #%s';
  clReg_Reg:        LMask := 'SUB R[%d], R[%d]';
  clReg_Var:        LMask := 'SUB R[%d], V[%s]';
  clReg_Const:      LMask := 'SUB R[%d], C[%s]';
  clVar_Const:      LMask := 'SUB V[%s], C[%d]';
  clVar_Reg:        LMask := 'SUB V[%s], R[%d]';
  clVar_Var:        LMask := 'SUB V[%s], V[%s]';
  clVar_Value:      LMask := 'SUB V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadInlineData(Reader);
    end;
  clReg_Const, clVar_Const:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadConst(Reader);
    end;
  clReg_Reg, clReg_Var,
  clVar_Reg, clVar_Var:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadIndex(Reader);
    end;
  clVar_Value:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadInlineData(Reader);
    end;
  end;

  result := Format(LMask, [LFirst, LSecond]);
end;

function TASMDisassembler.PAdd(const Reader: TReader; const  layout, ParamA, ParamB: Byte):String;
var
  LMask:  string;
  LFirst, LSecond:  variant;
begin
  case Layout of
  clReg_Value:      LMask := 'ADD R[%d], #%s';
  clReg_Reg:        LMask := 'ADD R[%d], R[%d]';
  clReg_Var:        LMask := 'ADD R[%d], V[%s]';
  clReg_Const:      LMask := 'ADD R[%d], C[%s]';
  clVar_Const:      LMask := 'ADD V[%s], C[%d]';
  clVar_Reg:        LMask := 'ADD V[%s], R[%d]';
  clVar_Var:        LMask := 'ADD V[%s], V[%s]';
  clVar_Value:      LMask := 'ADD V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadInlineData(Reader);
    end;
  clReg_Const, clVar_Const:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadConst(Reader);
    end;
  clReg_Reg, clReg_Var,
  clVar_Reg, clVar_Var:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadIndex(Reader);
    end;
  clVar_Value:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadInlineData(Reader);
    end;
  end;

  result := Format(LMask, [LFirst, LSecond]);
end;

function TASMDisassembler.PMove(const Reader: TReader;
    const  layout,ParamA,ParamB:Byte):String;
var
  LMask:  string;
  LFirst, LSecond:  variant;
begin
  case Layout of
  clReg_Value:      LMask := 'MOVE R[%d], #%s';
  clReg_Reg:        LMask := 'MOVE R[%d], R[%d]';
  clReg_Var:        LMask := 'MOVE R[%d], V[%s]';
  clReg_Const:      LMask := 'MOVE R[%d], C[%s]';
  clVar_Const:      LMask := 'MOVE V[%s], C[%d]';
  clVar_Reg:        LMask := 'MOVE V[%s], R[%d]';
  clVar_Var:        LMask := 'MOVE V[%s], V[%s]';
  clVar_Value:      LMask := 'MOVE V[%s], D[%s]';
  end;

  case Layout of
  clReg_Value:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadInlineData(Reader);
    end;
  clReg_Const, clVar_Const:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadConst(Reader);
    end;
  clReg_Reg, clReg_Var,
  clVar_Reg, clVar_Var:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadIndex(Reader);
    end;
  clVar_Value:
    begin
      LFirst  := ReadIndex(Reader);
      LSecond := ReadInlineData(Reader);
    end;
  end;
  result := Format(LMask, [LFirst, LSecond]);
end;

function TASMDisassembler.PMUL(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
var
  LMask:  string := 'MUL';
begin
  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PDIV(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
var
  LMask:  string;
begin
  case Layout of
  clReg_Value:      LMask := 'DIV R[%d], #%s';
  clReg_Reg:        LMask := 'DIV R[%d], R[%d]';
  clReg_Var:        LMask := 'DIV R[%d], V[%s]';
  clReg_Const:      LMask := 'DIV R[%d], C[%s]';
  clVar_Const:      LMask := 'DIV V[%s], C[%d]';
  clVar_Reg:        LMask := 'DIV V[%s], R[%d]';
  clVar_Var:        LMask := 'DIV V[%s], V[%s]';
  clVar_Value:      LMask := 'DIV V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PMOD(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
begin
  var LMask := 'MOD ';

  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PLSR(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
begin
  var LMask := 'LSR ';
  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PLSL(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
begin
  var LMask := 'LSL ';

  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%d]';
  clReg_Const:      LMask += 'R[%d], C[%d]';
  clVar_Const:      LMask += 'V[%d], C[%d]';
  clVar_Reg:        LMask += 'V[%d], R[%d]';
  clVar_Var:        LMask += 'V[%d], V[%d]';
  clVar_Value:      LMask += 'V[%d], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PBSET(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
begin
  var LMask := 'BSET ';

  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PBTST(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
begin
  var LMask := 'BTST ';

  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PBCLR(const Reader: TReader; const  layout, ParamA, ParamB: Byte): string;
begin
  var LMask := 'BCLR ';

  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PCMP(const Reader: TReader; const  layout, ParamA, ParamB: Byte):String;
begin
  var LMask := 'CMP ';
  case Layout of
  clReg_Value:      LMask += 'R[%d], #%s';
  clReg_Reg:        LMask += 'R[%d], R[%d]';
  clReg_Var:        LMask += 'R[%d], V[%s]';
  clReg_Const:      LMask += 'R[%d], C[%s]';
  clVar_Const:      LMask += 'V[%s], C[%d]';
  clVar_Reg:        LMask += 'V[%s], R[%d]';
  clVar_Var:        LMask += 'V[%s], V[%s]';
  clVar_Value:      LMask += 'V[%s], #%s';
  end;

  case Layout of
  clReg_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Const, clVar_Const:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadConst(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clReg_Reg, clReg_Var, clVar_Reg, clVar_Var:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadIndex(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  clVar_Value:
    begin
      var LFirst  := ReadIndex(Reader);
      var LSecond := ReadInlineData(Reader);
      result := Format(LMask, [LFirst, LSecond]);
    end;
  end;
end;

function TASMDisassembler.PAnd(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  result := 'AND';
end;

function TASMDisassembler.POr(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  result := 'OR';
end;

function TASMDisassembler.PNot(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  result := 'NOT';
end;

function TASMDisassembler.PXOR(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  result := 'XOR';
end;

function TASMDisassembler.PRts(const Reader:TReader;
  const  Kind, ParamA, ParamB: byte):String;
begin
  result := 'RTS';
end;

function TASMDisassembler.PJMP(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  var LOffset := Reader.ReadLong();
  result := 'JMP $' + IntToHex(LOffset, 8);
end;

function TASMDisassembler.PJsr(const Reader:TReader;
  const  Kind, ParamA, ParamB: byte):String;
begin
  var LOffset := Reader.ReadLong();
  result := 'JSR $' + IntToHex(LOffset, 8);
end;

function TASMDisassembler.PBLE(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  var LOffset := Reader.ReadLong();
  result := 'BLE $' + IntToHex(LOffset, 8);
end;

function TASMDisassembler.PBGT(const Reader:TReader;const  Kind, ParamA, ParamB: byte): string;
begin
  var LOffset := Reader.ReadLong();
  result := 'BGT $' + IntToHex(LOffset, 8);
end;

function TASMDisassembler.PBeq(const Reader:TReader;
  const  Kind, ParamA, ParamB: byte):String;
begin
  var LOffset := Reader.ReadLong();
  result := 'BEQ $' + IntToHex(LOffset, 8);
end;

function TASMDisassembler.PBne(const Reader:TReader;
  const  Kind, ParamA, ParamB: byte):String;
begin
  var LOffset := Reader.ReadLong();
  result := 'BNE $' + IntToHex(LOffset,8);
end;

function TASMDisassembler.NOOP(const Reader:TReader;
  const layout, ParamA, ParamB:Byte):String;
begin
  result := 'NOOP';
end;

function TASMDisassembler.PSys(const Reader: TReader;
  const  Kind, ParamA, ParamB: byte):String;
begin
  (* Read the system-call id *)
  var LSysId := reader.ReadLong();
  result := format('SYS $%s', [LSysId.ToHexString(2)]);
end;

procedure TASMDisassembler.RegisterEmitter(const Instruction: word;
  const Emitter: TDisassemblyEmitProc);
begin
  var opcode := Instruction;
  FLut[opcode] := @Emitter;
end;

procedure TASMDisassembler.LoadFromStream(const Stream: TStream);
begin
  FBuffer.Size := 0;

  if Stream <> NIL then
  begin
    var LToRead := Stream.Size - Stream.Position;
    if LToRead > 0 then
    begin
      Fbuffer.CopyFrom(Stream, LToRead);
      FBuffer.position := 0;
    end;
  end;
end;

procedure TASMDisassembler.LoadFrombuffer(const Buffer: TBinaryData);
begin
  FBuffer.Size := 0;
  if Buffer <> NIL then
  begin
    var LBytes := Buffer.ReadBytes(0, Buffer.Size);
    FBuffer.write(LBytes);
    FBuffer.position := 0;
    { var Temp := Buffer.ToStream();
    if assigned(temp) then
    begin
      try
        FBuffer.CopyFrom(Temp, Temp.Size);
      finally
        Temp.free;
      end;
    end;
    FBuffer.position := 0; }
  end else
  raise exception.create('Invalid buffer error');
end;

function TASMDisassembler.Next(var Text: string): boolean;
begin
  if Failed then
    ClearLastError();

  Text := '';
  if FBuffer <> NIL then
  begin
    if (FBuffer.Position < FBuffer.Size-1) then
    begin
      var LReader := TReader.Create(FBuffer as IBinaryTransport);
      try

        // Read instruction nnemonic
        var LMnemonic := LReader.ReadInteger();
        var LInst := LMnemonic and $0000FFFF;
        var LLayout := LMnemonic and $00FF0000 shr 16;
        var LParamA := (LMnemonic and $FF000000 shr 24) shr 4;
        var LParamB := (LMnemonic and $FF000000 shl 4) shr 28;

        if FLut.hasOwnProperty(LInst) then
        begin
          try
            var lookup := FLut;
            var Entry: TDisassemblyEmitProc;
            asm
              @Entry = @lookup[@LInst];
            end;
            //writelnF("Found mnemonic for: %s", [IntToHex(LInst, 2)]);
            Text := Entry(LReader, LLayout, LParamA, LParamB);
          except
            on e: exception do
            begin
              var err := Format('Disassemble failed, exception [%s] with [%s]', [e.classname,e.message]);
              {$IFDEF DEBUG_OUT}
              writeln(err);
              {$ENDIF}
              SetLastError(err);
              exit;
            end;
          end;

          result := true;

        end else
          SetLastErrorF
            ('Invalid or unknown instruction [%s]',
            [IntToHex(LInst,2)]);
      finally
        LReader.free;
      end;

    end;
  end;
end;


end.
