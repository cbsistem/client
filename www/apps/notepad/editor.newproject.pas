unit editor.newproject;

interface

uses 
  W3C.DOM,
  System.Types,
  System.Types.Graphics,
  System.colors,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,

  qtx.smart.controls.toolbar,

  editor.dialogheader,

  SmartCL.Theme,
  SmartCL.Css.Classes,

  SmartCL.System,
  SmartCL.Components,
  SmartCL.Css.Stylesheet,
  SmartCL.Controls.Image;

type

  TEditorCustomDialog = class(TW3CustomControl)
  end;

  TEditorDialogNewProject = class(TEditorCustomDialog)
  private
    FHeader:  TDialogHeader;
    FFooter:  TQTXToolbar;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
    procedure StyleTagObject;  override;

    procedure SetupHeader(const CB: TStdCallback);
    procedure SetupFooter(const CB: TStdCallback);

  public
    property  Header: TDialogHeader read FHeader;
    property  Footer: TQTXToolbar read FFooter;
  end;

implementation


//############################################################################
// TEditorDialogNewProject
//############################################################################

procedure TEditorDialogNewProject.InitializeObject;
begin
  inherited;
  SetSize(500, 400);
end;

procedure TEditorDialogNewProject.FinalizeObject;
begin
  FHeader.free;
  FFooter.free;
  inherited;
end;

procedure TEditorDialogNewProject.ObjectReady;
begin
  inherited;

  SetupHeader( procedure (Success: boolean)
  begin
    if Success then
    begin
      SetupFooter( procedure (Success: boolean)
      begin
        invalidate();
      end);
    end;
  end);
end;

procedure TEditorDialogNewProject.SetupHeader(const CB: TStdCallback);
begin
  FHeader := TDialogHeader.Create(self);
  TW3Dispatch.WaitFor([FHeader], procedure ()
  begin
    Fheader.Title.Font.Size := 14;
    FHeader.Title.Caption := '<b>Create a new project</b>';
    FHeader.Text.Caption := 'Please fill out the details below and click '
      + 'create to setup a new LDEF assembly project';

    if assigned(CB) then
      CB(true);
  end);
end;

procedure TEditorDialogNewProject.SetupFooter(const CB: TStdCallback);
begin
  FFooter := TQTXToolbar.Create(self);
  TW3Dispatch.WaitFor([FFooter], procedure ()
  begin
    FFooter.Alignment := TQTXToolbarElementAlignment.celAlignRight;
    try
      var LCreate := FFooter.Add('Create Project');
      FFooter.AddSeparator().Width := 24;
      var LCancel := FFooter.Add('Cancel');
    finally
      FFooter.LayoutChildren();
      FFooter.Invalidate();
      if assigned(CB) then
        CB(true);
    end;

  end);
end;

procedure TEditorDialogNewProject.StyleTagObject;
begin
  inherited StyleTagObject;
  ThemeReset();
  ThemeBackground := TW3ThemeBackground.bsToolContainerBackground;
  ThemeBorder := TW3ThemeBorder.btToolContainerBorder;
end;

procedure TEditorDialogNewProject.Resize;
begin
  inherited;
  if TW3Dispatch.AssignedAndReady([FHeader]) then
  begin
    var LBounds := AdjustClientRect(ClientRect);
    var LRect := LBounds;
    LRect.bottom := 108;
    FHeader.SetBounds(LRect);

    LRect := LBounds;
    LRect.top := LRect.bottom - FFooter.height;
    FFooter.SetBounds(LRect);

  end;
end;




end.
