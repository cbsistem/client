unit vmachine.model;

interface

{$I 'vmachine.inc'}

uses 
  {$IFDEF DEBUG_OUT}
    SmartCL.System,
  {$ENDIF}
    W3C.TypedArray
  , System.Types
  , System.Types.Convert
  , System.Streams
  , System.Stream.Reader
  , System.Stream.Writer
  , System.Memory.Buffer
  , System.Text.Parser
  , vmachine.types
  , vmachine.data
  ;

type

  EAsmParserContext = class(EW3Exception);


  TAsmParserContext = class(TParserContext)
  public
    function    GetSymbolInfo(Text: string): TAsmSymbolInfo;
    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TAsmModelLabel = class(TParserModelObject)
  public
    property  Name: string;
    property  LineNr: integer;
    property  ByteOffset: integer;
  end;

  TAsmModelNamedConst = class(TParserModelObject)
  public
    property  Name: string;
    property  TextValue: string;
    property  Value: variant;
    property  LineNr: integer;
  end;

  TAsmModelNamedDataOrigin = (noInline, noFile);

  TAsmModelNamedData = class(TParserModelObject)
  private
    FRawData:   TBinaryData;
  public
    property    DataOrigin: TAsmModelNamedDataOrigin;   // inline "dc.b xyz" or filename?
    property    Name: string;                           // name of the resource chunk
    property    DataText: string;                       // Data text (only applies is Origin = noInline)
    property    DataBuffer: TBinaryData read FRawData;  // Data in binary form

    constructor Create(const AParent: TParserModelObject); override;
    destructor  Destroy; override;
  end;

  TAsmModelSourceFile = class(TParserModelObject)
  private
    FLabels:    array of TAsmModelLabel;
    FConsts:    array of TAsmModelNamedConst;
    FData:      array of TAsmModelNamedData;
    FCode:      TAsmInstrArray;
  public
    property    Code: TAsmInstrArray read FCode;

    property    Labels[index: integer]: TAsmModelLabel
                read (FLabels[index])
                write (FLabels[index] := Value);

    property    Constants[index: integer]: TAsmModelNamedConst
                read  (FConsts[index])
                write (FConsts[index] := value);

    property    LabelCount: integer read (FLabels.Count);
    property    ConstCount: integer read (FConsts.Count);

    function    AddLabel(Name: string): TAsmModelLabel; overload;
    function    AddLabel(const Name: string; const LineNr: integer): TAsmModelLabel; overload;
    function    AddLabel(const Name: string; const LineNr, ByteOffset: integer): TAsmModelLabel; overload;

    function    AddNamedConst(const Name: string; const TextValue: string): TAsmModelNamedConst; overload;
    function    AddNamedConst(const Name: string; const TextValue: string; LineNr: integer): TAsmModelNamedConst; overload;

    function    AddNamedData(const Name: string; const DataText: string): TAsmModelNamedData;

    procedure   AddInstruction(const Data: TAsmInstruction);

    function    GetDataByName(Name: string): TAsmModelNamedData;

    function    GetNamedConstant(const Name: string): TAsmModelNamedConst;
    function    GetLabelByName(Name: string): TAsmModelLabel;
    function    GetLabelForLineNr(LineNr: integer): TAsmModelLabel;
    function    GetIndexOfLabel(Name: string): integer; overload;
    function    GetIndexOfLabel(const Obj: TAsmModelLabel): integer; overload;

    destructor  Destroy; override;
  end;


implementation

uses vmachine.assembler;

//##########################################################################
// TAsmModelNamedData
//##########################################################################

constructor TAsmModelNamedData.Create(const AParent: TParserModelObject);
begin
  inherited Create(AParent);
  FRawData := TBinaryData.Create(nil);
end;

destructor TAsmModelNamedData.Destroy;
begin
  FRawData.free;
  inherited;
end;

//##########################################################################
// TAsmParserContext
//##########################################################################

constructor TAsmParserContext.Create;
begin
  inherited Create('');
end;

destructor TAsmParserContext.Destroy;
begin
  inherited;
end;

function TAsmParserContext.GetSymbolInfo(Text: string): TAsmSymbolInfo;
begin
  // Default to invalid
  result.EntryIndex := -1;
  result.SymbolType := TAsmSymbolType.cstInvalid;

  Text := Text.Trim();
  if Text.length > 0 then
  begin

    // Check for PTR
    if  text.StartsWith("(") then
    begin
      if text.EndsWith(")") then
      begin
        //Its a pointer
        result.SymbolPTR := true;
        text := text.DeleteLeft(1);
        text := text.DeleteRight(1);
      end;
    end;

    // Check for label, this must be done before we check
    // for alpha-numeric identifiers
    var lbInstance := TAsmModelSourceFile(Model).GetLabelByName(Text);
    if lbInstance <> nil then
    begin
      result.SymbolType := TAsmSymbolType.cstLabel;
      result.SymbolValue := Text;
      result.EntryIndex := TAsmModelSourceFile(Model).GetIndexOfLabel(lbInstance);
      exit;
    end;

    case Text[1].ToLower() of
    'r':
      begin
        if text[2] = '[' then
          result.SymbolType := TAsmSymbolType.cstRegister;
      end;

    // variable reference
    'v':
      begin
        if text[2] = '[' then
          result.SymbolType := TAsmSymbolType.cstVariable;
      end;

    'c':
      begin
        if text[2] = '[' then
          result.SymbolType := TAsmSymbolType.cstConstant;
      end;

    // DC reference
    'd':
      begin
        if Text = 'dc' then
          result.SymbolType := TAsmSymbolType.cstDC;
      end;

    // Inline string reference
    '"':
      begin
        result.SymbolType := TAsmSymbolType.cstString;
        delete(Text, 1, 1);
        var index := Text.IndexOf('"',1);
        if index = High(Text) then
        begin
          delete(Text, high(text),1);
          result.SymbolValue := Text;
          exit;
        end else
        SetLastError('Unterminated string error');
        exit;
      end;

    // integer constant
    '#':
      begin
        var token := copy(Text, 2, length(Text)-1);
        if  TAsmToolbox.ValidDecChars(token) then
        begin
          // Plain value?
          result.SymbolType := TAsmSymbolType.cstNumber;
          result.SymbolValue := token;
        end else
        begin
          // Reference to a defined constant?
          if TAsmToolbox.ValidConstText(token) then
          begin
            result.SymbolType := TAsmSymbolType.cstNamedConstant;
            result.SymbolValue := token;
          end else
          begin
            SetLastError('Invalid integer error');
            exit;
          end;
        end;
      end;

    // Standard hex number
    CNT_PREFIX_HEXVALUE:
      begin
        if TAsmToolbox.ValidHexChars(copy(Text, 2, length(Text)-1)) then
        begin
          result.SymbolType := TAsmSymbolType.cstHexNumber;
          result.SymbolValue := Text;
          exit;
        end else
        begin
          SetLastError('Invalid hex value error');
          exit;
        end;
      end;

    // binary value
    CNT_PREFIX_BINVALUE:
      begin
        if TAsmToolbox.ValidBinChars(copy(Text, 2, length(Text)-1)) then
        begin
          result.SymbolType := TAsmSymbolType.cstBinNumber;
          result.SymbolValue := Text;
          exit;
        end else
        begin
          SetLastError('Invalid binary value error');
          exit;
        end;
      end;

    // Decimal numbers
    '0'..'9':
      begin
        // Not hex? Check for normal decimal numbers
        if TAsmToolbox.ValidDecChars(Text) then
        begin
          result.SymbolType := TAsmSymbolType.cstNumber;
          result.SymbolValue := Text;
          exit;
        end else
        begin
          SetLastError('Invalid integer error');
          exit;
        end;
      end;
    else
      begin
        exit;
      end;
    end;

    // We we get here, we have eliminated the following
    // parameter types:
    //  - #integer
    //  - "string"
    //  - "$" hexvalue
    //  - decimal number
    //  - %Binary number
    //  - #number
    //  - #named_constant

    // Remove R, V or C
    if result.SymbolType in
      [ cstRegister,
        cstVariable,
        cstConstant
      ] then
      delete(Text, 1, 1);

    // Extract param index
    if Text[1]='[' then
    begin
      var index := Text.IndexOf(']',1);
      if index = High(Text) then
      begin
        delete(Text, 1, 1);
        delete(Text, high(text),1);
        result.EntryIndex := StrToInt(Text);
      end;
    end;
  end;
end;

//##########################################################################
// TAsmModelSourceFile
//##########################################################################

destructor TAsmModelSourceFile.Destroy;
begin
  try
    for var labelitem in FLabels do
    begin
      labelitem.free;
    end;
  finally
    FLabels.Clear();
  end;

  try
    for var constitem in FConsts do
    begin
      constitem.free;
    end;
  finally
    FConsts.Clear();
  end;

  inherited;
end;

procedure TAsmModelSourceFile.AddInstruction(const Data: TAsmInstruction);
begin
  FCode.Add(Data);
end;

function TAsmModelSourceFile.GetNamedConstant(const Name: string): TAsmModelNamedConst;
begin
  var TempName := Name.Trim().ToLower();
  if TempName.Length > 0 then
  begin
    for var NamedConst in FConsts do
    begin
      if NamedConst.Name = TempName then
      begin
        result := NamedConst;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.GetDataByName(Name: string): TAsmModelNamedData;
begin
  Name := Name.Trim().ToLower();
  if Name.Length > 0 then
  begin
    for var xItem in FData do
    begin
      if xItem.Name = Name then
      begin
        result := xItem;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.AddNamedData(const Name: string; const DataText: string): TAsmModelNamedData;
begin
  if GetDataByName(Name) = nil then
  begin
    var LContext := TAsmParserContext(Context);
    result := TAsmModelNamedData.Create(self);
    result.Name := Name.Trim().ToLower();
    result.DataText := DataText.Trim();
    FData.add(result);
  end else
  raise EW3Exception.CreateFmt("Failed to add named data, item [%s] already in collection", [Name]);
end;

function TAsmModelSourceFile.AddNamedConst(const Name: string;
         const TextValue: string): TAsmModelNamedConst;
begin
  var LContext := TAsmParserContext(Context);

  // Register the const first
  result := TAsmModelNamedConst.Create(self);
  result.Name := Name.Trim().ToLower();
  result.TextValue := TextValue;
  FConsts.add(result);

  try

    //Now lets figure out what the hell it is
    var info := LContext.GetSymbolInfo(TextValue);
    case info.SymbolType of
    cstNamedConstant:
      begin
        raise EW3Exception.CreateFmt
        ('A named constant with that name (%s) already exists error', [TextValue]);
      end;
    cstString:
      begin
        var temp := copy(TextValue,2, length(TextValue)-2);
        result.Value := string(temp);
      end;
    cstHexNumber:
      begin
        var temp := copy(TextValue,2, length(TextValue)-1);
        result.value := HexToInt(temp);
      end;
    cstBinNumber:
      begin
        //var temp := copy(TextValue,2, length(TextValue)-1);
        result.value := TAsmToolbox.BinToInt(TextValue);
      end;
    cstNumber:
      begin
        result.Value := StrToInt(TextValue);
      end;
    else
      begin
        raise EW3Exception.CreateFmt("Invalid constant value for %s (%s)", [Name, TextValue]);
      end;
    end;

  except
    on e: exception do
    begin
      {$IFDEF DEBUG_OUT}
      writeln(e.message);
      {$ELSE}
      raise EW3Exception.Create(e.message);
      {$ENDIF}
    end;
  end;

  //self.Context.ConstList.Alloc(constId,
  {$IFDEF DEBUG_OUT}
  writelnF("Const found: %s = %s", [Name, TVariant.AsString(result.Value)]);
  {$ENDIF}
end;

function TAsmModelSourceFile.AddNamedConst(const Name: string;
         const TextValue: string; LineNr: integer): TAsmModelNamedConst;
begin
  result := AddNamedConst(Name, TextValue);
  result.LineNr := LineNr;
end;

function TAsmModelSourceFile.GetIndexOfLabel(Name: string): integer;
begin
  var LLabel := GetLabelByName(Name);
  if LLabel <> nil then
    result := FLabels.IndexOf(LLabel)
  else
    result := -1;
end;

function TAsmModelSourceFile.GetIndexOfLabel(const Obj: TAsmModelLabel): integer;
begin
  if Obj <> nil then
    result := FLabels.IndexOf(obj)
  else
    result := -1;
end;

function TAsmModelSourceFile.GetLabelForLineNr(LineNr: integer): TAsmModelLabel;
begin
  result := nil;
  if LineNr >= 0 then
  begin
    for var lb in FLabels do
    begin
      if lb.LineNr = LineNr then
      begin
        result := lb;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.GetLabelByName(Name: string): TAsmModelLabel;
begin
  Name := Name.trim().ToLower();
  if Name.length > 0 then
  begin
    for var lb in FLabels do
    begin
      if lb.Name.ToLower() = Name then
      begin
        result := lb;
        break;
      end;
    end;
  end;
end;

function TAsmModelSourceFile.AddLabel(Name: string): TAsmModelLabel;
begin
  Name := Name.trim();
  if Name.length > 0 then
  begin
    result := TAsmModelLabel.Create(self);
    result.Name := Name;
    FLabels.add(result);
  end else
  raise EModelObject.Create('Failed to register label in model, name trims to empty error');
end;

function TAsmModelSourceFile.AddLabel(const Name: string; const LineNr: integer): TAsmModelLabel;
begin
  result := AddLabel(Name);
  result.LineNr := LineNr;
end;

function TAsmModelSourceFile.AddLabel(const Name: string; const LineNr, ByteOffset: integer): TAsmModelLabel;
begin
  result := AddLabel(Name);
  result.LineNr := LineNr;
  result.ByteOffset := ByteOffset;
end;


end.
