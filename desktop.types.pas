unit desktop.types;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Time,
  System.Dictionaries,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Objects,
  System.Device.Storage,

  Desktop.Window,
  Desktop.Control,

  desktop.network.connection,
  desktop.loginprofile,

  SmartCL.Theme,
  SmartCL.Messages,
  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

const

  CNT_STYLE_Background            = $CFCFCF;
  CNT_STYLE_BACKGROUND_BRIGHT     = $EEEAE6;

  CNT_STYLE_WINDOW_FRAME_DARK     = $19234D;
  CNT_STYLE_WINDOW_FRAME_LIGHT    = $92BFFF;

  CNT_STYLE_WINDOW_BASE_SELECTED   = $618ECE;
  CNT_STYLE_WINDOW_BASE_UNSELECTED = $CFCFCF;

  CNT_TEXT_HIGHLIGHTED = $204D8D;

  CNT_STYLE_EDGE_LIGHT    = $F7F7F7;
  CNT_STYLE_EDGE_DARK     = $626262;
  CNT_STYLE_EDGE_DARKEST  = $3C3C3C;

  CNT_PREFS_LOGIN_PROFILE_DEFAULTNAME = 'default';

  CNT_DESKTOP_PRODUCT_NAME  = 'Amibian.js';

resourcestring
  PID_DESKTOP = '{C923FDB5-740B-4ADA-B785-398AC7433255}';
  PID_LOGIN   = '{4BBAE121-FDC7-41C6-90C9-6CF4ACEB01F1}';


type

  // process identifier
  TPID  = string;

  TWbHostInfo = class(JObject)
    hiSecure: boolean;
    hiHostName: string;
    hiHostPort: integer;
    hiWebEndpoint:   string;      // clean http endpoint for the server
    htWebSocketEndpoint: string;  // clean websocket endpoint for the server
  end;

  TWbFileIOCB = procedure (Success: boolean; const Message: string; Filename: string; const Data: TStream );

  TWbClickBlocker = class(TW3BlockBox)
  public
    property  PID: TPID;
  end;
  TWbClickBlockerArray = array of TWbClickBlocker;

  IWbDesktop = interface
    ['{2B00D011-4A26-4ADF-A128-651148A2E20F}']
    procedure SetFocusedWindow(const Window: TWbCustomWindow);
    procedure SetFocusedWindowEx(const Window: TWbCustomWindow; const BringToFront: boolean);

    // Checks if a TW3BlockBox is active under Application->Display
    function  GetModalIsActive: boolean;

    function  GetActiveThemePath: string;
    function  GetActiveThemeName: string;

    function  IsDesktop(const Control:TW3CustomControl): boolean;
    function  FindWindowFor(const Control: TW3CustomControl): TWbCustomWindow;
    function  GetActiveWindow: TWbCustomWindow;
    function  GetWindowList: TWbWindowList;

    function  GetApplicationRegion: TRect;
    function  GetWindowHost: TW3CustomControl;
    procedure RegisterWindow(const Window: TWbCustomWindow);
    procedure UnRegisterWindow(const Window: TWbCustomWindow);
    function  GetInitialPosition(const DefWidth, DefHeight: Integer): TRect;

    procedure SetGlobalCursor(const PID: TPID; const GlobalCursor: TCursor);
    function  GetGlobalCursor(const PID: TPID): TCursor;

    function  KnownWindow(const Window: TWbCustomWindow): boolean;

    function  GetWorkbenchChannel: IWbNetwork;
    function  GetConnection(SetupDefaultProfile: boolean): TWbNetworkClient;

    function  GetHostInfo:  TWbHostInfo;
    function  GetLoginProfile: TWbLoginProfile;
    function  GetUserName: string;

    procedure SendWindowToBack(const Window: TWbCustomWindow);
    procedure BringWindowToFront(const Window: TWbCustomWindow);

    function  ShowClickBlocker(const PID: TPID): TWbClickBlocker;
    procedure CloseClickBlocker(const PID: TPID);

    function  GetDevicesMounted: boolean;
    function  GetUserDevice: IW3StorageDevice;
    function  StreamGraphicToURL(const Stream: TStream): string;
  end;

  function  UnEscapeText(Text: string): string;
  function StripZeroBytes(Text:  string): string;

  procedure RegisterDesktop(const Desktop: IWbDesktop);
  function  GetDesktop: IWbDesktop;

implementation

uses System.Consts;

var
__LDesktop: IWbDesktop;

function UnEscapeText(Text: string): string;
begin
  var squote := "'";
  var dquote := '"';
  var sq_mask := '\' + squote;
  var dq_mask := '\' + dquote;

  result := stringreplace(Text, '\/', '/', [rfReplaceAll]);
  result := stringreplace(result, sq_mask, squote, [rfReplaceAll]);
  result := stringreplace(result, dq_mask, dquote, [rfReplaceAll]);
end;

function StripZeroBytes(Text:  string): string;
begin
  Text := Text.trim();
  if Text.length > 0 then
  begin
    while Text[Text.length] = TDataType.ByteToChar(0) do
    begin
      delete(Text, Text.length, 1);
    end;
  end;
  result := text;
end;


//#############################################################################
// TW3Intuition
//#############################################################################

procedure RegisterDesktop(const Desktop: IWbDesktop);
begin
  __LDesktop := Desktop;
end;

function  GetDesktop: IWbDesktop;
begin
  result := __LDesktop;
end;


end.
