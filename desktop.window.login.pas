unit desktop.window.login;

{.$DEFINE DEMOCODE}

interface

uses
  System.Types,
  System.Types.Graphics,
  System.Colors,

  Desktop.Types,
  desktop.loginprofile,
  desktop.header,
  Desktop.Window,
  Desktop.Edit,
  Desktop.Button,
  Desktop.Switch,

  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,

  SmartCL.Layout,
  SmartCL.Application,
  SmartCL.System,
  SmartCL.Time,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Fonts,
  SmartCL.Controls.Combobox,
  SmartCL.Controls.Label
  ;

type

  TWbLoginEvent = procedure (Sender: TObject; Local: boolean);

  TWbLoginWindow = class(TWbWindow)
  private
    FRememberInfo: TWbToggleSwitch;
    FRememberInfoLabel: TW3Label;
    FInputLayout: TLayout;
    FHeader: TWbLoginHeader;
    FLogin:   TWbButton;
    FLocal:   TWbButton;
    FCancel:  TWbButton;

    lbServer: TW3Label;
    FServer: TW3Combobox;

    lbUsernameTitle: TW3Label;
    FUserName:  TWbEditBox;

    lbPasswordTitle: TW3Label;
    FPassword: TWbEditBox;

  protected
    procedure HandleLogin(Sender: TObject);
    procedure HandleLocal(Sender: TObject);
    procedure HandleCancel(Sender: TObject);

    procedure LoadCredentials;
    procedure SaveCredentials;

    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  UsernameEdit: TWbEditbox read FUserName;
    property  PasswordEdit: TWbEditbox read FPassword;

    property  OnLogin: TWbLoginEvent;
    property  OnLoginCancel: TNotifyEvent;
  end;


implementation

uses
  SmartCL.Controls.EditBox;


//#############################################################################
// TWbPreferencesWindow
//#############################################################################

procedure TWbLoginWindow.InitializeObject;
begin
  inherited;
  Header.Title.Caption :='Authentication';
  FHeader := TWbLoginHeader.Create(Content);
  FRememberInfoLabel := TW3Label.Create(Content);
  FRememberInfo := TWbToggleSwitch.Create(Content);
  lbServer := TW3Label.Create(Content);
  FServer := TW3Combobox.Create(content);
  lbUsernameTitle := TW3Label.Create(Content);
  FUserName := TWbEditbox.Create(Content);
  lbPasswordTitle := TW3Label.Create(Content);
  FPassword := TWbEditbox.Create(Content);
  FCancel := TWbButton.Create(Content);
  FLocal := TWbButton.Create(Content);
  FLogin := TWbButton.Create(Content);
end;

procedure TWbLoginWindow.FinalizeObject;
begin
  FLogin.free;
  FLocal.free;
  FCancel.free;
  FRememberInfo.free;
  FRememberInfoLabel.free;
  inherited;
end;

procedure TWbLoginWindow.ObjectReady;
begin
  inherited;

  FRememberInfoLabel.SetBounds(10,10, 200 ,32);
  FRememberInfoLabel.Caption := "Remember credentials";
  FRememberInfoLabel.AlignText := TTextAlign.taRight;
  FRememberInfoLabel.font.color := clWhite;
  FRememberInfoLabel.TextShadow.Shadow(1, 1, 2,clBlack);

  FRememberInfo.SetBounds(220,10,100,32);

  lbServer.font.color := clWhite;
  lbServer.TextShadow.Shadow(1,1,2,clBlack);
  lbServer.AlignText:= TTextAlign.taRight;
  lbServer.Caption :='Hostname';
  lbServer.SetBounds(0,0,300,22);

  FServer.Name :='edHostName';

  var Access := GetDesktop();
  var LHostInfo := Access.GetHostInfo();
  FServer.add(LHostInfo.hiHostName);

  //FServer.add('127.0.0.1');
  //FServer.add('desktop.smartmobilestudio.com');
  //FServer.add('quartexhq.myasustor.com');

  lbUserNameTitle.font.color := clWhite;
  lbUsernameTitle.TextShadow.Shadow(1,1,2,clBlack);
  lbUserNameTitle.AlignText:= TTextAlign.taRight;
  lbUserNameTitle.Caption :='Username';
  lbUserNameTitle.SetBounds(0,0,300,22);

  FUserName.Name :='edUserName';
  FUserName.Font.Name:='Courier New';
  FUSerName.PlaceHolder :='Your username';

  lbPasswordTitle.AlignText:= TTextAlign.taRight;
  lbPasswordTitle.font.color := clWhite;
  lbPasswordTitle.TextShadow.Shadow(1,1,2,clBlack);
  lbPasswordTitle.Caption :='Password';
  lbPasswordTitle.SetBounds(0,0,300,22);

  FPassword.Font.Name:='Courier New';
  FPassword.AutoCapitalize := false;
  FPassword.AutoCorrect := false;
  FPassword.InputType :=  itPassword;
  FPassword.PlaceHolder :='Your password';

  {$IFDEF DEMOCODE}
  FUsername.Text := 'admin';
  FUsername.Enabled := false;

  FPassword.Text := 'admin';
  Fpassword.Enabled := false;
  {$ENDIF}

  FLogin.Caption := 'Login';
  FLogin.OnClick := @HandleLogin;
  FLogin.width := 80;

  FLocal.Caption :='Offline';
  FLocal.OnClick := @HandleLocal;

  FCancel.Caption := 'Cancel';
  FCancel.OnClick := @HandleCancel;
  FCancel.width := 80;

  {$IFDEF DEMOCODE}
  FLocal.Enabled := false;
  FCancel.Enabled := false;
  {$ENDIF}

  FHeader.Title.Caption := '<b>Ragnarok login</b>';
  FHeader.Text.Caption  := "Please provide your credentials for your"
                          +  " remote Ragnarok server. For offline work click "
                          +  "the <i>offline</i> button";

  FInputLayout := Layout.Client(
  [
    Layout.Top(Layout.Height(108), Fheader),
    layout.Client(Layout.Margins(10).Spacing(10),
    [
      Layout.top(Layout.Spacing(10).Height(22),
        [
          layout.left(Layout.Width(100),lbServer),
          layout.client(Layout.Stretch, FServer)
        ]),

      Layout.top(Layout.Spacing(10).Height(22),
        [
          layout.left(Layout.Width(100),lbUsernameTitle),
          layout.client(Layout.Stretch, FUserName)
        ]),

      Layout.top(Layout.Spacing(10).Height(22),
        [
          layout.left(Layout.Width(100),lbPasswordTitle),
          layout.client(Layout.Stretch, FPassword)
        ]),

      Layout.top(Layout.Spacing(10),
        [
          layout.client(Layout.Stretch,FRememberInfoLabel),
          layout.right(Layout.Width(140), FRememberInfo)
        ]),

      Layout.bottom(Layout.Spacing(10).Height(38),
        [
        Layout.Client(Layout.Margins(2).Spacing(10),
            [
              Layout.left(FLogin),
              Layout.client(FLocal),
              Layout.Right(FCancel)
            ])
        ])
      ])
  ]);

  TW3Dispatch.WaitFor([FHeader, FRememberInfoLabel,
      FRememberInfo, lbServer, FServer, lbUserNameTitle, FUserName,
      lbPasswordTitle, FPassword, FCancel, FLocal, FLogin],
  procedure ()
  begin
    TW3Dispatch.WaitFor([self], procedure ()
    begin
      //Resize();
      FRememberInfo.Checked := true;
      FRememberInfo.Invalidate();
      LoadCredentials();
    end);
  end);

end;

procedure TWbLoginWindow.SaveCredentials;
begin
  if FRememberInfo.Checked then
  begin
    var LAccess := GetDesktop();
    if LAccess <> nil then
    begin
      var LProfile := LAccess.GetLoginProfile();
      if LProfile <> nil then
      begin
        LProfile.HostName := FServer.Item[FServer.SelectedIndex];
        LProfile.Username := FUserName.Text.Trim();
        LProfile.Password := FPassword.Text.Trim();
        LProfile.SaveCredentials(CNT_PREFS_LOGIN_PROFILE_DEFAULTNAME);
      end;
    end;
  end;
end;

procedure TWbLoginWindow.LoadCredentials;
begin
  var LAccess := GetDesktop();
  if LAccess <> nil then
  begin
    var LProfile := LAccess.GetLoginProfile();
    if LProfile <> nil then
    begin

      if LProfile.Exists(CNT_PREFS_LOGIN_PROFILE_DEFAULTNAME) then
      begin
        try
          LProfile.LoadCredentials(CNT_PREFS_LOGIN_PROFILE_DEFAULTNAME);

          // Is the hostname in the list?
          var LHost := LProfile.HostName.trim();
          var LIndex := FServer.Items.IndexOf(LHost);

          // If not, add it
          if ( LIndex < 0) then
            LIndex := FServer.Add(LHost);

          // Select existing or added based on hostname
          FServer.SelectedIndex := LIndex;

          FUserName.Text := LProfile.Username;
          FPassword.Text := LProfile.Password;
        except
          on e: exception do
          begin
            showmessage(e.message);
          end;
        end;
      end;

    end;
  end;
end;

procedure TWbLoginWindow.HandleLogin(Sender: TObject);
begin
  SaveCredentials();
  if assigned(OnLogin) then
    OnLogin(self, false);
end;

procedure TWbLoginWindow.HandleLocal(Sender: TObject);
begin
  if assigned(OnLogin) then
    OnLogin(self, true);
end;

procedure TWbLoginWindow.HandleCancel(Sender: TObject);
begin
  if assigned(OnLoginCancel) then
    OnLoginCancel(self);
end;

procedure TWbLoginWindow.Resize;

  procedure ReWind;
  begin
    if not (csDestroying in ComponentState) then
      TW3Dispatch.Execute(Resize, 10);
  end;

begin
  inherited;
  if (csReady in ComponentState) then
  begin
    if QueryChildrenReady() then
    begin
      if FInputLayout <> nil then
        FInputLayout.Resize(Content)
      else
      ReWind();
    end else
    ReWind();
  end else
  ReWind();
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var StyleCode := #'
    .TWbLoginWindow {
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
    }
  ';
  Sheet.Append(StyleCode);
end;

end.
