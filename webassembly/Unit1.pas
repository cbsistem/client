﻿namespace DFMTest;

interface

uses RemObjects.Elements.RTL.Delphi, RemObjects.Elements.RTL.Delphi.VCL;

const
  CNT_Default_Text = "[Type something here]";

type

  [export]
  TForm1 = public class(TForm)
  private
    { Private declarations }
    procedure Button1Click(Sender: TObject);
  public
    { Public declarations }
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    ListBox1: TListBox;

    constructor create(AOwner: TComponent);
  end;

var
  Form1: TForm1;

implementation

constructor TForm1.create(AOwner: TComponent);
begin
  inherited create(AOwner);
  // setup our button
  self.Button1.Caption := "Click to add";
  self.Button1.OnClick := @Button1Click;

  // mild adjustment of our label
  self.Label1.Width := 165;

  // fancy windows background
  self.Handle.style.background := "#F0F0F0";

  // give our textbox a default string
  self.Edit1.Text := CNT_Default_Text;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if Edit1.Text.ToLower() <> CNT_Default_Text.ToLower() then
  begin
    ListBox1.Items.Add(Form1.Edit1.Text);
    Edit1.Text := "";
  end;
end;

end.