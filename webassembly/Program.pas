﻿namespace DFMTest;

uses
  qtxlib,
  RemObjects.Elements.RTL.Delphi, RemObjects.Elements.RTL.Delphi.VCL;

type

  [Export]
  Program = public class
  public
    method HelloWorld;
    begin
      Application := new TApplication(nil);
      Application.Initialize;
      Application.CreateForm(typeOf(TForm1), Form1);
      Application.Run;

      // test BTree
      var LLookup := new BTree;
      try
        LLookup.Add($1200, "hello world");
        LLookup.Add($1300, "this is cool");

        var LData := LLookup.Read($1300);
        writeLn(LData);
        //ShowMessage(LData.ToString());
      finally
        disposeAndNil(LLookup);
      end;

      var lBytes: array of Byte;
      lBytes := System.TextConvert.StringToUTF8("this is cool");

      writeLn("Bytes:" + lBytes.Length.ToString());

      TDispatch.&Repeat( procedure ()
      begin
        writeLn("Repeat fired");
      end, 2000, 4);

      { TDispatch.Execute( procedure ()
      begin
        writeLn("Dispatch #1 fired");
      end, 2000);

      TDispatch.Execute( procedure ()
      begin
        writeLn("Dispatch #2 fired");
      end, 1000);

      TDispatch.Execute( procedure ()
      begin
        writeLn("Dispatch #3 fired");
      end, 500); }

      // test pixel buffer
      var pixbuffer := new PixelBuffer();
      try
        pixbuffer.Allocate(20, 20, PixelFormat.pf8bit);
        try
          var x, y: Integer;

          // Write to every odd pixel
          for y := 0 to pixbuffer.Height -1 do
          begin
            for x := 0 to pixbuffer.Width -1 do
            begin
              var LOffset := pixbuffer.OffsetForPixel(x, y);
              case (x mod 2) of
              0: pixbuffer.Write(LOffset, [$00]);
              1: pixbuffer.Write(LOffset, [$FF]);
              end;
            end;
          end;

          // Read back and dump as ascii
          for y := 0 to pixbuffer.Height -1 do
          begin
            var Lline: String;
            Lline := y.ToString() + ':';
            if Lline.Length < 3 then Lline := (" " + Lline);

            for x := 0 to pixbuffer.Width -1 do
            begin
              var LOffset := pixbuffer.OffsetForPixel(x, y);
              case pixbuffer.Read(LOffset, 1)[0] of
              $00:  Lline := Lline + "_";
              $FF:  Lline := Lline + "x";
              else  Lline := Lline + "?";
              end;
            end;
            writeLn(Lline);
            Lline := '';
          end;


        finally
          pixbuffer.Release();
        end;
      finally
        disposeAndNil(pixbuffer);
      end;
    end;

  end;

end.