export declare class DFMTest {
    instance: any;
    module: any;
    private constructor();
    static instantiate(url?: string): Promise<DFMTest>;

    RemObjects_Elements_RTL_Delphi_TClassActivator_CreateInstance(aType:RemObjects_Elements_System_Type, aParameters:any): RemObjects_Elements_System_Object;
    RemObjects_Elements_RTL_Delphi_TCollectionItem(aCollection:RemObjects_Elements_RTL_Delphi_TCollection): RemObjects_Elements_RTL_Delphi_TCollectionItem;
    RemObjects_Elements_RTL_Delphi_TCollection(aItemClass:RemObjects_Elements_System_Type): RemObjects_Elements_RTL_Delphi_TCollection;
    RemObjects_Elements_RTL_Delphi_TPersistent(): RemObjects_Elements_RTL_Delphi_TPersistent;
    RemObjects_Elements_RTL_Delphi_TStringList_Create0(): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList_Create1(aOwnsObject:boolean): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList_Create2(aQuoteChar:number, aDelimiter:number): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList_Create3(aQuoteChar:number, aDelimiter:number, aOptions:number): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList_Create4(aDuplicates:number, aSorted:boolean, aCaseSensitive:boolean): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList0(): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList1(aOwnsObject:boolean): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList2(aQuoteChar:number, aDelimiter:number): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList3(aQuoteChar:number, aDelimiter:number, aOptions:number): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStringList4(aDuplicates:number, aSorted:boolean, aCaseSensitive:boolean): RemObjects_Elements_RTL_Delphi_TStringList;
    RemObjects_Elements_RTL_Delphi_TStream(): RemObjects_Elements_RTL_Delphi_TStream;
    RemObjects_Elements_RTL_Delphi_TCustomMemoryStream_Create(): RemObjects_Elements_RTL_Delphi_TCustomMemoryStream;
    RemObjects_Elements_RTL_Delphi_TCustomMemoryStream(): RemObjects_Elements_RTL_Delphi_TCustomMemoryStream;
    RemObjects_Elements_RTL_Delphi_TMemoryStream_Create(): RemObjects_Elements_RTL_Delphi_TCustomMemoryStream;
    RemObjects_Elements_RTL_Delphi_TMemoryStream(): RemObjects_Elements_RTL_Delphi_TMemoryStream;
    RemObjects_Elements_RTL_Delphi_TOSVersion_Check0(aMajor:number): boolean;
    RemObjects_Elements_RTL_Delphi_TOSVersion_Check1(aMajor:number, aMinor:number): boolean;
    RemObjects_Elements_RTL_Delphi_TOSVersion_Check2(aMajor:number, aMinor:number, aServicePackMajor:number): boolean;
    RemObjects_Elements_RTL_Delphi_TStringBuilder_Create0(): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    RemObjects_Elements_RTL_Delphi_TStringBuilder_Create1(aCapacity:number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    RemObjects_Elements_RTL_Delphi_TStringBuilder_Create2(aCapacity:number, aMaxCapacity:number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    RemObjects_Elements_RTL_Delphi_TStringBuilder_op_Equality(Value1:RemObjects_Elements_RTL_Delphi_TStringBuilder, Value2:RemObjects_Elements_RTL_Delphi_TStringBuilder): boolean;
    RemObjects_Elements_RTL_Delphi_TStringBuilder0(): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    RemObjects_Elements_RTL_Delphi_TStringBuilder1(aCapacity:number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    RemObjects_Elements_RTL_Delphi_TStringBuilder2(aCapacity:number, aMaxCapacity:number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    RemObjects_Elements_RTL_Delphi___Global_MinuteOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastMonths(aNow:number, aThen:number, aMonths:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_RecodeMilliSecond(aValue:number, aMilliSecond:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondOfTheMinute(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastMinutes(aNow:number, aThen:number, aMinutes:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastSeconds(aNow:number, aThen:number, aSeconds:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheDay(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondsBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsValidDateMonthWeek(aYear:number, aMonth:number, aWeekOfMonth:number, aDayOfWeek:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheHour(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DateTimeToMilliseconds(aDateTime:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheMinute(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOfTheSecond(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastMilliSeconds(aNow:number, aThen:number, aMilliSeconds:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_Abs0(Val:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Abs1(Val:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Abs2(Val:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Cos(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Exp(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Max0(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Max1(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Max2(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Min0(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Min1(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Min2(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Now(): number;
    RemObjects_Elements_RTL_Delphi___Global_Sin(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Date(): number;
    RemObjects_Elements_RTL_Delphi___Global_Frac(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsAM(aValue:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_IsPM(aValue:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_Sqrt(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Time(): number;
    RemObjects_Elements_RTL_Delphi___Global_DayOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Floor(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Log10(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Power(Base:number, Exponent:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Round(Val:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Today(): number;
    RemObjects_Elements_RTL_Delphi___Global_Trunc(Val:number): number;
    RemObjects_Elements_RTL_Delphi___Global_ArcCos(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_ArcSin(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_ArcTan(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DateOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HourOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncDay(aValue:number, aNumberOfDays:number): number;
    RemObjects_Elements_RTL_Delphi___Global_TimeOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WeekOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_YearOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_ArcTan2(Y:number, X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DaySpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncHour(aValue:number, aNumberOfHours:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncWeek(aValue:number, aNumberOfWeeks:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncYear(aValue:number, aNumberOfYears:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsToday(aValue:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MonthOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Tangent(X:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HourSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncMonth(DateTime:number, NumberOfMonths:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MinuteOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SameDate(A:number, B:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_SameTime(A:number, B:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_SecondOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Tomorrow(): number;
    RemObjects_Elements_RTL_Delphi___Global_WeekSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_YearSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DayOfWeek(DateTime:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfADay0(aYear:number, aDayOfYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfADay1(aYear:number, aMonth:number, aDay:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncMinute(aValue:number, aNumberOfMinutes:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncSecond(aValue:number, aNumberOfSeconds:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsSameDay(aValue:number, aBasis:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MonthSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeDay(aValue:number, aDay:number): number;
    RemObjects_Elements_RTL_Delphi___Global_Yesterday(): number;
    RemObjects_Elements_RTL_Delphi___Global_DaysInYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EncodeDate(aYear:number, aMonth:number, aDay:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EncodeTime(aHour:number, aMin:number, aSec:number, aMSec:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfAYear(aYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsLeapYear(Year:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MinuteSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeDate(aValue:number, aYear:number, aMonth:number, aDay:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeHour(aValue:number, aHour:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeTime(aValue:number, aHour:number, aMinute:number, aSecond:number, aMilliSecond:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeYear(aValue:number, aYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_CompareDate(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_CompareTime(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_CurrentYear(): number;
    RemObjects_Elements_RTL_Delphi___Global_DateInRange(aDate:number, aStartDate:number, aEndDate:number, aInclusive:boolean): boolean;
    RemObjects_Elements_RTL_Delphi___Global_DaysBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DaysInAYear(aYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DaysInMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfAMonth(aYear:number, aMonth:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfTheDay(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsValidDate(aYear:number, aMonth:number, aDay:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_IsValidTime(aHour:number, aMinute:number, aSecond:number, aMilliSecond:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_RecodeMonth(aValue:number, aMonth:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfADay0(aYear:number, aDayOfYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfADay1(aYear:number, aMonth:number, aDay:number): number;
    RemObjects_Elements_RTL_Delphi___Global_TimeInRange(aTime:number, aStartTime:number, aEndTime:number, aInclusive:boolean): boolean;
    RemObjects_Elements_RTL_Delphi___Global_WeeksInYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DayOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DayOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DaysInAMonth(aYear:number, aMonth:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HourOfTheDay(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HoursBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsInLeapYear(aValue:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_NthDayOfWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeMinute(aValue:number, aMinute:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeSecond(aValue:number, aSecond:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SameDateTime(A:number, B:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_StartOfAYear(aYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WeeksBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WeeksInAYear(aYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_YearsBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DayOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EncodeDateDay(aYear:number, aDayOfYear:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EndOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HourOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HourOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondOf(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MonthsBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfAMonth(aYear:number, aMonth:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfTheDay(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WeekOfTheYear0(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DateTimeToUnix(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_EncodeDateTime(aYear:number, aMonth:number, aDay:number, aHour:number, aMinute:number, aSecond:number, aMilliSecond:number): number;
    RemObjects_Elements_RTL_Delphi___Global_HourOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IncMilliSecond(aValue:number, aNumberOfMilliSeconds:number): number;
    RemObjects_Elements_RTL_Delphi___Global_IsValidDateDay(aYear:number, aDayOfYear:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MinuteOfTheDay(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MinutesBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MonthOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_RecodeDateTime(aValue:number, aYear:number, aMonth:number, aDay:number, aHour:number, aMinute:number, aSecond:number, aMilliSecond:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondOfTheDay(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondsBetween(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_UnixToDateTime(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastDays(aNow:number, aThen:number, aDays:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_CompareDateTime(A:number, B:number): number;
    RemObjects_Elements_RTL_Delphi___Global_DateTimeInRange(aDateTime:number, aStartDateTime:number, aEndDateTime:number, aInclusive:boolean): boolean;
    RemObjects_Elements_RTL_Delphi___Global_IsValidDateTime(aYear:number, aMonth:number, aDay:number, aHour:number, aMinute:number, aSecond:number, aMilliSecond:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_IsValidDateWeek(aYear:number, aWeekOfYear:number, aDayOfWeek:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_MilliSecondSpan(aNow:number, aThen:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MinuteOfTheHour(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MinuteOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_MinuteOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondOfTheHour(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondOfTheWeek(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_SecondOfTheYear(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_StartOfTheMonth(aValue:number): number;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastHours(aNow:number, aThen:number, aHours:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastWeeks(aNow:number, aThen:number, aWeeks:number): boolean;
    RemObjects_Elements_RTL_Delphi___Global_WithinPastYears(aNow:number, aThen:number, aYears:number): boolean;
    RemObjects_Elements_RTL_Delphi_VCL_TApplication_Create(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TApplication;
    RemObjects_Elements_RTL_Delphi_VCL_TApplication(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TApplication;
    RemObjects_Elements_RTL_Delphi_VCL_TScreen(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TScreen;
    RemObjects_Elements_RTL_Delphi_VCL_TFiler(aStream:RemObjects_Elements_RTL_Delphi_TStream, BufSize:number): RemObjects_Elements_RTL_Delphi_VCL_TFiler;
    RemObjects_Elements_RTL_Delphi_VCL_TReader(aStream:RemObjects_Elements_RTL_Delphi_TStream, BufSize:number): RemObjects_Elements_RTL_Delphi_VCL_TReader;
    RemObjects_Elements_RTL_Delphi_VCL_TWriter(aStream:RemObjects_Elements_RTL_Delphi_TStream, BufSize:number): RemObjects_Elements_RTL_Delphi_VCL_TWriter;
    RemObjects_Elements_RTL_Delphi_VCL_TResourceStream(Instance:number, aResName:any): RemObjects_Elements_RTL_Delphi_VCL_TResourceStream;
    RemObjects_Elements_RTL_Delphi_VCL_ComponentsHelper_CreateComponent0(aClassName:any, aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TComponent;
    RemObjects_Elements_RTL_Delphi_VCL_ComponentsHelper_CreateComponent1(aType:RemObjects_Elements_System_Type, aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TComponent;
    RemObjects_Elements_RTL_Delphi_VCL_TMargins_InitDefaults(Margins:RemObjects_Elements_RTL_Delphi_VCL_TMargins);
    RemObjects_Elements_RTL_Delphi_VCL_TMargins(): RemObjects_Elements_RTL_Delphi_VCL_TMargins;
    RemObjects_Elements_RTL_Delphi_VCL_TPadding_InitDefaults(Margins:RemObjects_Elements_RTL_Delphi_VCL_TMargins);
    RemObjects_Elements_RTL_Delphi_VCL_TPadding(): RemObjects_Elements_RTL_Delphi_VCL_TPadding;
    RemObjects_Elements_RTL_Delphi_VCL_TNativeControl(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TNativeControl;
    RemObjects_Elements_RTL_Delphi_VCL_TFont(): RemObjects_Elements_RTL_Delphi_VCL_TFont;
    RemObjects_Elements_RTL_Delphi_VCL_TCustomForm(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TCustomForm;
    RemObjects_Elements_RTL_Delphi_VCL_TForm(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TForm;
    RemObjects_Elements_RTL_Delphi_VCL_TButton_Create(AOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TButton;
    RemObjects_Elements_RTL_Delphi_VCL_TButton(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TButton;
    RemObjects_Elements_RTL_Delphi_VCL_TLabel_Create(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TLabel;
    RemObjects_Elements_RTL_Delphi_VCL_TLabel(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TLabel;
    RemObjects_Elements_RTL_Delphi_VCL_TEdit(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TEdit;
    RemObjects_Elements_RTL_Delphi_VCL_TListControlItems0(): RemObjects_Elements_RTL_Delphi_VCL_TListControlItems;
    RemObjects_Elements_RTL_Delphi_VCL_TListControlItems1(aOwnsObject:boolean): RemObjects_Elements_RTL_Delphi_VCL_TListControlItems;
    RemObjects_Elements_RTL_Delphi_VCL_TListControlItems2(aQuoteChar:number, aDelimiter:number): RemObjects_Elements_RTL_Delphi_VCL_TListControlItems;
    RemObjects_Elements_RTL_Delphi_VCL_TListControlItems3(aQuoteChar:number, aDelimiter:number, aOptions:number): RemObjects_Elements_RTL_Delphi_VCL_TListControlItems;
    RemObjects_Elements_RTL_Delphi_VCL_TListControlItems4(aDuplicates:number, aSorted:boolean, aCaseSensitive:boolean): RemObjects_Elements_RTL_Delphi_VCL_TListControlItems;
    RemObjects_Elements_RTL_Delphi_VCL_TListBox(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TListBox;
    RemObjects_Elements_RTL_Delphi_VCL_TListControl(aOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TListControl;
    RemObjects_Elements_RTL_Delphi_VCL___Global_PlatformShowMessage(aMessage:string);
    RemObjects_Elements_RTL_Delphi_VCL___Global_ShowMessage(aMessage:string);
    RemObjects_Elements_RTL_Convert_HexStringToUInt32(aValue:any): number;
    RemObjects_Elements_RTL_Convert_HexStringToUInt64(aValue:any): number;
    RemObjects_Elements_RTL_Convert_ToDoubleInvariant(aValue:any): number;
    RemObjects_Elements_RTL_Convert_ToStringInvariant(aValue:number, aDigitsAfterDecimalPoint:number): any;
    RemObjects_Elements_RTL_Convert_Utf8BytesToString(aBytes:any, aLength:any): any;
    RemObjects_Elements_RTL_Convert_HexStringToByteArray(aData:any): any;
    RemObjects_Elements_RTL_Convert_TryHexStringToUInt32(aValue:any): any;
    RemObjects_Elements_RTL_Convert_TryHexStringToUInt64(aValue:any): any;
    RemObjects_Elements_RTL_Convert_TryToDoubleInvariant(aValue:any): any;
    RemObjects_Elements_RTL_Convert_Base64StringToByteArray(S:any): any;
    RemObjects_Elements_RTL_Convert_MilisecondsToTimeString(aMS:number): any;
    RemObjects_Elements_RTL_Convert_ToByte0(aValue:boolean): number;
    RemObjects_Elements_RTL_Convert_ToByte1(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToByte2(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToByte3(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToByte4(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToByte5(aValue:any): number;
    RemObjects_Elements_RTL_Convert_ToChar0(aValue:boolean): number;
    RemObjects_Elements_RTL_Convert_ToChar1(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToChar2(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToChar3(aValue:any): number;
    RemObjects_Elements_RTL_Convert_ToChar4(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt320(aValue:boolean): number;
    RemObjects_Elements_RTL_Convert_ToInt321(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt322(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt323(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt324(aValue:any): number;
    RemObjects_Elements_RTL_Convert_ToInt325(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt640(aValue:boolean): number;
    RemObjects_Elements_RTL_Convert_ToInt641(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt642(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt643(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToInt644(aValue:any): number;
    RemObjects_Elements_RTL_Convert_ToInt645(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToDouble0(aValue:boolean): number;
    RemObjects_Elements_RTL_Convert_ToDouble1(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToDouble2(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToDouble3(aValue:any, aLocale:RemObjects_Elements_RTL_Locale): number;
    RemObjects_Elements_RTL_Convert_ToDouble4(aValue:number): number;
    RemObjects_Elements_RTL_Convert_ToString0(aValue:boolean): any;
    RemObjects_Elements_RTL_Convert_ToString1(aValue:number): any;
    RemObjects_Elements_RTL_Convert_ToString2(aValue:number, aDigitsAfterDecimalPoint:number, aLocale:RemObjects_Elements_RTL_Locale): any;
    RemObjects_Elements_RTL_Convert_ToString3(aValue:number, aBase:number): any;
    RemObjects_Elements_RTL_Convert_ToString4(aValue:number, aBase:number): any;
    RemObjects_Elements_RTL_Convert_ToString5(aValue:RemObjects_Elements_System_Object): any;
    RemObjects_Elements_RTL_Convert_ToString6(aValue:number, aBase:number): any;
    RemObjects_Elements_RTL_Convert_ToString7(aValue:number, aBase:number): any;
    RemObjects_Elements_RTL_Convert_ToBoolean0(aValue:number): boolean;
    RemObjects_Elements_RTL_Convert_ToBoolean1(aValue:number): boolean;
    RemObjects_Elements_RTL_Convert_ToBoolean2(aValue:number): boolean;
    RemObjects_Elements_RTL_Convert_ToBoolean3(aValue:any): boolean;
    RemObjects_Elements_RTL_Convert_ToBoolean4(aValue:number): boolean;
    RemObjects_Elements_RTL_Convert_TryToInt32(aValue:any): any;
    RemObjects_Elements_RTL_Convert_TryToInt64(aValue:any): any;
    RemObjects_Elements_RTL_Convert_ToHexString0(aData:any): any;
    RemObjects_Elements_RTL_Convert_ToHexString1(aData:any, aCount:number): any;
    RemObjects_Elements_RTL_Convert_ToHexString2(aData:any, aOffset:number, aCount:number): any;
    RemObjects_Elements_RTL_Convert_ToHexString3(aData:any): any;
    RemObjects_Elements_RTL_Convert_ToHexString4(aValue:number, aWidth:number): any;
    RemObjects_Elements_RTL_Convert_ToUtf8Bytes(aValue:any): any;
    RemObjects_Elements_RTL_Convert_TryToDouble(aValue:any, aLocale:RemObjects_Elements_RTL_Locale): any;
    RemObjects_Elements_RTL_Convert_ToOctalString(aValue:number, aWidth:number): any;
    RemObjects_Elements_RTL_Convert_ToBase64String(S:any, aStartIndex:number, aLength:number): any;
    RemObjects_Elements_RTL_Convert_ToBinaryString(aValue:number, aWidth:number): any;
    RemObjects_Elements_RTL_DateParser(): RemObjects_Elements_RTL_DateParser;
    RemObjects_Elements_RTL_DateTime_op_LessThanOrEqual(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): boolean;
    RemObjects_Elements_RTL_DateTime_op_GreaterThanOrEqual(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): boolean;
    RemObjects_Elements_RTL_DateTime_Compare(Value1:RemObjects_Elements_RTL_DateTime, Value2:RemObjects_Elements_RTL_DateTime): number;
    RemObjects_Elements_RTL_DateTime_ToOADate(aDateTime:RemObjects_Elements_RTL_DateTime): number;
    RemObjects_Elements_RTL_DateTime_TryParse0(aDateTime:any, aOptions:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime_TryParse1(aDateTime:any, aLocale:RemObjects_Elements_RTL_Locale, aOptions:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime_TryParse2(aDateTime:any, aFormat:any, aOptions:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime_TryParse3(aDateTime:any, aFormat:any, aLocale:RemObjects_Elements_RTL_Locale, aOptions:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime_TimeSince(aOtherDateTime:RemObjects_Elements_RTL_DateTime): any;
    RemObjects_Elements_RTL_DateTime_FromOADate(aOADate:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime_op_Addition(a:RemObjects_Elements_RTL_DateTime, b:any): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime_op_Equality(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): boolean;
    RemObjects_Elements_RTL_DateTime_op_LessThan(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): boolean;
    RemObjects_Elements_RTL_DateTime_op_Inequality(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): boolean;
    RemObjects_Elements_RTL_DateTime_op_GreaterThan(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): boolean;
    RemObjects_Elements_RTL_DateTime_op_Subtraction0(a:RemObjects_Elements_RTL_DateTime, b:RemObjects_Elements_RTL_DateTime): any;
    RemObjects_Elements_RTL_DateTime_op_Subtraction1(a:RemObjects_Elements_RTL_DateTime, b:any): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime0(): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime1(aYear:number, aMonth:number, aDay:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime2(aYear:number, aMonth:number, aDay:number, anHour:number, aMinute:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime3(aYear:number, aMonth:number, aDay:number, anHour:number, aMinute:number, aSecond:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_DateTime4(aTicks:number): RemObjects_Elements_RTL_DateTime;
    RemObjects_Elements_RTL_String_EqualsIgnoringCase0(ValueA:any, ValueB:any): boolean;
    RemObjects_Elements_RTL_String_EqualsIgnoringCase1(self:any, Value:any): boolean;
    RemObjects_Elements_RTL_String_IsNullOrWhiteSpace(Value:any): boolean;
    RemObjects_Elements_RTL_String_CompareToIgnoreCase(self:any, Value:any): number;
    RemObjects_Elements_RTL_String_ContainsAnyNonASCII0(self:any): boolean;
    RemObjects_Elements_RTL_String_ContainsAnyNonASCII1(self:any, aStartIndex:number): boolean;
    RemObjects_Elements_RTL_String_op_LessThanOrEqual(Value1:any, Value2:any): boolean;
    RemObjects_Elements_RTL_String_ToUnicodeCharacters(self:any): any;
    RemObjects_Elements_RTL_String_ToUnicodeCodePoints(self:any): any;
    RemObjects_Elements_RTL_String_op_GreaterThanOrEqual(Value1:any, Value2:any): boolean;
    RemObjects_Elements_RTL_String_SplitAtLastOccurrenceOf(self:any, aSeparator:any): any;
    RemObjects_Elements_RTL_String_UnicodeCodePointAtIndex(self:any, aIndex:number): number;
    RemObjects_Elements_RTL_String_SplitAtFirstOccurrenceOf(self:any, aSeparator:any): any;
    RemObjects_Elements_RTL_String_ToUnicodeCodePointIndices(self:any): any;
    RemObjects_Elements_RTL_String_EqualsIgnoringCaseInvariant0(ValueA:any, ValueB:any): boolean;
    RemObjects_Elements_RTL_String_EqualsIgnoringCaseInvariant1(self:any, Value:any): boolean;
    RemObjects_Elements_RTL_String_SubstringToLastOccurrenceOf(self:any, aSeparator:any): any;
    RemObjects_Elements_RTL_String_UnicodeCodePointBeforeIndex(self:any, aIndex:number): number;
    RemObjects_Elements_RTL_String_SubstringToFirstOccurrenceOf(self:any, aSeparator:any): any;
    RemObjects_Elements_RTL_String_SubstringFromLastOccurrenceOf(self:any, aSeparator:any): any;
    RemObjects_Elements_RTL_String_SubstringFromFirstOccurrenceOf(self:any, aSeparator:any): any;
    RemObjects_Elements_RTL_String_IsIndexInsideOfAJoinedUnicodeCharacter(self:any, aIndex:number): boolean;
    RemObjects_Elements_RTL_String_StartIndexOfJoinedUnicodeCharacterAtIndex(self:any, aIndex:number): number;
    RemObjects_Elements_RTL_String_IndexAfterJoinedUnicodeCharacterCoveringIndex(self:any, aIndex:number): number;
    RemObjects_Elements_RTL_String_Join0(aSeparator:any, Values:any): any;
    RemObjects_Elements_RTL_String_Join1(aSeparator:any, Values:any): any;
    RemObjects_Elements_RTL_String_Trim(self:any, TrimChars:any): any;
    RemObjects_Elements_RTL_String_Split(self:any, aSeparator:any, aRemoveEmptyEntries:boolean): any;
    RemObjects_Elements_RTL_String_Equals0(ValueA:any, ValueB:any): boolean;
    RemObjects_Elements_RTL_String_Equals1(self:any, Value:any): boolean;
    RemObjects_Elements_RTL_String_Format(aFormat:any, aParams:any): any;
    RemObjects_Elements_RTL_String_PadEnd(self:any, TotalWidth:number, PaddingChar:number): any;
    RemObjects_Elements_RTL_String_Compare(Value1:any, Value2:any): number;
    RemObjects_Elements_RTL_String_TrimEnd(self:any, TrimChars:any): any;
    RemObjects_Elements_RTL_String_EndsWith(self:any, Value:any, IgnoreCase:boolean): boolean;
    RemObjects_Elements_RTL_String_PadStart(self:any, TotalWidth:number, PaddingChar:number): any;
    RemObjects_Elements_RTL_String_CompareTo(self:any, Value:any): number;
    RemObjects_Elements_RTL_String_TrimStart(self:any, TrimChars:any): any;
    RemObjects_Elements_RTL_String_IndexOfAny(self:any, AnyOf:any, StartIndex:number): number;
    RemObjects_Elements_RTL_String_StartsWith(self:any, Value:any, IgnoreCase:boolean): boolean;
    RemObjects_Elements_RTL_String_ContainsAny0(self:any, AnyOf:any): boolean;
    RemObjects_Elements_RTL_String_ContainsAny1(self:any, AnyOf:any, aStartIndex:number): boolean;
    RemObjects_Elements_RTL_String_GetSequence(self:any): any;
    RemObjects_Elements_RTL_String_LastIndexOf0(self:any, Value:number, StartIndex:number): number;
    RemObjects_Elements_RTL_String_LastIndexOf1(self:any, Value:any, StartIndex:number): number;
    RemObjects_Elements_RTL_String_ToByteArray0(self:any): any;
    RemObjects_Elements_RTL_String_ToByteArray1(self:any, aEncoding:any): any;
    RemObjects_Elements_RTL_String_ToCharArray(self:any): any;
    RemObjects_Elements_RTL_String_ToHexString(self:any): any;
    RemObjects_Elements_RTL_String_ContainsOnly0(self:any, AnyOf:any): boolean;
    RemObjects_Elements_RTL_String_ContainsOnly1(self:any, AnyOf:any, aStartIndex:number): boolean;
    RemObjects_Elements_RTL_String_op_Addition0(Value1:RemObjects_Elements_System_Object, Value2:any): any;
    RemObjects_Elements_RTL_String_op_Addition1(Value1:any, Value2:RemObjects_Elements_System_Object): any;
    RemObjects_Elements_RTL_String_op_Addition2(Value1:any, Value2:any): any;
    RemObjects_Elements_RTL_String_op_Equality(Value1:any, Value2:any): boolean;
    RemObjects_Elements_RTL_String_op_Implicit(Value:number): any;
    RemObjects_Elements_RTL_String_op_LessThan(Value1:any, Value2:any): boolean;
    RemObjects_Elements_RTL_String_IsNullOrEmpty(Value:any): boolean;
    RemObjects_Elements_RTL_String_LastIndexOfAny(self:any, AnyOf:any, StartIndex:number): number;
    RemObjects_Elements_RTL_String_op_Inequality(Value1:any, Value2:any): boolean;
    RemObjects_Elements_RTL_String_op_GreaterThan(Value1:any, Value2:any): boolean;
    RemObjects_Elements_RTL_Encoding_GetBytes(self:any, aValue:any, aIncludeBOM:boolean): any;
    RemObjects_Elements_RTL_Encoding_GetString0(self:any, aValue:any): any;
    RemObjects_Elements_RTL_Encoding_GetString1(self:any, aValue:any, aOffset:number, aCount:number): any;
    RemObjects_Elements_RTL_Encoding_GetString2(self:any, aValue:any): any;
    RemObjects_Elements_RTL_Encoding_GetEncoding(aName:any): any;
    RemObjects_Elements_RTL_RTLException0(): RemObjects_Elements_RTL_RTLException;
    RemObjects_Elements_RTL_RTLException1(aMessage:any): RemObjects_Elements_RTL_RTLException;
    RemObjects_Elements_RTL_RTLException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_RTLException;
    RemObjects_Elements_RTL_UrlException0(): RemObjects_Elements_RTL_UrlException;
    RemObjects_Elements_RTL_UrlException1(aMessage:any): RemObjects_Elements_RTL_UrlException;
    RemObjects_Elements_RTL_UrlException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_UrlException;
    RemObjects_Elements_RTL_UrlParserException0(): RemObjects_Elements_RTL_UrlParserException;
    RemObjects_Elements_RTL_UrlParserException1(aMessage:any): RemObjects_Elements_RTL_UrlParserException;
    RemObjects_Elements_RTL_UrlParserException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_UrlParserException;
    RemObjects_Elements_RTL_ConversionException0(): RemObjects_Elements_RTL_ConversionException;
    RemObjects_Elements_RTL_ConversionException1(aMessage:any): RemObjects_Elements_RTL_ConversionException;
    RemObjects_Elements_RTL_ConversionException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_ConversionException;
    RemObjects_Elements_RTL_ArgumentNullException_RaiseIfNil(Value:RemObjects_Elements_System_Object, Name:any);
    RemObjects_Elements_RTL_ArgumentNullException0(): RemObjects_Elements_RTL_ArgumentNullException;
    RemObjects_Elements_RTL_ArgumentNullException1(aMessage:any): RemObjects_Elements_RTL_ArgumentNullException;
    RemObjects_Elements_RTL_ArgumentOutOfRangeException0(): RemObjects_Elements_RTL_ArgumentOutOfRangeException;
    RemObjects_Elements_RTL_ArgumentOutOfRangeException1(aMessage:any): RemObjects_Elements_RTL_ArgumentOutOfRangeException;
    RemObjects_Elements_RTL_ArgumentOutOfRangeException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_ArgumentOutOfRangeException;
    RemObjects_Elements_RTL_FormatException0(): RemObjects_Elements_RTL_FormatException;
    RemObjects_Elements_RTL_FormatException1(aMessage:any): RemObjects_Elements_RTL_FormatException;
    RemObjects_Elements_RTL_FormatException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_FormatException;
    RemObjects_Elements_RTL_InvalidOperationException0(): RemObjects_Elements_RTL_InvalidOperationException;
    RemObjects_Elements_RTL_InvalidOperationException1(aMessage:any): RemObjects_Elements_RTL_InvalidOperationException;
    RemObjects_Elements_RTL_InvalidOperationException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_InvalidOperationException;
    RemObjects_Elements_RTL_Guid_NewGuid(): RemObjects_Elements_RTL_Guid;
    RemObjects_Elements_RTL_Guid_TryParse(aValue:any): RemObjects_Elements_RTL_Guid;
    RemObjects_Elements_RTL_Guid_op_Equality(a:RemObjects_Elements_RTL_Guid, b:RemObjects_Elements_RTL_Guid): boolean;
    RemObjects_Elements_RTL_Guid_op_Inequality(a:RemObjects_Elements_RTL_Guid, b:RemObjects_Elements_RTL_Guid): boolean;
    RemObjects_Elements_RTL_Guid0(aValue:any): RemObjects_Elements_RTL_Guid;
    RemObjects_Elements_RTL_Guid1(aValue:any): RemObjects_Elements_RTL_Guid;
    RemObjects_Elements_RTL_JsonArray_Load(JsonString:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray_op_Implicit0(aValue:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray_op_Implicit1(aValue:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray_op_Implicit2(aValue:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray_op_Implicit3(aValue:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray_op_Implicit4(aValue:RemObjects_Elements_RTL_JsonArray): any;
    RemObjects_Elements_RTL_JsonArray_op_Implicit5(aValue:RemObjects_Elements_RTL_JsonArray): any;
    RemObjects_Elements_RTL_JsonArray0(): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray1(aItems:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray2(aItems:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray3(aItems:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonArray4(aItems:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonDeserializer(JsonString:any): RemObjects_Elements_RTL_JsonDeserializer;
    RemObjects_Elements_RTL_JsonNode_Create0(aValue:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonNode_Create1(aValue:any): RemObjects_Elements_RTL_JsonArray;
    RemObjects_Elements_RTL_JsonNode_Create2(aValue:any): RemObjects_Elements_RTL_JsonObject;
    RemObjects_Elements_RTL_JsonNode_Create3(aValue:any): RemObjects_Elements_RTL_JsonBooleanValue;
    RemObjects_Elements_RTL_JsonNode_Create4(aValue:any): RemObjects_Elements_RTL_JsonFloatValue;
    RemObjects_Elements_RTL_JsonNode_Create5(aValue:any): RemObjects_Elements_RTL_JsonIntegerValue;
    RemObjects_Elements_RTL_JsonNode_Create6(aValue:any): RemObjects_Elements_RTL_JsonStringValue;
    RemObjects_Elements_RTL_JsonException0(): RemObjects_Elements_RTL_JsonException;
    RemObjects_Elements_RTL_JsonException1(aMessage:any): RemObjects_Elements_RTL_JsonException;
    RemObjects_Elements_RTL_JsonException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_JsonException;
    RemObjects_Elements_RTL_JsonNodeTypeException0(): RemObjects_Elements_RTL_JsonNodeTypeException;
    RemObjects_Elements_RTL_JsonNodeTypeException1(aMessage:any): RemObjects_Elements_RTL_JsonNodeTypeException;
    RemObjects_Elements_RTL_JsonNodeTypeException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_JsonNodeTypeException;
    RemObjects_Elements_RTL_JsonParserException0(): RemObjects_Elements_RTL_JsonParserException;
    RemObjects_Elements_RTL_JsonParserException1(aMessage:any): RemObjects_Elements_RTL_JsonParserException;
    RemObjects_Elements_RTL_JsonParserException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_JsonParserException;
    RemObjects_Elements_RTL_JsonUnexpectedTokenException0(): RemObjects_Elements_RTL_JsonUnexpectedTokenException;
    RemObjects_Elements_RTL_JsonUnexpectedTokenException1(aMessage:any): RemObjects_Elements_RTL_JsonUnexpectedTokenException;
    RemObjects_Elements_RTL_JsonUnexpectedTokenException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_JsonUnexpectedTokenException;
    RemObjects_Elements_RTL_JsonObject_Load(JsonString:any): RemObjects_Elements_RTL_JsonObject;
    RemObjects_Elements_RTL_JsonObject0(): RemObjects_Elements_RTL_JsonObject;
    RemObjects_Elements_RTL_JsonObject1(aItems:any): RemObjects_Elements_RTL_JsonObject;
    RemObjects_Elements_RTL_JsonSerializer(Value:RemObjects_Elements_RTL_JsonNode): RemObjects_Elements_RTL_JsonSerializer;
    RemObjects_Elements_RTL_JsonTokenizer0(aJson:any): RemObjects_Elements_RTL_JsonTokenizer;
    RemObjects_Elements_RTL_JsonTokenizer1(aJson:any, SkipWhitespaces:boolean): RemObjects_Elements_RTL_JsonTokenizer;
    RemObjects_Elements_RTL_JsonStringValue_op_Equality0(aLeft:RemObjects_Elements_System_Object, aRight:RemObjects_Elements_RTL_JsonStringValue): boolean;
    RemObjects_Elements_RTL_JsonStringValue_op_Equality1(aLeft:RemObjects_Elements_RTL_JsonStringValue, aRight:RemObjects_Elements_System_Object): boolean;
    RemObjects_Elements_RTL_JsonStringValue_op_Implicit(aValue:any): RemObjects_Elements_RTL_JsonStringValue;
    RemObjects_Elements_RTL_JsonStringValue(aValue:any): RemObjects_Elements_RTL_JsonStringValue;
    RemObjects_Elements_RTL_JsonIntegerValue_op_Implicit0(aValue:number): RemObjects_Elements_RTL_JsonIntegerValue;
    RemObjects_Elements_RTL_JsonIntegerValue_op_Implicit1(aValue:number): RemObjects_Elements_RTL_JsonIntegerValue;
    RemObjects_Elements_RTL_JsonIntegerValue_op_Implicit2(aValue:RemObjects_Elements_RTL_JsonIntegerValue): number;
    RemObjects_Elements_RTL_JsonIntegerValue_op_Implicit3(aValue:RemObjects_Elements_RTL_JsonIntegerValue): number;
    RemObjects_Elements_RTL_JsonIntegerValue_op_Implicit4(aValue:RemObjects_Elements_RTL_JsonIntegerValue): number;
    RemObjects_Elements_RTL_JsonIntegerValue_op_Implicit5(aValue:RemObjects_Elements_RTL_JsonIntegerValue): RemObjects_Elements_RTL_JsonFloatValue;
    RemObjects_Elements_RTL_JsonIntegerValue(aValue:number): RemObjects_Elements_RTL_JsonIntegerValue;
    RemObjects_Elements_RTL_JsonFloatValue_op_Implicit0(aValue:number): RemObjects_Elements_RTL_JsonFloatValue;
    RemObjects_Elements_RTL_JsonFloatValue_op_Implicit1(aValue:number): RemObjects_Elements_RTL_JsonFloatValue;
    RemObjects_Elements_RTL_JsonFloatValue_op_Implicit2(aValue:RemObjects_Elements_RTL_JsonFloatValue): number;
    RemObjects_Elements_RTL_JsonFloatValue(aValue:number): RemObjects_Elements_RTL_JsonFloatValue;
    RemObjects_Elements_RTL_JsonBooleanValue_op_Implicit(aValue:boolean): RemObjects_Elements_RTL_JsonBooleanValue;
    RemObjects_Elements_RTL_JsonBooleanValue(aValue:boolean): RemObjects_Elements_RTL_JsonBooleanValue;
    RemObjects_Elements_RTL_JsonNullValue(aValue:boolean): RemObjects_Elements_RTL_JsonNullValue;
    RemObjects_Elements_RTL_NumberFormatInfo0(aIsReadOnly:boolean): RemObjects_Elements_RTL_NumberFormatInfo;
    RemObjects_Elements_RTL_NumberFormatInfo1(aDecimalSeparator:number, aThousandsSeparator:number, aCurrency:any, aIsReadOnly:boolean): RemObjects_Elements_RTL_NumberFormatInfo;
    RemObjects_Elements_RTL_DateTimeFormatInfo(aLocale:RemObjects_Elements_System_Locale, aIsReadonly:boolean): RemObjects_Elements_RTL_DateTimeFormatInfo;
    RemObjects_Elements_RTL_Locale_op_Implicit0(aValue:RemObjects_Elements_RTL_Locale): RemObjects_Elements_System_Locale;
    RemObjects_Elements_RTL_Locale_op_Implicit1(aValue:RemObjects_Elements_System_Locale): RemObjects_Elements_RTL_Locale;
    RemObjects_Elements_RTL_Locale0(aLocale:any): RemObjects_Elements_RTL_Locale;
    RemObjects_Elements_RTL_Locale1(aLocaleID:RemObjects_Elements_System_Locale): RemObjects_Elements_RTL_Locale;
    RemObjects_Elements_RTL_Path_CombineWindowsPath(aBasePath:any, aPaths:any): any;
    RemObjects_Elements_RTL_Path_GetParentDirectory(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetWindowsFileName(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetNetworkServerName(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetUnixParentDirectory(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetPathWithoutExtension(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetWindowsParentDirectory(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetFileNameWithoutExtension(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetUnixFileNameWithoutExtension(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetWindowsFileNameWithoutExtension(aFileName:any): any;
    RemObjects_Elements_RTL_Path_Combine(aBasePath:any, aPaths:any): any;
    RemObjects_Elements_RTL_Path_GetPath(aFullPath:any, aBasePath:any): any;
    RemObjects_Elements_RTL_Path_GetFileName(aFileName:any): any;
    RemObjects_Elements_RTL_Path_GetExtension(aFileName:any): any;
    RemObjects_Elements_RTL_Path_ChangeExtension(aFileName:any, NewExtension:any): any;
    RemObjects_Elements_RTL_Path_CombineUnixPath(aBasePath:any, aPaths:any): any;
    RemObjects_Elements_RTL_Path_GetUnixFileName(aFileName:any): any;
    RemObjects_Elements_RTL_WrappedPlatformStream_op_Implicit(aStream:RemObjects_Elements_RTL_WrappedPlatformStream): RemObjects_Elements_System_Stream;
    RemObjects_Elements_RTL_MemoryStream_op_Implicit(aValue:any): RemObjects_Elements_RTL_MemoryStream;
    RemObjects_Elements_RTL_MemoryStream0(): RemObjects_Elements_RTL_MemoryStream;
    RemObjects_Elements_RTL_MemoryStream1(aValue:any): RemObjects_Elements_RTL_MemoryStream;
    RemObjects_Elements_RTL_MemoryStream2(aValue:any, aCanWrite:boolean): RemObjects_Elements_RTL_MemoryStream;
    RemObjects_Elements_RTL_MemoryStream3(aCapacity:number): RemObjects_Elements_RTL_MemoryStream;
    RemObjects_Elements_RTL_MemoryStream4(aValue:any): RemObjects_Elements_RTL_MemoryStream;
    RemObjects_Elements_RTL_UnicodeException0(): RemObjects_Elements_RTL_UnicodeException;
    RemObjects_Elements_RTL_UnicodeException1(aMessage:any): RemObjects_Elements_RTL_UnicodeException;
    RemObjects_Elements_RTL_UnicodeException2(aFormat:any, aParams:any): RemObjects_Elements_RTL_UnicodeException;
    RemObjects_Elements_RTL_StringFormatter_FormatString(aFormat:any, args:any): any;
    RemObjects_Elements_RTL_Uri_TryUriWithString(aUriString:any): RemObjects_Elements_RTL_Uri;
    RemObjects_Elements_RTL_Uri_op_Equality(Value1:RemObjects_Elements_RTL_Uri, Value2:RemObjects_Elements_RTL_Uri): boolean;
    RemObjects_Elements_RTL_Uri_UriWithString(aUriString:any): RemObjects_Elements_RTL_Uri;
    RemObjects_Elements_RTL_Uri_op_Inequality(Value1:RemObjects_Elements_RTL_Uri, Value2:RemObjects_Elements_RTL_Uri): boolean;
    RemObjects_Elements_RTL_Url_TryUrlWithString(aUrlString:any): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Url_UrlWithComponents(aScheme:any, aHost:any, aPort:number, aPath:any, aQueryString:any, aFragment:any, aUser:any): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Url_UrlWithWindowsPath(aPath:any, aIsDirectory:boolean): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Url_AddPercentEncodingsToPath(aString:any): any;
    RemObjects_Elements_RTL_Url_RemovePercentEncodingsFromPath(aString:any, aAlsoRemovePlusCharacter:boolean): any;
    RemObjects_Elements_RTL_Url_TryRemovePercentEncodingsFromPath(aString:any, aAlsoRemovePlusCharacter:boolean): any;
    RemObjects_Elements_RTL_Url_op_Equality0(Value1:RemObjects_Elements_System_Object, Value2:RemObjects_Elements_RTL_Url): boolean;
    RemObjects_Elements_RTL_Url_op_Equality1(Value1:RemObjects_Elements_RTL_Url, Value2:RemObjects_Elements_System_Object): boolean;
    RemObjects_Elements_RTL_Url_op_Equality2(Value1:RemObjects_Elements_RTL_Url, Value2:RemObjects_Elements_RTL_Url): boolean;
    RemObjects_Elements_RTL_Url_UrlWithString(aUrlString:any): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Url_op_Inequality0(Value1:RemObjects_Elements_System_Object, Value2:RemObjects_Elements_RTL_Url): boolean;
    RemObjects_Elements_RTL_Url_op_Inequality1(Value1:RemObjects_Elements_RTL_Url, Value2:RemObjects_Elements_System_Object): boolean;
    RemObjects_Elements_RTL_Url_op_Inequality2(Value1:RemObjects_Elements_RTL_Url, Value2:RemObjects_Elements_RTL_Url): boolean;
    RemObjects_Elements_RTL_Url_UrlWithFilePath0(aPath:any, aIsDirectory:boolean): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Url_UrlWithFilePath1(aPath:any, aUrl:RemObjects_Elements_RTL_Url, aIsDirectory:boolean): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Url_UrlWithUnixPath(aPath:any, aIsDirectory:boolean): RemObjects_Elements_RTL_Url;
    RemObjects_Elements_RTL_Urn_TryUrnWithString(aUrnString:any): RemObjects_Elements_RTL_Urn;
    RemObjects_Elements_RTL_Urn_UrnWithString(aUrnString:any): RemObjects_Elements_RTL_Urn;
    RemObjects_Elements_RTL_XmlDocument_FromUrl(aUrl:RemObjects_Elements_RTL_Url): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_FromBinary(aBinary:any): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_FromString(aString:any): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_TryFromUrl0(aUrl:RemObjects_Elements_RTL_Url): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_TryFromUrl1(aUrl:RemObjects_Elements_RTL_Url, aAllowBrokenDocument:boolean): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_TryFromBinary0(aBinary:any): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_TryFromBinary1(aBinary:any, aAllowBrokenDocument:boolean): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_TryFromString0(aString:any): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_TryFromString1(aString:any, aAllowBrokenDocument:boolean): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_WithRootElement0(aName:any): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlDocument_WithRootElement1(aElement:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlDocument;
    RemObjects_Elements_RTL_XmlNode(): RemObjects_Elements_RTL_XmlNode;
    RemObjects_Elements_RTL_XmlElement0(aLocalName:any): RemObjects_Elements_RTL_XmlElement;
    RemObjects_Elements_RTL_XmlElement1(aLocalName:any, aValue:any): RemObjects_Elements_RTL_XmlElement;
    RemObjects_Elements_RTL_XmlAttribute0(aLocalName:any, aNamespace:RemObjects_Elements_RTL_XmlNamespace, aValue:any): RemObjects_Elements_RTL_XmlAttribute;
    RemObjects_Elements_RTL_XmlAttribute1(aParent:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlAttribute;
    RemObjects_Elements_RTL_XmlComment(aParent:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlComment;
    RemObjects_Elements_RTL_XmlCData(aParent:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlCData;
    RemObjects_Elements_RTL_XmlNamespace(aPrefix:any, aUri:RemObjects_Elements_RTL_Uri): RemObjects_Elements_RTL_XmlNamespace;
    RemObjects_Elements_RTL_XmlProcessingInstruction(aParent:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlProcessingInstruction;
    RemObjects_Elements_RTL_XmlText(aParent:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlText;
    RemObjects_Elements_RTL_XmlDocumentType(aParent:RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlDocumentType;
    RemObjects_Elements_RTL_XmlRange(): RemObjects_Elements_RTL_XmlRange;
    RemObjects_Elements_RTL_XmlDocCurrentPosition(): RemObjects_Elements_RTL_XmlDocCurrentPosition;
    RemObjects_Elements_RTL_XmlException0(aMessage:any): RemObjects_Elements_RTL_XmlException;
    RemObjects_Elements_RTL_XmlException1(aMessage:any, aRow:number, aColumn:number): RemObjects_Elements_RTL_XmlException;
    RemObjects_Elements_RTL_XmlParser0(aXmlString:any): RemObjects_Elements_RTL_XmlParser;
    RemObjects_Elements_RTL_XmlParser1(aXmlString:any, aOptions:RemObjects_Elements_RTL_XmlFormattingOptions): RemObjects_Elements_RTL_XmlParser;
    RemObjects_Elements_RTL_XmlFormattingOptions(): RemObjects_Elements_RTL_XmlFormattingOptions;
    RemObjects_Elements_RTL_XmlErrorInfo(): RemObjects_Elements_RTL_XmlErrorInfo;
    RemObjects_Elements_RTL_XmlTokenizer(aXml:any): RemObjects_Elements_RTL_XmlTokenizer;
    RemObjects_Elements_System_Attribute(): RemObjects_Elements_System_Attribute;
    RemObjects_Elements_System_AttributeUsageAttribute(aTargets:number): RemObjects_Elements_System_AttributeUsageAttribute;
    RemObjects_Elements_System_CallingConventionAttribute(aCC:number): RemObjects_Elements_System_CallingConventionAttribute;
    RemObjects_Elements_System_FlagsAttribute(): RemObjects_Elements_System_FlagsAttribute;
    RemObjects_Elements_System_SkipDebugAttribute(): RemObjects_Elements_System_SkipDebugAttribute;
    RemObjects_Elements_System_ObsoleteAttribute0(): RemObjects_Elements_System_ObsoleteAttribute;
    RemObjects_Elements_System_ObsoleteAttribute1(aMsg:string, aFail:boolean): RemObjects_Elements_System_ObsoleteAttribute;
    RemObjects_Elements_System_ConditionalAttribute(aCond:string): RemObjects_Elements_System_ConditionalAttribute;
    RemObjects_Elements_System_DllExportAttribute0(): RemObjects_Elements_System_DllExportAttribute;
    RemObjects_Elements_System_DllExportAttribute1(aName:string): RemObjects_Elements_System_DllExportAttribute;
    RemObjects_Elements_System_DllImportAttribute(aDllName:string): RemObjects_Elements_System_DllImportAttribute;
    RemObjects_Elements_System_UnionAttribute(): RemObjects_Elements_System_UnionAttribute;
    RemObjects_Elements_System_GCSkipIfOnStackAttribute(): RemObjects_Elements_System_GCSkipIfOnStackAttribute;
    RemObjects_Elements_System_Console_Write0();
    RemObjects_Elements_System_Console_Write1(s:RemObjects_Elements_System_Object);
    RemObjects_Elements_System_Console_Write2(s:string);
    RemObjects_Elements_System_Console_ReadChar(): number;
    RemObjects_Elements_System_Console_ReadLine(): string;
    RemObjects_Elements_System_Console_WriteLine0();
    RemObjects_Elements_System_Console_WriteLine1(s:RemObjects_Elements_System_Object);
    RemObjects_Elements_System_Console_WriteLine2(s:string);
    RemObjects_Elements_System_Convert_HexStringToUInt64(s:string): number;
    RemObjects_Elements_System_Convert_UInt64ToHexString(v:number, aDigits:number): string;
    RemObjects_Elements_System_Convert_ToByte(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToInt16(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToInt32(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToInt64(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToSByte(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToDouble(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToSingle(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToUInt16(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToUInt32(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToUInt64(o:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_Convert_ToBoolean(o:RemObjects_Elements_System_Object): boolean;
    RemObjects_Elements_System_DynamicInvokeException(aMessage:string): RemObjects_Elements_System_DynamicInvokeException;
    RemObjects_Elements_System_DynamicHelpers_Unary(aLeft:RemObjects_Elements_System_Object, aOp:number): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_DynamicHelpers_Binary(aLeft:RemObjects_Elements_System_Object, aRight:RemObjects_Elements_System_Object, aOp:number): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_DynamicHelpers_Invoke0(aInstance:RemObjects_Elements_System_Object, aGetFlags:number, aArgs:any): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_DynamicHelpers_Invoke1(aInstance:RemObjects_Elements_System_Object, aName:string, aGetFlags:number, aArgs:any): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_DynamicHelpers_GetMember(aInstance:RemObjects_Elements_System_Object, aName:string, aGetFlags:number, aArgs:any): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_DynamicHelpers_SetMember(aInstance:RemObjects_Elements_System_Object, aName:string, aGetFlags:number, aArgs:any);
    RemObjects_Elements_System_DynamicMethodGroup(aInst:RemObjects_Elements_System_Object, aItems:any): RemObjects_Elements_System_DynamicMethodGroup;
    RemObjects_Elements_System_Exception(aMessage:string): RemObjects_Elements_System_Exception;
    RemObjects_Elements_System_AssertionException(aMessage:string): RemObjects_Elements_System_AssertionException;
    RemObjects_Elements_System_NotImplementedException0(): RemObjects_Elements_System_NotImplementedException;
    RemObjects_Elements_System_NotImplementedException1(aMessage:string): RemObjects_Elements_System_NotImplementedException;
    RemObjects_Elements_System_NotSupportedException0(): RemObjects_Elements_System_NotSupportedException;
    RemObjects_Elements_System_NotSupportedException1(aMessage:string): RemObjects_Elements_System_NotSupportedException;
    RemObjects_Elements_System_NullReferenceException0(): RemObjects_Elements_System_NullReferenceException;
    RemObjects_Elements_System_NullReferenceException1(s:string): RemObjects_Elements_System_NullReferenceException;
    RemObjects_Elements_System_InvalidCastException(aMessage:string): RemObjects_Elements_System_InvalidCastException;
    RemObjects_Elements_System_AbstractMethodException(): RemObjects_Elements_System_AbstractMethodException;
    RemObjects_Elements_System_DivideByZeroException(): RemObjects_Elements_System_DivideByZeroException;
    RemObjects_Elements_System_FormatException(aMessage:string): RemObjects_Elements_System_FormatException;
    RemObjects_Elements_System_OverflowException(aMessage:string): RemObjects_Elements_System_OverflowException;
    RemObjects_Elements_System_ArgumentNullException(aMessage:string): RemObjects_Elements_System_ArgumentNullException;
    RemObjects_Elements_System_ArgumentOutOfRangeException(aMessage:string): RemObjects_Elements_System_ArgumentOutOfRangeException;
    RemObjects_Elements_System_ArgumentException(aMessage:string): RemObjects_Elements_System_ArgumentException;
    RemObjects_Elements_System_IndexOutOfRangeException(aMessage:string): RemObjects_Elements_System_IndexOutOfRangeException;
    RemObjects_Elements_System_FloatToString_ConvertToDecimal0(aValue:number, aNumberOfDecimalDigits:number): string;
    RemObjects_Elements_System_FloatToString_ConvertToDecimal1(aValue:number, aNumberOfDecimalDigits:number, aLocale:RemObjects_Elements_System_Locale): string;
    RemObjects_Elements_System_FloatToString_Convert0(aValue:number, aPrecision:number): string;
    RemObjects_Elements_System_FloatToString_Convert1(aValue:number, aPrecision:number, aLocale:RemObjects_Elements_System_Locale): string;
    RemObjects_Elements_System_GCHandles_Allocate(aValue:RemObjects_Elements_System_Object): number;
    RemObjects_Elements_System_GCHandles_Free(aValue:number);
    RemObjects_Elements_System_GCHandles_Get(aValue:number): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_Object_ReferenceEquals(a:RemObjects_Elements_System_Object, b:RemObjects_Elements_System_Object): boolean;
    RemObjects_Elements_System_Object(): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_NumberFormatInfo(aDecimalSeparator:number, aThousandsSeparator:number, aCurrency:string, aIsReadOnly:boolean): RemObjects_Elements_System_NumberFormatInfo;
    RemObjects_Elements_System_DateTimeFormatInfo(aLocale:string, aIsReadonly:boolean): RemObjects_Elements_System_DateTimeFormatInfo;
    RemObjects_Elements_System_Locale0(aLocale:string): RemObjects_Elements_System_Locale;
    RemObjects_Elements_System_Locale1(aLocaleID:string, aIsReadOnly:boolean): RemObjects_Elements_System_Locale;
    RemObjects_Elements_System_Math_Acos(d:number): number;
    RemObjects_Elements_System_Math_Asin(d:number): number;
    RemObjects_Elements_System_Math_Atan(d:number): number;
    RemObjects_Elements_System_Math_Atan2(x:number, y:number): number;
    RemObjects_Elements_System_Math_Ceiling0(d:number): number;
    RemObjects_Elements_System_Math_Ceiling1(d:number): number;
    RemObjects_Elements_System_Math_Cos(d:number): number;
    RemObjects_Elements_System_Math_Cosh(d:number): number;
    RemObjects_Elements_System_Math_Exp(d:number): number;
    RemObjects_Elements_System_Math_Abs0(i:number): number;
    RemObjects_Elements_System_Math_Abs1(i:number): number;
    RemObjects_Elements_System_Math_Abs2(i:number): number;
    RemObjects_Elements_System_Math_Floor0(d:number): number;
    RemObjects_Elements_System_Math_Floor1(d:number): number;
    RemObjects_Elements_System_Math_fmod(x:number, y:number): number;
    RemObjects_Elements_System_Math_fmodf(x:number, y:number): number;
    RemObjects_Elements_System_Math_Log(a:number): number;
    RemObjects_Elements_System_Math_Log10(a:number): number;
    RemObjects_Elements_System_Math_Max0(a:number, b:number): number;
    RemObjects_Elements_System_Math_Max1(a:number, b:number): number;
    RemObjects_Elements_System_Math_Max2(a:number, b:number): number;
    RemObjects_Elements_System_Math_Min0(a:number, b:number): number;
    RemObjects_Elements_System_Math_Min1(a:number, b:number): number;
    RemObjects_Elements_System_Math_Min2(a:number, b:number): number;
    RemObjects_Elements_System_Math_Pow0(x:number, y:number): number;
    RemObjects_Elements_System_Math_Pow1(x:number, y:number): number;
    RemObjects_Elements_System_Math_Exp2(d:number): number;
    RemObjects_Elements_System_Math_Log2(a:number): number;
    RemObjects_Elements_System_Math_Sign(d:number): number;
    RemObjects_Elements_System_Math_IEEERemainder(x:number, y:number): number;
    RemObjects_Elements_System_Math_Round(a:number): number;
    RemObjects_Elements_System_Math_Sin(x:number): number;
    RemObjects_Elements_System_Math_Sinh(x:number): number;
    RemObjects_Elements_System_Math_Sqrt(d:number): number;
    RemObjects_Elements_System_Math_Tan(d:number): number;
    RemObjects_Elements_System_Math_Tanh(d:number): number;
    RemObjects_Elements_System_Math_Truncate0(d:number): number;
    RemObjects_Elements_System_Math_Truncate1(d:number): number;
    RemObjects_Elements_System_Math(): RemObjects_Elements_System_Math;
    RemObjects_Elements_System_MemoryStream0(): RemObjects_Elements_System_MemoryStream;
    RemObjects_Elements_System_MemoryStream1(aCapacity:number): RemObjects_Elements_System_MemoryStream;
    RemObjects_Elements_System_Monitor(): RemObjects_Elements_System_Monitor;
    RemObjects_Elements_System_GCHashSet(): RemObjects_Elements_System_GCHashSet;
    RemObjects_Elements_System_GCList(): RemObjects_Elements_System_GCList;
    RemObjects_Elements_System_String_FromRepeatedChar(c:number, aCharCount:number): string;
    RemObjects_Elements_System_String_op_LessThanOrEqual(Value1:string, Value2:string): boolean;
    RemObjects_Elements_System_String_op_GreaterThanOrEqual(Value1:string, Value2:string): boolean;
    RemObjects_Elements_System_String_Join0(Separator:string, Value:any): string;
    RemObjects_Elements_System_String_Join1(Separator:string, Value:any): string;
    RemObjects_Elements_System_String_Join2(Separator:string, Value:any, StartIndex:number, Count:number): string;
    RemObjects_Elements_System_String_Join3(Separator:string, Value:any): string;
    RemObjects_Elements_System_String_Format(aFormat:string, aArguments:any): string;
    RemObjects_Elements_System_String_Compare(aLeft:string, aRight:string): number;
    RemObjects_Elements_System_String_FromChar(c:number): string;
    RemObjects_Elements_System_String_FromPChar0(c:any): string;
    RemObjects_Elements_System_String_FromPChar1(c:any, aCharCount:number): string;
    RemObjects_Elements_System_String_op_Addition0(aLeft:number, aRight:string): string;
    RemObjects_Elements_System_String_op_Addition1(aLeft:RemObjects_Elements_System_Object, aRight:string): string;
    RemObjects_Elements_System_String_op_Addition2(aLeft:string, aChar:number): string;
    RemObjects_Elements_System_String_op_Addition3(aLeft:string, aRight:RemObjects_Elements_System_Object): string;
    RemObjects_Elements_System_String_op_Addition4(aLeft:string, aRight:string): string;
    RemObjects_Elements_System_String_op_Equality(aValue1:string, aValue2:string): boolean;
    RemObjects_Elements_System_String_op_Implicit(Value:number): string;
    RemObjects_Elements_System_String_op_LessThan(Value1:string, Value2:string): boolean;
    RemObjects_Elements_System_String_FromCharArray(aArray:any): string;
    RemObjects_Elements_System_String_IsNullOrEmpty(value:string): boolean;
    RemObjects_Elements_System_String_FromPAnsiChars0(c:any): string;
    RemObjects_Elements_System_String_FromPAnsiChars1(c:any, aCharCount:number): string;
    RemObjects_Elements_System_String_op_Inequality(aValue1:string, aValue2:string): boolean;
    RemObjects_Elements_System_String_op_GreaterThan(Value1:string, Value2:string): boolean;
    RemObjects_Elements_System_StringBuilder0(): RemObjects_Elements_System_StringBuilder;
    RemObjects_Elements_System_StringBuilder1(aCapacity:number): RemObjects_Elements_System_StringBuilder;
    RemObjects_Elements_System_StringBuilder2(Data:string): RemObjects_Elements_System_StringBuilder;
    RemObjects_Elements_System_TextConvert_StringToUTF8(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_UTF8ToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF8ToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_TextConvert_ASCIIToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_ASCIIToString1(aValue:any, aOffset:number, aCount:number): string;
    RemObjects_Elements_System_TextConvert_StringToASCII(aValue:string): any;
    RemObjects_Elements_System_TextConvert_StringToUTF16(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_StringToUTF32(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_UTF16ToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF16ToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_TextConvert_UTF32ToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF32ToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_TextConvert_StringToUTF16BE(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_StringToUTF16LE(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_StringToUTF32BE(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_StringToUTF32LE(aValue:string, aGenerateBOM:boolean): any;
    RemObjects_Elements_System_TextConvert_UTF16BEToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF16BEToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_TextConvert_UTF16LEToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF16LEToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_TextConvert_UTF32BEToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF32BEToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_TextConvert_UTF32LEToString0(aValue:any): string;
    RemObjects_Elements_System_TextConvert_UTF32LEToString1(aValue:any, aOffset:number, len:number): string;
    RemObjects_Elements_System_CustomAttribute(aType:RemObjects_Elements_System_Type, aCtor:any, aArgs:any): RemObjects_Elements_System_CustomAttribute;
    RemObjects_Elements_System_CustomAttributeArgument(aName:string, aValue:RemObjects_Elements_System_Object): RemObjects_Elements_System_CustomAttributeArgument;
    RemObjects_Elements_System_Type_op_Equality(a:RemObjects_Elements_System_Type, b:RemObjects_Elements_System_Type): boolean;
    RemObjects_Elements_System_Type_op_Inequality(a:RemObjects_Elements_System_Type, b:RemObjects_Elements_System_Type): boolean;
    RemObjects_Elements_System_Type_TypeIsValueType(aType:any): boolean;
    RemObjects_Elements_System_Type(aValue:any): RemObjects_Elements_System_Type;
    RemObjects_Elements_System_Debug_Throw(s:string);
    RemObjects_Elements_System_Debug_Assert(aCheck:boolean, aMessage:string, aFile:string, aLine:number);
    RemObjects_Elements_System_Utilities_AbstractCall();
    RemObjects_Elements_System_Utilities_Initialize();
    RemObjects_Elements_System_Utilities_IsInstance(aInstance:RemObjects_Elements_System_Object, aType:any): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_Utilities_IsInterfaceInstance(aInstance:RemObjects_Elements_System_Object, aType:any, aHashCode:number): any;
    RemObjects_Elements_System_Utilities_NewArray(aTY:any, aElementSize:number, aElements:number): any;
    RemObjects_Elements_System_Utilities_NewDelegate(aTY:any, aSelf:RemObjects_Elements_System_Object, aPtr:any): RemObjects_Elements_System_Delegate;
    RemObjects_Elements_System_Utilities_CreateDivideByZeroException(): RemObjects_Elements_System_Exception;
    RemObjects_Elements_System_Utilities_CreateIndexOutOfRangeException(aIndex:number, aMax:number): RemObjects_Elements_System_Exception;
    RemObjects_Elements_System_Utilities_CreateInvalidCastException(): RemObjects_Elements_System_Exception;
    RemObjects_Elements_System_Utilities_CreateNullReferenceException(): RemObjects_Elements_System_Exception;
    RemObjects_Elements_System_Utilities_CreateNullReferenceExceptionEx(s:string): RemObjects_Elements_System_Exception;
    RemObjects_Elements_System_Utilities_GetObjectTypeName(aObj:RemObjects_Elements_System_Object): any;
    RemObjects_Elements_System_Utilities_GetObjectToString(aObj:RemObjects_Elements_System_Object): any;
    RemObjects_Elements_System_Utilities_CalcHash(buf:any, len:number): number;
    RemObjects_Elements_System_Utilities_RegisterThread();
    RemObjects_Elements_System_Utilities_UnregisterThread();
    RemObjects_Elements_System_rpmalloc___Global_atomic_cas_ptr(dst:any, val:any, ref:any): number;
    RemObjects_Elements_System_rpmalloc___Global_rpaligned_alloc(alignment:number, size:number): any;
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_config(): any;
    RemObjects_Elements_System_rpmalloc___Global_atomic_load_ptr(src:any): any;
    RemObjects_Elements_System_rpmalloc___Global_rpposix_memalign(memptr:any, alignment:number, size:number): number;
    RemObjects_Elements_System_rpmalloc___Global_atomic_store_ptr(dst:any, val:any);
    RemObjects_Elements_System_rpmalloc___Global_rpaligned_realloc(ptr:any, alignment:number, size:number, oldsize:number, flags:number): any;
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_finalize();
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_initialize(): number;
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_usable_size(ptr:any): number;
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_thread_collect();
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_thread_finalize();
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_global_statistics(stats:any);
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_initialize_config(config:any): number;
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_thread_initialize();
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_thread_statistics(stats:any);
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc_is_thread_initialized(): number;
    RemObjects_Elements_System_rpmalloc___Global_rpfree(ptr:any);
    RemObjects_Elements_System_rpmalloc___Global_rpcalloc(num:number, size:number): any;
    RemObjects_Elements_System_rpmalloc___Global_rpmalloc(size:number): any;
    RemObjects_Elements_System_rpmalloc___Global_rprealloc(ptr:any, size:number): any;
    RemObjects_Elements_System_rpmalloc___Global_rpmemalign(alignment:number, size:number): any;
    RemObjects_Elements_System_rpmalloc___Global_atomic_add32(val:any, add:number): number;
    RemObjects_Elements_System_rpmalloc___Global_atomic_incr32(val:any): number;
    RemObjects_Elements_System_rpmalloc___Global_atomic_load32(src:any): number;
    RemObjects_Elements_System_rpmalloc___Global_atomic_store32(dst:any, val:number);
    RemObjects_Elements_System_EcmaScriptObject_op_Implicit(val:RemObjects_Elements_System_EcmaScriptObject): number;
    RemObjects_Elements_System_EcmaScriptObject(aValue:number): RemObjects_Elements_System_EcmaScriptObject;
    RemObjects_Elements_System_WebAssembly_GetElementByName(id:string): any;
    RemObjects_Elements_System_WebAssembly_AjaxRequestBinary(url:string): any;
    RemObjects_Elements_System_WebAssembly_GetObjectForHandle(aHandle:number): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_WebAssembly_GetStringFromHandle(handle:number, aFree:boolean): string;
    RemObjects_Elements_System_WebAssembly_Eval(s:string): any;
    RemObjects_Elements_System_WebAssembly_UnwrapCall(aType:RemObjects_Elements_System_Type, aVal:RemObjects_Elements_System_Object): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_WebAssembly_AjaxRequest(url:string): string;
    RemObjects_Elements_System_WebAssembly_CreateArray(): any;
    RemObjects_Elements_System_WebAssembly_CreateProxy(o:RemObjects_Elements_System_Object): RemObjects_Elements_System_EcmaScriptObject;
    RemObjects_Elements_System_WebAssembly_CreateHandle(aVal:RemObjects_Elements_System_Object, StringAsObject:boolean): number;
    RemObjects_Elements_System_WebAssembly_CreateObject(): any;
    RemObjects_Elements_System_WebAssembly_InvokeMethod(aPtr:any, args:any): RemObjects_Elements_System_Object;
    RemObjects_Elements_System_WebAssembly_ReleaseProxy(o:RemObjects_Elements_System_EcmaScriptObject);
    RemObjects_Elements_System_WebAssembly_ClearInterval(aVal:number);
    RemObjects_Elements_System_WebAssembly_CreateElement(aName:string): any;
    RemObjects_Elements_System_WebAssembly_CreateTextNode(aName:string): any;
    RemObjects_Elements_System_WebAssembly_GetElementById(id:string): any;
    RemObjects_Elements_System_WebAssembly_GetWindowObject(): any;
    RemObjects_Elements_System_ExternalCalls_ElementsBeginCatch(obj:any): any;
    RemObjects_Elements_System_ExternalCalls_ElementsEndCatch();
    RemObjects_Elements_System_ExternalCalls_GetExceptionForEH(val:any): any;
    RemObjects_Elements_System_ExternalCalls_RaiseException(aRaiseAddress:any, aRaiseFrame:any, aRaiseObject:RemObjects_Elements_System_Object);
    RemObjects_Elements_System_ExternalCalls_ElementsRethrow();
    RemObjects_Elements_System_ExternalCalls_memcpy(destination:any, source:any, aNum:number): any;
    RemObjects_Elements_System_ExternalCalls_memmove(destination:any, source:any, aNum:number): any;
    RemObjects_Elements_System_ExternalCalls_memset(ptr:any, value:number, aNum:number): any;
    RemObjects_Elements_System_ExternalCalls_strlen(c:any): number;
    RemObjects_Elements_System_ExternalCalls_wcslen(c:any): number;
    RemObjects_Elements_System___Global_free(v:any);
    RemObjects_Elements_System___Global_malloc(size:number): any;
    RemObjects_Elements_System___Global_IUnknown_VMTImpl_AddRef(aSelf:any): number;
    RemObjects_Elements_System___Global_IUnknown_VMTImpl_Release(aSelf:any): number;
    RemObjects_Elements_System___Global_IUnknown_VMTImpl_QueryInterface(aSelf:any, riid:any, ppvObject:any): number;
    RemObjects_Elements_System___Global_memcpy(destination:any, source:any, num:number): any;
    RemObjects_Elements_System___Global_memset(ptr:any, value:number, num:number): any;
    RemObjects_Elements_System___Global_memmove(destination:any, source:any, num:number): any;
    RemObjects_Elements_System___Global_op_Pow0(a:number, b:number): number;
    RemObjects_Elements_System___Global_op_Pow1(a:number, b:number): number;
    RemObjects_Elements_System___Global_op_Pow2(a:number, b:number): number;
    Program(): Program;
    qtxlib_BTreeNode(): qtxlib_BTreeNode;
    qtxlib_BTreeError(aMessage:string): qtxlib_BTreeError;
    qtxlib_BTree_Build(): qtxlib_IBTree;
    qtxlib_BTree(): qtxlib_BTree;
    qtxlib_PixelBuffer(): qtxlib_PixelBuffer;
    qtxlib_Surface(): qtxlib_Surface;
    qtxlib_PixelRageError(aMessage:string): qtxlib_PixelRageError;
    TForm1(AOwner:RemObjects_Elements_RTL_Delphi_VCL_TComponent): TForm1;
}
export interface RemObjects_Elements_System_Object{
    ToString(): string;
}

export interface RemObjects_Elements_System_Type extends RemObjects_Elements_System_Object{
    Equals(other: RemObjects_Elements_System_Object): boolean;
    GetHashCode(): number;
    IsSubclassOf(aType: RemObjects_Elements_System_Type): boolean;
    Instantiate(): RemObjects_Elements_System_Object;
    Instantiate(): RemObjects_Elements_System_Object;
    IsAssignableFrom(aOrg: RemObjects_Elements_System_Type): boolean;
    RTTI: any;
    Name: string;
    Flags: number;
    Code: number;
    IsSigned: boolean;
    IsInteger: boolean;
    IsIntegerOrFloat: boolean;
    IsFloat: boolean;
    IsEnum: boolean;
    IsDelegate: boolean;
    DefFlags: number;
    SizeOfType: number;
    BoxedDataOffset: number;
    SubType: RemObjects_Elements_System_Type;
    IsValueType: boolean;
    BaseType: RemObjects_Elements_System_Type;
    Interfaces: any;
    Attributes: any;
    NestedTypes: any;
    Members: any;
    Methods: any;
    Fields: any;
    Constants: any;
    Properties: any;
    Events: any;
    GenericArguments: any;
    COMGuids: any;
}

export interface RemObjects_Elements_RTL_Delphi_TPersistent extends RemObjects_Elements_System_Object{
    AssignError(Source: RemObjects_Elements_RTL_Delphi_TPersistent);
    AssignTo(Dest: RemObjects_Elements_RTL_Delphi_TPersistent);
    GetOwner(): RemObjects_Elements_RTL_Delphi_TPersistent;
    DefineProperties(Filer: RemObjects_Elements_System_Object);
    Assign(Source: RemObjects_Elements_RTL_Delphi_TPersistent);
}

export interface MetaClass extends RemObjects_Elements_System_Object{
    ActualType(): RemObjects_Elements_System_Type;
    new(aCollection: RemObjects_Elements_RTL_Delphi_TCollection): RemObjects_Elements_System_Object;
}

export interface RemObjects_Elements_RTL_Delphi_TCollectionItem extends RemObjects_Elements_RTL_Delphi_TPersistent{
    GetIndex(): number;
    Changed(AllItems: boolean);
    GetDisplayName(): string;
    SetCollection(aValue: RemObjects_Elements_RTL_Delphi_TCollection);
    SetIndex(aValue: number);
    SetDisplayName(aValue: string);
    GetMetaClass(): MetaClass;
    Collection: RemObjects_Elements_RTL_Delphi_TCollection;
    ID: number;
    Index: number;
    DisplayName: string;
}

export interface RemObjects_Elements_RTL_Delphi_TCollection extends RemObjects_Elements_RTL_Delphi_TPersistent{
    GetCapacity(): number;
    GetPropName(): string;
    InsertItem(aItem: RemObjects_Elements_RTL_Delphi_TCollectionItem);
    RemoveItem(aItem: RemObjects_Elements_RTL_Delphi_TCollectionItem);
    SetCapacity(aValue: number);
    Notify(aItem: RemObjects_Elements_RTL_Delphi_TCollectionItem, Action: number);
    Changed();
    GetItem(aIndex: number): RemObjects_Elements_RTL_Delphi_TCollectionItem;
    SetItem(aIndex: number, aValue: RemObjects_Elements_RTL_Delphi_TCollectionItem);
    SetItemName(aItem: RemObjects_Elements_RTL_Delphi_TCollectionItem);
    Update(aItem: RemObjects_Elements_RTL_Delphi_TCollectionItem);
    Owner(): RemObjects_Elements_RTL_Delphi_TPersistent;
    Add(): RemObjects_Elements_RTL_Delphi_TCollectionItem;
    BeginUpdate();
    Clear();
    Delete(aIndex: number);
    EndUpdate();
    FindItemID(aID: number): RemObjects_Elements_RTL_Delphi_TCollectionItem;
    Insert(aIndex: number): RemObjects_Elements_RTL_Delphi_TCollectionItem;
    NextID: number;
    PropName: string;
    UpdateCount: number;
    Capacity: number;
    Count: number;
    ItemClass: RemObjects_Elements_System_Type;
}

export interface RemObjects_Elements_RTL_Delphi_TStream extends RemObjects_Elements_System_Object{
    GetPosition(): number;
    SetPosition(Pos: number);
    Skip(Amount: number): number;
    CheckBufferRead(aBytesRead: number, aSizeToRead: number);
    CheckBufferReadCount(aBytesRead: number, aCount: number);
    GetSize(): number;
    SetSize(NewSize: number);
    SetSize(NewSize: number);
    Read(Buffer: any, Offset: number, Count: number): number;
    Write(Buffer: any, Offset: number, Count: number): number;
    Write(Buffer: any, Count: number): number;
    Read1Byte(): number;
    Read2Bytes(): number;
    Read4Bytes(): number;
    Read8Bytes(): number;
    Write1Byte(aValue: number);
    Write2Bytes(aValue: number);
    Write4Bytes(aValue: number);
    Write8Bytes(aValue: number);
    ReadBytes(Count: number): any;
    ReadData(Buffer: any, Count: number): number;
    WriteData(Buffer: any, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: boolean): number;
    WriteData(Buffer: boolean, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    WriteData(Buffer: number): number;
    WriteData(Buffer: number, Count: number): number;
    Seek(Offset: number, Origin: number): number;
    Seek(Offset: number, Origin: number): number;
    Seek(Offset: number, Origin: number): number;
    CopyFrom(Source: RemObjects_Elements_RTL_Delphi_TStream, Count: number): number;
    Position: number;
    Size: number;
}

export interface RemObjects_Elements_RTL_Delphi_TStrings extends RemObjects_Elements_RTL_Delphi_TPersistent{
    GetUseLocale(): boolean;
    SetUseLocale(aValue: boolean);
    GetUpdating(): boolean;
    GetStrictDelimiter(): boolean;
    SetStrictDelimiter(aValue: boolean);
    GetTrailingLineBreak(): boolean;
    SetTrailingLineBreak(aValue: boolean);
    GetCapacity(): number;
    GetCount(): number;
    GetObject(aIndex: number): RemObjects_Elements_System_Object;
    PutObject(aIndex: number, aObject: RemObjects_Elements_System_Object);
    SetCapacity(aNewCapacity: number);
    SetEncoding(Value: any);
    SetUpdateState(aUpdating: boolean);
    AddStrings(aStrings: RemObjects_Elements_RTL_Delphi_TStrings);
    AddStrings(aStrings: any);
    AddStrings(aStrings: any, aObjects: any);
    Assign(aSource: RemObjects_Elements_RTL_Delphi_TPersistent);
    SetStrings(aSource: RemObjects_Elements_RTL_Delphi_TStrings);
    BeginUpdate();
    Clear();
    Delete(aIndex: number);
    EndUpdate();
    Equals(aStrings: RemObjects_Elements_RTL_Delphi_TStrings): boolean;
    Exchange(aIndex1: number, aIndex2: number);
    IndexOfObject(aObject: RemObjects_Elements_System_Object): number;
    LoadFromStream(aStream: RemObjects_Elements_RTL_Delphi_TStream);
    LoadFromStream(aStream: RemObjects_Elements_RTL_Delphi_TStream, aEncoding: any);
    Move(aCurIndex: number, aNewIndex: number);
    SaveToStream(aStream: RemObjects_Elements_RTL_Delphi_TStream);
    SaveToStream(aStream: RemObjects_Elements_RTL_Delphi_TStream, aEncoding: any);
    ToStringArray(): any;
    ToObjectArray(): any;
    UpdateCount: number;
    Updating: boolean;
    Capacity: number;
    Count: number;
    Delimiter: number;
    Encoding: any;
    QuoteChar: number;
    NameValueSeparator: number;
    StrictDelimiter: boolean;
    TrailingLineBreak: boolean;
    UseLocale: boolean;
    Options: number;
}

export interface RemObjects_Elements_RTL_Delphi_TStringList extends RemObjects_Elements_RTL_Delphi_TStrings{
    Setup();
    SetSorted(Value: boolean);
    SetCaseSensitive(Value: boolean);
    Changed();
    Changing();
    GetObject(aIndex: number): RemObjects_Elements_System_Object;
    PutObject(aIndex: number, aObject: RemObjects_Elements_System_Object);
    SetCapacity(aNewCapacity: number);
    SetUpdateState(aUpdating: boolean);
    GetCapacity(): number;
    Assign(aSource: RemObjects_Elements_RTL_Delphi_TPersistent);
    Clear();
    Delete(aIndex: number);
    Exchange(aIndex1: number, aIndex2: number);
    Sort();
    GetCount(): number;
    <Sort>b__0(x: any, y: any): number;
    Duplicates: number;
    Sorted: boolean;
    CaseSensitive: boolean;
    OwnsObjects: boolean;
}

export interface RemObjects_Elements_RTL_Delphi_TCustomMemoryStream extends RemObjects_Elements_RTL_Delphi_TStream{
    Read(Buffer: any, Offset: number, Count: number): number;
    Seek(Offset: number, Origin: number): number;
    SaveToStream(aStream: RemObjects_Elements_RTL_Delphi_TStream);
    Write(Buffer: any, Offset: number, Count: number): number;
    Memory: any;
}

export interface RemObjects_Elements_RTL_Delphi_TMemoryStream extends RemObjects_Elements_RTL_Delphi_TCustomMemoryStream{
    Clear();
    LoadFromStream(aStream: RemObjects_Elements_RTL_Delphi_TStream);
    SetSize(NewSize: number);
    SetSize(NewSize: number);
    Capacity: number;
}

export interface RemObjects_Elements_RTL_Delphi_TStringBuilder extends RemObjects_Elements_System_Object{
    GetChars(aIndex: number): number;
    SetChars(aIndex: number, Value: number);
    Append(Value: boolean): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: RemObjects_Elements_System_Object): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: any): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: number, RepeatCount: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Append(Value: any, StartIndex: number, CharCount: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    AppendLine(): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Clear();
    CopyTo(SourceIndex: number, Destination: any, DestinationIndex: number, aCount: number);
    EnsureCapacity(aCapacity: number): number;
    Equals(aValue: RemObjects_Elements_RTL_Delphi_TStringBuilder): boolean;
    Insert(aIndex: number, Value: boolean): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: any): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: RemObjects_Elements_System_Object): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Insert(aIndex: number, Value: any, startIndex: number, charCount: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Remove(StartIndex: number, RemLength: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Replace(OldChar: number, NewChar: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    Replace(OldChar: number, NewChar: number, StartIndex: number, Count: number): RemObjects_Elements_RTL_Delphi_TStringBuilder;
    ToString(): string;
    Capacity: number;
    Length: number;
    MaxCapacity: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TComponent extends RemObjects_Elements_RTL_Delphi_TPersistent{
    setName(aValue: string);
    setOwner(aValue: RemObjects_Elements_RTL_Delphi_VCL_TComponent);
    Loaded();
    SetComponentState(aState: number);
    RemoveComponentState(aState: number);
    ComponentState: number;
    Name: string;
    Owner: RemObjects_Elements_RTL_Delphi_VCL_TComponent;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TFont extends RemObjects_Elements_RTL_Delphi_TPersistent{
    setColor(aValue: number);
    setName(aValue: string);
    setSize(aValue: number);
    setStyle(aValue: number);
    setHeight(aValue: number);
    GetOrientation(): number;
    SetOrientation(aValue: number);
    GetPitch(): number;
    SetPitch(aValue: number);
    GetQuality(): number;
    SetQuality(aValue: number);
    NotifyChanged(aPropName: string);
    PlatformUpdate();
    PlatformSetHeight(aValue: number);
    Color: number;
    Name: string;
    Size: number;
    Style: number;
    Charset: number;
    Height: number;
    PixelsPerInch: number;
    Orientation: number;
    Pitch: number;
    Quality: number;
}

export interface MetaClass extends RemObjects_Elements_System_Object{
    ActualType(): RemObjects_Elements_System_Type;
    InitDefaults(Margins: RemObjects_Elements_RTL_Delphi_VCL_TMargins);
    new(): RemObjects_Elements_System_Object;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TMargins extends RemObjects_Elements_RTL_Delphi_TPersistent{
    SetBottom(value: number);
    SetRight(value: number);
    SetTop(value: number);
    SetLeft(value: number);
    GetMetaClass(): MetaClass;
    Left: number;
    Top: number;
    Right: number;
    Bottom: number;
}

export interface RemObjects_Elements_System_IDisposable extends RemObjects_Elements_System_Object{
    Dispose();
}

export interface RemObjects_Elements_System_IDynamicObject extends RemObjects_Elements_System_Object{
    GetMember(aName: string, aGetFlags: number, aArgs: any): RemObjects_Elements_System_Object;
    SetMember(aName: string, aGetFlags: number, aArgs: any): RemObjects_Elements_System_Object;
    Invoke(aName: string, aGetFlags: number, aArgs: any): RemObjects_Elements_System_Object;
}

export interface RemObjects_Elements_System_EcmaScriptObject extends RemObjects_Elements_System_Object, RemObjects_Elements_System_IDisposable, RemObjects_Elements_System_IDynamicObject{
    GetMember(aName: string, aGetFlags: number, aArgs: any): RemObjects_Elements_System_Object;
    SetMember(aName: string, aGetFlags: number, aArgs: any): RemObjects_Elements_System_Object;
    Invoke(aName: string, aGetFlags: number, aArgs: any): RemObjects_Elements_System_Object;
    Release();
    Dispose();
    DefineProperty(aName: string, aValue: RemObjects_Elements_System_Object, aFlags: number);
    DefineProperty(aName: string, aGet: any, aSet: any, aFlags: number);
    DefineProperty(aName: string, aGet: any, aSet: any, aFlags: number);
    Call(aName: string, args: any): RemObjects_Elements_System_Object;
    Call(args: any): RemObjects_Elements_System_Object;
    Handle: number;
    Count: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TControl extends RemObjects_Elements_RTL_Delphi_VCL_TComponent{
    GetCaption(): string;
    SetCaption(aValue: string);
    SetWidth(aValue: number);
    SetHeight(aValue: number);
    SetTop(aValue: number);
    SetLeft(aValue: number);
    SetParent(aValue: RemObjects_Elements_RTL_Delphi_VCL_TNativeControl);
    setFont(value: RemObjects_Elements_RTL_Delphi_VCL_TFont);
    SetColor(aValue: number);
    SetVisible(aValue: boolean);
    SetParentFont(aValue: boolean);
    SetAlign(value: number);
    SetMargins(aValue: RemObjects_Elements_RTL_Delphi_VCL_TMargins);
    SetAlignWithMargins(value: boolean);
    SetTabOrder(aValue: number);
    GetClientHeight(): number;
    SetClientHeight(aValue: number);
    GetClientWidth(): number;
    SetClientWidth(aValue: number);
    CreateHandle();
    HandleNeeded();
    HandleAllocated(): boolean;
    Changed(aObject: RemObjects_Elements_System_Object, propName: string);
    RequestAlign();
    MouseDown(aButton: number, aShift: number, X: number, Y: number);
    Notification(aComponent: RemObjects_Elements_RTL_Delphi_VCL_TComponent, aOperation: number);
    PlatformGetCaption(): string;
    PlatformSetCaption(aValue: string);
    PlatformSetWidth(aValue: number);
    PlatformSetHeight(aValue: number);
    PlatformSetTop(aValue: number);
    PlatformSetLeft(aValue: number);
    PlatformSetParent(aValue: RemObjects_Elements_RTL_Delphi_VCL_TControl);
    PlatformSetColor(aValue: number);
    PlatformSetVisible(aValue: boolean);
    PlatformSetTabOrder(aValue: number);
    PlatformGetDefaultName(): string;
    PlatformApplyDefaults();
    PlatformInitControl();
    PlatformFontChanged();
    Click();
    InsertControl(aControl: RemObjects_Elements_RTL_Delphi_VCL_TControl);
    Show();
    Handle: any;
    Font: RemObjects_Elements_RTL_Delphi_VCL_TFont;
    Parent: RemObjects_Elements_RTL_Delphi_VCL_TNativeControl;
    ParentFont: boolean;
    Caption: string;
    Height: number;
    Width: number;
    ClientHeight: number;
    ClientWidth: number;
    Controls: any;
    Left: number;
    Top: number;
    Color: number;
    Visible: boolean;
    TabOrder: number;
    Align: number;
    Margins: RemObjects_Elements_RTL_Delphi_VCL_TMargins;
    AlignWithMargins: boolean;
    ExplicitLeft: number;
    ExplicitTop: number;
    ExplicitWidth: number;
    ExplicitHeight: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TPadding extends RemObjects_Elements_RTL_Delphi_VCL_TMargins{
    GetMetaClass(): MetaClass;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TNativeControl extends RemObjects_Elements_RTL_Delphi_VCL_TControl{
    SetPadding(value: RemObjects_Elements_RTL_Delphi_VCL_TPadding);
    SetTabStop(value: boolean);
    DoEnter();
    DoExit();
    PlatformSetTapStop(value: boolean);
    PlatformNativeCreated();
    AlignControl(aControl: RemObjects_Elements_RTL_Delphi_VCL_TControl);
    Padding: RemObjects_Elements_RTL_Delphi_VCL_TPadding;
    TabStop: boolean;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TCustomForm extends RemObjects_Elements_RTL_Delphi_VCL_TNativeControl{
    PlatformSetCaption(aValue: string);
    OldCreateOrder: boolean;
    PixelsPerInch: number;
    TextHeight: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TForm extends RemObjects_Elements_RTL_Delphi_VCL_TCustomForm{
    CreateHandle();
    Show();
    Show(aRootView: any);
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TApplication extends RemObjects_Elements_RTL_Delphi_VCL_TComponent{
    Initialize();
    Run();
    Terminate();
    Finished: boolean;
    MainForm: RemObjects_Elements_RTL_Delphi_VCL_TForm;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TScreen extends RemObjects_Elements_RTL_Delphi_VCL_TComponent{
    PlatformGetScreenHeight(): number;
    PlatformGetScreenWidth(): number;
    PixelsPerInch: number;
    Height: number;
    Width: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TFiler extends RemObjects_Elements_System_Object{
    SetRoot(aValue: RemObjects_Elements_RTL_Delphi_VCL_TComponent);
    Root: RemObjects_Elements_RTL_Delphi_VCL_TComponent;
    Ancestor: RemObjects_Elements_RTL_Delphi_TPersistent;
    IgnoreChildren: boolean;
}

export interface RemObjects_Elements_System_MemberInfo extends RemObjects_Elements_System_Object{
    DeclaringType: RemObjects_Elements_System_Type;
    Attributes: any;
    Access: number;
    Name: string;
    Type: RemObjects_Elements_System_Type;
    IsStatic: boolean;
}

export interface RemObjects_Elements_System_MethodInfo extends RemObjects_Elements_System_MemberInfo{
    Invoke(aInstance: RemObjects_Elements_System_Object, aArgs: any): RemObjects_Elements_System_Object;
    Flags: number;
    IsStatic: boolean;
    Arguments: any;
    VmtOffset: number;
    Pointer: any;
}

export interface RemObjects_Elements_System_PropertyInfo extends RemObjects_Elements_System_MemberInfo{
    GetValue(aInst: RemObjects_Elements_System_Object, aArgs: any): RemObjects_Elements_System_Object;
    SetValue(aInst: RemObjects_Elements_System_Object, aArgs: any, aValue: RemObjects_Elements_System_Object);
    Flags: number;
    IsStatic: boolean;
    ReadMethod: any;
    WriteMethod: any;
    Read: RemObjects_Elements_System_MethodInfo;
    Write: RemObjects_Elements_System_MethodInfo;
    Arguments: any;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TReader extends RemObjects_Elements_RTL_Delphi_VCL_TFiler{
    ReadComponentData(aInstance: RemObjects_Elements_RTL_Delphi_VCL_TComponent);
    FindProperty(aType: RemObjects_Elements_System_Type, aName: any): RemObjects_Elements_System_PropertyInfo;
    ReadProperty(aInstance: RemObjects_Elements_RTL_Delphi_TPersistent);
    EndOfList(): boolean;
    ReadComponent(aComponent: RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TComponent;
    ReadData(Instance: RemObjects_Elements_RTL_Delphi_VCL_TComponent);
    ReadPropValue(aInstance: RemObjects_Elements_System_Object, aValueType: number, aProperty: RemObjects_Elements_System_PropertyInfo): RemObjects_Elements_System_Object;
    ReadRootComponent(aRoot: RemObjects_Elements_RTL_Delphi_VCL_TComponent): RemObjects_Elements_RTL_Delphi_VCL_TComponent;
    ReadSignature();
    ReadStr(): any;
    ReadValue(): number;
    Owner: RemObjects_Elements_RTL_Delphi_VCL_TComponent;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TWriter extends RemObjects_Elements_RTL_Delphi_VCL_TFiler{
    WriteListBegin();
    WriteListEnd();
    WriteSignature();
    WriteValue(aValue: number);
    WriteUTF8Str(aValue: any);
    WriteString(aValue: any);
    WriteIdent(aIdent: any);
    WriteInteger(aValue: number);
    WriteInt64(aValue: number);
    WriteDouble(aValue: number);
    WriteSet(aValue: number);
    Write(aBuffer: any, aOffset: number, aCount: number);
    WriteVar(aValue: number);
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TResourceStream extends RemObjects_Elements_RTL_Delphi_TCustomMemoryStream{
    ReadComponent(aInstance: RemObjects_Elements_RTL_Delphi_VCL_TComponent);
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TButton extends RemObjects_Elements_RTL_Delphi_VCL_TNativeControl{
    CreateHandle();
    PlatformSetCaption(aValue: string);
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TLabel extends RemObjects_Elements_RTL_Delphi_VCL_TNativeControl{
    CreateHandle();
    PlatformSetCaption(aValue: string);
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TEdit extends RemObjects_Elements_RTL_Delphi_VCL_TNativeControl{
    CreateHandle();
    PlatformSetText(aValue: string);
    PlatformGetText(): string;
    PlatformGetMaxLength(): number;
    PlatformSetMaxLength(aValue: number);
    PlatformGetReadOnly(): boolean;
    PlatformSetReadOnly(aValue: boolean);
    MaxLength: number;
    ReadOnly: boolean;
    Text: string;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TListControl extends RemObjects_Elements_RTL_Delphi_VCL_TNativeControl{
    GetItemIndex(): number;
    SetItemIndex(aValue: number);
    Clear();
    ClearSelection();
    DeleteSelected();
    GetCount(): number;
    SelectAll();
    ItemIndex: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TListControlItems extends RemObjects_Elements_RTL_Delphi_TStringList{
    Clear();
    Delete(aIndex: number);
    PlatformClear();
    PlatformDelete(aIndex: number);
    ListControl: RemObjects_Elements_RTL_Delphi_VCL_TListControl;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TMultiSelectListControl extends RemObjects_Elements_RTL_Delphi_VCL_TListControl{
    GetSelCount(): number;
    SetMultiSelect(aValue: boolean);
    MultiSelect: boolean;
    SelCount: number;
}

export interface RemObjects_Elements_RTL_Delphi_VCL_TListBox extends RemObjects_Elements_RTL_Delphi_VCL_TMultiSelectListControl{
    SetItems(aValue: RemObjects_Elements_RTL_Delphi_TStrings);
    GetItemIndex(): number;
    SetItemIndex(aValue: number);
    GetSelCount(): number;
    SetMultiSelect(aValue: boolean);
    Loaded();
    Clear();
    ClearSelection();
    DeleteSelected();
    GetCount(): number;
    SelectAll();
    CreateHandle();
    PlatformSelectAll();
    PlatformGetSelected(aIndex: number): boolean;
    PlatformSetSelected(aIndex: number, value: boolean);
    PlatformGetSelCount(): number;
    PlatformGetMultiSelect(): boolean;
    PlatformSetMultiSelect(value: boolean);
    PlatformClearSelection();
    PlatformDeleteSelected();
    PlatformSetItemIndex(value: number);
    PlatformGetItemIndex(): number;
    Items: RemObjects_Elements_RTL_Delphi_TStrings;
    ItemHeight: number;
}

export interface RemObjects_Elements_RTL_NumberFormatInfo extends RemObjects_Elements_System_Object{
    SetDecimalSeparator(aValue: number);
    SetThousandsSeparator(aValue: number);
    SetCurrency(aValue: any);
    DecimalSeparator: number;
    ThousandsSeparator: number;
    Currency: any;
    IsReadOnly: boolean;
}

export interface RemObjects_Elements_RTL_DateTimeFormatInfo extends RemObjects_Elements_System_Object{
    SetShortDayNames(aValue: any);
    SetLongDayNames(aValue: any);
    SetShortMonthNames(aValue: any);
    SetLongMonthNames(aValue: any);
    SetDateSeparator(aValue: any);
    SetTimeSeparator(aValue: any);
    SetAMString(aValue: any);
    SetPMString(aValue: any);
    SetShortTimePattern(aValue: any);
    SetLongTimePattern(aValue: any);
    SetShortDatePattern(aValue: any);
    SetLongDatePattern(aValue: any);
    CheckReadOnly();
    ShortDayNames: any;
    LongDayNames: any;
    ShortMonthNames: any;
    LongMonthNames: any;
    DateSeparator: any;
    TimeSeparator: any;
    AMString: any;
    PMString: any;
    ShortTimePattern: any;
    LongTimePattern: any;
    ShortDatePattern: any;
    LongDatePattern: any;
    IsReadOnly: boolean;
}

export interface RemObjects_Elements_System_NumberFormatInfo extends RemObjects_Elements_System_Object{
    SetCurrency(value: string);
    SetThousandsSeparator(value: number);
    SetDecimalSeparator(value: number);
    CheckReadOnly();
    DecimalSeparator: number;
    ThousandsSeparator: number;
    Currency: string;
    IsReadOnly: boolean;
}

export interface RemObjects_Elements_System_DateTimeFormatInfo extends RemObjects_Elements_System_Object{
    SetShortDayNames(aValue: any);
    SetLongDayNames(aValue: any);
    SetShortMonthNames(aValue: any);
    SetLongMonthNames(aValue: any);
    SetDateSeparator(aValue: string);
    SetTimeSeparator(aValue: string);
    SetAMString(aValue: string);
    SetPMString(aValue: string);
    SetShortTimePattern(aValue: string);
    SetLongTimePattern(aValue: string);
    SetShortDatePattern(aValue: string);
    SetLongDatePattern(aValue: string);
    CheckReadOnly();
    ShortDayNames: any;
    LongDayNames: any;
    ShortMonthNames: any;
    LongMonthNames: any;
    DateSeparator: string;
    TimeSeparator: string;
    AMString: string;
    PMString: string;
    ShortTimePattern: string;
    LongTimePattern: string;
    ShortDatePattern: string;
    LongDatePattern: string;
    IsReadOnly: boolean;
}

export interface RemObjects_Elements_System_Locale extends RemObjects_Elements_System_Object{
    GetIdentifier(): string;
    Identifier: string;
    NumberFormat: RemObjects_Elements_System_NumberFormatInfo;
    DateTimeFormat: RemObjects_Elements_System_DateTimeFormatInfo;
    PlatformLocale: string;
}

export interface RemObjects_Elements_RTL_Locale extends RemObjects_Elements_System_Object{
    GetIdentifier(): any;
    Identifier: any;
    NumberFormat: RemObjects_Elements_RTL_NumberFormatInfo;
    DateTimeFormat: RemObjects_Elements_RTL_DateTimeFormatInfo;
    PlatformLocale: RemObjects_Elements_System_Locale;
}

export interface RemObjects_Elements_RTL_DateParser extends RemObjects_Elements_System_Object{
}

export interface RemObjects_Elements_RTL_DateTime extends RemObjects_Elements_System_Object, any{
    AddDays(Value: number): RemObjects_Elements_RTL_DateTime;
    AddHours(Value: number): RemObjects_Elements_RTL_DateTime;
    AddMinutes(Value: number): RemObjects_Elements_RTL_DateTime;
    AddMonths(Value: number): RemObjects_Elements_RTL_DateTime;
    AddSeconds(Value: number): RemObjects_Elements_RTL_DateTime;
    AddMilliSeconds(Value: number): RemObjects_Elements_RTL_DateTime;
    AddYears(Value: number): RemObjects_Elements_RTL_DateTime;
    Add(Value: any): RemObjects_Elements_RTL_DateTime;
    CompareTo(Value: RemObjects_Elements_RTL_DateTime): number;
    ToString(aTimeZone: any): any;
    ToString(Format: any, aTimeZone: any): any;
    ToString(Format: any, Culture: any, aTimeZone: any): any;
    ToString(): string;
    ToShortDateString(aTimeZone: any): any;
    ToShortTimeString(aTimeZone: any): any;
    ToShortPrettyDateAndTimeString(aTimeZone: any): any;
    ToShortPrettyDateString(aTimeZone: any): any;
    ToLongPrettyDateString(aTimeZone: any): any;
    Hour: number;
    Minute: number;
    Second: number;
    Year: number;
    Month: number;
    Day: number;
    DayOfWeek: number;
    Date: RemObjects_Elements_RTL_DateTime;
    TimeSince: any;
    Ticks: number;
}

export interface RemObjects_Elements_System_Exception extends RemObjects_Elements_System_Object{
    ToString(): string;
    ExceptionAddress: any;
    Message: string;
}

export interface RemObjects_Elements_RTL_RTLException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_RTL_UrlException extends RemObjects_Elements_RTL_RTLException{
}

export interface RemObjects_Elements_RTL_UrlParserException extends RemObjects_Elements_RTL_UrlException{
}

export interface RemObjects_Elements_RTL_ConversionException extends RemObjects_Elements_RTL_RTLException{
}

export interface RemObjects_Elements_System_ArgumentException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_RTL_ArgumentNullException extends RemObjects_Elements_System_ArgumentException{
}

export interface RemObjects_Elements_RTL_ArgumentOutOfRangeException extends RemObjects_Elements_System_ArgumentException{
}

export interface RemObjects_Elements_RTL_FormatException extends RemObjects_Elements_RTL_RTLException{
}

export interface RemObjects_Elements_RTL_InvalidOperationException extends RemObjects_Elements_RTL_RTLException{
}

export interface RemObjects_Elements_RTL_Guid extends RemObjects_Elements_System_Object{
    Equals(aValue: RemObjects_Elements_System_Object): boolean;
    GetHashCode(): number;
    ToByteArray(): any;
    ToString(): any;
    ToString(Format: number): any;
}

export interface RemObjects_Elements_RTL_JsonNode extends RemObjects_Elements_System_Object{
    CantGetItem(Key: any): RemObjects_Elements_RTL_JsonNode;
    CantGetItem(aIndex: number): RemObjects_Elements_RTL_JsonNode;
    CantSetItem(Key: any, Value: RemObjects_Elements_RTL_JsonNode);
    CantSetItem(Key: any, Value: any);
    CantSetItem(Key: any, Value: boolean);
    CantSetItem(Key: any, Value: number);
    CantSetItem(Key: any, Value: number);
    CantSetItem(aIndex: number, Value: RemObjects_Elements_RTL_JsonNode);
    CantGetKeys(): any;
    GetIntegerValue(): number;
    GetFloatValue(): number;
    GetBooleanValue(): boolean;
    GetStringValue(): any;
    SetIntegerValue(aValue: number);
    SetFloatValue(aValue: number);
    SetBooleanValue(aValue: boolean);
    SetStringValue(aValue: any);
    ToString(): string;
    ToJson(): any;
    Count: number;
    Keys: any;
    IntegerValue: number;
    FloatValue: number;
    BooleanValue: boolean;
    StringValue: any;
}

export interface RemObjects_Elements_System_IEnumerator extends RemObjects_Elements_System_Object, RemObjects_Elements_System_IDisposable{
    MoveNext(): boolean;
    Current: RemObjects_Elements_System_Object;
}

export interface RemObjects_Elements_System_IEnumerable extends RemObjects_Elements_System_Object{
    GetEnumerator(): RemObjects_Elements_System_IEnumerator;
}

export interface RemObjects_Elements_RTL_JsonArray extends RemObjects_Elements_RTL_JsonNode, any, RemObjects_Elements_System_IEnumerable, any{
    GetItem(aIndex: number): RemObjects_Elements_RTL_JsonNode;
    SetItem(aIndex: number, aValue: RemObjects_Elements_RTL_JsonNode);
    Add(aValue: RemObjects_Elements_RTL_JsonNode);
    Add(aValues: any);
    Add(aValues: any);
    Add(aValues: any);
    Add(aValues: any);
    Insert(aIndex: number, aValue: RemObjects_Elements_RTL_JsonNode);
    Clear();
    RemoveAt(aIndex: number);
    ToStrings(): any;
    ToStringList(): any;
    ToJson(): any;
    GetSequence(): any;
    GetEnumerator(): any;
    GetEnumeratorNonGen(): RemObjects_Elements_System_IEnumerator;
    Count: number;
    Items: any;
}

export interface RemObjects_Elements_RTL_JsonObject extends RemObjects_Elements_RTL_JsonNode, any, RemObjects_Elements_System_IEnumerable, any{
    GetItem(aKey: any): RemObjects_Elements_RTL_JsonNode;
    SetItem(aKey: any, aValue: RemObjects_Elements_RTL_JsonNode);
    SetItem(aKey: any, aValue: any);
    SetItem(aKey: any, aValue: boolean);
    SetItem(aKey: any, aValue: number);
    SetItem(aKey: any, aValue: number);
    GetKeys(): any;
    GetProperties(): any;
    Add(aKey: any, aValue: RemObjects_Elements_RTL_JsonNode);
    Remove(aKey: any): boolean;
    Clear();
    ContainsKey(aKey: any): boolean;
    ContainsExplicitJsonNullValueForKey(aKey: any): boolean;
    ToJson(): any;
    GetSequence(): any;
    GetEnumerator(): any;
    GetEnumeratorNonGen(): RemObjects_Elements_System_IEnumerator;
    Count: number;
    Keys: any;
    Properties: any;
}

export interface RemObjects_Elements_RTL_JsonDeserializer extends RemObjects_Elements_System_Object{
    Expected(Values: any);
    ReadObject(): RemObjects_Elements_RTL_JsonObject;
    ReadArray(): RemObjects_Elements_RTL_JsonArray;
    ReadProperties(): any;
    ReadPropery(): any;
    ReadKey(): any;
    ReadValues(): any;
    ReadValue(): RemObjects_Elements_RTL_JsonNode;
    Deserialize(): RemObjects_Elements_RTL_JsonNode;
}

export interface RemObjects_Elements_RTL_JsonBooleanValue extends any{
    ToJson(): any;
}

export interface RemObjects_Elements_RTL_JsonFloatValue extends any{
    ToJson(): any;
}

export interface RemObjects_Elements_RTL_JsonIntegerValue extends any{
    ToJson(): any;
}

export interface RemObjects_Elements_RTL_JsonStringValue extends any{
    ToJson(): any;
}

export interface RemObjects_Elements_RTL_JsonException extends RemObjects_Elements_RTL_RTLException{
}

export interface RemObjects_Elements_RTL_JsonNodeTypeException extends RemObjects_Elements_RTL_JsonException{
}

export interface RemObjects_Elements_RTL_JsonParserException extends RemObjects_Elements_RTL_JsonException{
}

export interface RemObjects_Elements_RTL_JsonUnexpectedTokenException extends RemObjects_Elements_RTL_JsonParserException{
}

export interface RemObjects_Elements_RTL_JsonSerializer extends RemObjects_Elements_System_Object{
    IncOffset();
    DecOffset();
    AppendOffset();
    VisitObject(Value: RemObjects_Elements_RTL_JsonObject);
    VisitArray(Value: RemObjects_Elements_RTL_JsonArray);
    VisitString(Value: RemObjects_Elements_RTL_JsonStringValue);
    VisitInteger(Value: RemObjects_Elements_RTL_JsonIntegerValue);
    VisitFloat(Value: RemObjects_Elements_RTL_JsonFloatValue);
    VisitBoolean(Value: RemObjects_Elements_RTL_JsonBooleanValue);
    VisitNull();
    VisitName(Value: any);
    Visit(Value: RemObjects_Elements_RTL_JsonNode);
    Serialize(): any;
}

export interface RemObjects_Elements_RTL_JsonTokenizer extends RemObjects_Elements_System_Object{
    CharIsIdentifier(C: number): boolean;
    CharIsWhitespace(C: number): boolean;
    Parse();
    ParseIdentifier();
    ParseWhitespace();
    ParseNumber();
    ParseString();
    Next(): boolean;
    Json: any;
    Row: number;
    Column: number;
    Value: any;
    Token: number;
    IgnoreWhitespaces: boolean;
}

export interface RemObjects_Elements_RTL_JsonNullValue extends any{
    ToJson(): any;
}

export interface RemObjects_Elements_RTL_Stream extends RemObjects_Elements_System_Object, RemObjects_Elements_System_IDisposable{
    Seek(Offset: number, Origin: number): number;
    Close();
    Flush();
    Read(Buffer: any, Offset: number, Count: number): number;
    Read(Buffer: any, Count: number): number;
    Write(Buffer: any, Offset: number, Count: number): number;
    Write(Buffer: any, Count: number): number;
    ReadByte(): number;
    WriteByte(aValue: number);
    GetLength(): number;
    SetPosition(Value: number);
    GetPosition(): number;
    CopyTo(Destination: RemObjects_Elements_RTL_Stream);
    Dispose();
    Length: number;
    Position: number;
    CanRead: boolean;
    CanSeek: boolean;
    CanWrite: boolean;
}

export interface RemObjects_Elements_RTL_WrappedPlatformStream extends RemObjects_Elements_RTL_Stream{
    Read(Buffer: any, Offset: number, Count: number): number;
    Write(Buffer: any, Offset: number, Count: number): number;
    Seek(Offset: number, Origin: number): number;
    GetLength(): number;
    SetLength(Value: number);
    SetPosition(Value: number);
    GetPosition(): number;
    Length: number;
    Position: number;
}

export interface RemObjects_Elements_System_Stream extends RemObjects_Elements_System_Object{
    GetLength(): number;
    SetPosition(value: number);
    GetPosition(): number;
    IsValid(): boolean;
    Seek(Offset: number, Origin: number): number;
    Close();
    Flush();
    Read(aSpan: any): number;
    Read(Buffer: any, Offset: number, Count: number): number;
    Read(buf: any, Count: number): number;
    Write(aSpan: any): number;
    Write(Buffer: any, Offset: number, Count: number): number;
    Write(buf: any, Count: number): number;
    CopyTo(Destination: RemObjects_Elements_System_Stream);
    SetLength(value: number);
    CanRead: boolean;
    CanSeek: boolean;
    CanWrite: boolean;
    Length: number;
    Position: number;
}

export interface RemObjects_Elements_RTL_MemoryStream extends RemObjects_Elements_RTL_WrappedPlatformStream{
    GetCanRead(): boolean;
    GetCanSeek(): boolean;
    GetCanWrite(): boolean;
    GetBytes(): any;
    ToArray(): any;
    Close();
    Flush();
    Clear();
    WriteTo(Destination: RemObjects_Elements_RTL_Stream);
    Bytes: any;
    CanRead: boolean;
    CanSeek: boolean;
    CanWrite: boolean;
}

export interface RemObjects_Elements_RTL_UnicodeException extends RemObjects_Elements_RTL_RTLException{
}

export interface RemObjects_Elements_RTL_Uri extends RemObjects_Elements_System_Object{
}

export interface RemObjects_Elements_RTL_Url extends RemObjects_Elements_RTL_Uri{
    Parse(aUrlString: any): boolean;
    GetHostNameAndPort(): any;
    GetPathAndQueryString(): any;
    CopyWithPath(aPath: any): RemObjects_Elements_RTL_Url;
    IntGetFilePath(): any;
    GetWindowsPath(): any;
    GetUnixPath(): any;
    GetCanonicalVersion(): RemObjects_Elements_RTL_Url;
    GetPathExtension(): any;
    GetLastPathComponent(): any;
    GetFilePathWithoutLastComponent(): any;
    GetWindowsPathWithoutLastComponent(): any;
    GetPathWithoutLastComponent(): any;
    GetUnixPathWithoutLastComponent(): any;
    GetUrlWithoutLastComponent(): RemObjects_Elements_RTL_Url;
    DoUnixPathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aThreshold: number, aCaseInsensitive: boolean): any;
    CopyWithQueryString(aQueryString: any): RemObjects_Elements_RTL_Url;
    ToString(): string;
    ToAbsoluteString(): any;
    IsUnderneath(aPotentialBaseUrl: RemObjects_Elements_RTL_Url): boolean;
    UrlWithChangedPathExtension(aNewExtension: any): RemObjects_Elements_RTL_Url;
    UrlWithAddedPathExtension(aNewExtension: any): RemObjects_Elements_RTL_Url;
    UrlWithChangedLastPathComponent(aNewLastPathComponent: any): RemObjects_Elements_RTL_Url;
    GetParentUrl(): RemObjects_Elements_RTL_Url;
    SubUrl(aComponents: any): RemObjects_Elements_RTL_Url;
    SubUrl(aComponents: any, aIsDirectory: boolean): RemObjects_Elements_RTL_Url;
    SubUrlWithFilePath(aSubPath: any): RemObjects_Elements_RTL_Url;
    SubUrlWithFilePath(aSubPath: any, aIsDirectory: boolean): RemObjects_Elements_RTL_Url;
    UrlWithRelativeOrAbsoluteFileSubPath(aSubPath: any): RemObjects_Elements_RTL_Url;
    UrlWithRelativeOrAbsoluteWindowsSubPath(aSubPath: any): RemObjects_Elements_RTL_Url;
    UrlWithFragment(aFragment: any): RemObjects_Elements_RTL_Url;
    UrlWithoutFragment(): RemObjects_Elements_RTL_Url;
    CanGetPathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url): boolean;
    FilePathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aThreshold: number): any;
    FilePathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aAlways: boolean): any;
    WindowsPathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aThreshold: number): any;
    WindowsPathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aAlways: boolean): any;
    UnixPathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aThreshold: number): any;
    UnixPathRelativeToUrl(aUrl: RemObjects_Elements_RTL_Url, aAlways: boolean): any;
    Equals(obj: RemObjects_Elements_System_Object): boolean;
    GetHashCode(): number;
    Scheme: any;
    Host: any;
    Port: any;
    Path: any;
    QueryString: any;
    Fragment: any;
    User: any;
    HostAndPort: any;
    PathAndQueryString: any;
    CanonicalVersion: RemObjects_Elements_RTL_Url;
    IsFileUrl: boolean;
    IsAbsoluteWindowsFileURL: boolean;
    IsAbsoluteWindowsDriveLetterFileURL: boolean;
    IsAbsoluteWindowsNetworkDriveFileURL: boolean;
    FilePath: any;
    WindowsPath: any;
    UnixPath: any;
    PathExtension: any;
    LastPathComponent: any;
    LastPathComponentWithoutExtension: any;
    FilePathWithoutLastComponent: any;
    WindowsPathWithoutLastComponent: any;
    UnixPathWithoutLastComponent: any;
    UrlWithoutLastComponent: RemObjects_Elements_RTL_Url;
}

export interface RemObjects_Elements_RTL_Urn extends RemObjects_Elements_RTL_Uri{
    ToString(): string;
}

export interface RemObjects_Elements_RTL_XmlFormattingOptions extends RemObjects_Elements_System_Object{
    UniqueCopy(): RemObjects_Elements_RTL_XmlFormattingOptions;
    NewLineString(): any;
}

export interface RemObjects_Elements_RTL_XmlRange extends RemObjects_Elements_System_Object{
    FillRange(aStartLine: number, aStartColumn: number, aEndLine: number, aEndColumn: number);
    StartLine: number;
    StartColumn: number;
    EndLine: number;
    EndColumn: number;
}

export interface RemObjects_Elements_RTL_XmlNode extends RemObjects_Elements_System_Object{
    SetDocument(aDoc: RemObjects_Elements_RTL_XmlDocument);
    GetNodeType(): number;
    CheckName(aName: any): boolean;
    CharIsWhitespace(C: any): boolean;
    ConvertEntity(S: any, C: any): any;
    ToString(): string;
    ToString(aSaveFormatted: boolean, aFormatInsideTags: boolean, aFormatOptions: RemObjects_Elements_RTL_XmlFormattingOptions): any;
    UniqueCopy(): RemObjects_Elements_RTL_XmlNode;
    Parent: RemObjects_Elements_RTL_XmlElement;
    Document: RemObjects_Elements_RTL_XmlDocument;
    NodeType: number;
    NodeRange: RemObjects_Elements_RTL_XmlRange;
}

export interface RemObjects_Elements_RTL_XmlNamespace extends RemObjects_Elements_RTL_XmlNode{
    GetPrefix(): any;
    SetPrefix(aPrefix: any);
    ToString(): string;
    ToString(aFormatInsideTags: boolean, aFormatOptions: RemObjects_Elements_RTL_XmlFormattingOptions): any;
    UniqueCopy(): RemObjects_Elements_RTL_XmlNode;
    Prefix: any;
    Uri: RemObjects_Elements_RTL_Uri;
}

export interface RemObjects_Elements_RTL_XmlAttribute extends RemObjects_Elements_RTL_XmlNode{
    GetNamespace(): RemObjects_Elements_RTL_XmlNamespace;
    SetNamespace(aNamespace: RemObjects_Elements_RTL_XmlNamespace);
    GetLocalName(): any;
    GetValue(): any;
    GetFullName(): any;
    SetLocalName(aValue: any);
    SetValue(aValue: any);
    ToString(): string;
    ToString(aFormatInsideTags: boolean, aFormatOptions: RemObjects_Elements_RTL_XmlFormattingOptions): any;
    UniqueCopy(): RemObjects_Elements_RTL_XmlNode;
    Namespace: RemObjects_Elements_RTL_XmlNamespace;
    LocalName: any;
    Value: any;
    FullName: any;
    ValueRange: RemObjects_Elements_RTL_XmlRange;
}

export interface RemObjects_Elements_RTL_XmlElement extends RemObjects_Elements_RTL_XmlNode{
    GetNamespace(): RemObjects_Elements_RTL_XmlNamespace;
    GetNamespace(aUri: RemObjects_Elements_RTL_Uri): RemObjects_Elements_RTL_XmlNamespace;
    GetNamespace(aPrefix: any): RemObjects_Elements_RTL_XmlNamespace;
    SetNamespace(aNamespace: RemObjects_Elements_RTL_XmlNamespace);
    GetLocalName(): any;
    SetLocalName(aValue: any);
    GetFullName(): any;
    SetValue(aValue: any);
    GetAttributes(): any;
    GetAttribute(aName: any): RemObjects_Elements_RTL_XmlAttribute;
    GetAttribute(aName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace): RemObjects_Elements_RTL_XmlAttribute;
    GetNodes(): any;
    GetElements(): any;
    GetNamespaces(): any;
    GetDefaultNamespace(): RemObjects_Elements_RTL_XmlNamespace;
    SetPreserveSpace(aPreserveSpace: boolean);
    GetValue(aWithNested: boolean): any;
    ElementsWithName(aLocalName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace): any;
    ElementsWithNamespace(aNamespace: RemObjects_Elements_RTL_XmlNamespace): any;
    FirstElementWithName(aLocalName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace): RemObjects_Elements_RTL_XmlElement;
    AddAttribute(aAttribute: RemObjects_Elements_RTL_XmlAttribute);
    SetAttribute(aName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace, aValue: any);
    RemoveAttribute(aAttribute: RemObjects_Elements_RTL_XmlAttribute);
    RemoveAttribute(aName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace): RemObjects_Elements_RTL_XmlAttribute;
    AddElement(aElement: RemObjects_Elements_RTL_XmlElement);
    AddElement(aElement: RemObjects_Elements_RTL_XmlElement, aIndex: number);
    AddElement(aName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace, aValue: any): RemObjects_Elements_RTL_XmlElement;
    AddElement(aName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace, aValue: any, aIndex: number): RemObjects_Elements_RTL_XmlElement;
    AddElements(aElements: any);
    RemoveElement(aElement: RemObjects_Elements_RTL_XmlElement);
    RemoveElementsWithName(aName: any, aNamespace: RemObjects_Elements_RTL_XmlNamespace);
    RemoveAllElements();
    ReplaceElement(aExistingElement: RemObjects_Elements_RTL_XmlElement, aNewElement: RemObjects_Elements_RTL_XmlElement);
    AddNode(aNode: RemObjects_Elements_RTL_XmlNode);
    AddNamespace(aNamespace: RemObjects_Elements_RTL_XmlNamespace);
    AddNamespace(aPrefix: any, aUri: RemObjects_Elements_RTL_Uri): RemObjects_Elements_RTL_XmlNamespace;
    RemoveNamespace(aNamespace: RemObjects_Elements_RTL_XmlNamespace);
    RemoveNamespace(aPrefix: any);
    ToString(): string;
    ToString(aSaveFormatted: boolean, aFormatInsideTags: boolean, aFormatOptions: RemObjects_Elements_RTL_XmlFormattingOptions): any;
    UniqueCopy(): RemObjects_Elements_RTL_XmlNode;
    ToString.GetEmptyLines(aWS: any, aLineBreak: any): any;
    Namespace: RemObjects_Elements_RTL_XmlNamespace;
    DefinedNamespaces: any;
    DefaultNamespace: RemObjects_Elements_RTL_XmlNamespace;
    LocalName: any;
    Value: any;
    IsEmpty: boolean;
    EndTagName: any;
    OpenTagEndLine: number;
    OpenTagEndColumn: number;
    CloseTagRange: RemObjects_Elements_RTL_XmlRange;
    Attributes: any;
    Elements: any;
    Nodes: any;
    FullName: any;
    ChildIndex: number;
    PreserveSpace: boolean;
}

export interface RemObjects_Elements_RTL_XmlDocCurrentPosition extends RemObjects_Elements_System_Object{
    CurrentPosition: number;
    ParentTag: RemObjects_Elements_RTL_XmlElement;
    CurrentTag: RemObjects_Elements_RTL_XmlElement;
    CurrentTagIndex: number;
    CurrentAttribute: RemObjects_Elements_RTL_XmlAttribute;
    CurrentNamespace: RemObjects_Elements_RTL_XmlNamespace;
    CurrentIdentifier: any;
}

export interface RemObjects_Elements_RTL_XmlErrorInfo extends RemObjects_Elements_System_Object{
    FillErrorInfo(aMsg: any, aSuggestion: any, aRow: number, aColumn: number, aXmlDoc: RemObjects_Elements_RTL_XmlDocument);
}

export interface RemObjects_Elements_RTL_XmlDocument extends RemObjects_Elements_System_Object{
    GetNodes(): any;
    GetRoot(): RemObjects_Elements_RTL_XmlElement;
    SetRoot(aRoot: RemObjects_Elements_RTL_XmlElement);
    GetCurrentIdentifier(aColumn: number, aStart: number, aPrefixLength: number, aName: any): any;
    ToString(): string;
    ToString(aFormatOptions: RemObjects_Elements_RTL_XmlFormattingOptions): any;
    ToString(aSaveFormatted: boolean, aFormatOptions: RemObjects_Elements_RTL_XmlFormattingOptions): any;
    AddNode(aNode: RemObjects_Elements_RTL_XmlNode);
    GetCurrentCursorPosition(aRow: number, aColumn: number): RemObjects_Elements_RTL_XmlDocCurrentPosition;
    Nodes: any;
    Root: RemObjects_Elements_RTL_XmlElement;
    Version: any;
    Encoding: any;
    Standalone: any;
    ErrorInfo: RemObjects_Elements_RTL_XmlErrorInfo;
}

export interface RemObjects_Elements_RTL_XmlComment extends RemObjects_Elements_RTL_XmlNode{
    Value: any;
}

export interface RemObjects_Elements_RTL_XmlCData extends RemObjects_Elements_RTL_XmlNode{
    Value: any;
}

export interface RemObjects_Elements_RTL_XmlProcessingInstruction extends RemObjects_Elements_RTL_XmlNode{
    Target: any;
    Data: any;
}

export interface RemObjects_Elements_RTL_XmlText extends RemObjects_Elements_RTL_XmlNode{
    GetValue(): any;
    SetValue(aValue: any);
    Value: any;
}

export interface RemObjects_Elements_RTL_XmlDocumentType extends RemObjects_Elements_RTL_XmlNode{
    Name: any;
    SystemId: any;
    PublicId: any;
    Declaration: any;
}

export interface RemObjects_Elements_RTL_XmlException extends RemObjects_Elements_System_Exception{
    Row: number;
    Column: number;
}

export interface RemObjects_Elements_RTL_XmlParser extends RemObjects_Elements_System_Object{
    GetNamespaceForPrefix(aPrefix: any, aParent: RemObjects_Elements_RTL_XmlElement): RemObjects_Elements_RTL_XmlNamespace;
    ParseEntities(S: any): any;
    ResolveEntity(S: any): any;
    Parse(): RemObjects_Elements_RTL_XmlDocument;
}

export interface RemObjects_Elements_RTL_XmlTokenizer extends RemObjects_Elements_System_Object{
    CharIsIdentifier(C: number): boolean;
    CharIsWhitespace(C: number): boolean;
    CharIsNameStart(C: number): boolean;
    CharIsName(C: number): boolean;
    Parse();
    ParseWhitespace();
    ParseName();
    ParseValue();
    ParseSymbolData();
    ParseComment();
    ParseCData();
    Next(): boolean;
    Xml: any;
    Row: number;
    Column: number;
    Value: any;
    ErrorMessage: any;
    Token: number;
}

export interface RemObjects_Elements_System_Attribute extends RemObjects_Elements_System_Object{
}

export interface RemObjects_Elements_System_AttributeUsageAttribute extends RemObjects_Elements_System_Attribute{
    ValidOn: number;
    AllowMultiple: boolean;
    Inherited: boolean;
}

export interface RemObjects_Elements_System_CallingConventionAttribute extends RemObjects_Elements_System_Attribute{
    CC: number;
}

export interface RemObjects_Elements_System_FlagsAttribute extends RemObjects_Elements_System_Attribute{
}

export interface RemObjects_Elements_System_SkipDebugAttribute extends RemObjects_Elements_System_Attribute{
}

export interface RemObjects_Elements_System_ObsoleteAttribute extends RemObjects_Elements_System_Attribute{
    Message: string;
    Fail: boolean;
}

export interface RemObjects_Elements_System_ConditionalAttribute extends RemObjects_Elements_System_Attribute{
    Conditional: string;
}

export interface RemObjects_Elements_System_DllExportAttribute extends RemObjects_Elements_System_Attribute{
    Name: string;
}

export interface RemObjects_Elements_System_DllImportAttribute extends RemObjects_Elements_System_Attribute{
    DllName: string;
    EntryPoint: string;
}

export interface RemObjects_Elements_System_UnionAttribute extends RemObjects_Elements_System_Attribute{
}

export interface RemObjects_Elements_System_GCSkipIfOnStackAttribute extends RemObjects_Elements_System_Attribute{
}

export interface RemObjects_Elements_System_DynamicInvokeException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_DynamicMethodGroup extends RemObjects_Elements_System_Object{
    Inst: RemObjects_Elements_System_Object;
    Count: number;
}

export interface RemObjects_Elements_System_AssertionException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_NotImplementedException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_NotSupportedException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_NullReferenceException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_InvalidCastException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_AbstractMethodException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_DivideByZeroException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_FormatException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_OverflowException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_ArgumentNullException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_ArgumentOutOfRangeException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_IndexOutOfRangeException extends RemObjects_Elements_System_Exception{
}

export interface RemObjects_Elements_System_Math extends RemObjects_Elements_System_Object{
}

export interface RemObjects_Elements_System_MemoryStream extends RemObjects_Elements_System_Stream{
    SetCapacity(value: number);
    CheckCapacity(value: number);
    CalcCapacity(aNewCapacity: number): number;
    IsValid(): boolean;
    Seek(Offset: number, Origin: number): number;
    Read(aSpan: any): number;
    Write(aSpan: any): number;
    ToArray(): any;
    WriteTo(Destination: RemObjects_Elements_System_Stream);
    SetLength(value: number);
    CanRead: boolean;
    CanSeek: boolean;
    CanWrite: boolean;
    Capacity: number;
    Length: number;
}

export interface RemObjects_Elements_System_IMonitor extends RemObjects_Elements_System_Object{
    Wait();
    Release();
}

export interface RemObjects_Elements_System_Monitor extends RemObjects_Elements_System_Object, RemObjects_Elements_System_IDisposable, RemObjects_Elements_System_IMonitor{
    Wait();
    Release();
    Dispose();
}

export interface RemObjects_Elements_System_GCList extends RemObjects_Elements_System_Object{
    Add(anItem: number);
    Clear();
    Grow(aCapacity: number);
    Remove(val: number);
    RemoveAt(Index: number);
    RemoveRange(Index: number, aCount: number);
    Contains(val: number): boolean;
    Count: number;
}

export interface RemObjects_Elements_System_GCHashSet extends RemObjects_Elements_System_Object{
    DoAdd(hash: number, Item: number);
    DoRemove(Hash: number, aIndex: number);
    IndexOfItem(Hash: number, Item: number): number;
    CalcIndex(Hash: number): number;
    CalcNextCapacity(aCapacity: number): number;
    DoResize(aNewCapacity: number);
    Add(Item: number): boolean;
    Clear();
    Contains(Item: number): boolean;
    Remove(Item: number): boolean;
    AddAllItemsToList(aList: RemObjects_Elements_System_GCList);
    Count: number;
}

export interface RemObjects_Elements_System_StringBuilder extends RemObjects_Elements_System_Object{
    intIndexOf(OldValue: string, StartIndex: number, Count: number): number;
    intCopy(Source: any, Dest: any, CharCount: number);
    IntMove(SourceIndex: number, DestIndex: number, CharCount: number);
    CalcCapacity(aNewCapacity: number): number;
    Grow(Value: number);
    SetLength(value: number);
    SetChars(Index: number, value: number);
    GetChars(Index: number): number;
    SetCapacity(value: number);
    RaiseError(aMessage: string);
    Equals(obj: RemObjects_Elements_System_Object): boolean;
    Equals(sb: RemObjects_Elements_System_StringBuilder): boolean;
    Append(Value: string): RemObjects_Elements_System_StringBuilder;
    Append(Value: string, StartIndex: number, Count: number): RemObjects_Elements_System_StringBuilder;
    Append(Value: number, RepeatCount: number): RemObjects_Elements_System_StringBuilder;
    Append(Value: number): RemObjects_Elements_System_StringBuilder;
    AppendLine(): RemObjects_Elements_System_StringBuilder;
    AppendLine(Value: string): RemObjects_Elements_System_StringBuilder;
    Clear();
    Remove(StartIndex: number, Count: number): RemObjects_Elements_System_StringBuilder;
    Replace(OldChar: number, NewChar: number, StartIndex: number, Count: number): RemObjects_Elements_System_StringBuilder;
    Replace(OldValue: string, NewValue: string, StartIndex: number, Count: number): RemObjects_Elements_System_StringBuilder;
    Replace(OldChar: number, NewChar: number): RemObjects_Elements_System_StringBuilder;
    Replace(OldValue: string, NewValue: string): RemObjects_Elements_System_StringBuilder;
    ToString(): string;
    ToString(StartIndex: number, Count: number): string;
    Insert(Offset: number, Value: string): RemObjects_Elements_System_StringBuilder;
    EnsureCapacity(aCapacity: number): number;
    Capacity: number;
    Length: number;
}

export interface RemObjects_Elements_System_CustomAttribute extends RemObjects_Elements_System_Object{
    Type: RemObjects_Elements_System_Type;
    Constructor: any;
    ArgumentCount: number;
}

export interface RemObjects_Elements_System_CustomAttributeArgument extends RemObjects_Elements_System_Object{
    Name: string;
    Value: RemObjects_Elements_System_Object;
}

export interface RemObjects_Elements_System_Delegate extends RemObjects_Elements_System_Object{
    Self: RemObjects_Elements_System_Object;
    Ptr: any;
}

export interface Program extends RemObjects_Elements_System_Object{
    HelloWorld();
}

export interface qtxlib_BTreeNode extends RemObjects_Elements_System_Object{
}

export interface qtxlib_BTreeError extends RemObjects_Elements_System_Exception{
}

export interface qtxlib_IBTree extends RemObjects_Elements_System_Object{
    Add(Ident: number, Data: any): qtxlib_BTreeNode;
    Contains(Ident: number): boolean;
    Remove(Ident: number): boolean;
    Read(Ident: number): any;
    Write(Ident: number, NewData: any);
    GetEmpty(): boolean;
    GetRoot(): qtxlib_BTreeNode;
    GetCount(): number;
    ToList(): any;
    ToDataList(): any;
    Clear();
}

export interface qtxlib_BTree extends RemObjects_Elements_System_Object, qtxlib_IBTree{
    GetEmpty(): boolean;
    GetRoot(): qtxlib_BTreeNode;
    GetCount(): number;
    Add(Ident: number, Data: any): qtxlib_BTreeNode;
    Contains(Ident: number): boolean;
    Remove(Ident: number): boolean;
    Read(Ident: number): any;
    Write(Ident: number, NewData: any);
    ToList(): any;
    ToDataList(): any;
    Clear();
    Root: qtxlib_BTreeNode;
    Empty: boolean;
    Count: number;
}

export interface MetaClass extends RemObjects_Elements_System_Object{
    ActualType(): RemObjects_Elements_System_Type;
    new(): RemObjects_Elements_System_Object;
}

export interface qtxlib_PixelBuffer extends RemObjects_Elements_System_Object{
    CalcStride(Value: number, PixelByteSize: number, AlignSize: number): number;
    GetEmpty(): boolean;
    GetByte(Offset: number): number;
    SetByte(Offset: number, Value: number);
    OffsetForPixel(dx: number, dy: number): number;
    Allocate(NewWidth: number, NewHeight: number, PxFormat: number);
    Release();
    Read(Offset: number, ByteLength: number): any;
    Write(Offset: number, Data: any);
    GetMetaClass(): MetaClass;
    Width: number;
    Height: number;
    Stride: number;
    Empty: boolean;
    BufferSize: number;
    Format: number;
}

export interface MetaClass extends RemObjects_Elements_System_Object{
    ActualType(): RemObjects_Elements_System_Type;
    new(): RemObjects_Elements_System_Object;
}

export interface MetaClass extends RemObjects_Elements_System_Object{
    ActualType(): RemObjects_Elements_System_Type;
    new(PxSurface: qtxlib_Surface): RemObjects_Elements_System_Object;
}

export interface qtxlib_ColorSource extends RemObjects_Elements_System_Object{
    GetMetaClass(): MetaClass;
    Parent: qtxlib_Surface;
}

export interface qtxlib_Surface extends RemObjects_Elements_System_Object{
    SetStyle(Value: number);
    SetMode(Value: number);
    Allocate(NewWidth: number, NewHeight: number, PxFormat: number);
    Release();
    HasClipRect(): boolean;
    RemoveClipRect();
    FillRect(dx: number, dy: number, dx2: number, dy2: number, PxColor: number);
    Rectangle(dx: number, dy: number, dx2: number, dy2: number, PxColor: number);
    Ellipse(dx: number, dy: number, dx2: number, dy2: number, PxColor: number);
    Circle(dx: number, dy: number, dx2: number, dy2: number, PxColor: number);
    Platonic(dx: number, dy: number, dx2: number, dy2: number, Sides: number, PxColor: number);
    Polygon(Bounds: any, PxColor: number);
    HLine(dx: number, dx2: number);
    VLine(dy: number, dy2: number);
    Line(dx: number, dy: number, dx2: number, dy2: number, PxColor: number);
    GetMetaClass(): MetaClass;
    Buffer: qtxlib_PixelBuffer;
    Empty: boolean;
    Format: number;
    Style: number;
    Mode: number;
    ColorSource: qtxlib_ColorSource;
}

export interface qtxlib_PixelRageError extends RemObjects_Elements_System_Exception{
}

export interface TForm1 extends RemObjects_Elements_RTL_Delphi_VCL_TForm{
    Button1Click(Sender: RemObjects_Elements_System_Object);
}

