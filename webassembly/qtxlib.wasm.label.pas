﻿namespace qtxlib;

interface

uses
  qtxlib,
  RemObjects.Elements.RTL.Delphi,
  RemObjects.Elements.RTL.Delphi.VCL;

type

  TQTXLabel = class(TQTXControl)
  private
    //fContent: dynamic;
  protected
    { method PlatformSetWidth(aValue: Integer); virtual; partial;
    method PlatformSetHeight(aValue: Integer); partial;
    method PlatformSetTop(aValue: Integer); virtual; partial;
    method PlatformSetLeft(aValue: Integer); virtual; partial; }

    procedure PlatformSetCaption(aValue: String); override;

    procedure StyleHandle; override;
  public

    constructor create(aOwner: TComponent); override;



    finalizer;
    begin

    end;

  end;

implementation

const
  lbcss__: String = "
    .lb_$_enableLineBreak  { white-space: normal; }
    .lb_$_disableLineBreak { white-space: nowrap; }
    .lb_$_content {
      display: block;
      position: absolute;
      box-sizing: border-box;
      font-family: inherit;
      font-size: inherit;
      background: #FF0000;
      color: inherit;
      overflow: hidden;
    }

    .lb_$_content_ellipsis { width: 100%; text-overflow: ellipsis; }
    .lb_$_content_v_top    { top: 0%; transform: translate(0%, 0%); }
    .lb_$_content_v_center { top: 50%; -BrowserAPI-transform: translate(0%, -50%); transform: translate(0%, -50%);}
    .lb_$_content_v_bottom { top: 100%; -BrowserAPI-transform: translate(0%, -100%); transform: translate(0%, -100%);}
    .lb_$_content_h_left   { text-align: left; }
    .lb_$_content_h_center { width: 100%; text-align: center; }
    .lb_$_content_h_right  { width: 100%; text-align: right; }
  ";

var
  _StylesSetup: Boolean;

procedure SetupLabelStyles;
begin
  _StylesSetup := true;
  GlobalStyleSheet().AppendToStyleSheet(lbcss__);
end;



constructor TQTXLabel.create(aOwner: TComponent);
begin
  inherited create(aOwner);

  if not _StylesSetup then
    SetupLabelStyles();

  TDispatch.Execute( procedure ()
  begin
    var lContent := WebAssembly.CreateElement("div");
    lContent.innerHTML := "this kicks ass";
    lContent.class := ".lb_$_content .lb_$_content_h_center .lb_$_content_v_center";
    lContent.style.width := "200px";
    lContent.style.height := "18px";
    fHandle.appendChild(lContent);
  end, 100);

  writeLn("We got here!!");
end;

procedure TQTXLabel.StyleHandle;
begin
  inherited;

end;

procedure TQTXLabel.PlatformSetCaption(aValue: String);
begin
  inherited PlatformSetCaption(aValue);
  //fContent.innerHTML := aValue;
end;


end.