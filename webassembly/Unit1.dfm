﻿object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 385
  ClientWidth = 360
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 11
    Width = 63
    Height = 13
    Caption = 'Hello from Delphi!'
  end
  object Edit1: TEdit
    Left = 16
    Top = 39
    Width = 165
    Height = 21
    TabOrder = 0
    Text = 'This is an editbox'
  end
  object Button1: TButton
    Left = 187
    Top = 11
    Width = 141
    Height = 49
    Caption = 'Buttons are fun'
    TabOrder = 1
  end
  object ListBox1: TListBox
    Left = 16
    Top = 68
    Width = 312
    Height = 173
    ItemHeight = 13
    TabOrder = 2
  end
end