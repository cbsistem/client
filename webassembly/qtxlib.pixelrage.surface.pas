﻿namespace qtxlib;

//##################################################################################
// ____ ____  __ __    ___  _          ____    ____   ____    ___
// |    \    ||  |  |  /  _]| |     __ |    \  /    | /    |  /  _]
// |  o  )  | |  |  | /  [_ | |    |  ||  D  )|  o  ||   __| /  [_
// |   _/|  | |_   _||    _]| |___ |__||    / |     ||  |  ||    _]
// |  |  |  | |     ||   [_ |     | __ |    \ |  _  ||  |_ ||   [_
// |  |  |  | |  |  ||     ||     ||  ||  .  \|  |  ||     ||     |
// |__| |____||__|__||_____||_____||__||__|\_||__|__||___,_||_____
//
// Written by Jon-Lennart Aasenden @ 14.06.2019 under GPL v3 ~ Share the love!
//##################################################################################

interface

uses System, rtl, qtxlib;

type

  [Export]
  DrawStyle = public (
    dsOutline,  // Outline primitives
    dsFilled    // Fill primitives
    );

  [Export]
  DrawMode = public (
    dmNormal,   // Normal RGB
    dmBlend     // Apply alpha-blending
    );

  [Export]
  Surface = public class(Object)
  private
    FBuffer:    PixelBuffer;
    FClip:      Rect;
    FClipped:   Boolean;
    FStyle:     DrawStyle;
    FMode:      DrawMode;
    FSource:    ColorSource;
  protected
    procedure   SetStyle(const Value: DrawStyle); virtual;
    procedure   SetMode(const Value: DrawMode); virtual;
  public
    property    Buffer: PixelBuffer read FBuffer;
    property    &Empty: Boolean read (FBuffer.&Empty);
    property    Format: PixelFormat read (FBuffer.Format);
    property    ClipRect: Rect read FClip;
    property    Style: DrawStyle read FStyle write SetStyle;
    property    Mode: DrawMode read FMode write SetMode;
    property    &ColorSource: ColorSource read FSource;

    procedure   Allocate(NewWidth, NewHeight: Integer; const PxFormat: PixelFormat);
    procedure   Release;

    function    HasClipRect: Boolean;
    procedure   SetClipRect(const NewClipRect: Rect);
    procedure   RemoveClipRect;

    procedure   FillRect(Bounds: Rect; PxColor: Color); overload;
    procedure   FillRect(dx, dy, dx2, dy2: Integer; const PxColor: Color); overload;

    procedure   Rectangle(Bounds: Rect; PxColor: Color); overload;
    procedure   Rectangle(dx, dy, dx2, dy2: Integer; PxColor: Color); overload;

    procedure   Ellipse(Bounds: Rect; PxColor: Color); overload;
    procedure   Ellipse(dx, dy, dx2, dy2: Integer; PxColor: Color); overload;

    procedure   Circle(Bounds: Rect; PxColor: Color); overload;
    procedure   Circle(dx, dy, dx2, dy2: Integer; PxColor: Color); overload;

    procedure   Platonic(Bounds: Rect; Sides: Integer; PxColor: Color); overload;
    procedure   Platonic(dx, dy, dx2, dy2, Sides: Integer; PxColor: Color); overload;

    procedure   Polygon(Bounds: array of Point; PxColor: Color);

    procedure   HLine(dx, dx2: Integer);
    procedure   VLine(dy, dy2: Integer);
    procedure   Line(dx, dy, dx2, dy2: Integer; PxColor: Color);

    constructor Create; virtual;

    finalizer;
    begin
      Release();
      disposeAndNil(FBuffer);
    end;
  end;

implementation

//##################################################################################
// Surface
//##################################################################################

constructor Surface.Create;
begin
  inherited Create();
  FBuffer := PixelBuffer.Create();
end;

procedure Surface.Allocate(NewWidth, NewHeight: Integer; const PxFormat: PixelFormat);
begin
  // Allocate pixel buffer
  FBuffer.Allocate(NewWidth, NewHeight, PxFormat);

  // create color-source matching the pixel-format
  case PxFormat of
  PixelFormat.pf8bit:   FSource := ColorSource008.create(self);
  PixelFormat.pf15bit:  FSource := ColorSource555.create(self);
  PixelFormat.pf16bit:  FSource := ColorSource565.create(self);
  PixelFormat.pf24bit:  FSource := ColorSource888.create(self);
  PixelFormat.pf32bit:  FSource := ColorSource38A.create(self);
  else
    raise PixelRageError.create("Invalid pixel-format, not colorsource available error");
  end;
end;

procedure Surface.Release;
begin
  if HasClipRect() then
    RemoveClipRect();

  if not FBuffer.Empty then
    FBuffer.Release();

  if FSource <> nil then
    disposeAndNil(FSource);
end;

procedure Surface.SetStyle(const Value: DrawStyle);
begin
  FStyle := Value;
end;

procedure Surface.SetMode(const Value: DrawMode);
begin
  FMode := Value;
end;

function Surface.HasClipRect: Boolean;
begin
  result := FClipped;
end;

procedure Surface.SetClipRect(const NewClipRect: Rect);
begin
  RemoveClipRect();
  if not NewClipRect.Empty() then
  begin
    FClip := NewClipRect;
    FClip.Normalize();
    FClipped := true;
  end;
end;

procedure Surface.RemoveClipRect;
begin
  if FClipped then
  begin
    FClip.Reset();
    FClipped := false;
  end;
end;

procedure Surface.Polygon(Bounds: array of Point; PxColor: Color);
begin

end;

procedure Surface.FillRect(Bounds: Rect; PxColor: Color);
begin

end;

procedure Surface.FillRect(dx, dy, dx2, dy2: Integer; const PxColor: Color);
begin

end;

procedure Surface.Rectangle(Bounds: Rect; PxColor: Color);
begin

end;

procedure Surface.Rectangle(dx, dy, dx2, dy2: Integer; PxColor: Color);
begin

end;

procedure Surface.Ellipse(Bounds: Rect; PxColor: Color);
begin

end;

procedure Surface.Ellipse(dx, dy, dx2, dy2: Integer; PxColor: Color);
begin

end;

procedure Surface.Circle(Bounds: Rect; PxColor: Color);
begin

end;

procedure Surface.Circle(dx, dy, dx2, dy2: Integer; PxColor: Color);
begin

end;

procedure Surface.Platonic(Bounds: Rect; Sides: Integer; PxColor: Color);
begin

end;

procedure Surface.Platonic(dx, dy, dx2, dy2, Sides: Integer; PxColor: Color);
begin

end;

procedure Surface.HLine(dx, dx2: Integer);
begin

end;

procedure Surface.VLine(dy, dy2: Integer);
begin

end;

procedure Surface.Line(dx, dy, dx2, dy2: Integer; PxColor: Color);
begin

end;

end.