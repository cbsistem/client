﻿namespace qtxlib;

//####################################################################################
//  ____   ____  ____    ____  ____   __ __      ______  ____     ___    ___
// |    \ |    ||    \  /    ||    \ |  |  | __ |      ||    \   /  _]  /  _]
// |  o  ) |  | |  _  ||  o  ||  D  )|  |  ||  ||      ||  D  ) /  [_  /  [_
// |     | |  | |  |  ||     ||    / |  ~  ||__||_|  |_||    / |    _]|    _]
// |  O  | |  | |  |  ||  _  ||    \ |___, | __   |  |  |    \ |   [_ |   [_
// |     | |  | |  |  ||  |  ||  .  \|     ||  |  |  |  |  .  \|     ||     |
// |_____||____||__|__||__|__||__|\_||____/ |__|  |__|  |__|\_||_____||_____|
//
// Written by Jon-Lennart Aasenden @ 14.06.2019 under GPL v3 ~ Share the love!
//####################################################################################

interface

type

  [Export]
  BTreeId = public UInt32;

  [Export]
  BTreeNode = public class(Object)
  public
    Identifier: BTreeId;    //  Unique identifier
    Data:       dynamic;    //  Data association
    Left:       BTreeNode;  //  Left branch
    Right:      BTreeNode;  //  Right branch
  end;

  [Export]
  BTreeError = public class(Exception);

  [Export]
  BTreeProcessCB = public procedure (const Item: BTreeNode; var Cancel: Boolean) of object;

  IBTree = public interface
    function  &Add(const Ident: BTreeId; const Data: dynamic): BTreeNode;
    function  &Contains(const Ident: BTreeId): Boolean;
    function  &Remove(const Ident: BTreeId): Boolean;
    function  &Read(const Ident: BTreeId): dynamic;
    procedure &Write(const Ident: BTreeId; const NewData: dynamic);

    procedure ForEach(const Process: BTreeProcessCB);
    function  GetEmpty: Boolean;
    function  GetRoot: BTreeNode;
    function  GetCount: UInt32;

    function  ToList: List<BTreeNode>;
    function  ToDataList: List<dynamic>;
    procedure Clear;
  end;

  [Export]
  BTree = public class(Object, IBTree)
  private
    FRoot:    BTreeNode;
    FCurrent: BTreeNode;
  protected
    function  GetEmpty: Boolean; virtual;
    function  GetRoot: BTreeNode; virtual;
    function  GetCount: UInt32; virtual;
  public
    property  Root: BTreeNode read GetRoot;
    property  &Empty: Boolean read GetEmpty;
    property  Count: UInt32 read GetCount;

    function  &Add(const Ident: BTreeId; const Data: dynamic): BTreeNode; virtual;
    function  &Contains(const Ident: BTreeId): Boolean; virtual;
    function  &Remove(const Ident: BTreeId): Boolean; virtual;
    function  &Read(const Ident: BTreeId): dynamic; virtual;
    procedure &Write(const Ident: BTreeId; const NewData: dynamic); virtual;

    procedure ForEach(const Process: BTreeProcessCB); virtual;
    function  ToList: List<BTreeNode>;
    function  ToDataList: List<dynamic>;

    procedure Clear; virtual;

    class function Build: IBTree;

    finalizer;
    begin
      // Force GC to clean up
      FCurrent := nil;
      FRoot := nil;
    end;

  end;

implementation

//####################################################################################
// BTree
//####################################################################################

class function BTree.Build: IBTree;
begin
  result := BTree.create() as IBTree;
end;

function BTree.GetEmpty: Boolean;
begin
  result := FRoot = nil;
end;

function BTree.GetRoot: BTreeNode;
begin
  result := FRoot;
end;

function BTree.GetCount: UInt32;
var
LCount: UInt32;
begin
  ForEach( procedure (const Node: BTreeNode; var Cancel: Boolean)
  begin
    inc(LCount);
  end);
  result := LCount;
end;

procedure BTree.Clear;
begin
  // just decouple the references and let the GC handle it
  FCurrent := nil;
  FRoot := nil;
end;

function  BTree.ToList: List<BTreeNode>;
begin
  var LCache := new List<BTreeNode>;
  ForEach( procedure (const Item: BTreeNode; var Cancel: Boolean)
    begin
      LCache.Add(Item);
      Cancel := false;
    end);
  result := LCache;
end;

function BTree.ToDataList: List<dynamic>;
begin
  var LCache := new List<dynamic>;
  ForEach( procedure (const Item: BTreeNode; var Cancel: Boolean)
    begin
      LCache.Add(Item.Data);
      Cancel := false;
    end);
  result := LCache;
end;

function BTree.&Add(const Ident: BTreeId; const Data: dynamic): BTreeNode;
begin
  var LNode := new BTreeNode;
  LNode.Identifier := Ident;
  LNode.Data := Data;

  if FRoot = nil then
    FRoot := LNode;

  FCurrent := FRoot;

  while true do
  begin
    if (Ident < FCurrent.Identifier) then
    begin
      if (FCurrent.Left = nil) then
      begin
        FCurrent.Left := LNode;
        break;
      end else
      FCurrent := FCurrent.Left;
    end else
    if (Ident > FCurrent.Identifier) then
    begin
      if (FCurrent.Right = nil) then
      begin
        FCurrent.Right := LNode;
        break;
      end else
      FCurrent := FCurrent.Right;
    end else
    break;
  end;

  result := LNode;
end;

function BTree.&Contains(const Ident: BTreeId): Boolean;
begin
  if FRoot <> nil then
  begin
    FCurrent := FRoot;
    while ( (not Result) and (FCurrent <> nil) ) do
    begin
      if (Ident < FCurrent.Identifier) then
        FCurrent := FCurrent.Left
      else
        if (Ident > FCurrent.Identifier) then
          FCurrent := FCurrent.Right
        else
        begin
          Result := true;
        end
    end;
  end;
end;

function BTree.&Remove(const Ident: BTreeId): boolean;
var
  LFound: Boolean;
  LParent: BTreeNode;
  LReplacement,
  LReplacementParent: BTreeNode;
begin
  FCurrent := FRoot;

  while (not LFound) and (FCurrent<>nil) do
  begin
    if (Ident < FCurrent.Identifier) then
    begin
      LParent := FCurrent;
      FCurrent:= FCurrent.Left;
    end else
      if (Ident > FCurrent.Identifier) then
      begin
        LParent := FCurrent;
        FCurrent := FCurrent.Right;
      end else
        LFound := true;

    if (LFound) then
    begin
      var LChildCount := 0;
      if (FCurrent.Left <> nil) then
        inc(LChildCount);
      if (FCurrent.Right <> nil) then
        inc(LChildCount);

      if (FCurrent = FRoot) then
      begin
        case (LChildCount) of
          0:  begin
            FRoot := nil;
          end;
          1:  begin
            FRoot := if FCurrent.Right = nil then
              FCurrent.Left
            else
              FCurrent.Right;
          end;
          2:  begin
            LReplacement := FRoot.Left;
            while (LReplacement.Right <> nil) do
            begin
              LReplacementParent := LReplacement;
              LReplacement := LReplacement.Right;
            end;

            if (LReplacementParent <> nil) then
            begin
              LReplacementParent.Right := LReplacement.Left;
              LReplacement.Right := FRoot.Right;
              LReplacement.Left := FRoot.Left;
            end else
              LReplacement.Right := FRoot.Right;
          end;
        end;

        FRoot := LReplacement;
      end else
      begin
        case LChildCount of
          0:  if (FCurrent.Identifier < LParent.Identifier) then
            LParent.Left  := nil
            else
            LParent.Right := nil;
          1:  if (FCurrent.Identifier < LParent.Identifier) then
          begin
            if (FCurrent.Left = NIL) then
              LParent.Left := FCurrent.Right
            else
              LParent.Left := FCurrent.Left;
          end else
          begin
            if (FCurrent.Left = NIL) then
              LParent.Right := FCurrent.Right
            else
              LParent.Right := FCurrent.Left;
          end;
          2:  begin
            LReplacement := FCurrent.Left;
            LReplacementParent := FCurrent;

            while LReplacement.Right <> nil do
            begin
              LReplacementParent := LReplacement;
              LReplacement := LReplacement.Right;
            end;
            LReplacementParent.Right := LReplacement.Left;

            LReplacement.Right := FCurrent.Right;
            LReplacement.Left := FCurrent.Left;

            if (FCurrent.Identifier < LParent.Identifier) then
              LParent.Left := LReplacement
            else
              LParent.Right := LReplacement;
          end;
        end;
      end;
    end;
  end;

  result := LFound;
end;

function BTree.&Read(const Ident: BTreeId): dynamic;
begin
  FCurrent := FRoot;
  while FCurrent <> nil do
  begin
    if (Ident < FCurrent.Identifier) then
      FCurrent := FCurrent.Left
    else
      if (Ident > FCurrent.Identifier) then
        FCurrent := FCurrent.Right
      else
      begin
        result := FCurrent.Data;
        break;
      end
  end;
end;

procedure BTree.&Write(const Ident: BTreeId; const NewData: dynamic);
begin
  FCurrent := FRoot;
  while (FCurrent <> nil) do
  begin
    if (Ident < FCurrent.Identifier) then
      FCurrent := FCurrent.Left
    else
      if (Ident > FCurrent.Identifier) then
        FCurrent := FCurrent.Right
      else
      begin
        FCurrent.Data := NewData;
        break;
      end
  end;
end;

procedure BTree.ForEach(const Process: BTreeProcessCB);
  function ProcessNode(const Node: BTreeNode): Boolean;
  begin
    if Node <> nil then
    begin
      // Check left branch
      if Node.Left <> nil then
      begin
        result := ProcessNode(Node.Left);
        if result then
          exit;
      end;

      // Check current leaf
      Process(Node, result);
      if result then
        exit;

      // Check right branch
      if (Node.Right <> nil) then
      begin
        result := ProcessNode(Node.Right);
        if result then
          exit;
      end;
    end;
  end;
begin
  ProcessNode(FRoot);
end;

end.