﻿namespace qtxlib;

//##################################################################################
// ____ ____  __ __    ___  _          ____    ____   ____    ___
// |    \    ||  |  |  /  _]| |     __ |    \  /    | /    |  /  _]
// |  o  )  | |  |  | /  [_ | |    |  ||  D  )|  o  ||   __| /  [_
// |   _/|  | |_   _||    _]| |___ |__||    / |     ||  |  ||    _]
// |  |  |  | |     ||   [_ |     | __ |    \ |  _  ||  |_ ||   [_
// |  |  |  | |  |  ||     ||     ||  ||  .  \|  |  ||     ||     |
// |__| |____||__|__||_____||_____||__||__|\_||__|__||___,_||_____
//
// Written by Jon-Lennart Aasenden @ 14.06.2019 under GPL v3 ~ Share the love!
//##################################################################################

interface

uses System, rtl;


type

  // Define pointer type, if pointers are allowed.
  // This is used to quickly access pixel data
  PByte     = private ^Byte;
  PWord     = private ^Word;
  PLongWord = private ^LongWord;
  PRGBDef   = private ^RGBDef;
  PRGBADef  = private ^RGBADef;

  [Export]
  PixelBuffer = public class(Object)
  private
    FPixels:    PByte;
    FDepthLUT:  array of Integer;
    FStride:    Integer;
    FWidth:     Integer;
    FHeight:    Integer;
    FBytes:     Integer;
    FFormat:    PixelFormat;
  protected
    function    CalcStride(const Value, PixelByteSize, AlignSize: Integer): Integer; inline;
    function    GetEmpty: Boolean; inline;
    function    GetByte(const Offset: Integer): Byte; inline;
    procedure   SetByte(const Offset: Integer; const Value: Byte); inline;
  public
    property    Width: Integer read FWidth;
    property    Height: Integer read FHeight;
    property    Stride: Integer read FStride;
    property    &Empty: Boolean read GetEmpty;
    property    BufferSize: Integer read FBytes;
    property    Format: PixelFormat read FFormat;
    property    Buffer[const index: Integer]: Byte read GetByte write SetByte;

    function    OffsetForPixel(const dx, dy: Integer): Integer; inline;
    procedure   Allocate(NewWidth, NewHeight: Integer; const PxFormat: PixelFormat);
    procedure   Release();

    function    &Read(const Offset: Integer; ByteLength: Integer): array of Byte; inline;
    procedure   &Write(const Offset: Integer; const Data: array of Byte); inline;

    constructor Create; virtual;

    finalizer;
    begin
      Release();
    end;
end;

implementation

//##################################################################################
// PixelBuffer
//##################################################################################

constructor PixelBuffer.Create;
begin
  inherited Create();
  FDepthLUT := [0, 1, 2, 2, 3, 4];
  FFormat := PixelFormat.pfNone;
end;

function PixelBuffer.GetEmpty: Boolean;
begin
  result := FPixels = nil;
end;

function PixelBuffer.GetByte(const Offset: Integer): Byte;
begin
  var LStart := FPixels;
  inc(LStart, Offset);
  result := LStart^;
end;

procedure PixelBuffer.SetByte(const Offset: Integer; const Value: Byte);
begin
  var LStart := FPixels;
  inc(LStart, Offset);
  LStart^ := Value;
end;

// This routine "rounds off" the bytes allocated for each scanline.
// Its is only used when allocating a pixel-buffer (inline).
// Under Windows, OS-X and other targets, the OS expects device-independent-bitmaps to
// be aligned to a 4 byte boundary. Linux DIB's has a 8 byte boundary to take advantage
// of 64 bit CPU instructions for moving data.
function PixelBuffer.CalcStride(const Value, PixelByteSize, AlignSize: Integer): Integer;
begin
  result := Value * PixelByteSize;
  var xFetch := result mod AlignSize;
  if xFetch > 0 then
  begin
    inc(result, AlignSize);
    dec(result, xFetch);
    //result := ( (Result + AlignSize) - xFetch );
  end;
end;

procedure PixelBuffer.Allocate(NewWidth, NewHeight: Integer; const PxFormat: PixelFormat);
begin
  if not GetEmpty() then
    Release();

  if NewWidth < 1 then
    raise new PixelRageError("Failed to allocate pixel-buffer: Invalid width error");

  if NewHeight < 1 then
    raise new PixelRageError("Failed to allocate pixel-buffer: invalid height error");

  // Always calculate stride to nearest 32bit for WinAPI and GTK DIB compatebility
  FStride := CalcStride(NewWidth, FDepthLUT[PxFormat], 4);
  FFormat := PxFormat;
  FWidth := NewWidth;
  FHeight := NewHeight;
  FBytes := FStride * FHeight;

  try
    FPixels := PByte( System.malloc(FBytes) );
  except
    on e: Exception do
    begin
      FStride := 0;
      FFormat := PixelFormat.pfNone;
      FWidth := 0;
      FHeight := 0;
      FBytes := 0;
      FPixels := nil;
      raise new PixelRageError("Failed to allocate pixel-buffer: " + e.Message);
    end;
  end;
end;

procedure PixelBuffer.Release();
begin
  if not GetEmpty() then
  begin
    try
      try
        System.free( FPixels );
      except
        // sink exception
      end;
    finally
      FPixels := nil;
      FFormat := PixelFormat.pfNone;
      FWidth := 0;
      FHeight := 0;
      FStride := 0;
      FBytes := 0;
    end;
  end;
end;

function PixelBuffer.OffsetForPixel(const dx, dy: integer): Integer;
begin
  // should optimize well under LLVM
  var offx: Integer := dx * FDepthLUT[FFormat];
  result := dy * FStride;
  inc(result, offx);
end;

function PixelBuffer.&Read(const Offset: Integer; ByteLength: Integer): array of Byte;
begin
  // Prepare return buffer
  if ByteLength > 0 then
    result := new Byte[ByteLength];

  var LStart := FPixels;
  inc(LStart, Offset);

  case ByteLength of
  1:  result[0] := LStart^;
  2:  PWord(@result[0])^ := PWord(LStart)^;
  3:  PRGBDef(@result[0])^ := PRGBDef(LStart)^;
  4:  PLongWord(@result[0])^ := PLongWord(LStart)^;
  else
    begin
      var xOff: Integer := 0;
      while ByteLength > 0 do
      begin
        result[xOff] := LStart^;
        inc(xOff);
        inc(LStart);
        dec(ByteLength);
      end;
    end;
  end;
end;

procedure PixelBuffer.&Write(const Offset: Integer; const Data: array of Byte);
begin
  // setup pointer
  var LStart := FPixels;
  inc(LStart, Offset);

  case length(Data) of
  1:  LStart^ := Data[0];
  2:  PWord(LStart)^ := PWord(@Data[0])^;
  3:  PRGBDef(LStart)^ := PRGBDef(@Data[0])^;
  4:  PLongWord(LStart)^ := PLongWord(@Data[0])^;
  else
    begin
      // write each byte to the target
      for each el in Data do
        begin
        LStart^ := el;
        inc(LStart);
      end;
    end;
  end;
end;


end.