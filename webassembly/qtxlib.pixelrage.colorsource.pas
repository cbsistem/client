﻿namespace qtxlib;

//##################################################################################
// ____ ____  __ __    ___  _          ____    ____   ____    ___
// |    \    ||  |  |  /  _]| |     __ |    \  /    | /    |  /  _]
// |  o  )  | |  |  | /  [_ | |    |  ||  D  )|  o  ||   __| /  [_
// |   _/|  | |_   _||    _]| |___ |__||    / |     ||  |  ||    _]
// |  |  |  | |     ||   [_ |     | __ |    \ |  _  ||  |_ ||   [_
// |  |  |  | |  |  ||     ||     ||  ||  .  \|  |  ||     ||     |
// |__| |____||__|__||_____||_____||__||__|\_||__|__||___,_||_____
//
// Written by Jon-Lennart Aasenden @ 14.06.2019 under GPL v3 ~ Share the love!
//##################################################################################

interface

type

  // About color-sources:
  // A color-source is responsible for converting a normal color value,
  // meaning an RGBA value encoded into a longword (same as TColor in Delphi and C++ builder),
  // and turning that into pixel-data compatible with display APIs.
  // For example, if your surface is set to 15-bit, the colorsource will recognize that
  // and ColorToNative() will emit a 555 encoded pixel value.
  // The class below is the abstract base-class for such sources.
  ColorSource = public abstract class(Object)
  private
    FParent:    Surface;
  public
    property    Parent: Surface read FParent;
    procedure   ColorToNative(const PxColor: Color; var Data); virtual; abstract;
    class procedure ColorToRGB(const PxColor: Color; var R, G, B: Byte); inline;
    class procedure ColorToRGBA(const PxColor: Color; var R, G, B, A: Byte); inline;
    constructor Create(const PxSurface: Surface); virtual;
  end;

  ColorSource008 = public class(ColorSource)
  public
    procedure ColorToNative(const PxColor: Color; var Data); override;
  end;

  ColorSource555 = public class(ColorSource)
  public
    procedure ColorToNative(const PxColor: Color; var Data); override;
  end;

  ColorSource565 = public class(ColorSource)
  public
    procedure ColorToNative(const PxColor: Color; var Data); override;
  end;

  ColorSource888 = public class(ColorSource)
  public
    procedure ColorToNative(const PxColor: Color; var Data); override;
  end;

  ColorSource38A = public class(ColorSource)
  public
    procedure ColorToNative(const PxColor: Color; var Data); override;
  end;


implementation

//##################################################################################
// ColorSource
//##################################################################################

constructor ColorSource.Create(const PxSurface: Surface);
begin
  inherited Create();
  FParent := PxSurface;
end;

class procedure ColorSource.ColorToRGB(const PxColor: Color; var R, G, B: Byte);
begin
  R := Byte(PxColor);
  G := Byte( PxColor shr 8 );
  B := Byte( PxColor shr 16 );
end;

class procedure ColorSource.ColorToRGBA(const PxColor: Color; var R, G, B, A: Byte);
begin
  R := Byte(PxColor);
  G := Byte( PxColor shr 8 );
  B := Byte( PxColor shr 16 );
  A := Byte( PxColor shr 24 );
end;

//##################################################################################
// ColorSource008
//##################################################################################

procedure ColorSource008.ColorToNative(const PxColor: Color; var Data);
begin

end;

//##################################################################################
// ColorSource555
//##################################################################################

procedure ColorSource555.ColorToNative(const PxColor: Color; var Data);
var
  R, G, B: Byte;
begin
  ColorToRGB(PxColor, R, G, B);
  Data := Word((R shr 3) shl 10) + ((G shr 3) shl 5) + (B shr 3);
end;

//##################################################################################
// ColorSource555
//##################################################################################

procedure ColorSource565.ColorToNative(const PxColor: Color; var Data);
var
  R, G, B: Byte;
begin
  ColorToRGB(PxColor, R, G, B);
  Data := Word( (R shr 3) shl 11 or (G shr 2) shl 5 or (B shr 3) );
end;

//##################################################################################
// ColorSource888
//##################################################################################

procedure ColorSource888.ColorToNative(const PxColor: Color; var Data);
var
  R, G, B: Byte;
begin
  ColorToRGB(PxColor, R, G, B);
  var ref_ := PRGBDef(@Data);
  ref_^.R := R;
  ref_^.G := G;
  ref_^.B := B;
end;

//##################################################################################
// ColorSource38A
//##################################################################################

procedure ColorSource38A.ColorToNative(const PxColor: Color; var Data);
var
  R, G, B, A: Byte;
begin
  ColorToRGBA(PxColor, R, G, B, A);
  var ref_ := PRGBADef(@Data);
  ref_^.R := R;
  ref_^.G := G;
  ref_^.B := B;
  ref_^.A := A;
end;

end.