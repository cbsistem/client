﻿namespace qtxlib;

interface

uses
  qtxlib,
  RemObjects.Elements.RTL.Delphi,
  RemObjects.Elements.RTL.Delphi.VCL;

type

  TQTXDisplayMode = public (
    dmInlineBlock,
    dmBlock,
    dmFlex,
    dmTable,
    dmTableCaption,
    dmTableCell,
    dmTableRow,
    dmTableColumn,
    dmRunIn,
    dmListItem
  );

  TQTXPositionMode = public (
    pmRelative,
    pmAbsolute
  );

  TQTXComponentState = public (
    csCreating,
    csLoading,
    csReady,
    csDestroying
  );

  TQTXControl = public class(TCustomControl)
  private
    fDisplayMode: TQTXDisplayMode;
    fPositionMode:  TQTXPositionMode;
  protected
    procedure   PlatformSetWidth(aValue: Integer); override;
    procedure   PlatformSetHeight(aValue: Integer); //override;
    procedure   PlatformSetTop(aValue: Integer); override;
    procedure   PlatformSetLeft(aValue: Integer); override;

    procedure   SetLayoutMode(const Value: TQTXPositionMode); virtual;
    procedure   SetDisplayMode(const Value: TQTXDisplayMode); virtual;
    procedure   CreateHandle; override;

    procedure   StyleHandle; virtual;
    procedure   Resize; virtual;

  public
    property    PositionMode: TQTXPositionMode read fPositionMode write SetLayoutMode;
    property    DisplayMode: TQTXDisplayMode read fDisplayMode write SetDisplayMode;

    procedure   SetBounds(const aValue: TRect); virtual; overload;
    procedure   SetBounds(aLeft, aTop, aWidth, aHeight: Integer); virtual; overload;

    constructor create(aOwner: TComponent); virtual;
  end;

  TDOMHelper = abstract class
  public
    class function GetDocument: dynamic;
    class function GetBody: dynamic;
    class function GetHead: dynamic;
    class function GetComputedStyleSheet(const ForThisHandle: dynamic): dynamic;
    class function GetComputedStyle(const ForThisHandle: dynamic; StyleId: String): dynamic;
    class function GetNavigator: dynamic;
    class function GetUserAgent: String;

    class function GetIsFireFox: Boolean;
    class function GetIsChrome: Boolean;
    class function GetIsCordova: Boolean;
    class function GetIsWebkit: Boolean;

    class function GetCssPrefixId: String;
    class function CssPrefix(const CssName: String): String;

    class procedure SetAttribute(const el: dynamic; Name: String; const Value: dynamic); inline;
  end;



implementation

// CSS :: Displaymode lookup-table
const __DisplayLUT: array[TQTXDisplayMode] of String = ['inline-block', 'block', 'flex', 'table',
'table-caption', 'table-cell', 'table-row', 'table-column', 'run-in', 'list-item'];

// CSS :: Position mode lookup-table
const __PositionLUT: array[TQTXPositionMode] of String = ['relative', 'absolute'];

//########################################################################################
// TQTXControl
//########################################################################################

constructor TQTXControl.create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

procedure TQTXControl.CreateHandle;
begin
  fHandle := WebAssembly.CreateElement('div');
  StyleHandle();
end;

procedure TQTXControl.StyleHandle;
begin
  // set position behavior
  fPositionMode := fPositionMode.pmAbsolute;
  fHandle.style.position := "absolute";

  // set element display mode
  fDisplayMode := TQTXDisplayMode.dmInlineBlock;
  fHandle.style.display := 'inline-block';

  // clip overflow (no scrollbars by default)
  fHandle.style.overflow := 'hidden';
  //fHandle.style.background := "#FF0000";
end;

procedure TQTXControl.SetBounds(const aValue: TRect);
begin
  inherited PlatformSetLeft(aValue.Left);
  inherited PlatformSetTop(aValue.Top);
  inherited PlatformSetWidth((aValue.Right - aValue.Left) + 1);
  inherited PlatformSetHeight((aValue.Bottom - aValue.Top) + 1);
end;

procedure TQTXControl.SetBounds(aLeft, aTop, aWidth, aHeight: Integer);
begin
end;

procedure TQTXControl.PlatformSetWidth(aValue: Integer);
begin
  inherited PlatformSetWidth(aValue);
end;

procedure TQTXControl.PlatformSetHeight(aValue: Integer);
begin
  inherited PlatformSetHeight(aValue);
end;

procedure TQTXControl.PlatformSetTop(aValue: Integer);
begin
  inherited PlatformSetTop(aValue);
end;

procedure TQTXControl.PlatformSetLeft(aValue: Integer);
begin
  inherited PlatformSetLeft(aValue);
end;

procedure TQTXControl.Resize;
begin
  //
end;

procedure TQTXControl.SetLayoutMode(const Value: TQTXPositionMode);
begin
  if Value <> fPositionMode then
  begin
    fHandle.style.position := __PositionLUT[Value];
    // Invalidate() call here to force a resize
  end;
end;

procedure TQTXControl.SetDisplayMode(const Value: TQTXDisplayMode);
begin
  if Value <> fDisplayMode then
  begin
    fDisplayMode := Value;
    Handle.style.display := __DisplayLUT[fDisplayMode];
    // Invalidate() call here to force a resize
  end;
end;

//########################################################################################
// TDOMHelper
//########################################################################################

class function TDOMHelper.GetCssPrefixId: String;
begin
  if GetIsFireFox() then exit('moz');
  if GetIsWebkit() then exit("webkit");
end;

class function TDOMHelper.CssPrefix(const CssName: String): String;
begin
  result := "-" + GetCssPrefixId() + CssName;
end;

class procedure TDOMHelper.SetAttribute(const el: dynamic; Name: string; const Value: dynamic);
begin
  el.setAttribute(Name, Value);
end;

class function TDOMHelper.GetIsCordova: Boolean;
begin
  var lWindow := dynamic( WebAssembly.GetWindowObject() );
  result := lWindow.cordova <> nil;
end;

class function TDOMHelper.GetIsChrome: Boolean;
begin
  var lUserAgent := GetUserAgent();

  //example: UserAgent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36
  result := lUserAgent.ToLower().IndexOf("chrome") > 0;
end;

class function TDOMHelper.GetIsWebkit: Boolean;
begin
  var lUserAgent := GetUserAgent();
  result := lUserAgent.ToLower().IndexOf("webkit") >= 0;

  // Double check if it might be safari, some versions omits the webkit ident
  if not result then
    result := lUserAgent.ToLower().IndexOf("safari") >= 0;
end;

class function TDOMHelper.GetIsFireFox: Boolean;
begin
  var lUserAgent := GetUserAgent();
  result := lUserAgent.ToLower().IndexOf("firefox") >= 0;
end;

class function TDOMHelper.GetNavigator: dynamic;
begin
  result:= WebAssembly.GetWindowObject().navigator;
end;

class function TDOMHelper.GetUserAgent: String;
begin
  result := String( GetNavigator().userAgent );
end;

class function TDOMHelper.GetDocument: dynamic;
begin
  result := WebAssembly.GetWindowObject().document;
end;

class function TDOMHelper.GetHead: dynamic;
begin
  result := GetDocument().head;
end;

class function TDOMHelper.GetBody: dynamic;
begin
  result := GetDocument().body;
end;

class function TDOMHelper.GetComputedStyleSheet(const ForThisHandle: dynamic): dynamic;
begin
  result := GetDocument().defaultView.getComputedStyle(ForThisHandle, nil);
end;

class function TDOMHelper.GetComputedStyle(const ForThisHandle: dynamic; StyleId: String): dynamic;
begin
  var lSheet := GetComputedStyleSheet(ForThisHandle);
  result := lSheet.getPropertyValue(StyleId);
end;

end.