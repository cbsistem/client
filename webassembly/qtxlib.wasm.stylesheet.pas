﻿namespace qtxlib;

interface

uses
  qtxlib,
  RemObjects.Elements.RTL.Delphi,
  RemObjects.Elements.RTL.Delphi.VCL;

type

  TStyleSheet = public class(Object)
  private
    FHandle:    dynamic;
    FSheet:     dynamic;
    FRules:     dynamic;
  protected
    // These are here for a reason, there is no sheet/rule objects
    // until after css has been loaded and parsed
    function    GetSheet: dynamic; inline;
    function    GetRules: dynamic; inline;
    function    GetCount: Integer; inline;
    function    GetStyleName(const &Index: Integer): String; inline;
    function    GetStyleCss(const &Index: Integer): String; inline;
  public
    property    Handle: THandle read ( THandle(FHandle) );
    property    Count: Integer read GetCount;
    property    Names[&Index: Integer]: String read GetStyleName;
    property    CSS[&Index: Integer]: String read GetStyleCss;

    procedure   AppendToStyleSheet(const CssCode: String);

    function    &Add(RuleDef: String): Integer; overload;
    function    &Add(RuleName, RuleCode: String): Integer; overload;
    procedure   &Remove(const &Index: Integer); overload;
    procedure   &Remove(RuleName: String); overload;

    constructor create; virtual;

    finalizer;
    begin
      var lHead := TDOMHelper.GetHead();
      if lHead <> nil then
        lHead.removeChild(FHandle);
    end;
  end;

function GlobalStyleSheet: TStyleSheet;

implementation

var
  _GlobalStyleSheet: TStyleSheet;

function GlobalStyleSheet: TStyleSheet;
begin
  if _GlobalStyleSheet = nil then
    _GlobalStyleSheet := new TStyleSheet();
  result := _GlobalStyleSheet;
end;

//########################################################################################
// TStyleSheet
//########################################################################################

constructor TStyleSheet.create;
begin
  inherited Create;

  FHandle := WebAssembly.CreateElement("style");
  FHandle.type := 'text/css';

  // Chrome has a quirk that sadly was never removed
  if TDOMHelper.GetIsChrome() then
  begin
    writeLn("its chrome");
    // Most CSS-files are loaded from a file, which in 99.99% of all cases is
    // never empty; and if not, never checked explicitly like we have to.
    // So lets create a text-node for our manual css
    if FHandle.styleSheet = nil then
      FHandle.appendChild( WebAssembly.CreateTextNode("") );
  end;

  // Append stylesheet to document->head
  TDOMHelper.GetHead().appendChild(FHandle);

  if FHandle.styleSheet <> nil then
    FSheet := FHandle.styleSheet
  else
    FSheet := FHandle.sheet;
end;

procedure TStyleSheet.AppendToStyleSheet(const CssCode: String);
begin
  var lCode := String( FHandle.innerHTML);
  lCode := lCode + #13;
  lCode := lCode + CssCode;
  FHandle.innerHTML := CssCode;
end;

procedure TStyleSheet.&Remove(const &Index: Integer);
begin
  if &Index >= 0 then
    GetSheet().deleteRule(&Index);
end;

procedure TStyleSheet.&Remove(RuleName: String); overload;
var
x: Integer;
begin
var lCount := GetCount();
if lCount > 0 then
begin
  var lSheet := GetSheet();
  RuleName := RuleName.Trim().ToLower();
  for x := 0 to lCount-1 do
    begin
    if Names[x].Trim().ToLower() = RuleName then
    begin
      lSheet.deleteRule(x);
      break;
    end;
  end;
end;
end;

function TStyleSheet.&Add(RuleDef: String): Integer;
begin
  var lSheet := GetSheet();
  if lSheet.insertRule <> nil then
    result := Integer( lSheet.insertRule(RuleDef) )
  else
    result := Integer( lSheet.addRule(RuleDef) );
end;

function TStyleSheet.&Add(RuleName, RuleCode: String): Integer;
begin
  result := &Add("." + RuleName + " {" + RuleCode + "}");
end;

function TStyleSheet.GetStyleName(const &Index: Integer): String;
begin
  var lRules := GetRules();
  var lRuleObj := EcmaScriptObject(lRules).Items[&Index];
  result := String(dynamic(lRuleObj).selectorText);
end;

function TStyleSheet.GetStyleCss(const &Index: Integer): String;
begin
  var lRuleObj := EcmaScriptObject( GetRules() ).Items[&Index];
  result := String( dynamic(lRuleObj).cssText );
end;

function TStyleSheet.GetCount: Integer;
begin
  var lRules := GetRules();
  result := EcmaScriptObject(lRules).Count;
end;

function TStyleSheet.GetRules: dynamic;
begin
  var lSheet := GetSheet();
  result := if lSheet.cssRules <> nil then lSheet.cssRules else lSheet.rules;
end;

function TStyleSheet.GetSheet: dynamic;
begin
  if FHandle.styleSheet <> nil then
    result := FHandle.styleSheet
  else
    result := FHandle.sheet;
end;


end.