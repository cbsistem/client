﻿namespace qtxlib;

interface

uses
  qtxlib,
  RemObjects.Elements.RTL.Delphi,
  RemObjects.Elements.RTL.Delphi.VCL;

type

  EQTXError = class(Exception);

  TDispatchHandler = public procedure () of object;

  TDispatch = public abstract class
  public
    class procedure Execute(const EntryPoint: TDispatchHandler; const Delay: Integer);
    class procedure &Repeat(const EntryPoint: TDispatchHandler; const Delay: Integer; TimesToRun: Integer);
  end;

implementation

class procedure TDispatch.&Repeat(const EntryPoint: TDispatchHandler; const Delay: Integer; TimesToRun: Integer);
begin
  if TimesToRun > 0 then
  begin
    if assigned(EntryPoint) then
    begin
      // check delay, if <=0 then its no point spawning a delegate
      // we just call it directly
      if Delay > 0 then
      begin
        var lDelegate := new WebAssemblyDelegate( ()->
        begin
          if assigned(EntryPoint) then
            EntryPoint();

          dec(TimesToRun);
          if TimesToRun > 0 then
            TDispatch.&Repeat(EntryPoint, Delay, TimesToRun);
        end);
        WebAssembly.SetTimeout(lDelegate, Delay);
      end else
        EntryPoint();
    end else
      raise EQTXError.create("Execute failed, callback handler was nil error");
  end;
end;

class procedure TDispatch.Execute(const EntryPoint: TDispatchHandler; const Delay: Integer);
begin
  if assigned(EntryPoint) then
  begin
    // check delay, if <=0 then its no point spawning a delegate
    // we just call it directly
    if Delay > 0 then
    begin
      var lDelegate := new WebAssemblyDelegate( ()->
      begin
        if assigned(EntryPoint) then
          EntryPoint();
      end);
      WebAssembly.SetTimeout(lDelegate, Delay);
    end else
      EntryPoint();
  end else
  raise EQTXError.create("Execute failed, callback handler was nil error");
end;

end.