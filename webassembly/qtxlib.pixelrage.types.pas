﻿namespace qtxlib;

//##################################################################################
// ____ ____  __ __    ___  _          ____    ____   ____    ___
// |    \    ||  |  |  /  _]| |     __ |    \  /    | /    |  /  _]
// |  o  )  | |  |  | /  [_ | |    |  ||  D  )|  o  ||   __| /  [_
// |   _/|  | |_   _||    _]| |___ |__||    / |     ||  |  ||    _]
// |  |  |  | |     ||   [_ |     | __ |    \ |  _  ||  |_ ||   [_
// |  |  |  | |  |  ||     ||     ||  ||  .  \|  |  ||     ||     |
// |__| |____||__|__||_____||_____||__||__|\_||__|__||___,_||_____
//
// Written by Jon-Lennart Aasenden @ 14.06.2019 under GPL v3 ~ Share the love!
//##################################################################################

interface

uses System, rtl;

type

  // Color type, equal to TColor in Delphi
  [Export]
  Color = public UInt32;

  // Supported pixel formats
  [Export]
  PixelFormat = public (
    pfNone  = 0,
    pf8bit  = 1,  //___8 -- palette indexed
    pf15bit = 2,  //_555 -- 15 bit encoded
    pf16bit = 3,  //_565 -- 16 bit encoded
    pf24bit = 4,  //_888 -- 24 bit native
    pf32bit = 5   //888A -- 32 bit native
  );

  // Common exception type
  [Export]
  PixelRageError = public class(Exception);

  // 24-bit pixel datatype
  [Export]
  RGBDef = public record
    R:  Byte;
    G:  Byte;
    B:  Byte;
    function  ToRGB15: Word; inline;
    function  ToRGB16: Word; inline;
    function  ToRGB24: RGBDef; inline;
    function  ToRGB32: RGBADef; inline;
    function  ToColor: Color; inline;
    procedure &Set(const Rx, Gx, Bx: Byte);
  end;

  // 32-bit pixel datatype
  [Export]
  RGBADef = public record
    R:  Byte;
    G:  Byte;
    B:  Byte;
    A:  Byte;
    function  ToRGB15: Word; inline;
    function  ToRGB16: Word; inline;
    function  ToRGB24: RGBDef; inline;
    function  ToRGB32: RGBADef; inline;
    function  ToColor: Color; inline;
    procedure &Set(const Rx, Gx, Bx: Byte); inline; overload;
    procedure &Set(const Rx, Gx, Bx, Ax: Byte); inline; overload;
  end;

  [Export]
  Rect = public record
    Left:   Integer;
    Top:    Integer;
    Width:  Integer;
    Height: Integer;
    function  Right: Integer; inline;
    function  Bottom: Integer; inline;
    function  &Empty: Boolean; inline;
    procedure SetFull(const NewLeft, NewTop, NewWidth, NewHeight: Integer); inline;
    procedure SetDirect(const NewLeft, NewTop, NewRight, NewBottom: Integer); inline;
    procedure SetPos(const NewLeft, NewTop: Integer); inline;
    procedure SetSize(const NewWidth, NewHeight: Integer); inline;
    procedure Normalize; inline;
    procedure Reset;
  end;

  [Export]
  Point = public record
    X:  Integer;
    Y:  Integer;
  end;

implementation


//##################################################################################
// Rect
//##################################################################################

procedure Rect.Reset;
begin
  Left := 0;
  Top := 0;
  Width := 0;
  Height := 0;
end;

procedure Rect.Normalize;
begin
  if Width < 0 then
  begin
    Left := Left + Width;
    Width := System.Math.Abs(Width);
  end;

  if Height < 0 then
  begin
    Top := Top + Height;
    Height := System.Math.Abs(Height);
  end;
end;

procedure Rect.SetFull(const NewLeft, NewTop, NewWidth, NewHeight: Integer);
begin
  Left := NewLeft;
  Top := NewTop;
  Width := NewWidth;
  Height := NewHeight;
end;

procedure Rect.SetDirect(const NewLeft, NewTop, NewRight, NewBottom: Integer);
begin
  Left := NewLeft;
  Top := NewTop;
  Width := System.Math.Abs(NewRight - NewLeft);
  Height := System.Math.Abs(NewBottom - NewTop);
end;

procedure Rect.SetPos(const NewLeft, NewTop: Integer);
begin
  Left := NewLeft;
  Top := NewTop;
end;

procedure Rect.SetSize(const NewWidth, NewHeight: Integer);
begin
  Width := NewWidth;
  Height := NewHeight;
end;

function Rect.&Empty: Boolean;
begin
  result := ( (Width < 1) or (Height < 1) );
end;

function Rect.Right: Integer;
begin
  result := Left + Width -1;
end;

function Rect.Bottom: Integer;
begin
  result := Top + Height -1;
end;

//##################################################################################
// RGBADef
//##################################################################################

procedure RGBADef.&Set(const Rx, Gx, Bx: Byte);
begin
  self.R := Rx;
  self.G := Gx;
  self.B := Bx;
end;

procedure RGBADef.&Set(const Rx, Gx, Bx, Ax: Byte);
begin
  self.R := Rx;
  self.G := Gx;
  self.B := Bx;
  self.A := Ax;
end;

function RGBADef.ToRGB15: Word;
begin
  result := (R shr 3) shl 10 or (G shr 3) shl 5 or (B shr 3);
end;

function RGBADef.ToRGB16: Word;
begin
  result := (R shr 3) shl 11 or (G shr 2) shl 5 or (B shr 3);
end;

function RGBADef.ToRGB24: RGBDef;
begin
  result.R := R;
  result.G := G;
  result.B := B;
end;

function RGBADef.ToRGB32: RGBADef;
begin
  result.R := R;
  result.G := G;
  result.B := B;
  result.A := A;
end;

function RGBADef.ToColor: Color;
begin
  result := Color( (R or (G shl 8) or (B shl 16)) );
end;

//##################################################################################
// RGBDef
//##################################################################################

procedure RGBDef.&Set(const Rx, Gx, Bx: Byte);
begin
  self.R := Rx;
  self.G := Gx;
  self.B := Bx;
end;

function RGBDef.ToRGB15: Word;
begin
  result := (R shr 3) shl 10 or (G shr 3) shl 5 or (B shr 3);
end;

function RGBDef.ToRGB16: Word;
begin
  result := (R shr 3) shl 11 or (G shr 2) shl 5 or (B shr 3);
end;

function RGBDef.ToRGB24: RGBDef;
begin
  result.R := R;
  result.G := G;
  result.B := B;
end;

function RGBDef.ToRGB32: RGBADef;
begin
  result.R := R;
  result.G := G;
  result.B := B;
end;

function RGBDef.ToColor: Color;
begin
  result := Color( (R or (G shl 8) or (B shl 16)) );
end;


end.