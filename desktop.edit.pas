unit desktop.edit;

{$I 'Smart.inc'}

interface

uses
  W3C.DOM,
  System.Types,
  SmartCL.Theme,
  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,

  SmartCL.System,
  SmartCL.Components,
  SmartCL.Controls.Editbox;

type

  TWbEditbox = class(TW3EditBox)
  public
    procedure StyleTagObject; override;
  end;

implementation

procedure TWbEditbox.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;


initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #'
  .TWbEditbox {
    font-family: "Rubik", "Roboto Mono", "Courier New", "Ubuntu", verdana;
    border: 1px solid #fff;
    border-radius: 0;

    background-color: #CFCFCF;

    border-top: 1px solid #979797;
    border-left: 1px solid #979797;
    border-bottom: 1px solid #E0E0E0;
    border-right: 1px solid #E0E0E0;

    outline: 1px solid #42415A;
    outline-offset: 0px;
    outline-bottom: 1px solid #000;
  	-webkit-user-select: auto;
  }';
  StyleCode := StrReplace(StyleCode,"§",prefix);
  Sheet.Append(StyleCode);
end;

end.